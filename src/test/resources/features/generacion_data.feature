Feature: Carga Masiva de Ajuste de Stock
  Como usuario
  Quiero realizar ajuste de stock masivo a través del sistema SIC

  Background: 
    Given ingreso al sistema sic
    And voy a la opcion Carga Masiva de Ajuste de Stock

 	@SIC @SIC_CMAS @SIC_CMAS_GENERACION_DATA
  Scenario: Generar stock de 60000
    Given tengo informacion de stock de "60000" productos para la carga masiva de stock
    And genero un archivo de carga masiva de ajuste de stock
    