Feature: SIC - Configuracion de Movimientos de Inventario
  Como usuario
  Quiero agregar, eliminar, actualizar y consultar en la pantalla de Configuración de Movimientos de Inventario

  Background: 
    Given ingreso al sistema sic con perfil administrador
    And voy a la opcion Configuracion de Movimientos de Inventario

  @SIC @SIC_CMI @SIC_CMI_01
  Scenario: SIC_CMI_01 - Validar que el filtro nombre del sistema tenga las todas las opciones
    When doy click al combo nombre de sistema
    Then se verifica que se muestren todos los sistemas

  @SIC @SIC_CMI @SIC_CMI_02
  Scenario: SIC_CMI_02 - Validar que el filtro Nombre del Sistema es obligatorio
    Given el boton consultar esta deshabilitado
    When selecciono el sistema "PMM"
    Then se habilita el boton consultar

  @SIC @SIC_CMI @SIC_CMI_03
  Scenario: SIC_CMI_03 - Validar boton Limpiar
    Given el boton consultar esta deshabilitado
    When selecciono el sistema "PMM"
    And doy click en consultar
    And doy click en limpiar
    Then se limpia el resultado
    And los botones se deshabilitan a excepcion del boton limpiar

  @SIC @SIC_CMI @SIC_CMI_04
  Scenario: SIC_CMI_04 - Verificar boton Limpiar
    Given el boton consultar esta deshabilitado
    When selecciono el sistema "PMM"
    And doy click en consultar
    Then muestra las configuraciones asociadas al sistema

  @SIC @SIC_CMI @SIC_CMI_05
  Scenario: SIC_CMI_05 - Validar que el botón Eliminar exista y esté bloqueado mientras no selecciones ninguna configuración
    Given el boton eliminar esta deshabilitado
    When selecciono el sistema "PMM"
    And doy click en consultar
    Then se habilita el boton eliminar

  @SIC @SIC_CMI @SIC_CMI_06
  Scenario: SIC_CMI_06 - Validar el filtro Tipo de Transaccion
    When selecciono el sistema "PMM"
    And doy click en consultar
    And doy click en el combo tipo de transaccion
    Then se verifica que se muestren todos los tipos de transaccion

  @SIC @SIC_CMI @SIC_CMI_07
  Scenario: SIC_CMI_07 - Validar el filtro Tipo de Inventario
    When selecciono el sistema "PMM"
    And doy click en consultar
    And doy click en el combo tipo de inventario
    Then se verifica que se muestren todos los tipos de inventario

  @SIC @SIC_CMI @SIC_CMI_08
  Scenario: SIC_CMI_08 - Validar que el botón Agregar se habilite solo si tengo un sistema definido, tipo de actualizacion y tipo de inventario.
    Given el boton agregar esta deshabilitado
    When selecciono el sistema "PMM"
    And doy click en consultar
    And selecciono tipo de transaccion "ADJUST"
    And selecciono tipo de inventario 1
    Then se habilita el boton agregar

  @SIC @SIC_CMI @SIC_CMI_09
  Scenario: SIC_CMI_09 - Validar que pueda registrar configuraciones para el sistema B2B
    Given habilito configuracion para tipo de transaccion "ADJUST" tipo de inventario "1" para el sistema "B2B"
    When selecciono el sistema "B2B"
    And doy click en consultar
    And selecciono tipo de transaccion "ADJUST"
    And selecciono tipo de inventario 1
    And doy click en agregar
    Then muestra mensaje de confirmacion "Ya existe la configuración ingresada, ¿desea actualizarla?"

  @SIC @SIC_CMI @SIC_CMI_10
  Scenario: SIC_CMI_10 - Validar el filtro Tipo de Inventario
    Then se verifica que exista la columna "Tipo de Transacción"
    And se verifica que exista la columna "Tipo de Inventario"

