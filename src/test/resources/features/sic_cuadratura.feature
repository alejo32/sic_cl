Feature: SIC - Reporte de Cuadratura Automático

  Background: 
    Given ingreso al sistema sic con perfil administrador
    And voy a la opcion Reporte de Cuadratura Automatico

  @SIC_RCA  @SIC_RCA_02
  Scenario: SIC - Validar que se realice la descarga del archivo
    When doy clic en el ultimo reporte de cuadratura
    Then se descarga el reporte
