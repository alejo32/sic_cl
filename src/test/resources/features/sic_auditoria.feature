Feature: SIC - Auditoria de Transacciones de Inventario
  Como usuario
  Quiero realizar consultas en la pantalla de Auditoria de Transacciones de invenario y validar el resultado con  base de datos

  Background: 
    Given ingreso al sistema sic
    And voy a la opcion Auditoria de Transacciones de Inventario

  @SIC @SIC_ATI @SIC_ATI_01
  Scenario Outline: SIC_ATI_01 - Realizar consulta utilizando todos los filtros para sku tipo inventario Stock Disponible
    Given tengo una transaccion de un producto con tipo de inventario "1"
    When realizo la consulta de auditoria
    Then se validan en bd los datos mostrados en la pantalla de auditoria

    Examples: 
      | tipo_inventario |
      |               1 |

  @SIC @SIC_ATI @SIC_ATI_04
  Scenario: SIC_ATI_04 - Realizar consulta utilizando todos los filtros por defecto
    Given tengo una transaccion de un producto con tipo de inventario "1"
    When realizo la consulta de auditoria solo con los filtros obligatorios
    Then se validan en bd los datos mostrados en la pantalla de auditoria

  @SIC @SIC_ATI @SIC_ATI_05
  Scenario: SIC_ATI_05 - Realizar consulta sin ingresar codigo de venta
    Given tengo una transaccion de un producto con tipo de inventario "1"
    When realizo la consulta de auditoria sin ingresar codigo de venta
    Then muestra mensaje de error "El campo COD DE VENTA es obligatorio"

  @SIC @SIC_ATI @SIC_ATI_06
  Scenario: SIC_ATI_06 - Realizar consulta sin ingresar la fecha desde
    Given tengo una transaccion de un producto con tipo de inventario "1"
    When realizo la consulta de auditoria sin ingresar fecha desde
    Then muestra mensaje de error "El campo Desde es obligatorio"

  @SIC @SIC_ATI @SIC_ATI_07
  Scenario: SIC_ATI_07 - Realizar consulta sin ingresar la fecha hasta
    Given tengo una transaccion de un producto con tipo de inventario "1"
    When realizo la consulta de auditoria sin ingresar fecha hasta
    Then muestra mensaje de error "El campo Hasta es obligatorio"

  @SIC @SIC_ATI @SIC_ATI_08
  Scenario: SIC_ATI_08 - Realizar consulta y limpiar resultados
    Given tengo una transaccion de un producto con tipo de inventario "1"
    When realizo la consulta de auditoria
    And doy clic al boton Limpiar
    Then se limpian todos los filtros utilizados

  @SIC @SIC_ATI @SIC_ATI_09
  Scenario Outline: SIC_ATI_09 - Validar que se visualice Las transacciones y ajustes de inventario enviadas desde PMM hacia SIC
    Given tengo una transaccion de un producto con sistema origen "<sistema_origen>"
    When realizo la consulta de auditoria
    Then se validan en bd los datos mostrados en la pantalla de auditoria

    Examples: 
      | sistema_origen |
      | PMM            |
      | B2B            |
      | BO             |
      | BGT            |

  @SIC @SIC_ATI @SIC_ATI_13
  Scenario: SIC_ATI_13 - Validar que se visualice el botón Exporta y se genere archivo en formato CSV Zipeado
    Given tengo una transaccion de un producto con tipo de inventario "1"
    When realizo la consulta de auditoria
    And doy clic al boton Exportar
    Then validar la descarga del archivo

  @SIC @SIC_ATI @SIC_ATI_14
  Scenario: SIC_ATI_14 - Validar el tipo de producto por defecto sea '1-Ripley'
    Then se valida que tenga por defecto el tipo de producto "1-Ripley"

  @SIC @SIC_ATI @SIC_ATI_15
  Scenario: SIC_ATI_15 - Realizar una consulta, limpiar consulta y hacer click en exportar
    Given tengo una transaccion de un producto con tipo de inventario "1"
    When realizo la consulta de auditoria
    And doy click en limpiar el resultado de auditoria
    And doy clic al boton Exportar
    Then muestra mensaje de error "El campo COD DE VENTA es obligatorio"

  @SIC @SIC_ATI @SIC_ATI_16
  Scenario: SIC_ATI_16 - Validar mensaje de error al consultar un rango de fechas mayor a 10 dias
    When ingreso codigo de venta "2000000000001"
    And selecciono rango de fechas mayor a diez dias
    And doy click en consultar auditoria
    Then muestra mensaje de error "El rango de fechas, en la consulta, no puede ser mayor a 10 días."

  @SIC @SIC_ATI @SIC_ATI_17
  Scenario: SIC_ATI_17 - Realizar consulta utilizando todos los filtros para sku tipo inventario Stock Protegido
    Given tengo una transaccion de un producto con tipo de inventario "1"
    When realizo la consulta de auditoria
    Then se validan en bd los datos mostrados en la pantalla de auditoria

  @SIC @SIC_ATI @SIC_ATI_18
  Scenario: SIC_ATI_18 - Realizar consulta utilizando todos los filtros para sku tipo inventario Stock Disponible
    Given tengo una transaccion de un producto con tipo de inventario
      | tipo_inventario |
      |               1 |
      |               2 |
    When realizo la consulta de auditoria solo con los filtros obligatorios
    Then se validan en bd los datos mostrados por tipo de inventario
  