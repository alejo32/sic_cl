Feature: Ajuste de inventario vía SQS
  Como usuario
  Quiero realizar ajustes de inventario con la finalidad de cubrir todas las casuisticas que puedan existir

  @ADJUST @ADJUST_SQS @ADJUST_SQS_01dani @ADJUST_SQS_PMM @ADJUST_SQS_C
  Scenario Outline: PMM - Realizar ajuste de inventario de 1 producto
    Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos
    When envio un request de ajuste via SQS
    Then se registra en bd la cabecera con estado "C" mensaje ""
    And se validan los datos del ajuste en bd
    And se valida en SIC el inventario

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |

  @ADJUST_SQS_01_todo
  Scenario Outline: PMM - Realizar ajuste de inventario de 1 producto
    Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos con tipo de operacion "<operation>"
    When envio un request de ajuste via SQS a "<sistema_origen>"
    Then se registra en bd la cabecera con estado "C" mensaje ""
    And se validan los datos del ajuste en bd
    And se valida en SIC el inventario

    Examples:
      | sistema_origen | cantidad_productos | operation |
      | BGT            |                  1 |     S    |
      | PMM            |                  1 |     S    |
      | PMM            |                  1 |     S    |
      | BGT            |                  1 |     A    |
      | PMM            |                  1 |     A    |
      | PMM            |                  1 |     A    |

  @ADJUST @ADJUST_SQS @ADJUST_SQS_29 @ADJUST_SQS_PMM @ADJUST_SQS_C
  Scenario Outline: PMM - Realizar ajuste de inventario de un producto con stock 0
    Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos operacion "<operation>" y con stock en "<STOCK_PREVIO>"
    When envio un request de ajuste via SQS
    Then se registra en bd la cabecera con estado "C" mensaje ""
    And se validan los datos del ajuste en bd
    And se valida en SIC el inventario

    Examples:
      | sistema_origen | cantidad_productos | operation | STOCK_PREVIO |
      | PMM            |                  1 |     S     |  Negativo    |
      | PMM            |                  1 |     S     |  Positivo    |
      | PMM            |                  1 |     A     |  Negativo    |
      | PMM            |                  1 |     A     |  Positivo    |


  @ADJUST @ADJUST_SQS @ADJUST_SQS_29 @ADJUST_SQS_PMM @ADJUST_SQS_C
  Scenario Outline: PMM - Realizar ajuste de inventario de un producto con stock 0
    Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos con stock 0 con ajuste negativo
    When envio un request de ajuste via SQS
    Then se registra en bd la cabecera con estado "C" mensaje ""
    And se validan los datos del ajuste en bd
    And se valida en SIC el inventario

    Examples:
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |

  @ADJUST @ADJUST_SQS @ADJUST_SQS_09 @ADJUST_SQS_PMM @ADJUST_SQS_SinEstado
  Scenario Outline: PMM - En la cabecera del ajuste, no se envía el campo 'Sistema Origen'
    Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Sistema Origen
    When envio un request de ajuste via SQS
    Then validar que no se registre en la bd

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |

  @ADJUST @ADJUST_SQS @ADJUST_SQS_13 @ADJUST_SQS_PMM @ADJUST_SQS_SinEstado
  Scenario Outline: PMM - En la cabecera del ajuste, no se envía el campo 'Tipo de Movimiento'
    Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Tipo de Movimiento
    When envio un request de ajuste via SQS
    Then validar que no se registre en la bd

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |

  @ADJUST @ADJUST_SQS @ADJUST_SQS_17 @ADJUST_SQS_PMM @ADJUST_SQS_SinEstado
  Scenario Outline: PMM - En la cabecera del ajuste, no se envía el campo 'Usuario'
    Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Usuario
    When envio un request de ajuste via SQS
    Then se registra en bd la cabecera con estado "F" mensaje "El campo user es obligatorio"
    And no se registra en bd el detalle

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |

  @ADJUST @ADJUST_SQS @ADJUST_SQS_21 @ADJUST_SQS_PMM @ADJUST_SQS_F
  Scenario Outline: PMM - Realizar ajuste de inventario con tipo de operacion incorrecto
    Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos con Operacion "<tipo_operacion>"
    When envio un request de ajuste via SQS
    Then se registra en bd la cabecera con estado "F" mensaje ""
    And se registra en bd el detalle con estado "F" mensaje "Las operaciones permitidas son A y S"

    Examples: 
      | sistema_origen | cantidad_productos | tipo_operacion |
      | PMM            |                  1 | L              |
      | B2B            |                  1 | M              |
      | BO             |                  1 | R              |
      | BGT            |                  1 | Q              |

  @ADJUST @ADJUST_SQS @ADJUST_SQS_25 @ADJUST_SQS_PMM @ADJUST_SQS_C
  Scenario Outline: PMM - En la transacción de ajuste, se envía un producto que no existe
    Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos que no existen
    When envio un request de ajuste via SQS
    Then se registra en bd la cabecera con estado "F" mensaje ""
    And se registra en bd el detalle con estado "F" mensaje "SKU no existe"

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  2 |
      | B2B            |                  2 |
      | BO             |                  2 |
      | BGT            |                  2 |



  @ADJUST @ADJUST_SQS @ADJUST_SQS_33 @ADJUST_SQS_PMM @ADJUST_SQS_F @_I
  Scenario Outline: PMM - Realizar un ajuste de inventario con IDTRX repetidos en el detalle
    Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos con id detalle duplicado
    When envio un request de ajuste via SQS
    Then se registra en bd la cabecera con estado "F" mensaje ""
    And se registra en bd el detalle con estado "D" mensaje ""

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  2 |
      | B2B            |                  2	|
      | BO             |                  2 |
      | BGT            |                  2 |

  @ADJUST @ADJUST_SQS @ADJUST_SQS_37 @ADJUST_SQS_PMM @ADJUST_SQS_F @_I
  Scenario Outline: PMM - En la cabecera del ajuste, no se envía el campo 'Tipo Documento'
    Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Documento de Transaccion
    When envio un request de ajuste via SQS
    Then se registra en bd la cabecera con estado "F" mensaje ""
    And se registra en bd el detalle con estado "F" mensaje "No se encontro el campo documento de transaccion"

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |

  @ADJUST @ADJUST_SQS @ADJUST_SQS_41 @ADJUST_SQS_PMM @ADJUST_SQS_F
  Scenario Outline: PMM - En la cabecera del ajuste, no se envía el campo 'IDTRX'
    Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo IDTRX
    When envio un request de ajuste via SQS
    Then se registra en bd la cabecera con estado "F" mensaje ""
    And se registra en bd el detalle con estado "F" mensaje "El campo idtrx es obligatorio"

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |

  @ADJUST @ADJUST_SQS @ADJUST_SQS_45 @ADJUST_SQS_PMM @ADJUST_SQS_F
  Scenario Outline: PMM - En la cabecera del ajuste, no se envía el campo 'Bodega'
    Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Bodega
    When envio un request de ajuste via SQS
    Then se registra en bd la cabecera con estado "F" mensaje ""
    And se registra en bd el detalle con estado "F" mensaje "El campo fulfillment es obligatorio"

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |

  @ADJUST @ADJUST_SQS @ADJUST_SQS_49 @ADJUST_SQS_PMM @ADJUST_SQS_F
  Scenario Outline: PMM - En la cabecera del ajuste, no se envía el campo 'Tipo Inventario'
    Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Tipo de Inventario
    When envio un request de ajuste via SQS
    Then se registra en bd la cabecera con estado "F" mensaje ""
    And se registra en bd el detalle con estado "F" mensaje "El campo inventoryType es obligatorio"

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |

  @ADJUST @ADJUST_SQS @ADJUST_SQS_53 @ADJUST_SQS_PMM @ADJUST_SQS_F
  Scenario Outline: BGT - En el detalle del ajuste, no se envía el campo 'Operacion'
    Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Operacion
    When envio un request de ajuste via SQS
    Then se registra en bd la cabecera con estado "F" mensaje ""
    And se registra en bd el detalle con estado "F" mensaje "El campo operation es obligatorio"

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |

  @ADJUST @ADJUST_SQS @ADJUST_SQS_57 @ADJUST_SQS_PMM @ADJUST_SQS_F
  Scenario Outline: PMM - En el detalle del ajuste, se envía el campo 'IDTRX' repetido de otra transaccion
    Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos
    When envio un request de ajuste via SQS
    And envio nuevamente el mismo request de ajuste via SQS
    Then se registra en bd la cabecera con estado "F" mensaje ""
    And se registra en bd el detalle con estado "D" mensaje "Registro duplicado."

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |

  @ADJUST @ADJUST_SQS @ADJUST_SQS_61 @ADJUST_SQS_PMM @ADJUST_SQS_F
  Scenario Outline: PMM - En el detalle del ajuste, no se envía el campo 'Sku'
    Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos
    And borro el valor de el campo producto
    When envio un request de ajuste via SQS
    Then se registra en bd la cabecera con estado "F" mensaje ""
    And se registra en bd el detalle con estado "F" mensaje "El campo sku es obligatorio"

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |

  @ADJUST @ADJUST_SQS @ADJUST_SQS_65 @ADJUST_SQS_PMM @ADJUST_SQS_F
  Scenario Outline: PMM - Se envía una transacción y su estado cambia a 'Completado con Errores'
    Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos con error en un registro
    When envio un request de ajuste via SQS
    Then se registra en bd la cabecera con estado "CF" mensaje ""

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |

  @ADJUST @ADJUST_REST @ADJUST_SQS_69 @ADJUST_REST_PMM @ADJUST_REST_F
  Scenario Outline: PMM - En el detalle del ajuste, se envía una bodega que no existe
    Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos
    And asigno la bodega "<bodega_no_existe>"
    When envio un request de ajuste via SQS
    Then se registra en bd la cabecera con estado "F" mensaje ""
    And se registra en bd el detalle con estado "F" mensaje "Bodega no existe"

    Examples: 
      | sistema_origen | cantidad_productos | bodega_no_existe |
      | PMM            |                  1 |            29912 |
      | B2B            |                  1 |            11921 |
      | BO             |                  1 |            99182 |
      | BGT            |                  1 |            22133 |
  