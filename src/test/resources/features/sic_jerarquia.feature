Feature: SIC - Consulta de Stock por Jerarquia de Productos
  Como usuario
  Quiero realizar consultas de stock por jerarquia a traves del sistema SIC

  Background: 
    Given ingreso al sistema sic
    And voy a la opcion Consulta de Stock por Jerarquia de Productos

 @SIC @SIC_CSJ @SIC_CSJ_01
  Scenario: SIC_CSJ_01 - Validar que exista el filtro Tipo de Producto en la consulta y este seleccionado por defecto '1-Ripley'
    Then se valida que tenga por defecto el valor "1-Ripley"

  @SIC @SIC_CSJ @SIC_CSJ_02
  Scenario: SIC_CSJ_02 - Validar que exista el filtro Departamento en la consulta y sea obligatorio
    When no selecciono un departamento
    Then el boton consultar esta habilitado

  @SIC @SIC_CSJ @SIC_CSJ_03
  Scenario: SIC_CSJ_03 - Validar que muestre las lineas correspondientes al departamento escogido
    When selecciono el departamento "D101"
    Then se muestra la linea "500536"

  @SIC @SIC_CSJ @SIC_CSJ_04
  Scenario: SIC_CSJ_04 - Validar que muestre las lineas correspondientes a los multiples departamentos escogidos
    When selecciono el departamento "D101"
    And selecciono el departamento "D103"
    Then se muestra la linea "500536"
    And se muestra la linea "000061"

  @SIC @SIC_CSJ @SIC_CSJ_05
  Scenario: SIC_CSJ_05 - Validar que muestre las sublíneas correspondientes a los departamentos y lineas escogidos
    When selecciono el departamento "D101"
    And selecciono el departamento "D103"
    And selecciono la linea "500536"
    And selecciono la linea "000061"
    Then se muestra la sublinea "S001103"
    And se muestra la sublinea "S502038"

  @SIC @SIC_CSJ @SIC_CSJ_06
  Scenario: SIC_CSJ_06 - Validar que exista el filtro SKU en la consulta y que sea obligatorio
    When no ingreso un sku
    Then el boton consultar esta habilitado

  @SIC @SIC_CSJ @SIC_CSJ_07
  Scenario: SIC_CSJ_07 - Validar que exista el filtro Tipo de Inventario en la consulta
    When doy click al combo tipo de inventario
    Then se verifica se muestren todos los tipos de inventario

  @SIC @SIC_CSJ @SIC_CSJ_08
  Scenario: SIC_CSJ_08 - Validar que exista el filtro Tipo de Producto en la consulta y este seleccionado por defecto 'Stock mayor a:'
    Then se valida que el rango de stock tenga por defecto el valor "Stock mayor a:"

  @SIC @SIC_CSJ @SIC_CSJ_09
  Scenario: SIC_CSJ_09 - Validar que existan todas las opciones de rango de stock
    When doy click al combo rango de stock
    Then se verifica que se muestren las opciones de rango de stock
      | Stock mayor a:         |
      | Stock menor a:         |
      | Stock mayor o igual a: |
      | Stock menor o igual a: |
      | Stock igual a:         |
      | Stock distinto a:      |

  @SIC @SIC_CSJ @SIC_CSJ_10
  Scenario: SIC_CSJ_10 - Validar todos los campos del resultado de la consulta en la grilla con stock mayor a 0
    Given tengo informacion de stock por jerarquia de un producto con stock mayor a cero
    When selecciono la bodega donde el producto tiene stock
    And realizo la consulta
    Then se verifica el resultado mostrado con la base de datos

  @SIC @SIC_CSJ @SIC_CSJ_11
  Scenario: SIC_CSJ_11 - Validar todos los campos del resultado de la consulta en la grilla con stock menor a 0
    Given tengo informacion de stock por jerarquia de un producto con stock menor a cero
    When selecciono la bodega donde el producto tiene stock
    And realizo la consulta
    Then se verifica el resultado mostrado con la base de datos

  @SIC @SIC_CSJ @SIC_CSJ_12
  Scenario: SIC_CSJ_12 - Validar todos los campos del resultado de la consulta en la grilla con stock igual a 0
    Given tengo informacion de stock por jerarquia de un producto con stock igual a cero
    When selecciono la bodega donde el producto tiene stock
    And realizo la consulta
    Then se verifica el resultado mostrado con la base de datos

  @SIC @SIC_CSJ @SIC_CSJ_13
  Scenario: SIC_CSJ_13 - Validar todos los campos del resultado de la consulta en la grilla con stock mayor o igual a 10
    Given tengo informacion de stock por jerarquia de un producto con stock mayor o igual a "10"
    When selecciono la bodega donde el producto tiene stock
    And realizo la consulta con el filtro mayor igual a
    Then se verifica el resultado mostrado con la base de datos

  @SIC @SIC_CSJ @SIC_CSJ_14
  Scenario: SIC_CSJ_14 - Validar todos los campos del resultado de la consulta en la grilla con stock menor o igual a 20
    Given tengo informacion de stock por jerarquia de un producto con stock menor o igual a "20"
    When selecciono la bodega donde el producto tiene stock
    And realizo la consulta con el filtro menor igual a
    Then se verifica el resultado mostrado con la base de datos

  @SIC @SIC_CSJ @SIC_CSJ_15
  Scenario: SIC_CSJ_15 - Validar todos los campos del resultado de la consulta en la grilla con stock diferente a 10
    Given tengo informacion de stock por jerarquia de un producto con stock diferente a "10"
    When selecciono la bodega donde el producto tiene stock
    And realizo la consulta con el filtro distinto a
    Then se verifica el resultado mostrado con la base de datos

  @SIC @SIC_CSJ @SIC_CSJ_16
  Scenario: SIC_CSJ_16 - Validar consulta de stock para un SKU
    Given tengo informacion de stock por jerarquia de un producto con stock igual a cero
    When selecciono la bodega donde el producto tiene stock
    And realizo la consulta con filtro tipo inventario
    Then se verifica el resultado mostrado con la base de datos

  @SIC @SIC_CSJ @SIC_CSJ_17
  Scenario Outline: SIC_CSJ_17 - Validar que el resultado de la consulta sea un registro por cada SKU y tipo de inventario 
    Given tengo informacion de stock por jerarquia de un producto con tipo de inventario "<tipo_inventario>"
    When selecciono la bodega donde el producto tiene stock
    And realizo la consulta con filtro tipo inventario
    Then se verifica el resultado mostrado con la base de datos

    Examples: 
      | tipo_inventario |
      |               1 |
      |               2 |
 