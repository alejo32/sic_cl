  @StockProtegido
  Feature: Caso 5.3 - Acceder a opción Manetenedor para ingresar Stock Protegido
  
  Background: 
      Given ingreso al sistema sic con perfil administrador
      And voy a la opcion Mantenedor para ingresar stock de seguridad
  
  Scenario: Caso 5.4 - Realizamos busqueda por SKU para realizar su ajuste de Stock
      Given En combobox Bodegas selecciono opción Redex
      And En combobox Departamento selecciono opción Escolar
      When Presiono el botón Consultar Stock Protegido
      Then Se despliega grilla Configuraciones Activas y Vigentes
  
  @StockProtegidoNuevo
    Scenario: Caso 5.6 - Creamos nuevo evento para departamento seleccionado
      Given En combobox Bodegas selecciono opción Redex
      And En combobox Departamento selecciono opción Escolar
      And Presiono el botón Consultar Stock Protegido
      And Se despliega grilla Configuraciones Activas y Vigentes
      When Se presiona el Botón Nuevo Evento
      And Se despliega la grilla Configuración de Stock Protegido
      Then Ingresamos una fecha de inicio válida
      And Ingresamos una fecha de termino válida
      Then Ingresamos Stock para Nivel Uno
      And Ingresamos Stock para Nivel Dos
      Then Presionamos el Botón Guardar
      And Se despliega Pop Up con mensaje de alerta para Confirmar Stock
      And se despliega mensaje de alerta Confirmando
  
    @StockProtegidoEditar
    Scenario: Caso 5.7 - Editamos evento activo
      Given En combobox Bodegas selecciono opción Redex
      And En combobox Departamento selecciono opción Escolar
      And Presiono el botón Consultar Stock Protegido
      And Se despliega grilla Configuraciones Activas y Vigentes
      When Se presiona el Botón Editar
      And Se despliega la grilla Configuración de Stock Protegido
      Then Editamos Stock para Nivel Uno
      And Editamos Stock para Nivel Dos
      Then Presionamos el Botón Guardar
      And Se despliega Pop Up con mensaje de alerta para Confirmar Stock
      And se despliega mensaje de alerta Confirmando
  
    @StockProtegidoEliminar
    Scenario: Caso 5.8 - Eliminamos evento activo
      Given En combobox Bodegas selecciono opción Redex
      And En combobox Departamento selecciono opción Escolar
      And Presiono el botón Consultar Stock Protegido
      And Se despliega grilla Configuraciones Activas y Vigentes
      When Se presiona el Botón Eliminar
      Then Se despliega Pop Up con mensaje de alerta para Confirmar Stock
      And se despliega mensaje de alerta Confirmando
  
    @SIC_MSP @SIC_MSP_001
    Scenario: SIC_MSP_001 - El campo Tipo de Producto presenta la opción Ripley seleccionada por defecto.
      Given Valido la existencia de comobobox Tipo de Producto
      Then Se encuentra seleccionada por defecto la opción Ripley
  
    @SIC_MSP @SIC_MSP_002
    Scenario: SIC_MSP_002 - El campo Departamento no presenta ningún valor seleccionado por defecto pero sí permite elegir uno.
      Given Valido la existencia de campo Departamento
      When Campo Departamento se encuentra vacío
      Then Permite seleccionar un Departamento
  
    @SIC_MSP @SIC_MSP_003
    Scenario: Caso SIC_MSP_003 - El campo Línea, no presenta ningún valor seleccionado por defecto y no permite elegir uno.
      Given Valido la existencia del campo Línea
      Then No permite seleccionar un valor de Linea
  
    @SIC_MSP @SIC_MSP_004
    Scenario: Caso SIC_MSP_004 - El campo SubLínea, no presenta ningún valor seleccionado por defecto y no permite elegir uno.
      Given Valido la existencia del campo Sub Línea
      Then No permite seleccionar un valor de Sub Linea
  
    @SIC_MSP @SIC_MSP_005
    Scenario: Caso SIC_MSP_005 - El campo SKU, no presenta ningún valor seleccionado por defecto y no permite elegir uno.
      Given Valido la existencia del campo Cod. Venta
      Then No permite seleccionar un valor de Cod. Venta
  
    @SIC_MSP @SIC_MSP_006
    Scenario: Caso SIC_MSP_006 - El botón Consultar se muestra inhabilitado por defecto
      Given Valido la existencia del botón Consultar
      Then El botón Consultar se encuentra deshabilitado
  
    @SIC_MSP @SIC_MSP_007
    Scenario: Caso SIC_MSP_007 - La grilla se muestra sin ningún registro por defecto.
      Given Valido la existencia Grilla configuraciones
      Then La grilla se despliega sin registros
  
    @SIC_MSP @SIC_MSP_008
    Scenario: Caso SIC_MSP_008 - El botón Nuevo Evento se muestra inhabilitado por defecto
      Given Valido la existencia de Botón Nuevo Evento
      Then El botón Nuevo Evento se encuentra deshabilitado
  
    @SIC_MSP @SIC_MSP_009
    Scenario: Caso SIC_MSP_009 - El botón Editar se muestra inhabilitado por defecto
      Given Valido la existencia de Botón Editar
      Then El botón Editar se encuentra deshabilitado
  
    @SIC_MSP @SIC_MSP_010
    Scenario: Caso SIC_MSP_010 - El botón Eliminar se muestra inhabilitado por defecto
      Given Valido la existencia de Botón Eliminar
      Then El botón Eliminar se encuentra deshabilitado
  
    @SIC_MSP @SIC_MSP_011
    Scenario: Caso SIC_MSP_011 - El usuario habiendo seleccionado el tipo de producto Ripley, selecciona un departamento
      Given Se selecciona por defecto Tipo de Producto Ripley
      When Seleccionar Combobox Deptarmento
      Then Se despliegan todos los departamentos existente
  
    @SIC_MSP @SIC_MSP_012
    Scenario: Caso SIC_MSP_012 - El botón consultar se habilita al seleccionar tipo de producto y departamento
      Given Se selecciona por defecto Tipo de Producto Ripley
      When Seleccionar Combobox Deptarmento
      And Se despliegan todos los departamentos existente
      Then Se habilita el botón Consultar
  
    @SIC_MSP @SIC_MSP_013
    Scenario: Caso SIC_MSP_013 - El campo línea se habilita al seleccionar tipo de producto y departamento
      Given selecciono la bodega "10012"
      And Se selecciona por defecto Tipo de Producto Ripley
      When Campo Departamento se encuentra vacío
      And Permite seleccionar un Departamento
      Then Selecciono el combobox Linea
      
    @SIC_MSP @SIC_MSP_014
    Scenario: Caso SIC_MSP_014 - Visualizar en combo Departamento listado de departamentos almacenados en BD. Codigo Dpto - Nombre Dpto
      Given selecciono la bodega "10012"
      And Se selecciona por defecto Tipo de Producto Ripley
      When Campo Departamento se encuentra vacío
      Then Seleccionar Combobox Deptarmento
      And Se despliegan todos los departamentos existente
  
    @SIC_MSP @SIC_MSP_015
    Scenario: Caso SIC_MSP_015 - Visualizar en el combo de Línea el listado de Líneas almacenados en BD. Codigo Linea - Nombre Linea
      Given selecciono la bodega "10012"
      And Se selecciona por defecto Tipo de Producto Ripley
      And Permite seleccionar un Departamento
      Then Selecciono el combobox Linea
  
    @SIC_MSP @SIC_MSP_016
    Scenario: Caso SIC_MSP_016 - Se habilita el campo SubLínea al seleccionar una Línea
      Given selecciono la bodega "10012"
      And Se selecciona por defecto Tipo de Producto Ripley
      And Permite seleccionar un Departamento
      When Selecciono el combobox Linea
      And selecciono opción Botas
      Then Selecciono el combobox Sublinea
  
    @SIC_MSP @SIC_MSP_017
    Scenario: Caso SIC_MSP_017 - Selecciona una línea y elimina el departamento seleccionado
      Given selecciono la bodega "10012"
      And Se selecciona por defecto Tipo de Producto Ripley
      And Permite seleccionar un Departamento
      When Selecciono el combobox Linea
      And selecciono opción Botas
      Then Elimino departamento seleccionado
  
    @SIC_MSP @SIC_MSP_018
    Scenario: Caso SIC_MSP_018 - Se inhabilita el campo sublinea al eliminar todas las líneas seleccionadas
      Given selecciono la bodega "10012"
      And Se selecciona por defecto Tipo de Producto Ripley
      And Permite seleccionar un Departamento
      When Selecciono el combobox Linea
      And selecciono opción Botas
      Then Selecciono el combobox Sublinea
      And selecciono opción Polerones S001121
      And Elimino Líneas seleccionadas
  
    @SIC_MSP @SIC_MSP_019
    Scenario: Caso SIC_MSP_019 - Visualizar en combo SubLínea listado almacenados en la BD. Codigo SubLinea - Nombre SubLinea
      Given selecciono la bodega "10012"
      And Se selecciona por defecto Tipo de Producto Ripley
      And Permite seleccionar un Departamento
      When Selecciono el combobox Linea
      And selecciono opción Botas
      Then Selecciono el combobox Sublinea
  
    @SIC_MSP @SIC_MSP_020
    Scenario: Caso SIC_MSP_020 - Se habilita el campo SKU al seleccionar una sublínea
      Given selecciono la bodega "10012"
      And Se selecciona por defecto Tipo de Producto Ripley
      And Permite seleccionar un Departamento
      When Selecciono el combobox Linea
      And selecciono opción Botas
      Then Selecciono el combobox Sublinea
      And selecciono opción Polerones S001121
      And Se habilita campo Cod. Venta
  
    @SIC_MSP @SIC_MSP_024
    Scenario: Caso SIC_MSP_024 - El usuario selecciona más de un sku
      Given selecciono la bodega "10012"
      And Se selecciona por defecto Tipo de Producto Ripley
      And Permite seleccionar un Departamento
      And Selecciono el combobox Linea
      And selecciono opción Botas
      When Selecciono el combobox Sublinea
      And selecciono opción Polerones S001121
      And Se habilita campo Cod. Venta
      Then Selecciono campo Cod. Venta
      And Ingreso más de un SKU
  
    @SIC_MSP @SIC_MSP_025
    Scenario: Caso SIC_MSP_025 - El usuario selecciona Departamento, da clic en el botón Consultar sin tener una bodega seleccionada.
      Given Se selecciona por defecto Tipo de Producto Ripley
      When Permite seleccionar un Departamento
      Then Presiono Botón Consultar
      And se despliega mensaje de alerta Debe seleccionar una bodega para continuar.
  
    @SIC_MSP @SIC_MSP_026
    Scenario: Caso SIC_MSP_026 - El usuario seleccionar Línea, da clic en el botón Consultar sin tener una bodega seleccionada.
      Given Se selecciona por defecto Tipo de Producto Ripley
      When Permite seleccionar un Departamento
      And Selecciono el combobox Linea
      And selecciono opción Botas
      Then Presiono Botón Consultar
      And se despliega mensaje de alerta Debe seleccionar una bodega para continuar.
  
    @SIC_MSP @SIC_MSP_028
    Scenario: Caso SIC_MSP_028 - El usuario selecciona SubLínea, da clic en el botón Consultar sin tener una bodega seleccionada.
      Given Se selecciona por defecto Tipo de Producto Ripley
      When Permite seleccionar un Departamento
      And Selecciono el combobox Linea
      And selecciono opción Botas
      And Selecciono el combobox Sublinea
      And selecciono opción Polerones S001121
      And Se habilita campo Cod. Venta
      And Selecciono campo Cod. Venta
      And Ingreso un SKU
      Then Presiono Botón Consultar
      And se despliega mensaje de alerta Debe seleccionar una bodega para continuar.
  
    @SIC_MSP @SIC_MSP_029
    Scenario: Caso SIC_MSP_029 - El usuario da clic en el botón Consultar teniendo elegida un departamento
      Given selecciono la bodega "10012"
      And Se selecciona por defecto Tipo de Producto Ripley
      When Permite seleccionar un Departamento
      And Presiono Botón Consultar
      Then Se despliega grilla Configuraciones Activas y Vigentes
      And Mensaje en grilla No se encontraron resultados para los criterios de búsqueda ingresados
  
    @SIC_MSP @SIC_MSP_030
    Scenario: Caso SIC_MSP_030 - El usuario da clic en el botón Consultar teniendo elegida un departamento y una línea
      Given selecciono la bodega "10012"
      And Se selecciona por defecto Tipo de Producto Ripley
      When Permite seleccionar un Departamento
      And Selecciono el combobox Linea
      And selecciono opción Botas
      Then Presiono Botón Consultar
      And Se despliega grilla Configuraciones Activas y Vigentes
      And Mensaje en grilla No se encontraron resultados para los criterios de búsqueda ingresados
  
   	@SIC_MSP @SIC_MSP_031
    Scenario: Caso SIC_MSP_031 - El usuario da clic en el botón Consultar teniendo elegida un departamento, una línea y SubLínea
      Given selecciono la bodega "10012"
      And Se selecciona por defecto Tipo de Producto Ripley
      When Permite seleccionar un Departamento
      And Selecciono el combobox Linea
      And selecciono opción Botas
      And Selecciono el combobox Sublinea
      And selecciono opción Polerones S001121
      Then Presiono Botón Consultar
      And Se despliega grilla Configuraciones Activas y Vigentes
      And Mensaje en grilla No se encontraron resultados para los criterios de búsqueda ingresados
  
    @SIC_MSP @SIC_MSP_032
    Scenario: Caso SIC_MSP_032 - El usuario da clic en el botón Consultar teniendo elegida un departamento, una línea, SubLínea y un SKU
      Given selecciono la bodega "10012"
      And Se selecciona por defecto Tipo de Producto Ripley
      When Permite seleccionar un Departamento
      And Selecciono el combobox Linea
      And selecciono opción Botas
      And Selecciono el combobox Sublinea
      And selecciono opción Polerones S001121
      And Se habilita campo Cod. Venta
      And Selecciono campo Cod. Venta
      And Ingreso un SKU
      Then Presiono Botón Consultar
      And Se despliega grilla Configuraciones Activas y Vigentes
      And Mensaje en grilla No se encontraron resultados para los criterios de búsqueda ingresados
  
    @SIC_MSP @SIC_MSP_033
    Scenario: Caso SIC_MSP_033 - El usuario da clic en el botón Consultar teniendo elegida un departamento
      Given selecciono la bodega "10012"
      And Se selecciona por defecto Tipo de Producto Ripley
      When Permite seleccionar un Departamento con Configuración Activa
      And Presiono Botón Consultar
      Then Se despliega grilla Configuraciones Activas y Vigentes
  
    @SIC_MSP @SIC_MSP_034
    Scenario: Caso SIC_MSP_034 - El usuario da clic en el botón Consultar teniendo elegida un departamento y una Línea
      Given selecciono la bodega "10012"
      And Se selecciona por defecto Tipo de Producto Ripley
      When Permite seleccionar un Departamento con Configuración Activa
      And Selecciono el combobox Linea
      And selecciono opción Carteras
      And Presiono Botón Consultar
      Then Se despliega grilla Configuraciones Activas y Vigentes
  
    @SIC_MSP @SIC_MSP_035
    Scenario: Caso SIC_MSP_035 - El usuario da clic en el botón Consultar teniendo elegida un departamento, una Línea y una Sub Línea
      Given selecciono la bodega "10012"
      And Se selecciona por defecto Tipo de Producto Ripley
      When Permite seleccionar un Departamento con Configuración Activa
      And Selecciono el combobox Linea
      And selecciono opción Carteras
      And Selecciono el combobox Sublinea
      And selecciono opción Cuero S502038
      And Presiono Botón Consultar
      Then Se despliega grilla Configuraciones Activas y Vigentes
  
    @SIC_MSP @SIC_MSP_036
    Scenario: Caso SIC_MSP_036 - El usuario da clic en el botón Consultar teniendo elegida un departamento, una Línea, una Sub Línea y un SKU
      Given selecciono la bodega "10012"
      And Se selecciona por defecto Tipo de Producto Ripley
      When Permite seleccionar un Departamento con Configuración Activa
      And Selecciono el combobox Linea
      And selecciono opción Carteras
      And Selecciono el combobox Sublinea
      And selecciono opción Cuero S502038
      And Se habilita campo Cod. Venta
      And Selecciono campo Cod. Venta
      And Ingreso un SKU con Configuración Activa
      And Presiono Botón Consultar
      Then Se despliega grilla Configuraciones Activas y Vigentes
  
    @SIC_MSP @SIC_MSP_060
    Scenario: Caso SIC_MSP_060 - El usuario da clic en el botón Nuevo Evento habiendo realizado una consulta previamente.
      Given selecciono la bodega "10012"
      And En combobox Departamento selecciono opción Escolar
      And Presiono el botón Consultar Stock Protegido
      When Se despliega grilla Configuraciones Activas y Vigentes
      Then Se presiona el Botón Nuevo Evento
      And Se despliega la grilla Configuración de Stock Protegido
  
    @SIC_MSP @SIC_MSP_061
    Scenario: Caso SIC_MSP_061 - Realizar consulta y nuevo evento. El sistema no permite seleccionar una fecha anterior a la fecha actual
      Given selecciono la bodega "10012"
      And En combobox Departamento selecciono opción Escolar
      And Presiono el botón Consultar Stock Protegido
      And Se despliega grilla Configuraciones Activas y Vigentes
      When Se presiona el Botón Nuevo Evento
      And Se despliega la grilla Configuración de Stock Protegido
      Then Sistema no permite ingresar Fecha menor a la actual
  
    @SIC_MSP @SIC_MSP_062
    Scenario: Caso SIC_MSP_062 - Realizar consulta y nuevo evento. El sistema permite seleccionar una fecha igual a la fecha actual
      Given selecciono la bodega "10012"
      And En combobox Departamento selecciono opción Escolar
      And Presiono el botón Consultar Stock Protegido
      And Se despliega grilla Configuraciones Activas y Vigentes
      When Se presiona el Botón Nuevo Evento
      And Se despliega la grilla Configuración de Stock Protegido
      Then Ingresamos una fecha de inicio válida
      And Presionamos el Botón Guardar
  
    @SIC_MSP @SIC_MSP_063
    Scenario: Caso SIC_MSP_063 - Realizar consulta y nuevo evento. El sistema permite seleccionar una fecha posterior a la fecha actual
      Given selecciono la bodega "10012"
      And En combobox Departamento selecciono opción Escolar
      And Presiono el botón Consultar Stock Protegido
      And Se despliega grilla Configuraciones Activas y Vigentes
      When Se presiona el Botón Nuevo Evento
      And Se despliega la grilla Configuración de Stock Protegido
      Then Ingresamos una fecha de inicio mayor a la actual
      And Presionamos el Botón Guardar
  
   @SIC_MSP @SIC_MSP_064
    Scenario: Caso SIC_MSP_064 - Realizar consulta y nuevo evento. El sistema no permite seleccionar una fecha anterior a la fecha inicio
      Given selecciono la bodega "10012"
      And En combobox Departamento selecciono opción Escolar
      And Presiono el botón Consultar Stock Protegido
      And Se despliega grilla Configuraciones Activas y Vigentes
      When Se presiona el Botón Nuevo Evento
      And Se despliega la grilla Configuración de Stock Protegido
      Then Ingresamos una fecha de inicio mayor a la actual
      And Ingresamos una fecha de termino Menor a la Fecha de Inicio
      And Presionamos el Botón Guardar
  
   @SIC_MSP @SIC_MSP_065
    Scenario: Caso SIC_MSP_065 - Realizar consulta y nuevo evento. El sistema permite seleccionar una fecha posterior a la fecha inicio
      Given selecciono la bodega "10012"
      And En combobox Departamento selecciono opción Escolar
      And Presiono el botón Consultar Stock Protegido
      And Se despliega grilla Configuraciones Activas y Vigentes
      When Se presiona el Botón Nuevo Evento
      And Se despliega la grilla Configuración de Stock Protegido
      Then Ingresamos una fecha de inicio válida
      And Ingresamos una fecha de termino válida
      And Presionamos el Botón Guardar
  
    @SIC_MSP @SIC_MSP_066
    Scenario: Caso SIC_MSP_066 - Realizar consulta y nuevo evento. El sistema permite seleccionar una fecha posterior a la fecha inicio
      Given selecciono la bodega "10012"
      And En combobox Departamento selecciono opción Escolar
      And Presiono el botón Consultar Stock Protegido
      And Se despliega grilla Configuraciones Activas y Vigentes
      When Se presiona el Botón Nuevo Evento
      And Se despliega la grilla Configuración de Stock Protegido
      Then Seleccionamos Checkbox Permanente
  
    @SIC_MSP @SIC_MSP_070
    Scenario: Caso SIC_MSP_070 - Guardar un nuevo evento sin ingresar Campos Obligatorios
      Given selecciono la bodega "10012"
      And En combobox Departamento selecciono opción Escolar
      And Presiono el botón Consultar Stock Protegido
      And Se despliega grilla Configuraciones Activas y Vigentes
      When Se presiona el Botón Nuevo Evento
      And Se despliega la grilla Configuración de Stock Protegido
      Then Presionamos el Botón Guardar
      And Validamos mensaje de alerta Debe completar los campos obligatorios
  
    @SIC_MSP @SIC_MSP_071
    Scenario: Caso SIC_MSP_071 - Guardar un nuevo evento sin ingresar todos los niveles de protección
      Given selecciono la bodega "10012"
      And En combobox Departamento selecciono opción Escolar
      And Presiono el botón Consultar Stock Protegido
      And Se despliega grilla Configuraciones Activas y Vigentes
      When Se presiona el Botón Nuevo Evento
      And Ingresamos una fecha de inicio válida
      And Ingresamos una fecha de termino válida
      And Ingresamos Stock para Nivel Uno
      And Ingresamos Stock para Nivel Dos
      Then Presionamos el Botón Guardar
      And Se despliega mensaje de alerta para confirmar o cancelar la falta de niveles
  
    @SIC_MSP @SIC_MSP_072
    Scenario: Caso SIC_MSP_072 - Guardar un nuevo evento, sistema valida que existe una configuración
      Given selecciono la bodega "10012"
      And En combobox Departamento selecciono opción Escolar
      And Presiono el botón Consultar Stock Protegido
      And Se despliega grilla Configuraciones Activas y Vigentes
      When Se presiona el Botón Nuevo Evento
      And Ingresamos una fecha de inicio válida
      And Ingresamos una fecha de termino válida
      And Ingresamos Stock para Nivel Uno
      And Ingresamos Stock para Nivel Dos
      And Ingresamos Stock para Nivel Tres
      And Ingresamos Stock para Nivel Cuatro
      And Ingresamos Stock para Nivel Cinco
      Then Presionamos el Botón Guardar
      And Se despliega mensaje de alerta indicando que ya existe una configuración
  
    @SIC_MSP @SIC_MSP_073
    Scenario: Caso SIC_MSP_073 - Guardar un nuevo evento. El sistema cerrará el mensaje de alerta, y se volverá al Formulario de Configuración de Stock de seguridad.
      Given selecciono la bodega "10012"
      And En combobox Departamento selecciono opción Escolar
      And Presiono el botón Consultar Stock Protegido
      And Se despliega grilla Configuraciones Activas y Vigentes
      When Se presiona el Botón Nuevo Evento
      And Ingresamos una fecha de inicio válida
      And Ingresamos una fecha de termino válida
      And Ingresamos Stock para Nivel Uno
      And Ingresamos Stock para Nivel Dos
      And Ingresamos Stock para Nivel Tres
      And Ingresamos Stock para Nivel Cuatro
      And Ingresamos Stock para Nivel Cinco
      Then Presionamos el Botón Guardar
      And Se despliega Pop Up con mensaje de alerta para Confirmar Stock
      And se despliega mensaje de alerta Confirmando
  
    @SIC_MSP @SIC_MSP_080
    Scenario: Caso SIC_MSP_080 - Guardar un nuevo evento. El sistema cerrará el mensaje de alerta, y se volverá al Formulario de Configuración.
      Given selecciono la bodega "10012"
      When Permite seleccionar un Departamento
      And Selecciono el combobox Linea
      And selecciono opción Botas
      And Selecciono el combobox Sublinea
      And selecciono opción Polerones S001121
      And Se habilita campo Cod. Venta
      And Selecciono campo Cod. Venta
      And Ingreso un SKU
      Then Presiono Botón Consultar
      And Se despliega grilla Configuraciones Activas y Vigentes
      When Se presiona el Botón Nuevo Evento
      And Ingresamos una fecha de inicio válida
      And Ingresamos una fecha de termino válida
      And Ingresamos Stock para Nivel Uno
      And Ingresamos Stock para Nivel Dos
      And Ingresamos Stock para Nivel Tres
      And Ingresamos Stock para Nivel Cuatro
      And Ingresamos Stock para Nivel Cinco
      Then Presionamos el Botón Guardar
      And Se despliega Pop Up con mensaje de alerta para Confirmar Stock
      And se despliega mensaje de alerta Confirmando
  
    @SIC_MSP @SIC_MSP_084
    Scenario: Caso SIC_MSP_084 - El usuario da clic en el botón Cancelar del Formulario de Configuración de Stock Protegido.
      Given selecciono la bodega "10012"
      When Permite seleccionar un Departamento
      And Selecciono el combobox Linea
      And selecciono opción Botas
      And Selecciono el combobox Sublinea
      And selecciono opción Polerones S001121
      And Se habilita campo Cod. Venta
      And Selecciono campo Cod. Venta
      And Ingreso un SKU
      Then Presiono Botón Consultar
      And Se despliega grilla Configuraciones Activas y Vigentes
      When Se presiona el Botón Nuevo Evento
      And Ingresamos una fecha de inicio válida
      And Ingresamos una fecha de termino válida
      And Ingresamos Stock para Nivel Uno
      And Ingresamos Stock para Nivel Dos
      And Ingresamos Stock para Nivel Tres
      And Ingresamos Stock para Nivel Cuatro
      And Ingresamos Stock para Nivel Cinco
      Then Se presiona botón Cancelar
  
    @SIC_MSP @SIC_MSP_087
    Scenario: Caso SIC_MSP_087 - El usuario da clic en el botón Eliminar de la grilla de Configuraciones Vigentes y habiendo seleccionado una configuración
      Given selecciono la bodega "10012"
      And En combobox Departamento selecciono opción Escolar
      And Presiono el botón Consultar Stock Protegido
      And Se despliega grilla Configuraciones Activas y Vigentes
      When Se presiona el Botón Eliminar
      Then se despliega mensaje de alerta confirmando la selección
  
    @SIC_MSP @SIC_MSP_088
    Scenario: Caso SIC_MSP_088 - El usuario da clic en el botón Eliminar de la grilla de Configuraciones Vigentes, el usuario confirma la eliminación
      Given selecciono la bodega "10012"
      And En combobox Departamento selecciono opción Escolar
      And Presiono el botón Consultar Stock Protegido
      And Se despliega grilla Configuraciones Activas y Vigentes
      When Se presiona el Botón Eliminar
      Then Se despliega Pop Up con mensaje de alerta para Confirmar Stock
      And se despliega mensaje de alerta Confirmando
  
    @SIC_MSP @SIC_MSP_089
    Scenario: Caso SIC_MSP_089 - El usuario da clic en el botón Eliminar de la grilla de Configuraciones Vigentes, el usuario cancela la eliminación
      Given selecciono la bodega "10012"
      And En combobox Departamento selecciono opción Escolar
      And Presiono el botón Consultar Stock Protegido
      And Se despliega grilla Configuraciones Activas y Vigentes
      When Se presiona el Botón Eliminar
      Then En mensaje emergente se cancela la eliminación
  
    @SIC_MSP @SIC_MSP_090
    Scenario: Caso SIC_MSP_090 - El usuario da clic en el botón Editar de la grilla de Configuraciones Vigentes
      Given selecciono la bodega "10012"
      And En combobox Departamento selecciono opción Escolar
      And Presiono el botón Consultar Stock Protegido
      And Se despliega grilla Configuraciones Activas y Vigentes
      When Se presiona el Botón Editar
      And Se despliega la grilla Configuración de Stock Protegido
  
    @SIC_MSP @SIC_MSP_091
    Scenario: Caso SIC_MSP_091 - El usuario da clic en el botón Editar de la grilla de Configuraciones Vigentes habiendo seleccionado una configuración en estado activo
      Given selecciono la bodega "10012"
      And En combobox Departamento selecciono opción Escolar
      And Presiono el botón Consultar Stock Protegido
      And Se despliega grilla Configuraciones Activas y Vigentes
      When Se presiona el Botón Editar
      And Se despliega la grilla Configuración de Stock Protegido
      Then Editamos Stock para Nivel Uno
      And Editamos Stock para Nivel Dos
      Then Presionamos el Botón Guardar
  
    @SIC_MSP @SIC_MSP_094
    Scenario: Caso SIC_MSP_094 - El usuario da clic en el botón Editar de la grilla de Configuraciones Vigentes habiendo seleccionado una configuración en estado activo y cancela edición
      Given selecciono la bodega "10012"
      And En combobox Departamento selecciono opción Escolar
      And Presiono el botón Consultar Stock Protegido
      And Se despliega grilla Configuraciones Activas y Vigentes
      When Se presiona el Botón Editar
      And Se despliega la grilla Configuración de Stock Protegido
      Then Editamos Stock para Nivel Uno
      And Editamos Stock para Nivel Dos
      Then Presionamos el Botón Cancelar
  

