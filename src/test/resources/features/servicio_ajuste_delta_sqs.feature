Feature: Ajuste delta vía SQS
  Como usuario
  Quiero realizar ajustes de inventario con la finalidad de cubrir todas las casuisticas que puedan existir

#Caso solicitado por el dany
@SDELTA @SDELTA_SQS @SDELTA_SQS_00_xURL @SDELTA_SQS_C
  Scenario Outline: Realizar ajuste delta por URL 
    Given quiero realizar un ajuste delta via SQS para el sistema "<sistema_origen_SQS>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos, con stock "<Stock_previo>" previamente cargado en SIC
    When envio un request de ajuste via SQS a "<URL_SQS>"
    Then se registra en bd la cabecera con estado "C" mensaje ""
    And se valida en SIC el inventario

    Examples: 
      | sistema_origen_SQS | cantidad_productos | Stock_previo |  URL_SQS	|
      | PMM                |                  1 |      P       |  https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsicm-qa-cl-req-pmm.fifo/send-receive    |
      #| BGT               |                  1 |      P       |  https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsicm-qa-cl-req-bt.fifo/send-receive     |
      #| B2B               |                  1 |      P       |  https://console.aws.amazon.com/sqs/home?region=us-east-1#send-message:selected=https://sqs.us-east-1.amazonaws.com/322525823727/sicm-qa-cl-req-pmm.fifo;noRefresh=true;prefix=sicm-qa-cl-req$		|
      #| BO                |                  1 |      P       |  https://console.aws.amazon.com/sqs/home?region=us-east-1#send-message:selected=https://sqs.us-east-1.amazonaws.com/322525823727/sicm-qa-cl-req-pmm.fifo;noRefresh=true;prefix=sicm-qa-cl-req$		|
 
@SDELTA @SDELTA_SQS @SDELTA_SQS_01_BGT @SDELTA_SQS_PMM @SDELTA_SQS_C
  Scenario Outline: Realizar ajuste delta de 1 producto
    Given quiero realizar un ajuste delta via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos
    When envio un request de ajuste via SQS
    Then se registra en bd la cabecera con estado "C" mensaje ""
    And se valida en SIC el inventario

    Examples: 
      | sistema_origen | cantidad_productos |
      | BGT            |                  2 |

  @SDELTA @SDELTA_SQS @SDELTA_SQS_09 @SDELTA_REST_PMM @SDELTA_SQS_SinEstado
  Scenario Outline: En la cabecera del ajuste, no se envía el campo 'Sistema Origen'
    Given quiero realizar un ajuste delta via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Sistema Origen
    When envio un request de ajuste via SQS
    Then validar que no se registre en la bd

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |

  @SDELTA @SDELTA_SQS @SDELTA_SQS_13 @SDELTA_REST_PMM @SDELTA_SQS_SinEstado
  Scenario Outline: En la cabecera del ajuste, no se envía el campo 'Tipo de Movimiento'
    Given quiero realizar un ajuste delta via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Tipo de Movimiento
    When envio un request de ajuste via SQS
    Then validar que no se registre en la bd

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |

  @SDELTA @SDELTA_SQS @SDELTA_SQS_17 @SDELTA_REST_PMM @SDELTA_SQS_SinEstado
  Scenario Outline: En la cabecera del ajuste, no se envía el campo 'Usuario'
    Given quiero realizar un ajuste delta via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Usuario
    When envio un request de ajuste via SQS
    Then se registra en bd la cabecera con estado "F" mensaje "El campo user es obligatorio"
    And no se registra en bd el detalle

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |

  @SDELTA @SDELTA_SQS @SDELTA_SQS_21 @SDELTA_REST_PMM @SDELTA_SQS_F
  Scenario Outline: En la transacción de ajuste, se envía un producto que no existe
    Given quiero realizar un ajuste delta via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos que no existen
    When envio un request de ajuste via SQS
    Then se registra en bd la cabecera con estado "F" mensaje ""
    And se registra en bd el detalle con estado "F" mensaje "SKU no existe"

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  2 |
      | B2B            |                  2 |
      | BO             |                  2 |
      | BGT            |                  2 |

  @SDELTA @SDELTA_SQS @SDELTA_SQS_25 @SDELTA_REST_PMM @SDELTA_SQS_C
  Scenario Outline: Realizar ajuste delta de un producto con stock 0
    Given quiero realizar un ajuste delta via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos con stock 0 con ajuste negativo
    When envio un request de ajuste via SQS
    Then se registra en bd la cabecera con estado "C" mensaje ""
    And se validan los datos del ajuste en bd
    And se valida en SIC el inventario

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |

  @SDELTA @SDELTA_SQS @SDELTA_SQS_29 @SDELTA_REST_PMM @SDELTA_SQS_F @_I
  Scenario Outline: En la cabecera del ajuste, no se envía el campo 'Tipo Documento'
    Given quiero realizar un ajuste delta via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Documento de Transaccion
    When envio un request de ajuste via SQS
    Then se registra en bd la cabecera con estado "F" mensaje ""
    And se registra en bd el detalle con estado "F" mensaje "No se encontro el campo documento de transaccion"

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |

  @SDELTA @SDELTA_SQS @SDELTA_SQS_33 @SDELTA_REST_PMM @SDELTA_SQS_F
  Scenario Outline: En la cabecera del ajuste, no se envía el campo 'Bodega'
    Given quiero realizar un ajuste delta via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Bodega
    When envio un request de ajuste via SQS
    Then se registra en bd la cabecera con estado "F" mensaje ""
    And se registra en bd el detalle con estado "F" mensaje "El campo fulfillment es obligatorio"

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |

  @SDELTA @SDELTA_SQS @SDELTA_SQS_37 @SDELTA_REST_PMM @SDELTA_SQS_F
  Scenario Outline: En la cabecera del ajuste, no se envía el campo 'Tipo Inventario'
    Given quiero realizar un ajuste delta via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Tipo de Inventario
    When envio un request de ajuste via SQS
    Then se registra en bd la cabecera con estado "F" mensaje ""
    And se registra en bd el detalle con estado "F" mensaje "El campo inventoryType es obligatorio"

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |

  @SDELTA @SDELTA_SQS @SDELTA_SQS_41 @SDELTA_REST_PMM @SDELTA_SQS_C
  Scenario Outline: En el del ajuste, no se envía el campo 'SKU'
    Given quiero realizar un ajuste delta via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos
    And borro el valor de el campo producto
    When envio un request de ajuste via SQS
    Then se registra en bd la cabecera con estado "C" mensaje ""
    And se validan los datos del ajuste en bd
    And se valida en SIC el inventario

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |

  @SDELTA @SDELTA_SQS @SDELTA_SQS_45 @SDELTA_REST_PMM @SDELTA_SQS_F
  Scenario Outline: En el detalle del ajuste, se envía una bodega que no existe
    Given quiero realizar un ajuste delta via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos
    And asigno la bodega "<bodega_no_existe>"
    When envio un request de ajuste via SQS
    Then se registra en bd la cabecera con estado "F" mensaje ""
    And se registra en bd el detalle con estado "F" mensaje "Bodega no existe"

    Examples: 
      | sistema_origen | cantidad_productos | bodega_no_existe |
      | PMM            |                  1 |            29912 |
      | B2B            |                  1 |            11921 |
      | BO             |                  1 |            99182 |
      | BGT            |                  1 |            22133 |

  @SDELTA @SDELTA_SQS @SDELTA_SQS_49 @SDELTA_REST_PMM @SDELTA_SQS_C
  Scenario Outline: Realizar ajuste delta de 200 producto
    Given quiero realizar un ajuste delta via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos
    When envio un request de ajuste via SQS
    Then se registra en bd la cabecera con estado "C" mensaje ""
    And se validan los datos del ajuste en bd

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                200 |
      | B2B            |                200 |
      | BO             |                200 |
      | BGT            |                200 |

  @SDELTA @SDELTA_SQS @SDELTA_SQS_53 @SDELTA_REST_BGT @SDELTA_SQS_CF
  Scenario Outline: Se envía una transacción y su estado cambia a 'Completado con Errores'
    Given quiero realizar un ajuste delta via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos con error en un registro
    When envio un request de ajuste via SQS
    Then se registra en bd la cabecera con estado "CF" mensaje ""

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  3 |
      | B2B            |                  3 |
      | BO             |                  3 |
      | BGT            |                  3 |
  