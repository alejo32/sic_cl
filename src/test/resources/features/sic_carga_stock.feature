Feature: Carga Masiva de Ajuste de Stock
  Como usuario
  Quiero realizar ajuste de stock masivo a través del sistema SIC

  Background: 
    Given ingreso al sistema sic
    And voy a la opcion Carga Masiva de Ajuste de Stock

  @SIC @SIC_CMAS @SIC_CMAS_01
  Scenario Outline: SIC_CMAS_01 - Validar la correcta carga masiva de ajuste de stock disponible
    Given tengo informacion de stock de "<cantidad_productos>" productos de la bodega "<bodega>"
    And genero un archivo de carga masiva de ajuste de stock por "<cantidad_productos>"
    When selecciono la bodega "<bodega>"
    And cargo el archivo de carga masiva
    And doy click a realizar carga
    Then se valida la carga en estado "Completado"
    And se valida lo mostrado en el resumen de ejecucion
    

    Examples: 
      | cantidad_productos | bodega |
      |                  1 |  10012 |

  @SIC @SIC_CMAS @SIC_CMAS_02
  Scenario: SIC_CMAS_02 - Validar que en el formulario el campo tipo de producto tenga el valor por defecto '1-Ripley'
    Then se valida que por defecto tenga el valor "1-Ripley"

  @SIC @SIC_CMAS @SIC_CMAS_03
  Scenario: SIC_CMAS_03 - Realizar carga sin haber seleccionado un archivo
    When doy click a realizar carga
    Then se valida mensaje de error "Debe elegir un archivo para procesar."

  @SIC @SIC_CMAS @SIC_CMAS_04
  Scenario: SIC_CMAS_04 - Validar que se muestre el nombre del archivo cargado en el campo Archivo
    Given tengo informacion de stock de "10" productos de la bodega "10012"
    And genero un archivo de carga masiva de ajuste de stock
    When selecciono la bodega "10012"
    And cargo el archivo de carga masiva
    Then se valida que se visualice el nombre del archivo seleccionado
    

  @SIC @SIC_CMAS @SIC_CMAS_05
  Scenario Outline: SIC_CMAS_05 - Realizar una carga masiva con un archivo con extencion no permitida
    Given tengo informacion de stock de "<cantidad_productos>" productos de la bodega "<bodega>"
    And genero un archivo de carga masiva de ajuste de stock con extension "<extension>"
    When selecciono la bodega "<bodega>"
    And cargo el archivo de carga masiva
    And doy click a realizar carga
    Then se valida mensaje de error "El formato a cargar debe ser csv."
    
    Examples: 
      | cantidad_productos | bodega | extension |
      |                 10 |  10012 | .txt      |
      |                 10 |  10012 | .zip      |
      |                 10 |  10012 | .tar.gz   |
      |                 10 |  10012 | .xlsx     |

  @SIC @SIC_CMAS @SIC_CMAS_09
  Scenario: SIC_CMAS_09 - Realizar una carga masiva que termine en estado Completado Fallido
    Given tengo informacion de stock de "5" productos de la bodega "10012"
    And genero un archivo de carga masiva de ajuste de stock con errores en algunos registros
    When selecciono la bodega "10012"
    And cargo el archivo de carga masiva
    And doy click a realizar carga
    Then se valida la carga en estado "Completado con Errores"
    And se valida lo mostrado en el resumen de ejecucion

  @SIC @SIC_CMAS @SIC_CMAS_10
  Scenario: SIC_CMAS_10 - Realizar una carga masiva que termine en estado Fallido
    Given tengo informacion de stock de "0" productos de la bodega "10012"
    And genero un archivo de carga masiva de ajuste de stock "0"
    When selecciono la bodega "10012"
    And cargo el archivo de carga masiva
    And doy click a realizar carga
    Then se valida la carga en estado "Fallido"
    And se valida lo mostrado en el resumen de ejecucion

  @SIC @SIC_CMAS @SIC_CMAS_11
  Scenario: SIC_CMAS_11 - Realizar una carga masiva donde se envie un valor no numerico en el campo stock
    Given tengo informacion de stock de "10" productos de la bodega "10012"
    And genero un archivo de carga masiva de ajuste de stock con stock "X"
    When selecciono la bodega "10012"
    And cargo el archivo de carga masiva
    And doy click a realizar carga
    Then se valida la carga en estado "Fallido"
    And se valida lo mostrado en el resumen de ejecucion

  @SIC @SIC_CMAS @SIC_CMAS_12
  Scenario: SIC_CMAS_12 - Realziar una carga masiva donde no se envie el campo sku
    Given tengo informacion de stock de "10" productos de la bodega "10012"
    And genero un archivo de carga masiva de ajuste de stock sin valor en el campo sku
    When selecciono la bodega "10012"
    And cargo el archivo de carga masiva
    And doy click a realizar carga
    Then se valida la carga en estado "Fallido"
    And se valida lo mostrado en el resumen de ejecucion

  @SIC @SIC_CMAS @SIC_CMAS_13
  Scenario: SIC_CMAS_13 - Realziar una carga masiva donde no se envie el campo stock
    Given tengo informacion de stock de "10" productos de la bodega "10012"
    And genero un archivo de carga masiva de ajuste de stock sin valor en el campo stock
    When selecciono la bodega "10012"
    And cargo el archivo de carga masiva
    And doy click a realizar carga
    Then se valida la carga en estado "Fallido"
    And se valida lo mostrado en el resumen de ejecucion

  @SIC @SIC_CMAS @SIC_CMAS_14
  Scenario: SIC_CMAS_14 - Validar la correcta carga masiva de ajuste de stock tipo inventario
    Given tengo informacion de stock de "10" productos de la bodega "10012"
    And genero un archivo de carga masiva de ajuste de stock
    When selecciono la bodega "10012"
    And cargo el archivo de carga masiva
    And doy click a realizar carga
    Then se valida la carga en estado "Completado"
    And se valida lo mostrado en el resumen de ejecucion

  @SIC @SIC_CMAS @SIC_CMAS_15
  Scenario: SIC_CMAS_15 - Realizar una carga masiva con tipo de operacion A y S
    Given tengo informacion de stock de "10" productos de la bodega "10012"
    And genero un archivo de carga masiva de ajuste de stock con adicion y sustraccion
    When selecciono la bodega "10012"
    And cargo el archivo de carga masiva
    And doy click a realizar carga
    Then se valida la carga en estado "Completado"
    And se valida lo mostrado en el resumen de ejecucion

  @SIC @SIC_CMAS @SIC_CMAS_16
  Scenario: SIC_CMAS_16 - Realizar una carga masiva con tipo de operacion incorrecta
    Given tengo informacion de stock de "10" productos de la bodega "10012"
    And genero un archivo de carga masiva de ajuste de stock con tipo de operacion "T"
    When selecciono la bodega "10012"
    And cargo el archivo de carga masiva
    And doy click a realizar carga
    Then se valida la carga en estado "Fallido"
    And se valida lo mostrado en el resumen de ejecucion

    