Feature: Ajuste de inventario vía REST
  Como usuario
  Quiero realizar ajustes de inventario con la finalidad de cubrir todas las casuisticas que puedan existir

 @ADJUST @ADJUST_REST @ADJUST_REST_01 @ADJUST_REST_PMM @ADJUST_REST_C
  Scenario Outline: Realizar ajuste de inventario
    Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos 
    When envio un request de ajuste al api
    Then se registra en bd la cabecera con estado "C" mensaje ""
    And se validan los datos del ajuste en bd
    And se valida en SIC el inventario
    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |
      | PMM            |                 10 |
      | B2B            |                 10 |
      | BO             |                 10 |
      | BGT            |                 10 |

  @ADJUST @ADJUST_REST @ADJUST_REST_09 @ADJUST_REST_PMM @ADJUST_REST_C
  Scenario Outline: Realizar ajuste de inventario de un producto con stock 0
    Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos con stock 0 con ajuste negativo
    When envio un request de ajuste al api
    Then se registra en bd la cabecera con estado "C" mensaje ""
    And se validan los datos del ajuste en bd
    And se valida en SIC el inventario

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |      
  
  @ADJUST @ADJUST_REST @ADJUST_REST_13 @ADJUST_REST_PMM @ADJUST_REST_C
    Scenario Outline: Ajuste de 1 registro de producto con la operacion de sustracción.
      Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
      And preparo la trama de ajuste con "<cantidad_productos>" productos con ajuste negativo
      When envio un request de ajuste al api
      Then se registra en bd la cabecera con estado "C" mensaje ""
      And se validan los datos del ajuste en bd
      And se valida en SIC el inventario
  
      Examples: 
        | sistema_origen | cantidad_productos |
        | PMM            |                  1 |
        | B2B            |                  1 |
        | BO             |                  1 |
        | BGT            |                  1 |
  
    @ADJUST @ADJUST_REST @ADJUST_REST_17 @ADJUST_REST_PMM @ADJUST_REST_C
    Scenario Outline: Realizar ajuste de inventario de 5 productos sin stock
      Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
      And preparo la trama de ajuste con "<cantidad_productos>" productos sin stock para la bodega "10012"
      When envio un request de ajuste al api
      Then se registra en bd la cabecera con estado "C" mensaje ""
      And se validan los datos del ajuste en bd
      And se valida en SIC el inventario
  
      Examples: 
        | sistema_origen | cantidad_productos |
        | PMM            |                  5 |
        | B2B            |                  5 |
        | BO             |                  5 |
        | BGT            |                  5 |
  
    @ADJUST @ADJUST_REST @ADJUST_REST_21 @ADJUST_REST_PMM @ADJUST_REST_F
    Scenario Outline: Realizar ajuste de inventario con tipo de operacion incorrecto
      Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
      And preparo la trama de ajuste con "<cantidad_productos>" productos con Operacion "<tipo_operacion>"
      When envio un request de ajuste al api
      Then se registra en bd la cabecera con estado "F" mensaje ""
      And se registra en bd el detalle con estado "F" mensaje "El valor del campo operación es inválido, solo se consideran A o S"
  
      Examples: 
        | sistema_origen | cantidad_productos | tipo_operacion |
        | PMM            |                  1 | L              |
        | B2B            |                  1 | M              |
        | BO             |                  1 | R              |
        | BGT            |                  1 | Q              |
  
    @ADJUST @ADJUST_REST @ADJUST_REST_25 @ADJUST_REST_PMM @ADJUST_REST_F
    Scenario Outline: En la transacción de ajuste, se envía un producto que no existe
      Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
      And preparo la trama de ajuste con "<cantidad_productos>" productos que no existen
      When envio un request de ajuste al api
      Then se registra en bd la cabecera con estado "F" mensaje ""
      And se registra en bd el detalle con estado "F" mensaje "SKU no existe."
  
      Examples: 
        | sistema_origen | cantidad_productos |
        | PMM            |                  2 |
        | B2B            |                  2 |
        | BO             |                  2 |
        | BGT            |                  2 |
  
    @ADJUST @ADJUST_REST @ADJUST_REST_29 @ADJUST_REST_PMM @ADJUST_REST_F
    Scenario Outline: En la cabecera del ajuste, no se envía el campo 'Tipo de Movimiento'
      Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
      And preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Tipo de Movimiento
      When envio un request de ajuste al api
      Then validar que no se registre en la bd
  
      Examples: 
        | sistema_origen | cantidad_productos |
        | PMM            |                  1 |
        | B2B            |                  1 |
        | BO             |                  1 |
        | BGT            |                  1 |
  
    @ADJUST @ADJUST_REST @ADJUST_REST_33 @ADJUST_REST_PMM @ADJUST_REST_F
    Scenario Outline: En la cabecera del ajuste, no se envía el campo 'Usuario'
      Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
      And preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Usuario
      When envio un request de ajuste al api
      Then se registra en bd la cabecera con estado "F" mensaje "El campo usuario no puede estar vacio."
      And no se registra en bd el detalle
  
      Examples: 
        | sistema_origen | cantidad_productos |
        | PMM            |                  1 |
        | B2B            |                  1 |
        | BO             |                  1 |
        | BGT            |                  1 |
  
    @ADJUST @ADJUST_REST @ADJUST_REST_37 @ADJUST_REST_PMM @ADJUST_REST_F
    Scenario Outline: En la cabecera del ajuste, no se envía el campo 'Sistema Origen'
      Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
      And preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Sistema Origen
      When envio un request de ajuste al api
      Then validar que no se registre en la bd
  
      Examples: 
        | sistema_origen | cantidad_productos |
        | PMM            |                  1 |
        | B2B            |                  1 |
        | BO             |                  1 |
        | BGT            |                  1 |
  
    @ADJUST @ADJUST_REST @ADJUST_REST_41 @ADJUST_REST_PMM @ADJUST_REST_F
    Scenario Outline: En la cabecera del ajuste, no se envía el campo 'Tipo Documento'
      Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
      And preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Documento de Transaccion
      When envio un request de ajuste al api
      Then se registra en bd la cabecera con estado "F" mensaje ""
      And se registra en bd el detalle con estado "F" mensaje "No se encontro el campo documento de transaccion"
  
      Examples: 
        | sistema_origen | cantidad_productos |
        | PMM            |                  1 |
        | B2B            |                  1 |
        | BO             |                  1 |
        | BGT            |                  1 |
  
    @ADJUST @ADJUST_REST @ADJUST_REST_45 @ADJUST_REST_PMM @ADJUST_REST_F
    Scenario Outline: En la cabecera del ajuste, no se envía el campo 'IDTRX'
      Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
      And preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo IDTRX
      When envio un request de ajuste al api
      Then se registra en bd la cabecera con estado "F" mensaje ""
      And se registra en bd el detalle con estado "F" mensaje "El campo idtrx no puede estar vacio."
  
      Examples: 
        | sistema_origen | cantidad_productos |
        | PMM            |                  1 |
        | B2B            |                  1 |
        | BO             |                  1 |
        | BGT            |                  1 |
  
    @ADJUST @ADJUST_REST @ADJUST_REST_49 @ADJUST_REST_PMM @ADJUST_REST_F
    Scenario Outline: En la cabecera del ajuste, no se envía el campo 'Bodega'
      Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
      And preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Bodega
      When envio un request de ajuste al api
      Then se registra en bd la cabecera con estado "F" mensaje ""
      And se registra en bd el detalle con estado "F" mensaje "El campo fulfillment es obligatorio"
  
      Examples: 
        | sistema_origen | cantidad_productos |
        | PMM            |                  1 |
        | B2B            |                  1 |
        | BO             |                  1 |
        | BGT            |                  1 |
  
    @ADJUST @ADJUST_REST @ADJUST_REST_53 @ADJUST_REST_PMM @ADJUST_REST_F
    Scenario Outline: PMM - En la cabecera del ajuste, no se envía el campo 'Tipo Inventario'
      Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
      And preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Tipo de Inventario
      When envio un request de ajuste al api
      Then se registra en bd la cabecera con estado "F" mensaje ""
      And se registra en bd el detalle con estado "F" mensaje "El campo inventoryType es obligatorio"
  
      Examples: 
        | sistema_origen | cantidad_productos |
        | PMM            |                  1 |
        | B2B            |                  1 |
        | BO             |                  1 |
        | BGT            |                  1 |
  
    @ADJUST @ADJUST_REST @ADJUST_REST_57 @ADJUST_REST_BGT @ADJUST_REST_F
    Scenario Outline: PMM - En el detalle del ajuste, no se envía el campo 'Operacion'
      Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
      And preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Operacion
      When envio un request de ajuste al api
      Then se registra en bd la cabecera con estado "F" mensaje ""
      And se registra en bd el detalle con estado "F" mensaje "No se encontro el campo operacion"
  
      Examples: 
        | sistema_origen | cantidad_productos |
        | PMM            |                  1 |
        | B2B            |                  1 |
        | BO             |                  1 |
        | BGT            |                  1 |
  
    @ADJUST @ADJUST_REST @ADJUST_REST_61 @ADJUST_REST_PMM @ADJUST_REST_F
    Scenario Outline: PMM - En la transacción de ajuste, un registro posee el estado Duplicado
      Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
      And preparo la trama de ajuste con "<cantidad_productos>" productos con id detalle duplicado
      When envio un request de ajuste al api
      Then se registra en bd la cabecera con estado "F" mensaje ""
      And se registra en bd el detalle con estado "D" mensaje ""
  
      Examples: 
        | sistema_origen | cantidad_productos |
        | PMM            |                  2 |
        | B2B            |                  2 |
        | BO             |                  2 |
        | BGT            |                  2 |
  
    @ADJUST @ADJUST_REST @ADJUST_REST_65 @ADJUST_REST_PMM @ADJUST_REST_C
    Scenario Outline: PMM - Validar ajuste que de como resultado stock negativo
      Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
      And preparo la trama de ajuste con "<cantidad_productos>" productos con stock 0 con ajuste negativo
      When envio un request de ajuste al api
      Then se registra en bd la cabecera con estado "C" mensaje ""
      And se validan los datos del ajuste en bd
      And se valida en SIC el inventario
  
      Examples: 
        | sistema_origen | cantidad_productos |
        | PMM            |                  1 |
        | B2B            |                  1 |
        | BO             |                  1 |
        | BGT            |                  1 |
  
    @ADJUST @ADJUST_REST @ADJUST_REST_69 @ADJUST_REST_PMM @ADJUST_REST_C
    Scenario Outline: PMM - Validar ajuste de inventario de como resultado 0 de stock
      Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
      And preparo la trama de ajuste con "<cantidad_productos>" productos con stock igual al actual con ajuste negativo
      When envio un request de ajuste al api
      Then se registra en bd la cabecera con estado "C" mensaje ""
      And se validan los datos del ajuste en bd
      And se valida en SIC el inventario
  
      Examples: 
        | sistema_origen | cantidad_productos |
        | PMM            |                  1 |
        | B2B            |                  1 |
        | BO             |                  1 |
        | BGT            |                  1 |
  
    @ADJUST @ADJUST_REST @ADJUST_REST_73 @ADJUST_REST_PMM @ADJUST_REST_F
    Scenario Outline: PMM - En el detalle del ajuste, se envía una bodega que no existe
      Given quiero realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
      And preparo la trama de ajuste con "<cantidad_productos>" productos
      And asigno la bodega "<bodega_no_existe>"
      When envio un request de ajuste al api
      Then se registra en bd la cabecera con estado "F" mensaje ""
      And se registra en bd el detalle con estado "F" mensaje "Bodega no existe"
  
      Examples: 
        | sistema_origen | cantidad_productos | bodega_no_existe |
        | PMM            |                  1 |            29912 |
        | B2B            |                  1 |            11921 |
        | BO             |                  1 |            99182 |
        | BGT            |                  1 |            22133 |
  


  