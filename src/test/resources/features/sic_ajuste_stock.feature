Feature: Mantenedor para ingresar ajuste de inventario
  Como usuario
  Quiero realizar ajustes de inventario a traves del mantenedor SIC

  Background: 
    Given ingreso al sistema sic
    And voy a la opcion Mantenedor para Ingresar Ajuste de Inventario

  @SIC @SIC_MIAI @SIC_MIAI_01
  Scenario Outline: SIC_MIAI_01 - Realizar consulta para un producto con tipo de inventario 'Stock Disponible'
    Given tengo informacion de un producto con tipo de inventario "<tipo_inventario>"
    When realizo la consulta de stock actual
    Then se muestra el stock actual

    Examples: 
      | tipo_inventario |
      |               1 |
      |               2 |

  @SIC @SIC_MIAI @SIC_MIAI_03
  Scenario: SIC_MIAI_03 - Validar campo nuevo stock al seleccionar tipo de actualizacion de stock 'Incrementar'
    Given tengo informacion de un producto con tipo de inventario "1"
    And realizo la consulta de stock actual
    When selecciono tipo de actualizacion "Incrementar"
    And ingreso el stock a ajustar "15"
    Then se actualiza el valor del campo nuevo stock

  @SIC @SIC_MIAI @SIC_MIAI_04
  Scenario: SIC_MIAI_04 - Validar campo nuevo stock al seleccionar tipo de actualizacion de stock 'Disminuir'
    Given tengo informacion de un producto con tipo de inventario "1"
    And realizo la consulta de stock actual
    When selecciono tipo de actualizacion "Disminuir"
    And ingreso el stock a ajustar "12"
    Then se actualiza el valor del campo nuevo stock

  @SIC @SIC_MIAI @SIC_MIAI_05
  Scenario: SIC_MIAI_05 - Validar campo nuevo stock al seleccionar tipo de actualizacion de stock 'Reemplazar'
    Given tengo informacion de un producto con tipo de inventario "1"
    And realizo la consulta de stock actual
    When selecciono tipo de actualizacion "Reemplazar"
    And ingreso el stock a ajustar "60"
    Then se actualiza el valor del campo nuevo stock

  @SIC @SIC_MIAI @SIC_MIAI_06 @review
  Scenario: SIC_MIAI_06 - Validar el combo Bodegas exista y sea obligatorio
    When ingreso codigo venta "2000000000001"
    And selecciono tipo de inventario "1"
    And doy click a consultar
    Then se muestra mensaje flotante "Debe seleccionar una bodega para continuar"

  @SIC @SIC_MIAI @SIC_MIAI_07
  Scenario: SIC_MIAI_07 - Validar el campo codigo venta sea obligatorio
    When selecciono tipo de inventario "1"
    And selecciono una bodega al azar
    And doy click a consultar
    Then se muestra mensaje "Complete todos los campos requeridos (*)"

  @SIC @SIC_MIAI @SIC_MIAI_09
  Scenario: SIC_MIAI_09 - Validar existencia de registro de stock del producto en SIC
    When tengo informacion de un producto sin stock
    And selecciono una bodega al azar
    And realizo la consulta de stock actual SKU fijo
    Then se muestra mensaje SKU "No se encontró stock para el cod de venta: 2000300111177"

  @SIC @SIC_MIAI @SIC_MIAI_10
  Scenario: SIC_MIAI_10 - Validar el campo Ajuste exista y sea obligatorio
    When ingreso codigo venta "2000000000001"
    And selecciono tipo de inventario "1"
    And selecciono una bodega al azar
    And completo el campo observacion
    And no completo el campo ajuste
    Then el boton actualizar esta deshabilitado

  @SIC @SIC_MIAI @SIC_MIAI_11
  Scenario: SIC_MIAI_11 - Realizar incremento de stock de un producto con stock negativo
    Given tengo informacion de un producto con stock negativo
    And realizo la consulta de stock actual
    When selecciono tipo de actualizacion "Incrementar"
    And ingreso el stock a ajustar "15"
    Then se actualiza el valor del campo nuevo stock

  @SIC @SIC_MIAI @SIC_MIAI_12
  Scenario: SIC_MIAI_12 - Realizar incremento de stock de un producto con stock cero
    Given tengo informacion de un producto con stock cero
    And realizo la consulta de stock actual
    When selecciono tipo de actualizacion "Incrementar"
    And ingreso el stock a ajustar "15"
    Then se actualiza el valor del campo nuevo stock

  @SIC @SIC_MIAI @SIC_MIAI_13
  Scenario: SIC_MIAI_13 - Realizar reemplazo de stock de un producto con stock positivo
    Given tengo informacion de un producto con stock positivo
    And realizo la consulta de stock actual
    When selecciono tipo de actualizacion "Reemplazar"
    And ingreso el stock a ajustar "60"
    Then se actualiza el valor del campo nuevo stock

  @SIC @SIC_MIAI @SIC_MIAI_14
  Scenario: SIC_MIAI_14 - Validar el boton Limpiar
    Given tengo informacion de un producto con stock positivo
    And realizo la consulta de stock actual
    And selecciono tipo de actualizacion "Reemplazar"
    And ingreso el stock a ajustar "60"
    When doy click a limpiar
    Then los campos vuelven a su valor inicial
  