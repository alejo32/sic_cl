Feature: Carga Masiva de Stock Protegido

  Background: 
    Given ingreso al sistema sic
    And voy a la opcion Carga Masiva de Stock Seguridad

  @SIC @SIC_CMSP @SIC_CMSP_01
  Scenario: SIC_CMSP_01 - Realizar carga sin haber seleccionado un archivo
    When doy click a realizar carga
    Then se valida mensaje de error "El formato a cargar debe ser csv."

  @SIC @SIC_CMSP @SIC_CMSP_02
  Scenario: SIC_CMSP_02 - Validar que se muestre el nombre del archivo cargado en el campo Archivo
    Given tengo informacion de stock protegido
      | fec_ini             | fec_fin             | cod_venta     | bodega | nivel_1 | nivel_2 | nivel_3 | nivel_4 | nivel_5 |
      | 2020-02-04 10:00:00 | 2020-02-06 10:00:00 | 2013500230004 |  10012 |      10 |      20 |      30 |      40 |      50 |
    And genero un archivo de carga masiva de stock protegido
    When cargo el archivo de carga masiva de stock protegido
    Then se valida que se visualice el nombre del archivo seleccionado

  @SIC @SIC_CMSP @SIC_CMSP_03
  Scenario Outline: SIC_CMSP_03 - Realizar una carga masiva con un archivo con extencion no permitida
    Given tengo informacion de stock protegido
      | fec_ini             | fec_fin             | cod_venta     | bodega | nivel_1 | nivel_2 | nivel_3 | nivel_4 | nivel_5 |
      | 2020-02-04 10:00:00 | 2020-02-06 10:00:00 | 2013500230004 |  10012 |      10 |      20 |      30 |      40 |      50 |
    And genero un archivo de carga masiva de stock protegido con extension "<extension>"
    And cargo el archivo de carga masiva de stock protegido
    And doy click a realizar carga
    Then se valida mensaje de error "El formato a cargar debe ser csv."

    Examples: 
      | extension |
      | .txt      |
      | .zip      |
      | .tar.gz   |
      | .xlsx     |
      | .tar      |

  @SIC @SIC_CMSP @SIC_CMSP_04
  Scenario: SIC_CMSP_04 - Validar que en el formulario el campo tipo de producto tenga el valor por defecto '1-Ripley'
    Then se valida que por defecto tenga el valor "1-Ripley"

  @SIC @SIC_CMSP @SIC_CMSP_05
  Scenario: SIC_CMSP_05 - Realizar una carga con un archivo vacio
    Given genero un archivo de carga masiva de stock protegido vacio
    When selecciono la bodega "10012"
    And cargo el archivo de carga masiva de stock protegido
    And doy click a realizar carga
    Then se muestra en la cabecera el detalle "Archivo sin ningún registro."

  @SIC @SIC_CMSP @SIC_CMSP_06
  Scenario: SIC_CMSP_06 - Validar que el formulario contenga el boton examinar
    Then se muestra el boton examinar

  @SIC @SIC_CMSP @SIC_CMSP_07
  Scenario: SIC_CMSP_07 - Realizar una carga masiva se obtienen errores a nivel de detalle
    Given tengo informacion de stock protegido
      | fec_ini             | fec_fin             | cod_venta     | bodega | nivel_1 | nivel_2 | nivel_3 | nivel_4 | nivel_5 | Casos                             |
      | 2021-02-04 10:00:00 |                     | 2013500230004 |  10012 |      10 |      20 |      30 |      40 |      50 | Caso01 - Sin fecha fin            |
      |                     | 2021-02-04 10:00:00 | 2011834730009 |  10012 |      10 |      20 |      30 |      40 |      50 | Caso02 - Sin fecha inicio         |
      | 2021-02-04 10:00:00 | 2021-02-07 10:00:00 | 2001011570009 |        |      10 |      20 |      30 |      40 |      50 | Caso03 - Sin bodega               |
      | 2021-02-04 10:00:00 | 2021-02-07 10:00:00 | 2001011610002 |  10012 |         |         |         |         |         | Caso04 - Sin niveles              |
      | 2021-02-04 10:00:00 | 2021-02-06 10:00:00 |               |  10012 |      10 |      20 |      30 |      40 |      50 | Caso05 - Sin codVenta             |
      | 2019-01-04 10:00:00 | 2019-01-07 10:00:00 | 2001012290005 |  10012 |      10 |      20 |      30 |      40 |      50 | Caso06 - Fechas pasadas           |
      | 2021-02-04 10:00:00 | 2021-02-03 10:00:00 | 2001012320009 |  10012 |      10 |      20 |      30 |      40 |      50 | Caso07 - Fecha Inicio mayor a fin |
      | 2021-02-04 10:00:00 | 2021-02-06 10:00:00 | 2001013250008 |  10012 | ABCC    |      20 |      30 |      40 |      50 | Caso08 - Nivel1 valor no numerico |
    And genero un archivo de carga masiva de stock protegido
    When selecciono la bodega "10012"
    And cargo el archivo de carga masiva de stock protegido
    And doy click a realizar carga
    Then se valida la carga en estado "Fallido"
    And se muestra el detalle de los errores
      | PartNumber    | Estado  | Detalle                                                                           | Casos                             |
      | 2013500230004 | Fallido | Faltan campos obligatorios. ExpiryDate                                            | Caso01 - Sin fecha fin            |
      | 2011834730009 | Fallido | Faltan campos obligatorios. EffectiveDate                                         | Caso02 - Sin fecha inicio         |
      | 2001011570009 | Fallido | Faltan campos obligatorios. LocationId                                            | Caso03 - Sin bodega               |
      | 2001011610002 | Fallido | Faltan campos obligatorios. InventoryProtection1, InventoryProtection2, InventoryProtection3, InventoryProtection4, InventoryProtection5 	| Caso04 - Sin niveles              |
	  | 2001012290005 | Fallido | Las fechas de inicio y fin de vigencia deben ser iguales o mayores al día de hoy. | Caso06 - Fechas pasadas           |
	  | 2001012320009 | Fallido | La fecha Fin Vigencia debe ser mayor a la fecha Inicio Vigencia.                  | Caso07 - Fecha Inicio mayor a fin |
	  | 2001013250008 | Fallido | El valor ingresado no es un número LEVEL_PROTECTION1                              | Caso08 - Nivel1 valor no numerico |    

  @SIC @SIC_CMSP @SIC_CMSP_08
  Scenario: SIC_CMSP_08 - La cabecera del archivo no tiene el formato definido.
    Given tengo informacion de stock protegido
      | fec_ini             | fec_fin             | cod_venta     | bodega | nivel_1 | nivel_2 | nivel_3 | nivel_4 | nivel_5 |
      | 2020-02-04 10:00:00 | 2020-02-06 10:00:00 | 2013500230004 |  10012 |      10 |      20 |      30 |      40 |      50 |
    And genero un archivo de carga masiva de stock protegido con nombres de cabecera incorrectos
    When selecciono la bodega "10012"
    And cargo el archivo de carga masiva de stock protegido
    And doy click a realizar carga
    Then se muestra en la cabecera el detalle "La cabecera del archivo no tiene el formato definido."

  @SIC @SIC_CMSP @SIC_CMSP_09
  Scenario: SIC_CMSP_09 - Realizar una carga masiva en estado Fallido
    Given tengo informacion de stock protegido
      | fec_ini             | fec_fin             | cod_venta     | bodega | nivel_1 | nivel_2 | nivel_3 | nivel_4 | nivel_5 | Casos                             |
      |                     |                     | 2024752220002 |  10012 |     -10 |      20 |      30 |      40 |      50 | Caso01 - Carga Permanente         |
      | 2021-02-11 10:00:00 | 2021-02-14 10:00:00 | 2024752220002 |  10012 |       2 |      10 |      10 |      10 |      10 | Caso02 - Carga con fechas         |
      | 2021-02-10 10:00:00 | 2021-02-12 10:00:00 | 2024752220002 |  10012 |       2 |      10 |      10 |      10 |      10 | Caso03 - Traslape                 |
      | 2021-02-12 10:00:00 | 2021-02-13 10:00:00 | 2024752220002 |  10012 |       2 |      10 |      10 |      10 |      10 | Caso04 - Traslape incluido        |
      | 2021-02-10 10:00:00 | 2021-02-11 10:00:00 | 2024752220002 |  10012 |       2 |      10 |      10 |      10 |      10 | Caso05 - Traslape limite inferior |
      | 2021-02-14 10:00:00 | 2021-02-15 10:00:00 | 2024752220002 |  10012 |       2 |      10 |      10 |      10 |      10 | Caso06 - Traslape limite superior |
    And genero un archivo de carga masiva de stock protegido
    When selecciono la bodega "10012"
    And cargo el archivo de carga masiva de stock protegido
    And doy click a realizar carga
    Then se valida la carga en estado "Fallido"

  @SIC @SIC_CMSP @SIC_CMSP_10
  Scenario: SIC_CMSP_10 - Realizar una carga masiva en estado Completado
    Given tengo informacion de stock protegido
      | fec_ini             | fec_fin             | cod_venta     | bodega | nivel_1 | nivel_2 | nivel_3 | nivel_4 | nivel_5 | Casos                    |
      | 2021-03-01 10:00:00 | 2021-03-05 10:00:00 | 2000303893949 |  10012 |       2 |       5 |         |         |         | Caso01 - Producto sin SP |
      | 2021-03-01 10:00:00 | 2021-03-05 10:00:00 | 2000303893949 |  10012 |       2 |      10 |      10 |      10 |      10 | Caso02 - Producto con SP |
    And genero un archivo de carga masiva de stock protegido
    When selecciono la bodega "10012"
    And cargo el archivo de carga masiva de stock protegido
    And doy click a realizar carga
    Then se valida la carga en estado "Completado"
    
    
  