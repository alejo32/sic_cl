Feature: Incremental de Productos

  @SIC @SIC_INP @SIC_INP_01
  Scenario: SIC_INP_01 - Se envia una trama de 90 productos, la información es la misma que la almacenada en BD.
    Given tengo informacion de jerarquia de "90" productos
    And preparo la trama de incremental de productos
    When envio un request de incremental de productos via SQS
    Then se valida en bd que el incremental de productos no ha realizado cambios

  @SIC @SIC_INP @SIC_INP_02
  Scenario: SIC_INP_02 - Se envia una trama de 5 productos, la información es diferente a la almacenada en BD.
    Given tengo informacion de jerarquia de "5" productos
    And agrego el sufijo "-AUT" a la descripcion del producto
    And preparo la trama de incremental de productos
    When envio un request de incremental de productos via SQS
    Then se valida en bd los cambios realizados por el incremental del productos

  @SIC @SIC_INP @SIC_INP_03
  Scenario: SIC_INP_03 - Se envian 2 tramas de 5 productos, la información es diferente a la almacenada en BD.
    Given tengo "2" tramas de "5" productos con el sufijo "-AUT" a la descripcion del producto
    When envio varios request de incremental de productos via SQS
    Then se valida en bd los cambios realizados por el incremental del productos

  @SIC @SIC_INP @SIC_INP_04
  Scenario: SIC_INP_04 - Se envia una trama de 2 productos con el campo source errado
    Given tengo informacion de jerarquia de "2" productos
    And agrego el sufijo "-AUT" a la descripcion del producto
    And preparo la trama de incremental de productos con el campo source erroneo
    When envio un request de incremental de productos via SQS
    Then se valida en bd que el incremental de productos no ha realizado cambios

  @SIC @SIC_INP @SIC_INP_05
  Scenario: SIC_INP_05 - Se envia una trama de 1 productos con el campo Departamento errado
    Given tengo informacion de jerarquia de "1" productos
    And modifico el departamento al valor "F819"
    And preparo la trama de incremental de productos
    When envio un request de incremental de productos via SQS
    Then se valida en bd los cambios realizados por el incremental del productos

  @SIC @SIC_INP @SIC_INP_06
  Scenario: SIC_INP_06 - Se envia una trama de 1 productos con el campo ItemId errado
    Given tengo informacion de jerarquia de "1" productos
    And modifico el itemId al valor "9991199"
    And preparo la trama de incremental de productos
    When envio un request de incremental de productos via SQS
    Then se valida en bd que el incremental de productos no ha realizado cambios

  @SIC @SIC_INP @SIC_INP_07
  Scenario: SIC_INP_07 - Se envia una trama de 1 productos con el campo Style errado
    Given tengo informacion de jerarquia de "1" productos
    And modifico el style al valor "9999991999999"
    And preparo la trama de incremental de productos
    When envio un request de incremental de productos via SQS
    Then se valida en bd los cambios realizados por el incremental del productos

  @SIC @SIC_INP @SIC_INP_08
  Scenario: SIC_INP_08 - Se envia una trama de 1 productos con el campo SubLinea errado
    Given tengo informacion de jerarquia de "1" productos
    And modifico la sublinea al valor "SL99199"
    And preparo la trama de incremental de productos
    When envio un request de incremental de productos via SQS
    Then se valida en bd los cambios realizados por el incremental del productos

  @SIC @SIC_INP @SIC_INP_09
  Scenario: SIC_INP_09 - Se envia una trama de 1 productos con el campo Linea errado
    Given tengo informacion de jerarquia de "1" productos
    And modifico la linea al valor "9999"
    And preparo la trama de incremental de productos
    When envio un request de incremental de productos via SQS
    Then se valida en bd los cambios realizados por el incremental del productos

  @SIC @SIC_INP @SIC_INP_10
  Scenario: SIC_INP_10 - Actualizar la informacion de departamento de 10 productos
    Given tengo informacion de jerarquia de "10" productos
    And modifico el departamento al valor "D111"
    And preparo la trama de incremental de productos
    When envio un request de incremental de productos via SQS
    Then se valida en bd los cambios realizados por el incremental del productos

  @SIC @SIC_INP @SIC_INP_11
  Scenario: SIC_INP_11 - Actualizar la informacion de linea de 10 productos
    Given tengo informacion de jerarquia de "10" productos
    And modifico la linea al valor "000175"
    And preparo la trama de incremental de productos
    When envio un request de incremental de productos via SQS
    Then se valida en bd los cambios realizados por el incremental del productos

  @SIC @SIC_INP @SIC_INP_12
  Scenario: SIC_INP_12 - Actualizar la informacion de sublinea de 10 productos
    Given tengo informacion de jerarquia de "10" productos
    And modifico la sublinea al valor "S013692"
    And preparo la trama de incremental de productos
    When envio un request de incremental de productos via SQS
    Then se valida en bd los cambios realizados por el incremental del productos
    
  