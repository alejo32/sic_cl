Feature: Ajuste delta vía REST
  Como usuario
  Quiero realizar ajustes de inventario con la finalidad de cubrir todas las casuisticas que puedan existir

  @SDELTA @SDELTA_REST @SDELTA_REST_01 @SDELTA_REST_PMM @SDELTA_REST_C
  Scenario Outline: Realizar ajuste delta de 1 producto
    Given quiero realizar un ajuste delta via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos
    When envio un request de ajuste al api
    Then se registra en bd la cabecera con estado "C" mensaje ""
    And se validan los datos del ajuste en bd
    And se valida en SIC el inventario

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |


  @SDELTA @SDELTA_REST @SDELTA_REST_09 @SDELTA_REST_PMM @SDELTA_REST_C
  Scenario Outline: Realizar ajuste delta de un producto con stock 0
    Given quiero realizar un ajuste delta via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos con stock 0 con ajuste negativo
    When envio un request de ajuste al api
    Then se registra en bd la cabecera con estado "C" mensaje ""
    And se validan los datos del ajuste en bd
    And se valida en SIC el inventario

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |

  @SDELTA @SDELTA_REST @SDELTA_REST_13 @SDELTA_REST_PMM @SDELTA_REST_F
  Scenario Outline: En la cabecera del ajuste, no se envía el campo 'Tipo Inventario'
    Given quiero realizar un ajuste delta via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Tipo de Inventario
    When envio un request de ajuste al api
    Then se registra en bd la cabecera con estado "F" mensaje ""
    And se registra en bd el detalle con estado "F" mensaje "El campo inventoryType es obligatorio"

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |

  @SDELTA @SDELTA_REST @SDELTA_REST_17 @SDELTA_REST_PMM @SDELTA_REST_C
  Scenario Outline: Realizar ajuste delta de 5 productos sin stock
    Given quiero realizar un ajuste delta via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos sin stock para la bodega "10012"
    When envio un request de ajuste al api
    Then se registra en bd la cabecera con estado "C" mensaje ""
    And se validan los datos del ajuste en bd
    And se valida en SIC el inventario

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  5 |
      | B2B            |                  5 |
      | BO             |                  5 |
      | BGT            |                  5 |

  @SDELTA @SDELTA_REST @SDELTA_REST_21 @SDELTA_REST_PMM @SDELTA_REST_F
  Scenario Outline: En la transacción de ajuste, se envía un producto que no existe
    Given quiero realizar un ajuste delta via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos que no existen
    When envio un request de ajuste al api
    Then se registra en bd la cabecera con estado "F" mensaje ""
    And se registra en bd el detalle con estado "F" mensaje "SKU no existe."

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  2 |
      | B2B            |                  2 |
      | BO             |                  2 |
      | BGT            |                  2 |

  @SDELTA @SDELTA_REST @SDELTA_REST_25 @SDELTA_REST_PMM @SDELTA_REST_F
  Scenario Outline: En la cabecera del ajuste, no se envía el campo 'Tipo de Movimiento'
    Given quiero realizar un ajuste delta via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Tipo de Movimiento
    When envio un request de ajuste al api
    Then validar que no se registre en la bd

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |

  @SDELTA @SDELTA_REST @SDELTA_REST_29 @SDELTA_REST_PMM @SDELTA_REST_F
  Scenario Outline: En la cabecera del ajuste, no se envía el campo 'Usuario'
    Given quiero realizar un ajuste delta via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Usuario
    When envio un request de ajuste al api
    Then se registra en bd la cabecera con estado "F" mensaje "El campo usuario no puede estar vacio."
    And no se registra en bd el detalle

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |

  @SDELTA @SDELTA_REST @SDELTA_REST_33 @SDELTA_REST_PMM @SDELTA_REST_F
  Scenario Outline: En la cabecera del ajuste, no se envía el campo 'Sistema Origen'
    Given quiero realizar un ajuste delta via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Sistema Origen
    When envio un request de ajuste al api
    Then validar que no se registre en la bd

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |

  @SDELTA @SDELTA_REST @SDELTA_REST_37 @SDELTA_REST_PMM @SDELTA_REST_F
  Scenario Outline: En la cabecera del ajuste, no se envía el campo 'Operacion'
    Given quiero realizar un ajuste delta via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Operacion
    When envio un request de ajuste al api
    Then se registra en bd la cabecera con estado "C" mensaje ""
    And se validan los datos del ajuste en bd
    And se valida en SIC el inventario

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |

  @SDELTA @SDELTA_REST @SDELTA_REST_41 @SDELTA_REST_PMM @SDELTA_REST_F
  Scenario Outline: En la cabecera del ajuste, no se envía el campo 'Tipo Documento'
    Given quiero realizar un ajuste delta via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Documento de Transaccion
    When envio un request de ajuste al api
    Then se registra en bd la cabecera con estado "F" mensaje ""
    And se registra en bd el detalle con estado "F" mensaje "No se encontro el campo documento de transaccion"

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |

  @SDELTA @SDELTA_REST @SDELTA_REST_45 @SDELTA_REST_PMM @SDELTA_REST_F
  Scenario Outline: En la cabecera del ajuste, no se envía el campo 'Bodega'
    Given quiero realizar un ajuste delta via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Bodega
    When envio un request de ajuste al api
    Then se registra en bd la cabecera con estado "F" mensaje ""
    And se registra en bd el detalle con estado "F" mensaje "El campo fulfillment es obligatorio"

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |

  @SDELTA @SDELTA_REST @SDELTA_REST_49 @SDELTA_REST_PMM @SDELTA_REST_C
  Scenario Outline: Realizar ajuste delta de 200 producto
    Given quiero realizar un ajuste delta via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos
    When envio un request de ajuste al api
    Then se registra en bd la cabecera con estado "C" mensaje ""
    And se validan los datos del ajuste en bd

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                200 |
      | B2B            |                200 |
      | BO             |                200 |
      | BGT            |                200 |

  @SDELTA @SDELTA_REST @SDELTA_REST_53 @SDELTA_REST_PMM @SDELTA_REST_C
  Scenario Outline: En el del ajuste, no se envía el campo 'SKU'
    Given quiero realizar un ajuste delta via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos
    And borro el valor de el campo producto
    When envio un request de ajuste al api
    Then se registra en bd la cabecera con estado "F" mensaje ""
    And se registra en bd el detalle con estado "F" mensaje "El campo sku es obligatorio"

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |

  @SDELTA @SDELTA_REST @SDELTA_REST_57 @SDELTA_REST_BGT @SDELTA_REST_F
  Scenario Outline: En el detalle del ajuste, se envía una bodega que no existe
    Given quiero realizar un ajuste delta via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos
    And asigno la bodega "<bodega_no_existe>"
    When envio un request de ajuste al api
    Then se registra en bd la cabecera con estado "F" mensaje ""
    And se registra en bd el detalle con estado "F" mensaje "Bodega no existe"

    Examples: 
      | sistema_origen | cantidad_productos | bodega_no_existe |
      | PMM            |                  1 |            29912 |
      | B2B            |                  1 |            11921 |
      | BO             |                  1 |            99182 |
      | BGT            |                  1 |            22133 |

  @SDELTA @SDELTA_REST @SDELTA_REST_61 @SDELTA_REST_PMM @SDELTA_REST_F
  Scenario Outline: Se envía la misma cantidad para un producto
    Given quiero realizar un ajuste delta via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos
    And asigno stock igual al actual
    When envio un request de ajuste al api
    Then se registra en bd la cabecera con estado "F" mensaje ""
    And no se registra en bd el detalle

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |

  @SDELTA @SDELTA_REST @SDELTA_REST_65 @SDELTA_REST_BGT @SDELTA_REST_CF
  Scenario Outline: Se envía una transacción y su estado cambia a 'Completado con Errores'
    Given quiero realizar un ajuste delta via REST para el sistema "<sistema_origen>"
    And preparo la trama de ajuste con "<cantidad_productos>" productos con error en un registro
    When envio un request de ajuste al api
    Then se registra en bd la cabecera con estado "CF" mensaje ""

    Examples: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |
      | B2B            |                  1 |
      | BO             |                  1 |
      | BGT            |                  1 |
  