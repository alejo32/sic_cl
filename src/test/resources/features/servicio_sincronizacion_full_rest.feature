Feature: Sincronizacion Full
  Lista de casos de prueba para probar el correcto funcionamiento de Sincronizacion Full via REST

  @SFULL @SFULL_01 @SFULL_PMM @SFULL_F
  Scenario Outline: PMM - Enviar sfull, los archivos comprimidos tienen información y formato diferente a csv
    Given quiero realizar una sincronizacion full via REST para el sistema "<sistema_origen>"
    And tengo informacion de "<cantidad_productos>" productos
    And tengo informacion de "<cantidad_bodegas>" bodegas
    When genero un archivo por bodega con extension incorrecta
    And comprimo los archivos en formato ".zip"
    And cargo el archivo sfull en el servidor sftp
    And envio un request de sfull al api
    Then se registra en bd la cabecera con estado "F" mensaje "El archivo se encuentra vacío"

    Examples: 
      | sistema_origen | cantidad_productos | cantidad_bodegas |
      | PMM            |                 25 |                4 |
      | B2B            |                 25 |                4 |
      | BGT            |                 25 |                4 |

  @SFULL @SFULL_04 @SFULL_PMM @SFULL_F
  Scenario Outline: PMM - Se envia un archivo SFULL con extension incorrecta
    Given quiero realizar una sincronizacion full via REST para el sistema "<sistema_origen>"
    When genero un archivo sfull con extension "<extension>"
    And cargo el archivo sfull en el servidor sftp
    And envio un request de sfull al api
    Then se obtiene un response con mensaje "The filename must contain .csv|.tar|.gz|.zip"

    Examples: 
      | sistema_origen | extension |
      | PMM            | .txt      |
      | B2B            | .rar      |
      | BGT            | .xlsx     |

  @SFULL @SFULL_07 @SFULL_PMM @SFULL_F
  Scenario Outline: PMM - Se envia un archivo SFULL sin datos en la columna Stock
    Given quiero realizar una sincronizacion full via REST para el sistema "<sistema_origen>"
    And genero un archivo sfull con la columna stock vacia
    When cargo el archivo sfull en el servidor sftp
    And envio un request de sfull al api
    Then se registra en bd la cabecera con estado "F" mensaje ""
    And se registra en bd el detalle con estado "F" mensaje "El campo Stock debe ser numérico"

    Examples: 
      | sistema_origen |
      | PMM            |
      | B2B            |
      | BGT            |

  @SFULL @SFULL_10 @SFULL_PMM @SFULL_C
  Scenario Outline: PMM - Se envia un archivo SFULL sin datos en la columna Seller
    Given quiero realizar una sincronizacion full via REST para el sistema "<sistema_origen>"
    And genero un archivo sfull con la columna seller vacia
    When cargo el archivo sfull en el servidor sftp
    And envio un request de sfull al api
    Then se registra en bd la cabecera con estado "C" mensaje ""
    And se registra en bd el detalle con estado "C" mensaje ""

    Examples: 
      | sistema_origen |
      | PMM            |
      | B2B            |
      | BGT            |

  @SFULL @SFULL_13 @SFULL_PMM @SFULL_F
  Scenario Outline: PMM - Se envia un SFULL sin archivo
    Given quiero realizar una sincronizacion full via REST para el sistema "<sistema_origen>"
    When envio un request de sfull al api
    Then se registra en bd la cabecera con estado "F" mensaje "no se encontró en la ruta proporcionada..."
    And no se registra en bd el detalle

    Examples: 
      | sistema_origen |
      | PMM            |
      | B2B            |
      | BGT            |

  @SFULL @SFULL_16 @SFULL_PMM @SFULL_F
  Scenario Outline: PMM - Se envía la misma cantidad para un producto
    Given quiero realizar una sincronizacion full via REST para el sistema "<sistema_origen>"
    And obtengo "<cantidad_registros>" registros con stock igual al actual
    And genero un archivo sfull con los datos obtenidos
    When cargo el archivo sfull en el servidor sftp
    And envio un request de sfull al api
    Then se registra en bd la cabecera con estado "F" mensaje ""
    And no se registra en bd el detalle

    Examples: 
      | sistema_origen | cantidad_registros |
      | PMM            |                  5 |
      | B2B            |                  5 |
      | BGT            |                  5 |

  @SFULL @SFULL_19 @SFULL_PMM @SFULL_C
  Scenario Outline: PMM - Se envía un SFULL de 65 millones de productos
    Given quiero realizar una sincronizacion full via REST para el sistema "<sistema_origen>"
    And tengo informacion de "<cantidad_productos>" productos
    And tengo informacion de "<cantidad_bodegas>" bodegas
    When genero un archivo csv por bodega
    And comprimo los archivos en formato ".zip"
    And cargo el archivo sfull en el servidor sftp
    And envio un request de sfull al api
    Then se registra en bd la cabecera con estado "C" mensaje ""
    And se registra en bd el detalle con estado "C" mensaje ""

    Examples: 
      | sistema_origen | cantidad_productos | cantidad_bodegas |
      | PMM            |             325000 |              200 |
      | B2B            |             325000 |              200 |
      | BGT            |             325000 |              200 |

  @SFULL @SFULL_22 @SFULL_PMM @SFULL_C
  Scenario Outline: BGT - Sincronización de 100 productos
    Given quiero realizar una sincronizacion full via REST para el sistema "<sistema_origen>"
    And tengo informacion de "<cantidad_productos>" productos
    And tengo informacion de "<cantidad_bodegas>" bodegas
    When genero un archivo csv por bodega
    And comprimo los archivos en formato ".zip"
    And cargo el archivo sfull en el servidor sftp
    And envio un request de sfull al api
    Then se registra en bd la cabecera con estado "C" mensaje ""
    And se registra en bd el detalle con estado "C" mensaje ""

    Examples: 
      | sistema_origen | cantidad_productos | cantidad_bodegas |
      | PMM            |                 25 |                4 |
      | B2B            |                 25 |                4 |
      | BGT            |                 25 |                4 |

  @SFULL @SFULL_25 @SFULL_PMM @SFULL_CF
  Scenario Outline: PMM - Se envía una transacción y su estado cambia a 'Completado con Errores'
    Given quiero realizar una sincronizacion full via REST para el sistema "<sistema_origen>"
    And obtengo "<cantidad_registros>" registros con stock igual y diferente
    And genero un archivo sfull con los datos obtenidos
    When cargo el archivo sfull en el servidor sftp
    And envio un request de sfull al api
    Then se registra en bd la cabecera con estado "CF" mensaje ""
    And se registra en bd el detalle con estado "C" mensaje ""

    Examples: 
      | sistema_origen | cantidad_registros |
      | PMM            |                 10 |
      | B2B            |                 10 |
      | BGT            |                 10 |

  @SFULL @SFULL_28 @SFULL_PMM @SFULL_F
  Scenario Outline: PMM - Se envía una transacción con un producto que no existe
    Given quiero realizar una sincronizacion full via REST para el sistema "<sistema_origen>"
    And tengo informacion de "<cantidad_productos>" productos que no existe
    And tengo informacion de "<cantidad_bodegas>" bodegas
    When genero un archivo csv por bodega
    And comprimo los archivos en formato ".zip"
    And cargo el archivo sfull en el servidor sftp
    And envio un request de sfull al api
    Then se registra en bd la cabecera con estado "F" mensaje ""
    And se registra en bd el detalle con estado "C" mensaje "SKU no existe."

    Examples: 
      | sistema_origen | cantidad_productos | cantidad_bodegas |
      | PMM            |                 10 |                1 |
      | B2B            |                 10 |                1 |
      | BGT            |                 10 |                1 |

  @SFULL @SFULL_31 @SFULL_PMM @SFULL_F
  Scenario Outline: PMM - Se envía una transacción con una bodega que no existe
    Given quiero realizar una sincronizacion full via REST para el sistema "<sistema_origen>"
    And tengo informacion de "<cantidad_productos>" productos
    And tengo informacion de "<cantidad_bodegas>" bodega que no existe
    When genero un archivo csv por bodega
    And comprimo los archivos en formato ".zip"
    And cargo el archivo sfull en el servidor sftp
    And envio un request de sfull al api
    Then se registra en bd la cabecera con estado "F" mensaje ""
    And se registra en bd el detalle con estado "C" mensaje "bodega no existe."

    Examples: 
      | sistema_origen | cantidad_productos | cantidad_bodegas |
      | PMM            |                 10 |                1 |
      | B2B            |                 10 |                1 |
      | BGT            |                 10 |                1 |

  @SFULL @SFULL_34 @SFULL_PMM @SFULL_C
  Scenario Outline: PMM - En la cabecera de la trama, no se envía el campo 'Usuario'
    Given quiero realizar una sincronizacion full via REST para el sistema "<sistema_origen>"
    And tengo informacion de "<cantidad_productos>" productos
    And tengo informacion de "<cantidad_bodegas>" bodegas
    When genero un archivo csv por bodega
    And comprimo los archivos en formato ".zip"
    And cargo el archivo sfull en el servidor sftp
    And envio un request de sfull al api sin datos en la columna usuario
    Then se obtiene un response con mensaje de error "El campo usuario no puede estar vacio."

    Examples: 
      | sistema_origen | cantidad_productos | cantidad_bodegas |
      | PMM            |                  2 |                2 |
      | B2B            |                  2 |                2 |
      | BGT            |                  2 |                2 |

  @SFULL @SFULL_37 @SFULL_PMM @SFULL_C
  Scenario Outline: PMM - Se envia un archivo SFULL sin datos en la columna Sku
    Given quiero realizar una sincronizacion full via REST para el sistema "<sistema_origen>"
    And genero un archivo sfull con la columna sku vacia
    When cargo el archivo sfull en el servidor sftp
    And envio un request de sfull al api
    Then se registra en bd la cabecera con estado "F" mensaje ""
    And se registra en bd el detalle con estado "F" mensaje "SKU no existe."

    Examples: 
      | sistema_origen |
      | PMM            |
      | B2B            |
      | BGT            |

  @SFULL @SFULL_40 @SFULL_PMM @SFULL_C
  Scenario Outline: PMM - El archivo tiene el nombre correcto pero esta vacio
    Given quiero realizar una sincronizacion full via REST para el sistema "<sistema_origen>"
    And genero un archivo sfull sin datos
    When cargo el archivo sfull en el servidor sftp
    And envio un request de sfull al api
    Then se registra en bd la cabecera con estado "F" mensaje "El archivo se encuentra vacío"

    Examples: 
      | sistema_origen |
      | PMM            |
      | B2B            |
      | BGT            |
  