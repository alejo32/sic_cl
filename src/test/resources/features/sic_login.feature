Feature: SIC - Inicio de Sesion

@SIC @SIC_INI @SIC_INI_01
Scenario: SIC - Validar que al momento de ingresar al sistema exitosamente se muestre en la pantalla Dashboard
	Given ingreso al sistema sic con perfil administrador
    Then se muestra la pantalla dashboard
    
@SIC @SIC_INI @SIC_INI_02
Scenario: SIC - Validar que al hacer clic sobre el nombre de usuario muestre la opción de cerrar cesión y ejecute la acción retornando a la pantalla del login
    Given ingreso al sistema sic con perfil administrador
    And cierro sesion
    Then se muestra la pantalla login

@SIC @SIC_INI @SIC_INI_03
Scenario: SIC - Validar que cuando no se ingrese la contraseña en el login muestre un mensaje de validación
	Given ingreso al login
	When ingreso usuario "USERADM2"
	And doy click a iniciar sesion
	Then muestra mensaje de clave "Contraseña es un campo requerido"

@SIC @SIC_INI @SIC_INI_04
Scenario: SIC - Validar que cuando se ingrese una contraseña incorrecta el sistema muestre un mensaje de error
	Given ingreso al login
	When ingreso usuario "USERADM2"
	And ingreso password "111111"
	And doy click a iniciar sesion
	Then muestra mensaje de validacion "Parece que tu nombre de usuario, contraseña o país no coinciden. Inténtalo de nuevo."

@SIC @SIC_INI @SIC_INI_05
Scenario: SIC - Verificar que cuando no se ingrese el usuario en el login muestre un mensaje de validación
	Given ingreso al login
	When ingreso password "111111"
	And doy click a iniciar sesion
	Then muestra mensaje de usuario "Usuario es un campo requerido"

@SIC @SIC_INI @SIC_INI_06
Scenario: SIC - Verificar la existencia de los campos nombre de usuario, password y botón 'Iniciar Sesión'
	When ingreso al login
	Then verifico que existe el campo usuario
	And verifico que existe el campo password
	And verifico que existe el boton iniciar sesion

@SIC @SIC_INI @SIC_INI_07
Scenario: SIC - Verificar mensaje de expiro de sesion luego de 30 minutos
	When ingreso al sistema sic con perfil administrador
	And espero 30 minutos
	Then se muestra mensaje de expiro se sesion
   
   

  
