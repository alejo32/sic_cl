package utils;

import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

import static io.restassured.RestAssured.given;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.core.JsonProcessingException;

import io.restassured.http.Header;

public class ApiUtil {

	Header hAuthorization	= new Header("Authorization", "Basic YWRtaW46MGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6STFOaUo5");
	Header hContentType 	= new Header("Content-Type", "application/json");
	Header hAccept 			= new Header("Accept", "application/json");



	public Response EnviarRequest(String apiUrl, String tramaJson) throws JsonProcessingException {
		
		return sendPost(apiUrl, tramaJson);
	}

	private Response sendGet(String info) {
		Response response = given().baseUri(info).get();
		return response;
	}

	private Response sendPost(String info, Object payload) {
		SimpleDateFormat DateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();
		String fechaInicio = DateFormat.format(date);
		System.out.println("Inicio Request: " + fechaInicio);
		Response response = given()
				.header(hContentType).header(hAccept).header(hAuthorization).body(payload)
				.contentType("application/json").post(info);

		Date date2 = new Date();
		String fechaFin = DateFormat.format(date2);
		System.out.println("Fin Request: " + fechaFin);

		return response;
	}

	private Response sendPost(String info, String tramaJson) {
		SimpleDateFormat DateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();
		String fechaInicio = DateFormat.format(date);
		System.out.println("Inicio Request: " + fechaInicio);
		System.out.println(info);
		Response response = given()
				.header(hContentType).header(hAccept).header(hAuthorization).body(tramaJson)
				.contentType("application/json").post(info);

		Date date2 = new Date();
		String fechaFin = DateFormat.format(date2);
		System.out.println("Fin Request: " + fechaFin);

		ResponseBody body = response.getBody();
		System.out.println("Response Body is: " + body.asString());

		return response;
	}

	private Response sendPut(String info, Object payload) {
		Response response = given().body(payload).contentType("application/json").put(info);
		return response;

	}

	public String ExtraerIdTrxHeader(String jsonString, String pattern) {

		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(jsonString);
		if (m.find())
			return m.group(0);
		else {
			System.out.println("NO MATCH");
			return "";
		}

	}

}
