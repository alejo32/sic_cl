package utils;

import models.ECargaStockProtegido;
import models.ESku;
import models.EStock;

import java.io.*;
import java.util.List;

public class ArchivosUtil {

	BaseUtil util;

	public ArchivosUtil() {
		util = new BaseUtil();
	}

	public String ComprimirArchivos(String rutaCarpeta, String extension, List<File> oLista) {
		String nombreArchivo = util.GenerarNombreArchivo("AUT_COMPRIMIDO_", extension);
		String rutaArchivo = rutaCarpeta + "\\" + nombreArchivo;

		try {
			Zipper z = new Zipper(new File(rutaArchivo));
			z.zip(oLista);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return nombreArchivo;
	}

	public boolean ValidarDescargaArchivo(String downloadPath, String fileName) {

		boolean flag = false;
		File dir = new File(downloadPath);
		File[] dir_contents = dir.listFiles();

		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().equals(fileName))
				return flag = true;
		}

		return flag;
	}

	public String CrearArchivoSFull(String rutaCarpeta, List<ESku> lista, boolean conSku, boolean conBodega,
			boolean conseller, boolean conTipoInventario, boolean conStock) throws IOException {

		String nombreArchivo = util.GenerarNombreArchivo("AUT_SFULL_", ".csv");
		String rutaArchivo = rutaCarpeta + "\\" + nombreArchivo;

		FileWriter ArchRepo = new FileWriter(rutaArchivo, true);
		BufferedWriter Salida = new BufferedWriter(ArchRepo);

		if (lista != null) {
			for (ESku entidad : lista) {
				entidad.sku = conSku ? entidad.sku : "";
				entidad.bodega = conBodega ? entidad.bodega : "";
				entidad.seller = conseller ? entidad.seller : "";
				entidad.tipoInventario = conTipoInventario ? entidad.tipoInventario : "";
				entidad.stock = conStock ? entidad.stock : "";
				entidad.stockDespues = conStock ? (entidad.stockEsDiferente.equals("1")
						? String.valueOf((Integer.parseInt(entidad.stock) + util.GenerarRandom(500, 10)))
						: entidad.stock) : "";

				StringBuilder sb = new StringBuilder();
				sb.append(entidad.sku);
				sb.append(",");
				sb.append(entidad.bodega);
				sb.append(",");
				sb.append(entidad.seller);
				sb.append(",");
				sb.append(entidad.tipoInventario);
				sb.append(",");
				sb.append(entidad.stockDespues);

				Salida.write(sb.toString());
				Salida.newLine();
			}
		}

		Salida.close();
		return nombreArchivo;
	}

	public String CrearArchivoSFull(String rutaCarpeta, List<ESku> lista, String extensionArchivo) throws IOException {

		String nombreArchivo = util.GenerarNombreArchivo("AUT_SFULL_", ".csv");
		String rutaArchivo = rutaCarpeta + "\\" + nombreArchivo;
		FileWriter ArchRepo = new FileWriter(rutaArchivo, true);
		BufferedWriter Salida = new BufferedWriter(ArchRepo);
		if (lista != null) {
			for (ESku entidad : lista) {

				StringBuilder sb = new StringBuilder();
				sb.append(entidad.sku);
				sb.append(",");
				sb.append(entidad.bodega);
				sb.append(",");
				sb.append(entidad.seller);
				sb.append(",");
				sb.append(entidad.tipoInventario);
				sb.append(",");
				sb.append(entidad.stock);

				Salida.write(sb.toString());
				Salida.newLine();
			}
		}
		Salida.close();
		return nombreArchivo;
	}

	public String CrearArchivoMDADJUST(String rutaCarpeta, String extensionArchivo, List<EStock> lista, boolean conBodega, boolean conSku,
			boolean conTipoOperacion, boolean conStock) throws IOException {

		String nombreArchivo = util.GenerarNombreArchivo("AUT_MDADJUST_", extensionArchivo);
		String rutaArchivo = rutaCarpeta + "\\" + nombreArchivo;

		FileWriter ArchRepo = new FileWriter(rutaArchivo, true);
		BufferedWriter Salida = new BufferedWriter(ArchRepo);
		System.out.println("Mostrando Salida archivo CSV: " + Salida + "\n");

		String[] operaciones = { "A", "S", "R" };

		StringBuilder sbd = new StringBuilder();
		sbd.append("bodega");
		sbd.append(",");
		sbd.append("sku");
		sbd.append(",");
		sbd.append("tipo_operacion");
		sbd.append(",");
		sbd.append("stock");
		Salida.write(sbd.toString());
		Salida.newLine();

		if (lista != null) {
			for (EStock entidad : lista) {
				int select = util.GenerarRandom(operaciones.length);
				entidad.operacion = operaciones[select];
				entidad.stockDespues = util.GenerarRandom(30, 10);
				StringBuilder sb = new StringBuilder();
				sb.append(conBodega ? entidad.bodega : "");
				sb.append(",");
				sb.append(conSku ? entidad.codigoVenta : "");
				sb.append(",");
				sb.append(conTipoOperacion ? entidad.operacion : "");
				sb.append(",");
				sb.append(conStock ? entidad.stockDespues : "");
				Salida.write(sb.toString());
				Salida.newLine();
			}
		}
		Salida.close();
		return nombreArchivo;
	}

	public String CrearArchivoCMSP(String rutaCarpeta, String extensionArchivo, List<ECargaStockProtegido> oLista,boolean conCabeceraIncorrecta)
			throws IOException {

		String nombreArchivo = util.GenerarNombreArchivo("AUT_CMPROTECTED_", extensionArchivo);
		String rutaArchivo = rutaCarpeta + "\\" + nombreArchivo;

		FileWriter ArchRepo = new FileWriter(rutaArchivo, true);
		BufferedWriter Salida = new BufferedWriter(ArchRepo);

		StringBuilder sbd = new StringBuilder();

		sbd.append(conCabeceraIncorrecta?"fecIni":"EffectiveDate");
		sbd.append(",");
		sbd.append("ExpiryDate");
		sbd.append(",");
		sbd.append("ItemId");
		sbd.append(",");
		sbd.append("LocationId");
		sbd.append(",");
		sbd.append("InventoryProtection1");
		sbd.append(",");
		sbd.append("InventoryProtection2");
		sbd.append(",");
		sbd.append("InventoryProtection3");
		sbd.append(",");
		sbd.append("InventoryProtection4");
		sbd.append(",");
		sbd.append("InventoryProtection5");

		Salida.write(sbd.toString());
		Salida.newLine();

		if (oLista != null) {
			for (ECargaStockProtegido entidad : oLista) {
				StringBuilder sb = new StringBuilder();
				sb.append(entidad.effectiveDate);
				sb.append(",");
				sb.append(entidad.expiryDate);
				sb.append(",");
				sb.append(entidad.itemId);
				sb.append(",");
				sb.append(entidad.locationId);
				sb.append(",");
				sb.append(entidad.inventoryProtection1);
				sb.append(",");
				sb.append(entidad.inventoryProtection2);
				sb.append(",");
				sb.append(entidad.inventoryProtection3);
				sb.append(",");
				sb.append(entidad.inventoryProtection4);
				sb.append(",");
				sb.append(entidad.inventoryProtection5);
				Salida.write(sb.toString());
				Salida.newLine();
			}
		}
		Salida.close();
		return nombreArchivo;
	}

	public String CrearArchivoCMSPVacio(String rutaCarpeta, String extensionArchivo, boolean conCabeceraIncorrecta)
			throws IOException {

		String nombreArchivo = util.GenerarNombreArchivo("AUT_CMPROTECTED_", extensionArchivo);
		String rutaArchivo = rutaCarpeta + "\\" + nombreArchivo;

		FileWriter ArchRepo = new FileWriter(rutaArchivo, true);

		BufferedWriter Salida = new BufferedWriter(ArchRepo);

		if (conCabeceraIncorrecta) {
			StringBuilder sbd = new StringBuilder();

			sbd.append("fecIni");
			sbd.append(",");
			sbd.append("fecFin");
			sbd.append(",");
			sbd.append("item");
			sbd.append(",");
			sbd.append("bodega");
			sbd.append(",");
			sbd.append("nivel1");
			sbd.append(",");
			sbd.append("nivel2");
			sbd.append(",");
			sbd.append("nivel3");
			sbd.append(",");
			sbd.append("nivel4");
			sbd.append(",");
			sbd.append("nivel5");

			Salida.write(sbd.toString());

			Salida.newLine();
			Salida.close();
		}

		return nombreArchivo;
	}

	public String CrearArchivoMDADJUSTConErrores(String rutaCarpeta, String extensionArchivo, List<EStock> lista,
			boolean conSku, boolean conTipoOperacion, boolean conStock) throws IOException {

		String nombreArchivo	= util.GenerarNombreArchivo("AUT_MDADJUST_", extensionArchivo);
		String rutaArchivo 		= rutaCarpeta + "\\" + nombreArchivo;
		FileWriter ArchRepo 	= new FileWriter(rutaArchivo, true);
		BufferedWriter Salida 	= new BufferedWriter(ArchRepo);

		String[] operaciones = { "A", "" };

		StringBuilder sbd = new StringBuilder();
		sbd.append("sku");
		sbd.append(",");
		sbd.append("tipo_operacion");
		sbd.append(",");
		sbd.append("stock");
		Salida.write(sbd.toString());
		Salida.newLine();

		boolean conError = false;
		if (lista != null) {
			for (EStock entidad : lista) {
				int select = util.GenerarRandom(operaciones.length);
				if (!conError) {
					entidad.operacion = "Z";
					conError = true;
				} else
					entidad.operacion = operaciones[select];

				entidad.stockDespues = util.GenerarRandom(30, 10);

				StringBuilder sb = new StringBuilder();
				sb.append(conSku ? entidad.codigoVenta : "");
				sb.append(",");
				sb.append(conTipoOperacion ? entidad.operacion : "");
				sb.append(",");
				sb.append(conStock ? entidad.stockDespues : "");

				Salida.write(sb.toString());
				Salida.newLine();
			}
		}
		Salida.close();
		return nombreArchivo;
	}

	public String CrearArchivoMDADJUST(String rutaCarpeta, String extensionArchivo, List<EStock> lista,
			String tipoOperacion, String stock) throws IOException {

		String nombreArchivo = util.GenerarNombreArchivo("AUT_MDADJUST_", extensionArchivo);
		String rutaArchivo = rutaCarpeta + "\\" + nombreArchivo;

		FileWriter ArchRepo = new FileWriter(rutaArchivo, true);
		BufferedWriter Salida = new BufferedWriter(ArchRepo);

		StringBuilder sbd = new StringBuilder();
		sbd.append("sku");
		sbd.append(",");
		sbd.append("tipo_operacion");
		sbd.append(",");
		sbd.append("stock");
		Salida.write(sbd.toString());
		Salida.newLine();

		if (lista != null) {
			for (EStock entidad : lista) {

				StringBuilder sb = new StringBuilder();
				sb.append(entidad.codigoVenta);
				sb.append(",");
				sb.append(tipoOperacion);
				sb.append(",");
				sb.append(stock);

				Salida.write(sb.toString());
				Salida.newLine();
			}
		}
		Salida.close();
		return nombreArchivo;
	}

}
