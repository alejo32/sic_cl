package utils;

import models.*;
import models.sic.EGrillaAuditoria;
import pageObjects.BasePage;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class DatabaseUtil {

	//Se listan algunas de las bodegas que se visualizan en el combo bodega,
	// esto hasta que se tenga información de Roles y Perfiles
	
	String bodegasHabilitadas = "'10095','10012'";

	/*public void ConnectarTunelSSH(EConexionBD oEntidad) throws IOException, SQLException {
		System.out.println("-----------------------------------------------ConnectarTunelSSH");
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat(utils.Constant.FORMATO_FECHA2);
		System.out.println("Inicio: " + dateFormat.format(date));

		System.out.println("Conectandose a tunel SSH:" ( COMENTAR + oEntidad.sshUser + )  "| ********* |" + oEntidad.sshHost + "|" + oEntidad.dbPort + "|" + oEntidad.dbUser + "|" + oEntidad.dbPassword +"| *********");
		MySqlDbHelper mySqlDbHelper = new MySqlDbHelper();
		MySqlDbHelper.setConnection(
				mySqlDbHelper.ConnectDbWithTunnel(oEntidad.sshUser, oEntidad.sshPassword, oEntidad.sshHost, 
						oEntidad.sshPort, oEntidad.dbHost, oEntidad.dbPort, oEntidad.dbUser, oEntidad.dbPassword));
	}*/
	
	public void ConnectarTunelSSH(EConexionBD oEntidad) throws IOException, SQLException {
		System.out.println("-----------------------------------------------ConnectarBBDD");
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat(utils.Constant.FORMATO_FECHA2);
		System.out.println("Inicio: " + dateFormat.format(date));

		System.out.println("Conectandose a BBDD:" + oEntidad.sshHost + "|" + oEntidad.dbPort + "|" + oEntidad.dbUser + "|" + oEntidad.dbPassword );
		MySqlDbHelper mySqlDbHelper = new MySqlDbHelper();
		MySqlDbHelper.setConnection(mySqlDbHelper.ConnectDbWithTunnel(oEntidad.sshHost, oEntidad.dbPort, oEntidad.dbUser, oEntidad.dbPassword));
	}

	public boolean HabilitarConfiguracionMovimientoInventario(
			String codigoPais, 
			String tipoTransaccion, 
			String codigoSistema, 
			String tipoInventario) {
		int affectedRows = 0;
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();

			String sqlQuery = "UPDATE RIPLEY_TRX_TYPE " + "SET STATUS='A' "
					+ "WHERE TRANSACTION_TYPE_CODE='[TIPO_TRANSACCION]' " + "AND SYSTEM_CODE='[CODIGO_SISTEMA]' "
					+ "AND COUNTRY_CODE='[CODIGO_PAIS]' " + "AND INVENTORY_TYPE= [INVENTORY_TYPE];";
			sqlQuery = sqlQuery.replace("[TIPO_TRANSACCION]", tipoTransaccion);
			sqlQuery = sqlQuery.replace("[CODIGO_SISTEMA]", codigoSistema);
			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);
			sqlQuery = sqlQuery.replace("[INVENTORY_TYPE]", tipoInventario);

			System.out.println("Query HabilitarConfiguracionMovimientoInventario(): " +sqlQuery + "tipo transaction" + tipoTransaccion);
			affectedRows = stmt.executeUpdate(sqlQuery);

			// conn.close();
		} catch (Exception e) {
			System.err.println("ERROR - No se actualizó la configuración de movimiento de inventario. Revisar");
			System.err.println(e.getMessage());
		}
		return affectedRows > 0;
	}

	public boolean DeshabilitarConfiguracionMovimientoInventario(String codigoPais, String codigoSistema,
			String tipoTransaccion, String tipoInventario) {
		int affectedRows = 0;
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();

			String sqlQuery = "UPDATE RIPLEY_TRX_TYPE " + "SET STATUS='I' "
					+ "WHERE TRANSACTION_TYPE_CODE='[TIPO_TRANSACCION]' " + "AND SYSTEM_CODE='[CODIGO_SISTEMA]' "
					+ "AND COUNTRY_CODE='[CODIGO_PAIS]' " + "AND INVENTORY_TYPE= [INVENTORY_TYPE];";
			sqlQuery = sqlQuery.replace("[TIPO_TRANSACCION]", tipoTransaccion);
			sqlQuery = sqlQuery.replace("[CODIGO_SISTEMA]", codigoSistema);
			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);
			sqlQuery = sqlQuery.replace("[INVENTORY_TYPE]", tipoInventario);

			 System.out.println("SQL DeshabilitarConfiguracionMovimientoInventario(): " +sqlQuery);
			affectedRows = stmt.executeUpdate(sqlQuery);

			// conn.close();
		} catch (Exception e) {
			System.err.println("ERROR - No se actualizó la configuración de movimiento de inventario. Revisar");
			System.err.println(e.getMessage());
		}
		return affectedRows > 0;
	}

	public List<ESku> ObtenerInformacionProductos(String codigoPais, int cantidad)
			throws IOException, InterruptedException, SQLException {
		List<ESku> oLista = new ArrayList<ESku>();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;

			String sqlQuery = "\n"
					+ "select COD_VENTA, SELLER, FULFILLMENT_CODE,INVENTORY_TYPE, QTY, IF(RIGHT(COD_VENTA, 1) % 2 <> 0, true, false) ES_IMPAR from (\n"
					+ "SELECT t1.COD_VENTA,t1.SELLER, t2.FULFILLMENT_CODE,t2.INVENTORY_TYPE, t2.QTY FROM SKU t1\n"
					+ "INNER JOIN SKU_STOCK_RIPLEY t2\n" + "ON t1.SKU_ID = t2.SKU_ID\n"
					+ "WHERE t2.FULFILLMENT_CODE IN ([BODEGAS_HABILITADAS]) AND t1.COUNTRY_CODE='[CODIGO_PAIS]' AND LENGTH(t1.SKU)=8 AND LENGTH(t1.COD_VENTA)=13\n"
					+ "LIMIT 100000\n" + ") A \n" + "WHERE QTY>0\n" + "ORDER BY RAND()\n" + "LIMIT [CANTIDAD]";
			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);
			sqlQuery = sqlQuery.replace("[CANTIDAD]", String.valueOf(cantidad));
			sqlQuery = sqlQuery.replace("[BODEGAS_HABILITADAS]", bodegasHabilitadas);

			System.out.println("Query ObtenerInformacionProductos()" + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);
			while (rs.next()) {
				ESku oEntidad 			= new ESku();
				oEntidad.sku 			= rs.getString("COD_VENTA");
				oEntidad.seller 		= rs.getString("SELLER");
				oEntidad.bodega 		= rs.getString("FULFILLMENT_CODE");
				oEntidad.tipoInventario	= rs.getString("INVENTORY_TYPE");
				oEntidad.stock 			= rs.getString("QTY");
				oEntidad.iStock 		= rs.getInt("QTY");
				oEntidad.esImpar 		= rs.getString("ES_IMPAR");

				oLista.add(oEntidad);
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerInformacionProductos");
			System.err.println(e.getMessage());
		}
		return oLista;
	}

	public List<ESku> ObtenerInformacionProductos(String codigoPais, int cantidad, String tipoInventario)
			throws IOException, InterruptedException, SQLException {
		List<ESku> oLista = new ArrayList<ESku>();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;

			String sqlQuery = "\n"
					+ "select COD_VENTA, SELLER, FULFILLMENT_CODE,INVENTORY_TYPE, QTY, IF(RIGHT(COD_VENTA, 1) % 2 <> 0, true, false) ES_IMPAR from (\n"
					+ "SELECT t1.COD_VENTA,t1.SELLER, t2.FULFILLMENT_CODE,t2.INVENTORY_TYPE, t2.QTY FROM SKU t1\n"
					+ "INNER JOIN SKU_STOCK_RIPLEY t2\n" + "ON t1.SKU_ID = t2.SKU_ID\n"
					+ "WHERE t2.FULFILLMENT_CODE IN ([BODEGAS_HABILITADAS]) AND INVENTORY_TYPE='[INVENTORY_TYPE]' AND t1.COUNTRY_CODE='[CODIGO_PAIS]' AND LENGTH(t1.SKU)=8 AND LENGTH(t1.COD_VENTA)=13\n"
					+ "LIMIT 100000\n" + ") A \n" + "WHERE QTY>0\n" + "ORDER BY RAND()\n" + "LIMIT [CANTIDAD]";
			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);
			sqlQuery = sqlQuery.replace("[CANTIDAD]", String.valueOf(cantidad));
			sqlQuery = sqlQuery.replace("[BODEGAS_HABILITADAS]", bodegasHabilitadas);
			sqlQuery = sqlQuery.replace("[INVENTORY_TYPE]", tipoInventario);

			System.out.println("Query ObtenerInformacionProductos(): " + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);
			while (rs.next()) {
				ESku oEntidad = new ESku();
				oEntidad.sku = rs.getString("COD_VENTA");
				oEntidad.seller = rs.getString("SELLER");
				oEntidad.bodega = rs.getString("FULFILLMENT_CODE");
				oEntidad.tipoInventario = rs.getString("INVENTORY_TYPE");
				oEntidad.stock = rs.getString("QTY");
				oEntidad.iStock = rs.getInt("QTY");
				oEntidad.esImpar = rs.getString("ES_IMPAR");

				oLista.add(oEntidad);
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerInformacionProductos");
			System.err.println(e.getMessage());
		}
		return oLista;
	}

	public List<ESku> ObtenerInformacionProductosNoExiste(String codigoPais, int cantidad)
			throws IOException, InterruptedException, SQLException {
		List<ESku> oLista = new ArrayList<ESku>();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;

			String sqlQuery = "\n"
					+ "select COD_VENTA, SELLER, FULFILLMENT_CODE,INVENTORY_TYPE, QTY, IF(RIGHT(COD_VENTA, 1) % 2 <> 0, true, false) ES_IMPAR from (\n"
					+ "SELECT t1.COD_VENTA,t1.SELLER, t2.FULFILLMENT_CODE,t2.INVENTORY_TYPE, t2.QTY FROM SKU t1\n"
					+ "INNER JOIN SKU_STOCK_RIPLEY t2\n" + "ON t1.SKU_ID = t2.SKU_ID\n"
					+ "WHERE t2.FULFILLMENT_CODE IN ([BODEGAS_HABILITADAS]) AND t1.COUNTRY_CODE='[CODIGO_PAIS]'\n"
					+ "LIMIT 100000\n" + ") A \n" + "WHERE QTY>0\n" + "ORDER BY RAND()\n" + "LIMIT [CANTIDAD]";
			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);
			sqlQuery = sqlQuery.replace("[CANTIDAD]", String.valueOf(cantidad));
			sqlQuery = sqlQuery.replace("[BODEGAS_HABILITADAS]", bodegasHabilitadas);

			Random random = new Random();
			System.out.println("Query ObtenerInformacionProductosNoExiste(): " + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);
			while (rs.next()) {
				ESku oEntidad = new ESku();
				oEntidad.sku = "300011211" + (random.nextInt(9999 - 1000) + 1000);
				oEntidad.seller = rs.getString("SELLER");
				oEntidad.bodega = rs.getString("FULFILLMENT_CODE");
				oEntidad.tipoInventario = rs.getString("INVENTORY_TYPE");
				oEntidad.stock = rs.getString("QTY");
				oEntidad.iStock = rs.getInt("QTY");
				oEntidad.esImpar = rs.getString("ES_IMPAR");

				oLista.add(oEntidad);
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerInformacionProductosNoExiste");
			System.err.println(e.getMessage());
		}
		return oLista;
	}

	public List<ESku> ObtenerInformacionProductosSinStock(String codigoPais, int cantidad, String bodega)
			throws IOException, InterruptedException, SQLException {
		List<ESku> oLista = new ArrayList<ESku>();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;

			String sqlQuery = "\n"
					+ "SELECT t1.COD_VENTA,t1.SELLER,\"[FULFILLMENT_CODE]\" FULFILLMENT_CODE, 1 INVENTORY_TYPE, 1 QTY, 1 ES_IMPAR FROM SKU t1\n"
					+ "LEFT JOIN SKU_STOCK_RIPLEY t2\n" + "ON t1.SKU_ID=t2.SKU_ID\n"
					+ "WHERE t1.COUNTRY_CODE='[CODIGO_PAIS]'\n" + "AND t2.FULFILLMENT_CODE IS NULL\n"
					+ "AND LENGTH(t1.COD_VENTA)=13\n" + "LIMIT [CANTIDAD]";
			sqlQuery = sqlQuery.replace("[FULFILLMENT_CODE]", bodega);
			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);
			sqlQuery = sqlQuery.replace("[CANTIDAD]", String.valueOf(cantidad));

			System.out.println("Query ObtenerInformacionProductosSinStock(): " + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);
			while (rs.next()) {
				ESku oEntidad = new ESku();
				oEntidad.sku = rs.getString("COD_VENTA");
				oEntidad.seller = rs.getString("SELLER");
				oEntidad.bodega = rs.getString("FULFILLMENT_CODE");
				oEntidad.tipoInventario = rs.getString("INVENTORY_TYPE");
				oEntidad.stock = rs.getString("QTY");
				oEntidad.iStock = rs.getInt("QTY");
				oEntidad.esImpar = rs.getString("ES_IMPAR");

				oLista.add(oEntidad);
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerInformacionProductosSinStock");
			System.err.println(e.getMessage());
		}
		return oLista;
	}

	public List<ESku> ObtenerInformacionProductosConStock0(String codigoPais, int cantidad)
			throws IOException, InterruptedException, SQLException {
		List<ESku> oLista = new ArrayList<ESku>();
		try {
			Connection conn	= ConnectSIC();
			Statement stmt	= conn.createStatement();
			ResultSet rs;

			String sqlQuery = "\n"
					+ "select COD_VENTA, SELLER, FULFILLMENT_CODE,INVENTORY_TYPE, QTY, IF(RIGHT(COD_VENTA, 1) % 2 <> 0, true, false) ES_IMPAR from (\n"
					+ "SELECT t1.COD_VENTA,t1.SELLER, t2.FULFILLMENT_CODE,t2.INVENTORY_TYPE, t2.QTY FROM SKU t1\n"
					+ "INNER JOIN SKU_STOCK_RIPLEY t2\n" + "ON t1.SKU_ID = t2.SKU_ID\n"
					+ "WHERE t2.FULFILLMENT_CODE IN ([BODEGAS_HABILITADAS]) AND  t1.COUNTRY_CODE='[CODIGO_PAIS]'\n"
					+ "LIMIT 100000\n" + ") A \n" + "WHERE QTY=0\n" + "ORDER BY RAND()\n" + "LIMIT [CANTIDAD]";
			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);
			sqlQuery = sqlQuery.replace("[CANTIDAD]", String.valueOf(cantidad));
			sqlQuery = sqlQuery.replace("[BODEGAS_HABILITADAS]", bodegasHabilitadas);

			System.out.println("Query ObtenerInformacionProductosConStock0(): " + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);
			while (rs.next()) {
				ESku oEntidad 			= new ESku();
				oEntidad.sku			= rs.getString("COD_VENTA");
				oEntidad.seller 		= rs.getString("SELLER");
				oEntidad.bodega 		= rs.getString("FULFILLMENT_CODE");
				oEntidad.tipoInventario	= rs.getString("INVENTORY_TYPE");
				oEntidad.stock 			= rs.getString("QTY");
				oEntidad.iStock 		= rs.getInt("QTY");
				oEntidad.esImpar 		= rs.getString("ES_IMPAR");

				oLista.add(oEntidad);
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerInformacionProductosConStock0");
			System.err.println(e.getMessage());
		}
		return  oLista;
	}

	int intentos = 1;

	public ETransaction EsperarProcesamientoTransaccion(String idTrxHeader) throws InterruptedException {
		ETransaction oEntidad = ObtenerResultadoTransaccion(idTrxHeader);
		System.out.println("estado: " + oEntidad.status + "\n");
		System.out.println("Intento: ");
		try {
			while (oEntidad == null || !oEntidad.status.equals("F") && !oEntidad.status.equals("CF")
					&& !oEntidad.status.equals("C") && intentos < 540) {
				intentos++;
				System.out.print(".." + intentos + "\n ");
				BasePage.WaitSleep(20);
				oEntidad = ObtenerResultadoTransaccion(idTrxHeader);
			}
		} catch (Exception ex) {
			intentos++;
			System.out.print(".." + intentos);
			BasePage.WaitSleep(5);
			System.out.println("EsperarProcesamientoTransaccion()");
			if (intentos > 10)
				return null;
			else
				return EsperarProcesamientoTransaccion(idTrxHeader);
		}
		return oEntidad;
	}

	public ETransaction ObtenerResultadoTransaccion(String idTrxHeader) {
		ETransaction entidad = null;
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT TRANSACCION_ID,\r\n" + "		FILENAME, \r\n" + "		STATUS, \r\n"
					+ "		TOTAL_ITEMS, \r\n" + "		SUCCESS_ITEMS, \r\n" + "		DUPLICATED_ITEMS, \r\n"
					+ "		UNSENT_ITEMS, \r\n" + "		ERROR_ITEM, \r\n" + "		UNCHANGED_ITEMS, \r\n"
					+ "		COUNTRY_CODE, \r\n" + "		COMMENTS \r\n" + "FROM `TRANSACTION` \r\n"
					+ "WHERE IDTRX_HEADER='" + idTrxHeader + "'";
			
			System.out.println("query ObtenerResultadoTransaccion(): " + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			entidad = new ETransaction();
			while (rs.next()) {
				 System.out.println("RESULTADO TRANSACCION: "+ entidad.status);
				entidad.transactionId 	= rs.getString("TRANSACCION_ID");
				entidad.fileName 		= rs.getString("FILENAME");
				entidad.status 			= rs.getString("STATUS");
				entidad.totalItems 		= rs.getString("TOTAL_ITEMS");
				entidad.successItems 	= rs.getString("SUCCESS_ITEMS");
				entidad.duplicatedItems	= rs.getString("DUPLICATED_ITEMS");
				entidad.unsentItems 	= rs.getString("UNSENT_ITEMS");
				entidad.errorItems 		= rs.getString("ERROR_ITEM");
				entidad.unchangedItems 	= rs.getString("UNCHANGED_ITEMS");
				entidad.countryCode 	= rs.getString("COUNTRY_CODE");
				entidad.comments 		= rs.getString("COMMENTS");
			}
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerResultadoTransaccion ");
			System.err.println(e.getMessage());
		}

		return entidad;
	}

	public ETransaction ObtenerResultadoTransaccionByTransactionId(String transactionId) {
		ETransaction entidad = null;
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT TRANSACCION_ID,\r\n" + "		FILENAME, \r\n" + "		STATUS, \r\n"
					+ "		TOTAL_ITEMS, \r\n" + "		SUCCESS_ITEMS, \r\n" + "		DUPLICATED_ITEMS, \r\n"
					+ "		UNSENT_ITEMS, \r\n" + "		ERROR_ITEM, \r\n" + "		UNCHANGED_ITEMS, \r\n"
					+ "		COUNTRY_CODE, \r\n" + "		COMMENTS \r\n" + "FROM `TRANSACTION` \r\n"
					+ "WHERE TRANSACCION_ID=[TRANSACTION_ID]";
			
			sqlQuery = sqlQuery.replace("[TRANSACTION_ID]", transactionId);
			
			rs = stmt.executeQuery(sqlQuery);
			System.out.println("ObtenerResultadoTransaccionByTransactionId(): " + sqlQuery);
			entidad = new ETransaction();
			while (rs.next()) {
				entidad.transactionId 	= rs.getString("TRANSACCION_ID");
				entidad.fileName 		= rs.getString("FILENAME");
				entidad.status 			= rs.getString("STATUS");
				entidad.totalItems 		= rs.getString("TOTAL_ITEMS");
				entidad.successItems 	= rs.getString("SUCCESS_ITEMS");
				entidad.duplicatedItems	= rs.getString("DUPLICATED_ITEMS");
				entidad.unsentItems 	= rs.getString("UNSENT_ITEMS");
				entidad.errorItems 		= rs.getString("ERROR_ITEM");
				entidad.unchangedItems 	= rs.getString("UNCHANGED_ITEMS");
				entidad.countryCode 	= rs.getString("COUNTRY_CODE");
				entidad.comments 		= rs.getString("COMMENTS");
			}
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerResultadoTransaccion ");
			System.err.println(e.getMessage());
		}

		return entidad;
	}

	public String ObtenerNombreBodega(String codigoBodega) {
		String nombreBodega = "";
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT NAME FROM LOCATION WHERE COUNTRY_CODE='CL' AND CODE='[CODIGO_VENTA]';";
			sqlQuery = sqlQuery.replace("[CODIGO_VENTA]", codigoBodega);

			System.out.println("Query ObtenerNombreBodega(): " + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);
			while (rs.next()) {
				nombreBodega = rs.getString("NAME");

			}
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerNombreBodega");
			System.err.println(e.getMessage());
		}

		return nombreBodega;
	}

	public List<ETransactionDetail> ObtenerResultadoDetalleTransaccion(String transactionId) {
		List<ETransactionDetail> lista = null;
		System.out.println("Transaccion ID: " + transactionId);
		boolean first = true;
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT IDTRX,\r\n" + "		SKU,\r\n" + "		FULFILLMENT_CODE,\r\n"
					+ "		INVENTORY_TYPE,\r\n" + "		STOCK,\r\n" + "		PREV_STOCK,\r\n"
					+ "		OPERATION,\r\n" + "		CUD,\r\n" + "		STATUS,\r\n" + "		DETAIL\r\n"
					+ "FROM TRANSACTION_ITEM_LINE_RIPLEY WHERE TRANSACCION_ID='" + transactionId + "'";

			sqlQuery = sqlQuery.replace("[TRANSACTION_ID]", transactionId);
			
			System.out.println("Query ObtenerResultadoDetalleTransaccion(): " + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				if (first)
					lista = new ArrayList<ETransactionDetail>();
				first = false;
				ETransactionDetail entidad = new ETransactionDetail();
				entidad.idTrx 			= rs.getString("IDTRX");
				entidad.sku 			= rs.getString("SKU");
				entidad.fulfillmentCode	= rs.getString("FULFILLMENT_CODE");
				entidad.inventoryType 	= rs.getString("INVENTORY_TYPE");
				entidad.stock 			= rs.getString("STOCK");
				entidad.prevStock 		= rs.getString("PREV_STOCK");
				entidad.operation 		= rs.getString("OPERATION");
				entidad.cud 			= rs.getString("CUD");
				entidad.status 			= rs.getString("STATUS");
				entidad.detail 			= rs.getString("DETAIL");

				lista.add(entidad);
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerResultadoDetalleTransaccion");
			System.err.println(e.getMessage());
		}

		return lista;
	}

	public EStock ObtenerStockProductoBD(String codigoVenta, String bodega, String tipoInventario) {
		EStock oEntidad = new EStock();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT t2.FULFILLMENT_CODE, t2.INVENTORY_TYPE, t2.QTY, t2.USERNAME FROM SKU t1\n"
					+ "		INNER JOIN SKU_STOCK_RIPLEY t2\n" + "		ON t1.SKU_ID=t2.SKU_ID\n"
					+ "		WHERE t1.COUNTRY_CODE='CL'\n" + "		AND t1.COD_VENTA = '[COD_VENTA]'\n"
					+ "		AND t2.FULFILLMENT_CODE='[FULFILLMENT_CODE]'\n"
					+ "		AND t2.INVENTORY_TYPE='[INVENTORY_TYPE]'";

			sqlQuery = sqlQuery.replace("[COD_VENTA]", codigoVenta);
			sqlQuery = sqlQuery.replace("[FULFILLMENT_CODE]", bodega);
			sqlQuery = sqlQuery.replace("[INVENTORY_TYPE]", tipoInventario);

			System.out.println("Query ObtenerStockProductoBD(): " + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				oEntidad.bodega 		= rs.getString("FULFILLMENT_CODE");
				oEntidad.tipoInventario	= rs.getString("INVENTORY_TYPE");
				oEntidad.stock 			= rs.getInt("QTY");
				oEntidad.username 		= rs.getString("USERNAME");
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerStockProductoBD");
			System.err.println(e.getMessage());
		}

		return oEntidad;

	}

	public EStockJerarquia ObtenerInformacionStockJerarquiaProductoMayorCero(String codigoPais) {
		EStockJerarquia oEntidad = new EStockJerarquia();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT  t2.COD_VENTA,\n" + "		t1.FULFILLMENT_CODE,\n" + "		t2.DEPTO_CODE,\n"
					+ "		t2.LINE_CODE,\n" + "		t2.SUBLINE_CODE,\n" + "		t2.`STYLE`,\n" + "		t2.NAME,\n"
					+ "		t1.QTY,\n" + "		t1.INVENTORY_TYPE\n" + "FROM SKU_STOCK_RIPLEY t1\n"
					+ "INNER JOIN SKU t2 ON t1.SKU_ID=t2.SKU_ID\n"
					+ "WHERE  t1.FULFILLMENT_CODE IN ([BODEGAS_HABILITADAS]) AND t2.COUNTRY_CODE='[CODIGO_PAIS]'\n"
					+ "AND t1.QTY>0\n" + "AND LENGTH(t2.COD_VENTA)=13\n" + "LIMIT 1\n" + "";

			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);
			sqlQuery = sqlQuery.replace("[BODEGAS_HABILITADAS]", bodegasHabilitadas);

			System.out.println("Query ObtenerInformacionStockJerarquiaProductoMayorCero(): " + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.fulfillmentCode = rs.getString("FULFILLMENT_CODE");
				oEntidad.deptoCode = rs.getString("DEPTO_CODE");
				oEntidad.lineCode = rs.getString("LINE_CODE");
				oEntidad.subLineCode = rs.getString("SUBLINE_CODE");
				oEntidad.style = rs.getString("STYLE");
				oEntidad.name = rs.getString("NAME");
				oEntidad.qty = rs.getString("QTY");
				oEntidad.inventoryType = rs.getString("INVENTORY_TYPE");
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerInformacionStockJerarquiaProductoMayorCero");
			System.err.println(e.getMessage());
		}
		return oEntidad;
	}

	public EStockJerarquia ObtenerInformacionStockJerarquiaProducto(String codigoPais, String cantidad) {
		EStockJerarquia oEntidad = new EStockJerarquia();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT * FROM  (SELECT STYLE,COD_VENTA,SUBLINE_CODE,LINE_CODE,DEPTO_CODE,NAME\n"
					+ "FROM SKU WHERE COUNTRY_CODE='CL' AND LENGTH(COD_VENTA)=13 \n" + "LIMIT 100000) a\n"
					+ "order by RAND()\n" + "LIMIT [CANTIDAD]";

			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);
			sqlQuery = sqlQuery.replace("[CANTIDAD]", cantidad);

			rs = stmt.executeQuery("Query ObtenerInformacionStockJerarquiaProducto(): " + sqlQuery);

			while (rs.next()) {
				oEntidad.style = rs.getString("STYLE");
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.subLineCode = rs.getString("SUBLINE_CODE");
				oEntidad.lineCode = rs.getString("LINE_CODE");
				oEntidad.deptoCode = rs.getString("DEPTO_CODE");
				oEntidad.name = rs.getString("NAME");
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerInformacionStockJerarquiaProducto");
			System.err.println(e.getMessage());
		}

		return oEntidad;

	}

	public EStockJerarquia ObtenerInformacionStockJerarquiaProducto(String codigoPais, String cantidad,
			String tipoInventario) {
		EStockJerarquia oEntidad = new EStockJerarquia();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "select * from (\n"
					+ "SELECT t1.STYLE,t1.COD_VENTA,t1.SUBLINE_CODE,t1.LINE_CODE,t1.DEPTO_CODE,t1.NAME,t2.FULFILLMENT_CODE,t2.INVENTORY_TYPE, t2.QTY FROM SKU t1\n"
					+ "INNER JOIN SKU_STOCK_RIPLEY t2\n" + "ON t1.SKU_ID = t2.SKU_ID\n"
					+ "WHERE t2.FULFILLMENT_CODE='10012' AND  INVENTORY_TYPE='[TIPO_INVENTARIO]' AND t1.COUNTRY_CODE='[CODIGO_PAIS]' AND LENGTH(t1.SKU)=8 AND LENGTH(t1.COD_VENTA)=13\n"
					+ "LIMIT 100000\n" + ") A \n" + "ORDER BY RAND()\n" + "LIMIT [CANTIDAD]";

			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);
			sqlQuery = sqlQuery.replace("[CANTIDAD]", cantidad);
			sqlQuery = sqlQuery.replace("[TIPO_INVENTARIO]", tipoInventario);
			
			System.out.println("Query ObtenerInformacionStockJerarquiaProducto(): " + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				oEntidad.style = rs.getString("STYLE");
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.subLineCode = rs.getString("SUBLINE_CODE");
				oEntidad.lineCode = rs.getString("LINE_CODE");
				oEntidad.deptoCode = rs.getString("DEPTO_CODE");
				oEntidad.name = rs.getString("NAME");
				oEntidad.fulfillmentCode = rs.getString("FULFILLMENT_CODE");
				oEntidad.inventoryType = rs.getString("INVENTORY_TYPE");
				oEntidad.qty = rs.getString("QTY");
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerInformacionStockJerarquiaProducto");
			System.err.println(e.getMessage());
		}
		return oEntidad;
	}

	public EStockJerarquia ObtenerInformacionStockJerarquiaProductoMenorCero(String codigoPais) {
		EStockJerarquia oEntidad = new EStockJerarquia();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT  t2.COD_VENTA,\n" + "		t1.FULFILLMENT_CODE,\n" + "		t2.DEPTO_CODE,\n"
					+ "		t2.LINE_CODE,\n" + "		t2.SUBLINE_CODE,\n" + "		t2.`STYLE`,\n" + "		t2.NAME,\n"
					+ "		t1.QTY,\n" + "		t1.INVENTORY_TYPE\n" + "FROM SKU_STOCK_RIPLEY t1\n"
					+ "INNER JOIN SKU t2 ON t1.SKU_ID=t2.SKU_ID\n"
					+ "WHERE  t1.FULFILLMENT_CODE IN ([BODEGAS_HABILITADAS]) AND t2.COUNTRY_CODE='[CODIGO_PAIS]'\n"
					+ "AND t1.QTY<0\n" + "AND LENGTH(t2.COD_VENTA)=13\n" + "LIMIT 1\n" + "";

			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);
			sqlQuery = sqlQuery.replace("[BODEGAS_HABILITADAS]", bodegasHabilitadas);

			 System.out.println("Query ObtenerInformacionStockJerarquiaProductoMenorCero(): " + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.fulfillmentCode = rs.getString("FULFILLMENT_CODE");
				oEntidad.deptoCode = rs.getString("DEPTO_CODE");
				oEntidad.lineCode = rs.getString("LINE_CODE");
				oEntidad.subLineCode = rs.getString("SUBLINE_CODE");
				oEntidad.style = rs.getString("STYLE");
				oEntidad.name = rs.getString("NAME");
				oEntidad.qty = rs.getString("QTY");
				oEntidad.inventoryType = rs.getString("INVENTORY_TYPE");
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerInformacionStockJerarquiaProductoMayorCero");
			System.err.println(e.getMessage());
		}

		return oEntidad;

	}

	public EStockJerarquia ObtenerInformacionStockJerarquiaProductoIgualCero(String codigoPais) {
		EStockJerarquia oEntidad = new EStockJerarquia();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT  t2.COD_VENTA,\n" + "		t1.FULFILLMENT_CODE,\n" + "		t2.DEPTO_CODE,\n"
					+ "		t2.LINE_CODE,\n" + "		t2.SUBLINE_CODE,\n" + "		t2.`STYLE`,\n" + "		t2.NAME,\n"
					+ "		t1.QTY,\n" + "		t1.INVENTORY_TYPE\n" + "FROM SKU_STOCK_RIPLEY t1\n"
					+ "INNER JOIN SKU t2 ON t1.SKU_ID=t2.SKU_ID\n"
					+ "WHERE  t1.FULFILLMENT_CODE IN ([BODEGAS_HABILITADAS]) AND t2.COUNTRY_CODE='[CODIGO_PAIS]'\n"
					+ "AND t1.QTY=0\n" + "AND LENGTH(t2.COD_VENTA)=13\n" + "LIMIT 1\n" + "";

			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);
			sqlQuery = sqlQuery.replace("[BODEGAS_HABILITADAS]", bodegasHabilitadas);

			 System.out.println("Query ObtenerInformacionStockJerarquiaProductoIgualCero(): " + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.fulfillmentCode = rs.getString("FULFILLMENT_CODE");
				oEntidad.deptoCode = rs.getString("DEPTO_CODE");
				oEntidad.lineCode = rs.getString("LINE_CODE");
				oEntidad.subLineCode = rs.getString("SUBLINE_CODE");
				oEntidad.style = rs.getString("STYLE");
				oEntidad.name = rs.getString("NAME");
				oEntidad.qty = rs.getString("QTY");
				oEntidad.inventoryType = rs.getString("INVENTORY_TYPE");
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerInformacionStockJerarquiaProductoMayorCero");
			System.err.println(e.getMessage());
		}

		return oEntidad;

	}

	public EStockJerarquia ObtenerInformacionStockJerarquiaProductoMayorIgualA(String codigoPais, String stock) {
		EStockJerarquia oEntidad = new EStockJerarquia();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT  t2.COD_VENTA,\n" + "		t1.FULFILLMENT_CODE,\n" + "		t2.DEPTO_CODE,\n"
					+ "		t2.LINE_CODE,\n" + "		t2.SUBLINE_CODE,\n" + "		t2.`STYLE`,\n" + "		t2.NAME,\n"
					+ "		t1.QTY,\n" + "		t1.INVENTORY_TYPE\n" + "FROM SKU_STOCK_RIPLEY t1\n"
					+ "INNER JOIN SKU t2 ON t1.SKU_ID=t2.SKU_ID\n"
					+ "WHERE  t1.FULFILLMENT_CODE IN ([BODEGAS_HABILITADAS]) AND t2.COUNTRY_CODE='[CODIGO_PAIS]'\n"
					+ "AND t1.QTY>=[STOCK]\n" + "AND LENGTH(t2.COD_VENTA)=13\n" + "LIMIT 1\n" + "";

			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);
			sqlQuery = sqlQuery.replace("[STOCK]", stock);
			sqlQuery = sqlQuery.replace("[BODEGAS_HABILITADAS]", bodegasHabilitadas);

			 System.out.println("Query ObtenerInformacionStockJerarquiaProductoMayorIgualA(): " + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.fulfillmentCode = rs.getString("FULFILLMENT_CODE");
				oEntidad.deptoCode = rs.getString("DEPTO_CODE");
				oEntidad.lineCode = rs.getString("LINE_CODE");
				oEntidad.subLineCode = rs.getString("SUBLINE_CODE");
				oEntidad.style = rs.getString("STYLE");
				oEntidad.name = rs.getString("NAME");
				oEntidad.qty = rs.getString("QTY");
				oEntidad.inventoryType = rs.getString("INVENTORY_TYPE");
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerInformacionStockJerarquiaProductoMayorCero");
			System.err.println(e.getMessage());
		}

		return oEntidad;

	}

	public EStockJerarquia ObtenerInformacionStockJerarquiaProductoMenorIgualA(String codigoPais, String stock) {
		EStockJerarquia oEntidad = new EStockJerarquia();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT  t2.COD_VENTA,\n" + "		t1.FULFILLMENT_CODE,\n" + "		t2.DEPTO_CODE,\n"
					+ "		t2.LINE_CODE,\n" + "		t2.SUBLINE_CODE,\n" + "		t2.`STYLE`,\n" + "		t2.NAME,\n"
					+ "		t1.QTY,\n" + "		t1.INVENTORY_TYPE\n" + "FROM SKU_STOCK_RIPLEY t1\n"
					+ "INNER JOIN SKU t2 ON t1.SKU_ID=t2.SKU_ID\n"
					+ "WHERE  t1.FULFILLMENT_CODE IN ([BODEGAS_HABILITADAS]) AND t2.COUNTRY_CODE='[CODIGO_PAIS]'\n"
					+ "AND t1.QTY<=[STOCK]\n" + "AND LENGTH(t2.COD_VENTA)=13\n" + "LIMIT 1\n" + "";

			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);
			sqlQuery = sqlQuery.replace("[STOCK]", stock);
			sqlQuery = sqlQuery.replace("[BODEGAS_HABILITADAS]", bodegasHabilitadas);

			 System.out.println("Query ObtenerInformacionStockJerarquiaProductoMenorIgualA(): " + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.fulfillmentCode = rs.getString("FULFILLMENT_CODE");
				oEntidad.deptoCode = rs.getString("DEPTO_CODE");
				oEntidad.lineCode = rs.getString("LINE_CODE");
				oEntidad.subLineCode = rs.getString("SUBLINE_CODE");
				oEntidad.style = rs.getString("STYLE");
				oEntidad.name = rs.getString("NAME");
				oEntidad.qty = rs.getString("QTY");
				oEntidad.inventoryType = rs.getString("INVENTORY_TYPE");
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerInformacionStockJerarquiaProductoMayorCero");
			System.err.println(e.getMessage());
		}

		return oEntidad;

	}

	public EStockJerarquia ObtenerInformacionStockJerarquiaProductoDiferenteA(String codigoPais, String stock) {
		EStockJerarquia oEntidad = new EStockJerarquia();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT  t2.COD_VENTA,\n" + "		t1.FULFILLMENT_CODE,\n" + "		t2.DEPTO_CODE,\n"
					+ "		t2.LINE_CODE,\n" + "		t2.SUBLINE_CODE,\n" + "		t2.`STYLE`,\n" + "		t2.NAME,\n"
					+ "		t1.QTY,\n" + "		t1.INVENTORY_TYPE\n" + "FROM SKU_STOCK_RIPLEY t1\n"
					+ "INNER JOIN SKU t2 ON t1.SKU_ID=t2.SKU_ID\n"
					+ "WHERE  t1.FULFILLMENT_CODE IN ([BODEGAS_HABILITADAS]) AND t2.COUNTRY_CODE='[CODIGO_PAIS]'\n"
					+ "AND t1.QTY<>[STOCK]\n" + "AND LENGTH(t2.COD_VENTA)=13\n" + "LIMIT 1\n" + "";

			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);
			sqlQuery = sqlQuery.replace("[STOCK]", stock);
			sqlQuery = sqlQuery.replace("[BODEGAS_HABILITADAS]", bodegasHabilitadas);

			System.out.println("Query ObtenerInformacionStockJerarquiaProductoDiferenteA(): " + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.fulfillmentCode = rs.getString("FULFILLMENT_CODE");
				oEntidad.deptoCode = rs.getString("DEPTO_CODE");
				oEntidad.lineCode = rs.getString("LINE_CODE");
				oEntidad.subLineCode = rs.getString("SUBLINE_CODE");
				oEntidad.style = rs.getString("STYLE");
				oEntidad.name = rs.getString("NAME");
				oEntidad.qty = rs.getString("QTY");
				oEntidad.inventoryType = rs.getString("INVENTORY_TYPE");
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerInformacionStockJerarquiaProductoMayorCero");
			System.err.println(e.getMessage());
		}

		return oEntidad;

	}

	public EStock ObtenerProductoStockRipley(String cantidad, String tipoInventario) {
		EStock oEntidad = new EStock();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT t1.COD_VENTA,t2.FULFILLMENT_CODE, t2.INVENTORY_TYPE,t2.QTY,t2.USERNAME FROM SKU t1\n"
					+ "INNER JOIN SKU_STOCK_RIPLEY t2\n" + "ON t1.SKU_ID=t2.SKU_ID\n" + "WHERE t2.QTY>0\n"
					+ "AND t1.COUNTRY_CODE='CL'\n" + "AND LENGTH(t1.COD_VENTA)>12\n"
					+ "AND t2.INVENTORY_TYPE='[TIPO_INVENTARIO]'   AND t2.FULFILLMENT_CODE IN ([BODEGAS_HABILITADAS]) \n"
					+ "LIMIT [LIMIT]";

			sqlQuery = sqlQuery.replace("[LIMIT]", cantidad);
			sqlQuery = sqlQuery.replace("[TIPO_INVENTARIO]", tipoInventario);
			sqlQuery = sqlQuery.replace("[BODEGAS_HABILITADAS]", bodegasHabilitadas);

			System.out.println("Query ObtenerProductoStockRipley(): " + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.bodega = rs.getString("FULFILLMENT_CODE");
				oEntidad.tipoInventario = rs.getString("INVENTORY_TYPE");
				oEntidad.stock = rs.getInt("QTY");
				oEntidad.username = rs.getString("USERNAME");
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerProductoStockRipley");
			System.err.println(e.getMessage());
		}

		return oEntidad;

	}

	public List<EStock> ObtenerListaProductoStockRipley(String cantidad) {

		List<EStock> oListaProductos = new ArrayList<EStock>();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT * FROM(\n"
					+ "	SELECT t1.COD_VENTA,t2.FULFILLMENT_CODE, t2.INVENTORY_TYPE,t2.QTY FROM SKU t1\n"
					+ "	INNER JOIN SKU_STOCK_RIPLEY t2\n" + "	ON t1.SKU_ID=t2.SKU_ID\n" + "	WHERE t2.QTY>0\n"
					+ "	AND t1.COUNTRY_CODE='CL'\n" + "	AND LENGTH(t1.COD_VENTA)>12\n"
					+ "	AND t2.INVENTORY_TYPE='[TIPO_INVENTARIO]' \n" + "	LIMIT 61000)A \n" + "	ORDER BY RAND() \n"
					+ "	LIMIT [LIMIT]";

			sqlQuery = sqlQuery.replace("[LIMIT]", cantidad);
			sqlQuery = sqlQuery.replace("[TIPO_INVENTARIO]", "1");

			System.out.println("Query ObtenerListaProductoStockRipley(): " + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				EStock oEntidad = new EStock();
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.bodega = rs.getString("FULFILLMENT_CODE");
				oEntidad.tipoInventario = rs.getString("INVENTORY_TYPE");
				oEntidad.stock = rs.getInt("QTY");
				System.out.println("PRODUCTO ENCONTRADO: " + oEntidad.codigoVenta);
				oListaProductos.add(oEntidad);

			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerProductoStockRipley");
			System.err.println(e.getMessage());
		}

		return oListaProductos;
	}

	public List<EStock> ObtenerListaProductoStockRipley(String cantidad, String bodega) {

		List<EStock> oListaProductos = new ArrayList<EStock>();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT * FROM(\n"
					+ "	SELECT t1.COD_VENTA,t2.FULFILLMENT_CODE, t2.INVENTORY_TYPE,t2.QTY FROM SKU t1\n"
					+ "	INNER JOIN SKU_STOCK_RIPLEY t2\n" + "	ON t1.SKU_ID=t2.SKU_ID\n" + "	WHERE t2.QTY>0\n"
					+ "	AND t1.COUNTRY_CODE='CL'\n" + "	AND LENGTH(t1.COD_VENTA)>12\n"
					+ "	AND t2.INVENTORY_TYPE='[TIPO_INVENTARIO]' AND t2.FULFILLMENT_CODE IN ([BODEGAS_HABILITADAS]) \n"
					+ "	LIMIT 10000)A \n" + "	ORDER BY RAND() \n" + "	LIMIT [LIMIT]";

			sqlQuery = sqlQuery.replace("[LIMIT]", cantidad);
			sqlQuery = sqlQuery.replace("[BODEGAS_HABILITADAS]", "'" + bodega + "'");
			sqlQuery = sqlQuery.replace("[TIPO_INVENTARIO]", "1");
			
			System.out.println("Query ObtenerListaProductoStockRipley(): " + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				EStock oEntidad 		= new EStock();
				oEntidad.codigoVenta 	= rs.getString("COD_VENTA");
				oEntidad.bodega 		= rs.getString("FULFILLMENT_CODE");
				oEntidad.tipoInventario	= rs.getString("INVENTORY_TYPE");
				oEntidad.stock 			= rs.getInt("QTY");
				System.out.println("PRODUCTO ENCONTRADO: " + oEntidad.codigoVenta);
				oListaProductos.add(oEntidad);

			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerProductoStockRipley");
			System.err.println(e.getMessage());
		}

		return oListaProductos;
	}

	public EStock ObtenerProductoStockRipleyNegativo(String tipoInventario) {
		EStock oEntidad = new EStock();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT t1.COD_VENTA,t2.FULFILLMENT_CODE, t2.INVENTORY_TYPE,t2.QTY,t2.USERNAME FROM SKU t1\n"
					+ "INNER JOIN SKU_STOCK_RIPLEY t2\n" + "ON t1.SKU_ID=t2.SKU_ID\n" + "WHERE t2.QTY<0\n"
					+ "AND t1.COUNTRY_CODE='CL'\n" + "AND LENGTH(t1.COD_VENTA)>12\n"
					+ "AND t2.INVENTORY_TYPE='[TIPO_INVENTARIO]'  AND t2.FULFILLMENT_CODE IN ([BODEGAS_HABILITADAS]) \n"
					+ "LIMIT 1";

			sqlQuery = sqlQuery.replace("[TIPO_INVENTARIO]", tipoInventario);
			sqlQuery = sqlQuery.replace("[BODEGAS_HABILITADAS]", bodegasHabilitadas);

			System.out.println("Query ObtenerProductoStockRipleyNegativo(): " + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.bodega = rs.getString("FULFILLMENT_CODE");
				oEntidad.tipoInventario = rs.getString("INVENTORY_TYPE");
				oEntidad.stock = rs.getInt("QTY");
				oEntidad.username = rs.getString("USERNAME");
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerProductoStockRipley");
			System.err.println(e.getMessage());
		}

		return oEntidad;

	}

	public EStock ObtenerProductoStockRipleyPositivo(String tipoInventario) {
		EStock oEntidad = new EStock();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT t1.COD_VENTA,t2.FULFILLMENT_CODE, t2.INVENTORY_TYPE,t2.QTY,t2.USERNAME FROM SKU t1\n"
					+ "INNER JOIN SKU_STOCK_RIPLEY t2\n" + "ON t1.SKU_ID=t2.SKU_ID\n" + "WHERE t2.QTY>0\n"
					+ "AND t1.COUNTRY_CODE='CL'\n" + "AND LENGTH(t1.COD_VENTA)>12\n"
					+ "AND t2.INVENTORY_TYPE='[TIPO_INVENTARIO]'  AND t2.FULFILLMENT_CODE IN ([BODEGAS_HABILITADAS]) \n"
					+ "LIMIT 1";

			sqlQuery = sqlQuery.replace("[TIPO_INVENTARIO]", tipoInventario);
			sqlQuery = sqlQuery.replace("[BODEGAS_HABILITADAS]", bodegasHabilitadas);

			System.out.println("Query ObtenerProductoStockRipleyPositivo(): " + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.bodega = rs.getString("FULFILLMENT_CODE");
				oEntidad.tipoInventario = rs.getString("INVENTORY_TYPE");
				oEntidad.stock = rs.getInt("QTY");
				oEntidad.username = rs.getString("USERNAME");
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerProductoStockRipley");
			System.err.println(e.getMessage());
		}

		return oEntidad;

	}

	public EStock ObtenerProductoStockRipleyCero(String tipoInventario) {
		EStock oEntidad = new EStock();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT t1.COD_VENTA,t2.FULFILLMENT_CODE, t2.INVENTORY_TYPE,t2.QTY,t2.USERNAME FROM SKU t1\n"
					+ "INNER JOIN SKU_STOCK_RIPLEY t2\n" + "ON t1.SKU_ID=t2.SKU_ID\n" + "WHERE t2.QTY=0\n"
					+ "AND t1.COUNTRY_CODE='CL'\n" + "AND LENGTH(t1.COD_VENTA)>12\n"
					+ "AND t2.INVENTORY_TYPE='[TIPO_INVENTARIO]' AND t2.FULFILLMENT_CODE IN ([BODEGAS_HABILITADAS]) \n"
					+ "LIMIT 1";

			sqlQuery = sqlQuery.replace("[TIPO_INVENTARIO]", tipoInventario);
			sqlQuery = sqlQuery.replace("[BODEGAS_HABILITADAS]", bodegasHabilitadas);

			System.out.println("Query ObtenerProductoStockRipleyCero():" + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.bodega = rs.getString("FULFILLMENT_CODE");
				oEntidad.tipoInventario = rs.getString("INVENTORY_TYPE");
				oEntidad.stock = rs.getInt("QTY");
				oEntidad.username = rs.getString("USERNAME");
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerProductoStockRipley");
			System.err.println(e.getMessage());
		}

		return oEntidad;

	}

	public EStock ObtenerProductoSinStockRipley() {
		EStock oEntidad = new EStock();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT t1.COD_VENTA,'' FULFILLMENT_CODE, '1' INVENTORY_TYPE,'' QTY, '' USERNAME FROM SKU t1\n"
					+ "LEFT JOIN SKU_STOCK_RIPLEY t2\n" + "ON t1.SKU_ID=t2.SKU_ID\n" + "where LENGTH(t1.COD_VENTA)>12\n"
					+ "and t2.SKU_ID is NULL\n" + "LIMIT 1";

			System.out.println("Query ObtenerProductoSinStockRipley(): " + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.bodega = rs.getString("FULFILLMENT_CODE");
				oEntidad.tipoInventario = rs.getString("INVENTORY_TYPE");
				oEntidad.stock = rs.getInt("QTY");
				oEntidad.username = rs.getString("USERNAME");
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerProductoSinStockRipley");
			System.err.println(e.getMessage());
		}

		return oEntidad;

	}

	public List<ESku> ObtenerCodigoVenta(String codigoPais, int cantidad)
			throws IOException, InterruptedException, SQLException {
		List<ESku> oLista = new ArrayList<ESku>();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;

			String sqlQuery = "SELECT COD_VENTA FROM SKU WHERE COUNTRY_CODE='" + codigoPais
					+ "'  AND LENGTH(COD_VENTA)>11 ORDER BY RAND() LIMIT " + cantidad;

			System.out.println("Query ObtenerCodigoVenta()" + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);
			while (rs.next()) {
				ESku oEntidad	= new ESku();
				oEntidad.sku	= rs.getString("COD_VENTA");
				oLista.add(oEntidad);
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerCodigoVenta");
			System.err.println(e.getMessage());
		}
		return oLista;
	}

	public List<EBodega> ObtenerBodegas(String codigoPais, int cantidad)
			throws IOException, InterruptedException, SQLException {
		List<EBodega> oLista = new ArrayList<EBodega>();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;

			String sqlQuery = "SELECT CODE, NAME FROM LOCATION WHERE COUNTRY_CODE='" + codigoPais + "' LIMIT "
					+ cantidad;

			System.out.println("Query ObtenerBodegas(): " + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);
			while (rs.next()) {
				EBodega oEntidad 		= new EBodega();
				oEntidad.codigoBodega	= rs.getString("CODE");
				oEntidad.nombre 		= rs.getString("NAME");
				oEntidad.codigoPais 	= codigoPais;
				oLista.add(oEntidad);
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerBodegas");
			System.err.println(e.getMessage());
		}
		return oLista;
	}

	public List<ESku> ObtenerSkuConStockActual(String codigoPais, int cantidad)
			throws IOException, InterruptedException, SQLException {

		List<ESku> oLista = new ArrayList<ESku>();
		try {

			String bodega = ObtenerBodegaAlAzar(codigoPais, cantidad);
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;

			String sqlQuery = "SELECT  t1.SKU, t2.FULFILLMENT_CODE, t1.SELLER, t2.INVENTOR"
					+ "Y_TYPE, t2.QTY FROM SKU t1 \r\n" + "INNER JOIN SKU_STOCK_RIPLEY t2 ON t1.SKU_ID=t2.SKU_ID\r\n"
					+ "WHERE t1.COUNTRY_CODE='" + codigoPais + "'\r\n" + "AND t2.FULFILLMENT_CODE='" + bodega + "' \r\n"
					+ "AND LENGTH(t1.SELLER)>0\r\n" + "AND LENGTH(t2.INVENTORY_TYPE)>0 ORDER BY RAND()\r\n" + "LIMIT "
					+ cantidad;

			System.out.println("Obteniendo " + cantidad + " de " + codigoPais + " Sku con Stock actual");
			rs = stmt.executeQuery(sqlQuery);
			while (rs.next()) {
				ESku oEntidad = new ESku();
				oEntidad.sku = rs.getString("SKU");
				oEntidad.bodega = rs.getString("FULFILLMENT_CODE");
				oEntidad.seller = rs.getString("SELLER");
				oEntidad.tipoInventario = rs.getString("INVENTORY_TYPE");
				oEntidad.stock = rs.getString("QTY");
				oEntidad.stockEsDiferente = "0";
				oLista.add(oEntidad);
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerSkuConStockActual");
			System.err.println(e.getMessage());
		}

		return oLista;

	}

	public List<ESku> ObtenerSkuConStockActualYDiferente(String codigoPais, int cantidad)
			throws IOException, InterruptedException, SQLException {

		List<ESku> oLista = new ArrayList<ESku>();
		try {

			String bodega = ObtenerBodegaAlAzar(codigoPais, cantidad);
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;

			String sqlQuery = "SELECT t2.COD_VENTA, t1.FULFILLMENT_CODE, t2.SELLER, t1.INVENTORY_TYPE, \r\n"
					+ "		t1.QTY,IF(RIGHT(t2.COD_VENTA, 1) % 2 <> 0, true, false) STOCK_DIFERENTE\r\n"
					+ "FROM SKU_STOCK_RIPLEY t1\r\n" + "INNER JOIN SKU t2 on t1.SKU_ID=t2.SKU_ID\r\n"
					+ "WHERE t2.COUNTRY_CODE='" + codigoPais + "'\r\n" + "AND LENGTH(t2.SELLER)>0\r\n"
					+ "AND t1.FULFILLMENT_CODE='" + bodega + "'\r\n" + "LIMIT 100";

			System.out.println("Obteniendo " + cantidad + "sku's con stock de la bodega " + bodega + " del pais " + codigoPais);
			rs = stmt.executeQuery(sqlQuery);
			while (rs.next()) {
				ESku oEntidad = new ESku();
				oEntidad.sku = rs.getString("SKU");
				oEntidad.bodega = rs.getString("FULFILLMENT_CODE");
				oEntidad.seller = rs.getString("SELLER");
				oEntidad.tipoInventario = rs.getString("INVENTORY_TYPE");
				oEntidad.stock = rs.getString("QTY");
				oEntidad.stockEsDiferente = rs.getString("STOCK_DIFERENTE");
				oLista.add(oEntidad);
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}

		return oLista;

	}

	private String ObtenerBodegaAlAzar(String codigoPais, int cantidadRegistros) {

		String bodega = "10012";
//		try {
//			Connection conn = ConnectSIC();
//			Statement stmt = conn.createStatement();
//			ResultSet rs;
//
//			String sqlQuery = "SELECT FULFILLMENT_CODE FROM (\r\n"
//					+ "SELECT  t2.FULFILLMENT_CODE, COUNT(t2.FULFILLMENT_CODE) cantidad FROM SKU t1 \r\n"
//					+ "INNER JOIN SKU_STOCK_RIPLEY t2 ON t1.SKU_ID=t2.SKU_ID\r\n"
//					+ "INNER JOIN LOCATION t3 ON t2.FULFILLMENT_CODE=t3.CODE\r\n" + "WHERE t3.COUNTRY_CODE='"
//					+ codigoPais + "'\r\n" + "AND LENGTH(t2.FULFILLMENT_CODE)>0\r\n" + "AND LENGTH(t1.SELLER)>0\r\n"
//					+ "AND t3.NAME NOT LIKE '%NO USAR%' AND LENGTH(t2.INVENTORY_TYPE)>0\r\n"
//					+ "GROUP BY t2.FULFILLMENT_CODE\r\n" + "ORDER BY RAND()\r\n" + ") A\r\n" + "WHERE cantidad>"
//					+ cantidadRegistros + "\r\n" + "LIMIT 1";
//			System.out.println("ObtenerBodegaAlAzar: " + sqlQuery);
//			rs = stmt.executeQuery(sqlQuery);
//			while (rs.next()) {
//				bodega = rs.getString("FULFILLMENT_CODE");
//			}
//
//			if (bodega.equals("")) {
//
//				List<EBodega> oBodegaList = new ArrayList<EBodega>();
//				oBodegaList = ObtenerBodegas(codigoPais, 1);
//				bodega = oBodegaList.get(0).codigoBodega;
//				if (bodega.equals("")) {
//					oBodegaList = ObtenerBodegas(codigoPais, 1);
//				}
//				System.out.println("Obteniendo bodegaaa : " + bodega);
//			}
//			System.out.println(bodega);
//			// conn.close();
//		} catch (Exception e) {
//			System.err.println("Got an exception! - ObtenerBodegaAlAzar ");
//			System.err.println(e.getMessage());
//		}
//		System.out.println("Obteniendo bodega : " + bodega);
//
////		if (bodega == null || bodega.equals("")) {
////			bodega = ObtenerBodegaAlAzar(codigoPais, 1);
////		}

		return bodega;

	}

	public EGrillaAuditoria ObtenerTransaccionBy(
			String codigoPais, 
			String idTrxHeader, 
			String tipoInventario,
			String sistemaOrigen, 
			String estadoTransaccion, 
			String estadoRegistro, 
			String tipoTransaccion)
			throws IOException, InterruptedException, SQLException {

		EGrillaAuditoria oEGrillaAuditoria = new EGrillaAuditoria();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;

			String sqlQuery = "SELECT t1.TRANSACCION_ID, \n" + "		t1.IDTRX_HEADER, \n"
					+ "		t1.SYSTEM_CODE, \n" + "		t2.FULFILLMENT_CODE, \n" + "		t2.SKU, \n"
					+ "		t1.TRANSACTION_TYPE_CODE, \n" + "		t2.TRANSACTION_DOC,\n" + "		t1.USERNAME,\n"
					+ "		t1.STATUS STATUS_CABECERA,\n" + "		t2.STATUS STATUS_DETALLE,\n"
					+ "		t2.DETAIL,\n" + "		t2.INVENTORY_TYPE,\n"
					+ "		DATE_FORMAT(PROCESS_START, '%d-%m-%Y') PROCESS_START,\n"
					+ "		DATE_FORMAT(DATE_ADD(PROCESS_START,INTERVAL -1 DAY), '%d-%m-%Y') PROCESS_PREV,\n"
					+ "		DATE_FORMAT(DATE_ADD(PROCESS_START,INTERVAL 1 DAY), '%d-%m-%Y') PROCESS_NEXT,  \n"
					+ "		t2.PREV_STOCK,\n" + "		t2.STOCK,\n" + "		t2.OPERATION \n"
					+ "FROM TRANSACTION t1\n" + "INNER JOIN TRANSACTION_ITEM_LINE_RIPLEY t2\n"
					+ "ON t1.TRANSACCION_ID=t2.TRANSACCION_ID\n" + "WHERE t2.INVENTORY_TYPE=[INVENTORY_TYPE]\n"
					+ "AND t1.STATUS=[STATUS_HEADER]\n" + "AND t2.STATUS=[STATUS_DETAIL]\n"
					+ "AND t1.COUNTRY_CODE='CL'\n" + "AND TRANSACTION_TYPE_CODE=[TRANSACTION_TYPE_CODE]\n"
					+ "AND SYSTEM_CODE=[SYSTEM_CODE]\n" + "AND t1.IDTRX_HEADER=[IDTRX_HEADER]\n"
					+ "order by 1 desc LIMIT 1";

			sqlQuery = sqlQuery.replace("[INVENTORY_TYPE]", tipoInventario.equals("") ? "t2.INVENTORY_TYPE" : tipoInventario);
			sqlQuery = sqlQuery.replace("[IDTRX_HEADER]", idTrxHeader.equals("") ? "t1.IDTRX_HEADER" : "'" + idTrxHeader + "'");
			sqlQuery = sqlQuery.replace("[SYSTEM_CODE]", sistemaOrigen.equals("") ? "t1.SYSTEM_CODE" : "'" + sistemaOrigen + "'");
			sqlQuery = sqlQuery.replace("[STATUS_HEADER]", estadoTransaccion.equals("") ? "t1.STATUS" : "'" + estadoTransaccion + "'");
			sqlQuery = sqlQuery.replace("[STATUS_DETAIL]", estadoRegistro.equals("") ? "t2.STATUS" : "'" + estadoRegistro + "'");
			sqlQuery = sqlQuery.replace("[TRANSACTION_TYPE_CODE]", tipoTransaccion.equals("") ? "TRANSACTION_TYPE_CODE" : "'" + tipoTransaccion + "'");
			
			System.out.println("Query ObtenerTransaccionBy(): " + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);
			
			while (rs.next()) {
				oEGrillaAuditoria.transaccionId = rs.getString("TRANSACCION_ID");
				oEGrillaAuditoria.idtrxHeader = rs.getString("IDTRX_HEADER");
				oEGrillaAuditoria.codigoSistema = rs.getString("SYSTEM_CODE");
				oEGrillaAuditoria.codigoBodega = rs.getString("FULFILLMENT_CODE");
				oEGrillaAuditoria.codigoVenta = rs.getString("SKU");
				oEGrillaAuditoria.codigoTipoTransaccion = rs.getString("TRANSACTION_TYPE_CODE");
				oEGrillaAuditoria.documentoTransaccion = rs.getString("TRANSACTION_DOC");
				oEGrillaAuditoria.username = rs.getString("USERNAME");
				oEGrillaAuditoria.estadoCabecera = rs.getString("STATUS_CABECERA");
				oEGrillaAuditoria.estadoDetalle = rs.getString("STATUS_DETALLE");
				oEGrillaAuditoria.mensajeDetalle = rs.getString("DETAIL");
				oEGrillaAuditoria.tipoInventario = rs.getString("INVENTORY_TYPE");
				oEGrillaAuditoria.processStart = rs.getString("PROCESS_START");
				oEGrillaAuditoria.processPrev = rs.getString("PROCESS_PREV");
				oEGrillaAuditoria.processNext = rs.getString("PROCESS_NEXT");
				oEGrillaAuditoria.stockAntes = rs.getString("PREV_STOCK");
				oEGrillaAuditoria.stockDespues = rs.getString("STOCK");
				oEGrillaAuditoria.operacion = rs.getString("OPERATION");
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerSkuConStockActual");
			System.err.println(e.getMessage());
		}

		return oEGrillaAuditoria;
	}

	public List<EGrillaAuditoria> ObtenerTransaccionBy(String codigoPais, String idTrxHeader)
			throws IOException, InterruptedException, SQLException {

		List<EGrillaAuditoria> oListGrillaAuditoria = new ArrayList<EGrillaAuditoria>();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;

			String sqlQuery = "SELECT t1.TRANSACCION_ID, \n" + "		t1.IDTRX_HEADER, \n"
					+ "		t1.SYSTEM_CODE, \n" + "		t2.FULFILLMENT_CODE, \n" + "		t2.SKU, \n"
					+ "		t1.TRANSACTION_TYPE_CODE, \n" + "		t2.TRANSACTION_DOC,\n" + "		t1.USERNAME,\n"
					+ "		t1.STATUS STATUS_CABECERA,\n" + "		t2.STATUS STATUS_DETALLE,\n"
					+ "		t2.DETAIL,\n" + "		t2.INVENTORY_TYPE,\n"
					+ "		DATE_FORMAT(PROCESS_START, '%d-%m-%Y') PROCESS_START,\n"
					+ "		DATE_FORMAT(DATE_ADD(PROCESS_START,INTERVAL -1 DAY), '%d-%m-%Y') PROCESS_PREV,\n"
					+ "		DATE_FORMAT(DATE_ADD(PROCESS_START,INTERVAL 1 DAY), '%d-%m-%Y') PROCESS_NEXT,  \n"
					+ "		t2.PREV_STOCK,\n" + "		t2.STOCK,\n" + "		t2.OPERATION \n"
					+ "FROM TRANSACTION t1\n" + "INNER JOIN TRANSACTION_ITEM_LINE_RIPLEY t2\n "
					+ "ON t1.TRANSACCION_ID=t2.TRANSACCION_ID\n"
					+ "WHERE t1.COUNTRY_CODE='[PAIS]' AND t1.IDTRX_HEADER=[IDTRX_HEADER]\n" + "order by 1 desc";
			sqlQuery = sqlQuery.replace("[IDTRX_HEADER]",
					idTrxHeader.equals("") ? "t1.IDTRX_HEADER" : "'" + idTrxHeader + "'");
			sqlQuery = sqlQuery.replace("[PAIS]", idTrxHeader.equals("") ? "t1.COUNTRY_CODE" : codigoPais);

			System.out.println("Query ObtenerTransaccionBy(): " + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);
			while (rs.next()) {
				EGrillaAuditoria oEGrillaAuditoria = new EGrillaAuditoria();
				oEGrillaAuditoria.transaccionId = rs.getString("TRANSACCION_ID");
				oEGrillaAuditoria.idtrxHeader = rs.getString("IDTRX_HEADER");
				oEGrillaAuditoria.codigoSistema = rs.getString("SYSTEM_CODE");
				oEGrillaAuditoria.codigoBodega = rs.getString("FULFILLMENT_CODE");
				oEGrillaAuditoria.codigoVenta = rs.getString("SKU");
				oEGrillaAuditoria.codigoTipoTransaccion = rs.getString("TRANSACTION_TYPE_CODE");
				oEGrillaAuditoria.documentoTransaccion = rs.getString("TRANSACTION_DOC");
				oEGrillaAuditoria.username = rs.getString("USERNAME");
				oEGrillaAuditoria.estadoCabecera = rs.getString("STATUS_CABECERA");
				oEGrillaAuditoria.estadoDetalle = rs.getString("STATUS_DETALLE");
				oEGrillaAuditoria.mensajeDetalle = rs.getString("DETAIL");
				oEGrillaAuditoria.tipoInventario = rs.getString("INVENTORY_TYPE");
				oEGrillaAuditoria.processStart = rs.getString("PROCESS_START");
				oEGrillaAuditoria.processPrev = rs.getString("PROCESS_PREV");
				oEGrillaAuditoria.processNext = rs.getString("PROCESS_NEXT");
				oEGrillaAuditoria.stockAntes = rs.getString("PREV_STOCK");
				oEGrillaAuditoria.stockDespues = rs.getString("STOCK");
				oEGrillaAuditoria.operacion = rs.getString("OPERATION");
				oListGrillaAuditoria.add(oEGrillaAuditoria);
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerSkuConStockActual");
			System.err.println(e.getMessage());
		}

		return oListGrillaAuditoria;
	}

	public Connection ConnectSIC() {
		return MySqlDbHelper.getConnection();

	}

	public List<ESku> ObtenerDatosIncrementalProducto(String codigoPais, String cantidad)
			throws IOException, InterruptedException, SQLException {

		List<ESku> oLista = new ArrayList<ESku>();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;

			String sqlQuery = "SELECT * FROM(SELECT SKU,COD_VENTA,STYLE,SUBLINE_CODE,LINE_CODE,DEPTO_CODE, NAME,COUNTRY_CODE\n"
					+ "			  FROM SKU WHERE LENGTH(COD_VENTA)=13 AND COUNTRY_CODE='[CODIGO_PAIS]' LIMIT 10000 ) A\n"
					+ "ORDER BY RAND()\n" + "LIMIT [CANTIDAD]";

			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);
			sqlQuery = sqlQuery.replace("[CANTIDAD]", cantidad);
			
			System.out.println("Query ObtenerDatosIncrementalProducto(): " + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);
			
			while (rs.next()) {
				ESku oEntidad = new ESku();
				oEntidad.sku = rs.getString("SKU");
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.style = rs.getString("STYLE");
				oEntidad.sublineCode = rs.getString("SUBLINE_CODE");
				oEntidad.lineCode = rs.getString("LINE_CODE");
				oEntidad.deptoCode = rs.getString("DEPTO_CODE");
				oEntidad.description = rs.getString("NAME");
				oEntidad.countryCode = rs.getString("COUNTRY_CODE");
				oLista.add(oEntidad);
			}
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerDatosIncrementalProducto()");
			System.err.println(e.getMessage());
		}
		return oLista;

	}

	public ESku ObtenerInformacionJerarquiaProducto(String codigoVenta, String codigoPais)
			throws IOException, InterruptedException, SQLException {

		ESku oEntidad = new ESku();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;

			String sqlQuery = "SELECT SKU,COD_VENTA,STYLE,SUBLINE_CODE,LINE_CODE,DEPTO_CODE, NAME,COUNTRY_CODE\n"
					+ "			  FROM SKU WHERE COD_VENTA='[COD_VENTA]' AND COUNTRY_CODE='[CODIGO_PAIS]'";

			sqlQuery = sqlQuery.replace("[COD_VENTA]", codigoVenta);
			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);

			System.out.println("Query ObtenerInformacionJerarquiaProducto(): " + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);
			
			while (rs.next()) {
				oEntidad.sku = rs.getString("SKU");
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.style = rs.getString("STYLE");
				oEntidad.sublineCode = rs.getString("SUBLINE_CODE");
				oEntidad.lineCode = rs.getString("LINE_CODE");
				oEntidad.deptoCode = rs.getString("DEPTO_CODE");
				oEntidad.description = rs.getString("NAME");
				oEntidad.countryCode = rs.getString("COUNTRY_CODE");
			}
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerInformacionJerarquiaProducto()");
			System.err.println(e.getMessage());
		}
		return oEntidad;
	}

}
