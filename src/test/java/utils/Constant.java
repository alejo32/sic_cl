package utils;


public class Constant {

	/**Config Properties file **/
	public final static String CONFIG_PROPERTIES_DIRECTORY = "parametros.properties";
	
	public final static String GECKO_DRIVER_DIRECTORY = System.getProperty("user.dir") + "\\src\\test\\resources\\other\\webDriver\\geckodriver.exe";
	
	public final static String CHROME_DRIVER_DIRECTORY = System.getProperty("user.dir") + "\\src\\test\\resources\\other\\webDriver\\chromedriver.exe";
	
	public final static String IE_DRIVER_DIRECTORY = System.getProperty("user.dir") + "\\src\\test\\resources\\other\\webDriver\\IEDriverServer.exe";
	
	
	/** Util **/
	public final static String FORMATO_FECHA1 = "yyyyMMddHHmmss";
	
	public final static String FORMATO_FECHA2 = "yyyy-MM-dd HH:mm:ss";

	
}
