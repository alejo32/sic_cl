package utils;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class SFTPClientUtil {
	
	private Session session = null;
	
	 public void Connect(String host, int port, String user, String password) throws JSchException {
	      JSch jsch = new JSch();
         session = jsch.getSession(user, host, port);
         session.setPassword(password);

         session.setConfig("StrictHostKeyChecking", "no");
         session.connect();
     }
	 
	 public void Upload(String host, String source, String destination) throws JSchException, SftpException {
		System.out.println("Uploading file from "+source+" to "+host + " "+ destination);
		Channel channel = session.openChannel("sftp");
		channel.connect();
		ChannelSftp sftpChannel = (ChannelSftp) channel;
		sftpChannel.put(source, destination);
		sftpChannel.exit();
		System.out.println("done!");
		
	  }
	 
	 public void Download(String source, String destination) throws JSchException, SftpException {
	      Channel channel = session.openChannel("sftp");
	      channel.connect();
	      ChannelSftp sftpChannel = (ChannelSftp) channel;
	      sftpChannel.get(source, destination);
	      sftpChannel.exit();
	  }
	 
	 public void Disconnect() {
	      if (session != null) {
	          session.disconnect();
	      }
	  }
	 
}
