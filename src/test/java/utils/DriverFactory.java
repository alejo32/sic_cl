package utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

import pageObjects.aws.SQSPage;
import pageObjects.sic.AjusteStockPage;
import pageObjects.sic.AuditoriaPage;
import pageObjects.sic.CargaStockPage;
import pageObjects.sic.ConfiguracionPage;
import pageObjects.sic.CuadraturaPage;
import pageObjects.sic.LoginPage;
import pageObjects.sic.MenuPage;
import pageObjects.sic.StockProtegidoPage;
import pageObjects.sic.JerarquiaPage;

public class DriverFactory {
	public static WebDriver driver;
	public static MenuPage menuPage;
	public static LoginPage loginPage;
	public static AuditoriaPage auditoriaPage;
	public static JerarquiaPage jerarquiaPage;
	public static CargaStockPage cargaStockPage;
	public static CuadraturaPage cuadraturaPage;
	public static AjusteStockPage ajusteStockPage;
	public static ConfiguracionPage configuracionPage;
	public static pageObjects.aws.LoginPage awsLoginPage;
	public static SQSPage awsSQSPage;
	public static StockProtegidoPage	stockProtegidoPage;
//	public static CargaProtegidoPage	cargaProtegidoPage;

	public static DatabaseUtil database;
	public static JsonUtil tramaJson;
	public static ApiUtil servicio;
	public static ArchivosUtil archivo;
	public static SFTPClientUtil sftpClient;

	
	
	
	@SuppressWarnings("deprecation")
	public WebDriver getDriver() {
		try {
			String browserName = GetBrowser();
			System.out.println("BROWSER_NAME: " + browserName);

			browserName = System.getProperty("browser", "chrome");

			switch (browserName) {

				case "firefox":
					if (null == driver) {
						System.setProperty("webdriver.gecko.driver", Constant.GECKO_DRIVER_DIRECTORY);
						DesiredCapabilities capabilities = DesiredCapabilities.firefox();
						capabilities.setCapability("marionette", true);
						driver = new FirefoxDriver();
					}
					break;

				case "chrome":
					if (null == driver) {
						System.out.println("Entrando a chrome");
						System.setProperty("webdriver.chrome.driver", Constant.CHROME_DRIVER_DIRECTORY);
						driver = new ChromeDriver();
						driver.manage().window().maximize();
					}
					break;

				case "server":
					if (null == driver) {
						System.out.println("Entrando a server");

						System.setProperty("webdriver.chrome.driver", Constant.CHROME_DRIVER_DIRECTORY);

						ChromeOptions cop = new ChromeOptions();
						cop.addArguments("--headless");
						cop.addArguments("--disable-gpu");
						cop.addArguments("--start-maximized");
						cop.addArguments("--no-sandbox");
						cop.addArguments("--ignore-certificate-errors");
						cop.addArguments("--disable-popup-blocking");
						cop.addArguments("--window-size=1920,1080");
						cop.addArguments("--disable-dev-shm-usage");
						cop.addArguments("--incognito");

						try {
							driver = new RemoteWebDriver(new URL("http://10.0.149.90:4444/wd/hub"), cop);
						} catch (MalformedURLException e) {
							e.printStackTrace();
						}
					}
					break;

				case "ie":
					if (null == driver) {
						DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
						System.setProperty("webdriver.ie.driver", Constant.IE_DRIVER_DIRECTORY);
						capabilities.setCapability("ignoreZoomSetting", true);
						driver = new InternetExplorerDriver(capabilities);
						driver.manage().window().maximize();
					}
					break;
			}
		} catch (Exception e) {
			System.out.println("No se cargo browser:" + e.getMessage());
		} finally {
			driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
			loginPage 			= PageFactory.initElements(driver, LoginPage.class);
			menuPage 			= PageFactory.initElements(driver, MenuPage.class);
			cuadraturaPage 		= PageFactory.initElements(driver, CuadraturaPage.class);
			auditoriaPage 		= PageFactory.initElements(driver, AuditoriaPage.class);
			jerarquiaPage 		= PageFactory.initElements(driver, JerarquiaPage.class);
			ajusteStockPage 	= PageFactory.initElements(driver, AjusteStockPage.class);
			stockProtegidoPage	= PageFactory.initElements(driver, StockProtegidoPage.class);
			cargaStockPage 		= PageFactory.initElements(driver, CargaStockPage.class);
//			cargaProtegidoPage	= PageFactory.initElements(driver, CargaProtegidoPage.class);
			configuracionPage 	= PageFactory.initElements(driver, ConfiguracionPage.class);
			awsLoginPage 		= PageFactory.initElements(driver, pageObjects.aws.LoginPage.class);
			awsSQSPage 			= PageFactory.initElements(driver, pageObjects.aws.SQSPage.class);
			database			= new DatabaseUtil();
			tramaJson			= new JsonUtil();
			servicio			= new ApiUtil();
			archivo				= new ArchivosUtil();
		}
		return driver;
	}

	public String GetBrowser() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("browser");
	}

	public String GetSICUrl() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_url");
	}
	
	public String GetAWSUrl() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("aws_url");
	}
	

	public String GetSICUsuarioAdministrador() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_usuario_administrador");
	}

	public String GetSICPasswordAdministrador() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_password_administrador");
	}

	public String GetSICUsuarioEstandar() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_usuario_estandar");
	}

	public String GetSICPasswordEstandar() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_password_estandar");
	}

	public String GetSICCodigoPais() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("codigo_pais");
	}

	public String GetAWSAccountId() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("aws_accountId");
	}

	public String GetAWSUsuario() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("aws_username");
	}

	public String GetAWSPassword() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("aws_password");
	}
	public String GetSQSPMM() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("aws_mensaje_SQS_ajuste_PMM");
	}
	public String GetSQSBGT() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("aws_mensaje_SQS_ajuste_BGT");
	}
	public String GetSQSBO() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("aws_mensaje_SQS_ajuste_BO");
	}
	public String GetAWSUrlMensajeSQSAjuste() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("aws_mensaje_SQS_ajuste");
	}

	public String GetAWSUrlMensajeSQSIncrementalProductos() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("aws_mensaje_SQS_incremental_productos");
	}

	public String GetSFTPHost() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sftp_host");
	}

	public String GetSFTPPort() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sftp_port");
	}

	public String GetSFTPUser() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sftp_user");
	}

	public String GetSFTPPassword() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sftp_password");
	}

	public String GetSSHUser() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_ssh_user");
	}

	public String GetSSHPpk() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_ssh_ppk");
	}

	public String GetSSHHost() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_ssh_host");
	}

	public String GetSSHPort() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_ssh_port");
	}

	public String GetDBHost() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_db_host");
	}

	public String GetDBPort() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_db_port");
	}

	public String GetDBUser() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_db_user");
	}

	public String GetDBPassword() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_db_password");
	}

	public String GetAPIConnectAjuste() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("url_api_connect_ajuste");
	}

	public String GetAPIWrapperAjuste() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("url_wrapper_ajuste");
	}

	public String GetAPIConnectSFULL() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("url_api_connect_sfull");
	}

	public String GetAPIWrapperSFULL() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("url_wrapper_sfull");
	}

	public String GetRutaCarpetaLocalDescargas() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("ruta_descarga_archivos");
	}

	public String GetRutaCarpetaLocalSFULL() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sftp_sfull_path_local");
	}

	public String GetRutaCarpetaLocalMDADJUST() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("mdadjust_path_local");
	}
	public String GetRutaCarpetaLocalCMSP() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("cmsp_path_local");
	}

	public String GetRutaCarpetaRemotaSFULL() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sftp_sfull_path_destino");
	}
	
	private String tipoAjuste;

	public String GetTipoAjuste() {
		return tipoAjuste;
	}

	public void SetTipoAjuste(String tipoAjuste) {
		this.tipoAjuste = tipoAjuste;
	}	
}
