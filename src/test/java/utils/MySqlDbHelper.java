package utils;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.JSchException;


public class MySqlDbHelper {
		
	private static Connection conn;
	private boolean done=false;    
	
	public static void setConnection(Connection conn) {
		MySqlDbHelper.conn = conn;
	}


	public static Connection getConnection(){
		return conn;
	}

	public static void closeConnection() throws SQLException{
		conn.close();
	}

//	/* Conexion a BBDD con tunel PPK (avatar) */	
//	public Connection ConnectDbWithTunnel(String strSshUser,
//											String strSshPassword,
//											String strSshHost,
//											int nSshPort,
//											String strRemoteHost,
//											int nRemotePort,
//											String strDbUser,
//											String strDbPassword
//											) throws SQLException {		
//		Connection conn = null;
//		
//		try
//	    {				
//			
//			
//	      int nLocalPort = 3324;         
//	      //int nLocalPort = ThreadLocalRandom.current().nextInt(1, 99999);// local port number use to bind SSH tunnel
//	      try {
//	      doSshTunnel(strSshUser, strSshPassword, strSshHost, nSshPort, strRemoteHost, nLocalPort, nRemotePort);
//	      }catch(Exception ex) {
//	    	  System.out.println("OCURRIO UN ERROR: "+ex.getMessage());
//	      }
//	      done = true;
//	      Class.forName("com.mysql.cj.jdbc.Driver");
//	      conn = DriverManager.getConnection("jdbc:mysql://"+strSshHost+":"+nSshPort+"/SIC", strDbUser, strDbPassword);
//	      
//	    }
//	    catch( Exception e )
//	    {
//	      e.printStackTrace();
//	    }
//		
//	    	return conn;
//	}
	
	/* Conexion a BBDD con tunel PPK (avatar) */	
	public Connection ConnectDbWithTunnel(String strSshHost, int dbPort, String strDbUser, String strDbPassword) throws SQLException {		
		Connection conn = null;		
		try {
			
			int nLocalPort = 3306;         
			//int nLocalPort = ThreadLocalRandom.current().nextInt(1, 99999);// local port number use to bind SSH tunnel
			try {
				doSshTunnel(strSshHost, dbPort, strDbUser, strDbPassword);
			}catch(Exception ex) {
				System.out.println("OCURRIO UN ERROR: "+ex.getMessage());
			}
			done = true;
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://"+strSshHost+":"+dbPort+"/SIC", strDbUser, strDbPassword);	      
	    }
	    catch( Exception e )
	    {
	      e.printStackTrace();
	    }
		
	    	return conn;
	}
	
	private void doSshTunnel( String strSshHost, int dbPort, String strDbUser, String strDbPassword ) throws JSchException
	  {
	    final JSch jsch = new JSch();
	    Session session = jsch.getSession( strDbUser, strSshHost, 22 );
	    
//        jsch.addIdentity(sshKeyFilePath);
	    final Properties config = new Properties();
	    config.put( "StrictHostKeyChecking", "no" );
        config.put("ConnectionAttempts", "3");
        session.setConfig(config);
	    
        int nLocalPort = 3306; 
        
	    session.connect();
	    session.setPortForwardingL(nLocalPort, strSshHost, dbPort);
	    
	  }
	
	public void CloseDbConnection(Connection conn) throws SQLException {
		try
	    {
			conn.close();
	    }
	    catch( Exception e )
	    {
	      e.printStackTrace();
	    }
	    finally
	    {
	      System.exit(0);
	    }
	}
	
	

}
