package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Test;

public class ConnectBBDD {
	
	@Test
	public void testBD() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		System.out.println("Driver Loaded");
		
		Connection con = DriverManager.getConnection("jdbc:mysql://dbsicclqa.cgauft9vseph.us-east-1.rds.amazonaws.com:3306/SIC","sicqacladm","sicqadm2019");
		
		System.out.println("Conectado a MySQL BBDD");
		
		Statement stm = con.createStatement();
		
		ResultSet res = stm.executeQuery("select * from SIC.SKU limit 10");
		
		while(res.next()) {
			String SKUres = res.getString("SKU");
			System.out.println("Campo SKU:" + SKUres);
		}
	}
	
	
}
