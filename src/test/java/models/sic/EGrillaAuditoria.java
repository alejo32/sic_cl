package models.sic;

public class EGrillaAuditoria {

	public String transaccionId;
	public String idtrxHeader;
	public String codigoSistema;
	public String codigoBodega;
	public String codigoVenta;
	public String codigoTipoTransaccion;
	public String documentoTransaccion;
	public String username;
	public String estadoCabecera;
	public String estadoDetalle;
	public String mensajeDetalle;
	public String stockAntes;
	public String stockDespues;
	public String operacion;
	public String tipoInventario;
	public String processStart;
	public String processPrev;
	public String processNext;	

}