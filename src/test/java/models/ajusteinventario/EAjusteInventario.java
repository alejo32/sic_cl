package models.ajusteinventario;



import java.util.List;

public class EAjusteInventario {

	public String transactionId;  
	public String transactionType;
	public String originSystem;
	public String masterReport;
	public String masterDetail;
	public String user;
	public String registration;
	public String country;
	public List<EAjusteInventarioSku> skus;	
	
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void ESFull(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getOriginSystem() {
		return originSystem;
	}
	public void setOriginSystem(String originSystem) {
		this.originSystem = originSystem;
	}
	public String getMasterReport() {
		return masterReport;
	}
	public void setMasterReport(String masterReport) {
		this.masterReport = masterReport;
	}
	public String getMasterDetail() {
		return masterDetail;
	}
	public void setMasterDetail(String masterDetail) {
		this.masterDetail = masterDetail;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getRegistration() {
		return registration;
	}
	public void setRegistration(String registration) {
		this.registration = registration;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public List<EAjusteInventarioSku> getSkus() {
		return skus;
	}
	public void setSkus(List<EAjusteInventarioSku> skus) {
		this.skus = skus;
	}


}