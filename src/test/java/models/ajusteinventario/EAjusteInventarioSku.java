package models.ajusteinventario;

public class EAjusteInventarioSku {

	public String idtrx;
	public String sku;
	public String fulfillment;
	public String inventoryType;
	public int stock;
	public String operation;
	public String cud;
	public String fixInventory;
	public String transaction_doc;
	public String referenceId;
	public String referenceId2;
	public String referenceId3;
	public String originProcessDate;
	public String originDescription;
	public String comercialDate;

	public String getIdtrx() {
		return idtrx;
	}

	public void setIdtrx(String idtrx) {
		this.idtrx = idtrx;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getFulfillment() {
		return fulfillment;
	}

	public void setFulfillment(String fulfillment) {
		this.fulfillment = fulfillment;
	}

	public String getInventoryType() {
		return inventoryType;
	}

	public void setInventoryType(String inventoryType) {
		this.inventoryType = inventoryType;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getCud() {
		return cud;
	}

	public void setCud(String cud) {
		this.cud = cud;
	}

	public String getFixInventory() {
		return fixInventory;
	}

	public void setFixInventory(String fixInventory) {
		this.fixInventory = fixInventory;
	}

	public String getTransaction_doc() {
		return transaction_doc;
	}

	public void setTransaction_doc(String transaction_doc) {
		this.transaction_doc = transaction_doc;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getReferenceId2() {
		return referenceId2;
	}

	public void setReferenceId2(String referenceId2) {
		this.referenceId2 = referenceId2;
	}

	public String getReferenceId3() {
		return referenceId3;
	}

	public void setReferenceId3(String referenceId3) {
		this.referenceId3 = referenceId3;
	}

	public String getOriginProcessDate() {
		return originProcessDate;
	}

	public void setOriginProcessDate(String originProcessDate) {
		this.originProcessDate = originProcessDate;
	}

	public String getOriginDescription() {
		return originDescription;
	}

	public void setOriginDescription(String originDescription) {
		this.originDescription = originDescription;
	}

	public String getComercialDate() {
		return comercialDate;
	}

	public void setComercialDate(String comercialDate) {
		this.comercialDate = comercialDate;
	}

}