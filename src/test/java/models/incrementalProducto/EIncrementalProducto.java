package models.incrementalProducto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EIncrementalProducto {

	@JsonProperty("skus")
	public List<EIncrementalProductoSku> skus;

	@JsonProperty("MessageHeader")
	public EIncrementalProductoMessageHeader messageHeader;

}