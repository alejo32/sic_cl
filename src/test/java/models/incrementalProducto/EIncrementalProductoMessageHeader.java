package models.incrementalProducto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EIncrementalProductoMessageHeader {

	@JsonProperty("Source")
	public String source;
	
	@JsonProperty("Country")
	public String country;

}