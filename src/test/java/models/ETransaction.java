package models;

public class ETransaction {
	public String transactionId;
	public String idTrx;
	public String fileName;
	public String status;
	public String totalItems;
    public String successItems;
    public String duplicatedItems;
    public String unsentItems;
    public String errorItems;
    public String unchangedItems;
    public String countryCode;
    public String comments;
}
