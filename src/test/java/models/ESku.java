package models;

public class ESku {

	public String sku;  
	public String bodega;
	public String seller;
	public String tipoInventario;
	public String stock;
	public int iStock;
	public String stockDespues;
	public String stockEsDiferente;
	public String esImpar;

	public String codigoVenta;
	public String style;
	public String sublineCode;
	public String lineCode;
	public String deptoCode;
	public String description;
	public String countryCode;

}