package models.sincronizacionfull;

public class ESFull {

	public String transactionId;  
	public String transactionType;
	public String originSystem;
	public String masterReport;
	public String masterDetail;
	public String user;
	public String registration;
	public String fileName;
	public String country;

}