package stepDefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.BasePage;
import utils.DriverFactory;

public class StockProtegidoSteps extends DriverFactory {
	
	@Given("^En combobox Departamento selecciono opción Escolar$")
	public void en_combobox_Departamento_selecciono_opción_Escogida() throws Throwable {
	    stockProtegidoPage.selDepto();
	    BasePage.WaitSleep(1);
	    stockProtegidoPage.selDeptoEsc();
	    BasePage.WaitSleep(2);
	    BasePage.TakeScreenShot();
	    stockProtegidoPage.btnCOnsultaDis();
	}

	@When("^Presiono el botón Consultar Stock Protegido$")
	public void presiono_el_botón_Consultar_Stock_Protegido() throws Throwable {
	    stockProtegidoPage.btnConsultar();
	    BasePage.WaitSleep(2);
	    BasePage.TakeScreenShot();
	}

	@Then("^Se despliega grilla Configuraciones Activas y Vigentes$")
	public void se_despliega_grilla_Configuraciones_Activas_y_Vigentes() throws Throwable {
	    stockProtegidoPage.scrollConfig();
	    BasePage.WaitSleep(2);
	    BasePage.TakeScreenShot();
	}
	
	@When("^Se presiona el Botón Nuevo Evento$")
	public void se_presiona_el_Botón_Nuevo_Evento() throws Throwable {
	    stockProtegidoPage.btnEvento();
	    BasePage.WaitSleep(2);
	}

	@When("^Se despliega la grilla Configuración de Stock Protegido$")
	public void se_despliega_la_grilla_Configuración_de_Stock_Protegido() throws Throwable {
	    stockProtegidoPage.scrollStock();
	    BasePage.WaitSleep(2);
	    BasePage.TakeScreenShot();
	}

	@Then("^Ingresamos una fecha de inicio válida$")
	public void ingresamos_una_fecha_de_inicio_válida() throws Throwable {
		stockProtegidoPage.inpFecIni();
		BasePage.WaitSleep(2);
	    stockProtegidoPage.fechaInicio();
	    BasePage.WaitSleep(5);
	}

	@Then("^Ingresamos una fecha de termino válida$")
	public void ingresamos_una_fecha_de_termino_válida() throws Throwable {
	    stockProtegidoPage.inpFecFin();
	    BasePage.WaitSleep(2);
	    stockProtegidoPage.fechaFin();
	    BasePage.WaitSleep(2);
	}

	@Then("^Ingresamos Stock para Nivel Uno$")
	public void ingresamos_Stock_para_Nivel_Uno() throws Throwable {
	    stockProtegidoPage.inpLevel1();
	    BasePage.WaitSleep(1);
	}

	@Then("^Ingresamos Stock para Nivel Dos$")
	public void ingresamos_Stock_para_Nivel_Dos() throws Throwable {
	    stockProtegidoPage.inpLevel2();
	    BasePage.WaitSleep(1);
	}

	@Then("^Presionamos el Botón Guardar$")
	public void presionamos_el_Botón_Guardar() throws Throwable {
		stockProtegidoPage.btnGuardar();
	    BasePage.WaitSleep(3);
	    BasePage.TakeScreenShot();
	}

	@Then("^Se despliega Pop Up con mensaje de alerta para Confirmar Stock$")
	public void se_despliega_Pop_Up_con_mensaje_de_alerta_para_Confirmar_Stock() throws Throwable {
	    stockProtegidoPage.btnConfirmar();
	    BasePage.WaitSleep(2);
	}

	@Then("^se despliega mensaje de alerta Confirmando$")
	public void se_despliega_mensaje_de_alerta_Confirmando() throws Throwable {
		BasePage.TakeScreenShot();
	}
	
	@When("^Se presiona el Botón Editar$")
	public void se_presiona_el_Botón_Editar() throws Throwable {
	    stockProtegidoPage.rdbEditar();
	    BasePage.WaitSleep(1);
	    stockProtegidoPage.btnEditar();
	    BasePage.WaitSleep(2);
	    BasePage.TakeScreenShot();
	}

	@Then("^Editamos Stock para Nivel Uno$")
	public void editamos_Stock_para_Nivel_Uno() throws Throwable {
	    stockProtegidoPage.editLevel1();
	    BasePage.WaitSleep(1);
	}

	@Then("^Editamos Stock para Nivel Dos$")
	public void editamos_Stock_para_Nivel_Dos() throws Throwable {
	    stockProtegidoPage.editLevel2();
	    BasePage.WaitSleep(1);
	}
	
	@When("^Se presiona el Botón Eliminar$")
	public void se_presiona_el_Botón_Eliminar() throws Throwable {
		stockProtegidoPage.rdbEditar();
		BasePage.WaitSleep(1);
	    stockProtegidoPage.btnEliminar();
	    BasePage.WaitSleep(2);
	    BasePage.TakeScreenShot();
	}
	
	 /* SIC_MSP_001 */
	@Given("^Valido la existencia de comobobox Tipo de Producto$")
	public void valido_la_existencia_de_comobobox_Tipo_de_Producto() throws Throwable {
	    stockProtegidoPage.selTipoProd();
	    BasePage.WaitSleep(2);
	    BasePage.TakeScreenShot();
	}

	@Then("^Se encuentra seleccionada por defecto la opción Ripley$")
	public void se_encuentra_seleccionada_por_defecto_la_opción_Ripley() throws Throwable {
	    stockProtegidoPage.selTipoProducto();
	    BasePage.WaitSleep(2);
	    BasePage.TakeScreenShot();
	}
	
	/*  */
	@Given("^Valido la existencia de campo Departamento$")
	public void valido_la_existencia_de_campo_Departamento() throws Throwable {
	    stockProtegidoPage.selDepartamento();
	    BasePage.WaitSleep(2);
	}

	@When("^Campo Departamento se encuentra vacío$")
	public void campo_Departamento_se_encuentra_vacío() throws Throwable {
	    stockProtegidoPage.selDepto();
	    BasePage.WaitSleep(2);
	    BasePage.TakeScreenShot();
	}

	@Then("^Permite seleccionar un Departamento$")
	public void permite_seleccionar_un_Departamento() throws Throwable {
	    stockProtegidoPage.selDeptoEsc();
	    BasePage.WaitSleep(2);
	    BasePage.TakeScreenShot();
	}
	
	@Given("^Valido la existencia del campo Línea$")
	public void valido_la_existencia_del_campo_Línea() throws Throwable {
	    stockProtegidoPage.txtLinea();
	    BasePage.WaitSleep(1);
	}

	@Then("^No permite seleccionar un valor de Linea$")
	public void no_permite_seleccionar_un_valor_de_Linea() throws Throwable {
		BasePage.TakeScreenShot();
	}

	@Given("^Valido la existencia del campo Sub Línea$")
	public void valido_la_existencia_del_campo_Sub_Línea() throws Throwable {
	    stockProtegidoPage.txtSubLinea();
	    BasePage.WaitSleep(1);
	}

	@Then("^No permite seleccionar un valor de Sub Linea$")
	public void no_permite_seleccionar_un_valor_de_Sub_Linea() throws Throwable {
	    BasePage.TakeScreenShot();
	}

	@Given("^Valido la existencia del campo Cod\\. Venta$")
	public void valido_la_existencia_del_campo_Cod_Venta() throws Throwable {
	    stockProtegidoPage.txtCodVenta();
	    BasePage.WaitSleep(1);
}

	@Then("^No permite seleccionar un valor de Cod\\. Venta$")
	public void no_permite_seleccionar_un_valor_de_Cod_Venta() throws Throwable {
	    BasePage.TakeScreenShot();
	}

	@Given("^Valido la existencia del botón Consultar$")
	public void valido_la_existencia_del_botón_Consultar() throws Throwable {
	    stockProtegidoPage.btnCOnsulta();
	    BasePage.WaitSleep(1);
	    BasePage.TakeScreenShot();
	}

	@Then("^El botón Consultar se encuentra deshabilitado$")
	public void el_botón_Consultar_se_encuentra_deshabilitado() throws Throwable {
	    stockProtegidoPage.btnCOnsultaDis();
	    BasePage.WaitSleep(1);
	    BasePage.TakeScreenShot();
	}
	
	@Given("^Valido la existencia Grilla configuraciones$")
	public void valido_la_existencia_Grilla_configuraciones() throws Throwable {
	    stockProtegidoPage.grillConfig();
	    BasePage.WaitSleep(1);
	    
	}

	@Then("^La grilla se despliega sin registros$")
	public void la_grilla_se_despliega_sin_registros() throws Throwable {
		BasePage.TakeScreenShot();
	}

	@Given("^Valido la existencia de Botón Nuevo Evento$")
	public void valido_la_existencia_de_Botón_Nuevo_Evento() throws Throwable {
	    stockProtegidoPage.btnEventoDis();
	    BasePage.WaitSleep(1);
	}

	@Then("^El botón Nuevo Evento se encuentra deshabilitado$")
	public void el_botón_Nuevo_Evento_se_encuentra_deshabilitado() throws Throwable {
	    BasePage.TakeScreenShot();
	}

	@Given("^Valido la existencia de Botón Editar$")
	public void valido_la_existencia_de_Botón_Editar() throws Throwable {
	    stockProtegidoPage.btnEditarDis();
	    BasePage.WaitSleep(1);
	}

	@Then("^El botón Editar se encuentra deshabilitado$")
	public void el_botón_Editar_se_encuentra_deshabilitado() throws Throwable {
	    BasePage.TakeScreenShot();
	}

	@Given("^Valido la existencia de Botón Eliminar$")
	public void valido_la_existencia_de_Botón_Eliminar() throws Throwable {
	    stockProtegidoPage.btnEliminarDis();
	    BasePage.WaitSleep(1);
	}

	@Then("^El botón Eliminar se encuentra deshabilitado$")
	public void el_botón_Eliminar_se_encuentra_deshabilitado() throws Throwable {
	    BasePage.TakeScreenShot();
	}

	@Given("^Se selecciona por defecto Tipo de Producto Ripley$")
	public void se_selecciona_por_defecto_Tipo_de_Producto_Ripley() throws Throwable {
	    stockProtegidoPage.selProducto();
	    BasePage.WaitSleep(2);
	}

	@Then("^Seleccionar Combobox Deptarmento$")
	public void seleccionar_Combobox_Deptarmento() throws Throwable {
	    stockProtegidoPage.selDepto();
	    BasePage.WaitSleep(2);
	}

	@Then("^Se despliegan todos los departamentos existente$")
	public void se_despliegan_todos_los_departamentos_existente() throws Throwable {
	    BasePage.TakeScreenShot();
	}
	
	@Then("^Se habilita el botón Consultar$")
	public void se_habilita_el_botón_Consultar() throws Throwable {
	    stockProtegidoPage.btnConsultarEnable();
	    BasePage.WaitSleep(1);
	    stockProtegidoPage.selProducto();
	    BasePage.WaitSleep(1);
	    BasePage.TakeScreenShot();
	}
	
	@Then("^Se habilita campo Línea$")
	public void se_habilita_campo_Línea() throws Throwable {
	    stockProtegidoPage.txtLinea();
	    BasePage.WaitSleep(1);
	}
	
	@Then("^Selecciono el combobox Departamento$")
	public void selecciono_el_combobox_Departamento() throws Throwable {
	    stockProtegidoPage.selDepto();
	    BasePage.WaitSleep(2);
	    BasePage.TakeScreenShot();
	}
	
	@Then("^Selecciono el combobox Linea$")
	public void selecciono_el_combobox_Linea() throws Throwable {
	    stockProtegidoPage.txtLineaSel();
	    BasePage.WaitSleep(2);
	}

	@Then("^selecciono opción Botas$")
	public void selecciono_opción_Botas() throws Throwable {
	    stockProtegidoPage.txtLineaProd();
	    BasePage.WaitSleep(2);
	    BasePage.TakeScreenShot();
	}
	
	@Then("^Selecciono el combobox Sublinea$")
	public void selecciono_el_combobox_Sublinea() throws Throwable {
	    stockProtegidoPage.txtSubLineaSel();
	    BasePage.WaitSleep(2);
	}

	@Then("^selecciono opción Polerones S(\\d+)$")
	public void selecciono_opción_Polerones_S(int arg1) throws Throwable {
		stockProtegidoPage.txtSubLineaProd();
	    BasePage.WaitSleep(2);
	    BasePage.TakeScreenShot();
	}
	
	@Then("^Elimino departamento seleccionado$")
	public void elimino_departamento_seleccionado() throws Throwable {
	    stockProtegidoPage.delDepto();
	    BasePage.WaitSleep(2);	    
	    BasePage.TakeScreenShot();
	}

	@Then("^Elimino Líneas seleccionadas$")
	public void elimino_Líneas_seleccionadas() throws Throwable {
	    stockProtegidoPage.delLinea();
	    BasePage.WaitSleep(2);	    
	    BasePage.TakeScreenShot();
	}
	
	@Then("^Se habilita campo Cod\\. Venta$")
	public void se_habilita_campo_Cod_Venta() throws Throwable {
	    stockProtegidoPage.clickOut();
	    BasePage.WaitSleep(5);
	    stockProtegidoPage.txtSKU();
	    BasePage.WaitSleep(1);
	    BasePage.TakeScreenShot();
	}
	
	@Then("^Selecciono campo Cod\\. Venta$")
	public void selecciono_campo_Cod_Venta() throws Throwable {
		stockProtegidoPage.txtSKUClick();
	    BasePage.WaitSleep(2);
	}

	@Then("^Ingreso más de un SKU$")
	public void ingreso_más_de_un_SKU() throws Throwable {
		stockProtegidoPage.txtSKUSel();
		BasePage.WaitSleep(2);
		BasePage.TakeScreenShot();
	}
	
	@Then("^Presiono Botón Consultar$")
	public void presiono_Botón_Consultar() throws Throwable {
	    stockProtegidoPage.btnConsultar();
	    BasePage.WaitSleep(2);
	    BasePage.TakeScreenShot();
	}

	@Then("^se despliega mensaje de alerta Debe seleccionar una bodega para continuar\\.$")
	public void se_despliega_mensaje_de_alerta_Debe_seleccionar_una_bodega_para_continuar() throws Throwable {
	    stockProtegidoPage.msjAlerta();
	    BasePage.WaitSleep(2);
	    BasePage.TakeScreenShot();
	}
	
	@When("^Ingreso un SKU$")
	public void ingreso_un_SKU() throws Throwable {
	    stockProtegidoPage.txtSKUUnit();
	    BasePage.WaitSleep(1);
	    BasePage.TakeScreenShot();
	}
	
	@Then("^Mensaje en grilla No se encontraron resultados para los criterios de búsqueda ingresados$")
	public void mensaje_en_grilla_No_se_encontraron_resultados_para_los_criterios_de_búsqueda_ingresados() throws Throwable {
	    stockProtegidoPage.msjGrilla();
	    BasePage.WaitSleep(2);
	    BasePage.TakeScreenShot();
	}
	
	@When("^Permite seleccionar un Departamento con Configuración Activa$")
	public void permite_seleccionar_un_Departamento_con_Configuración_Activa() throws Throwable {
	    stockProtegidoPage.selDeptoActiva();
	    BasePage.WaitSleep(2);
	    BasePage.TakeScreenShot();
	}
	
	@When("^selecciono opción Carteras$")
	public void selecciono_opción_Carteras() throws Throwable {
	    stockProtegidoPage.txtLineaActiva();
	    BasePage.WaitSleep(2);
	    BasePage.TakeScreenShot();
	}
	
	@When("^selecciono opción Cuero S(\\d+)$")
	public void selecciono_opción_Cuero_S(int arg1) throws Throwable {
	    stockProtegidoPage.txtSubLineaActiva();
	    BasePage.WaitSleep(2);
	    BasePage.TakeScreenShot();
	}
	
	@When("^Ingreso un SKU con Configuración Activa$")
	public void ingreso_un_SKU_con_Configuración_Activa() throws Throwable {
	    stockProtegidoPage.txtSKUActivo();
	    BasePage.WaitSleep(2);
	    BasePage.TakeScreenShot();
	}
	
	@Then("^Ingresamos Stock para Nivel Tres$")
	public void ingresamos_Stock_para_Nivel_Tres() throws Throwable {
	    stockProtegidoPage.inpLevel3();
	}

	@Then("^Ingresamos Stock para Nivel Cuatro$")
	public void ingresamos_Stock_para_Nivel_Cuatro() throws Throwable {
	    stockProtegidoPage.inpLevel4();
	}

	@Then("^Ingresamos Stock para Nivel Cinco$")
	public void ingresamos_Stock_para_Nivel_Cinco() throws Throwable {
		stockProtegidoPage.inpLevel5();
	}
	
	@Then("^Sistema no permite ingresar Fecha menor a la actual$")
	public void sistema_no_permite_ingresar_Fecha_menor_a_la_actual() throws Throwable {
	    stockProtegidoPage.inpFecIni();
	    BasePage.WaitSleep(1);
	    BasePage.TakeScreenShot();
	}
	
	@Then("^Ingresamos una fecha de inicio mayor a la actual$")
	public void ingresamos_una_fecha_de_inicio_mayor_a_la_actual() throws Throwable {
		stockProtegidoPage.inpFecIni();
		BasePage.WaitSleep(2);
	    stockProtegidoPage.fechaIniMayor();
	    BasePage.WaitSleep(2);
	    BasePage.TakeScreenShot();
	}
	
	@Then("^Ingresamos una fecha de termino Menor a la Fecha de Inicio$")
	public void ingresamos_una_fecha_de_termino_Menor_a_la_Fecha_de_Inicio() throws Throwable {
	    stockProtegidoPage.inpFecFin();
	    BasePage.WaitSleep(2);
	    stockProtegidoPage.fechaFinMenor();
	    BasePage.WaitSleep(2);
	    BasePage.TakeScreenShot();
	}
	
	@Then("^Seleccionamos Checkbox Permanente$")
	public void seleccionamos_Checkbox_Permanente() throws Throwable {
	    stockProtegidoPage.chkPermanente();
	    BasePage.WaitSleep(1);
	    BasePage.TakeScreenShot();
	}
	
	@Then("^Validamos mensaje de alerta Debe completar los campos obligatorios$")
	public void validamos_mensaje_de_alerta_Debe_completar_los_campos_obligatorios() throws Throwable {
	    stockProtegidoPage.msjObligatorio();
	    BasePage.WaitSleep(2);
	    BasePage.TakeScreenShot();
	}
	
	@Then("^Se despliega mensaje de alerta para confirmar o cancelar la falta de niveles$")
	public void se_despliega_mensaje_de_alerta_para_confirmar_o_cancelar_la_falta_de_niveles() throws Throwable {
	    stockProtegidoPage.msjNiveles();
	    BasePage.WaitSleep(2);
	    BasePage.TakeScreenShot();
	}
	
	@Then("^Se despliega mensaje de alerta indicando que ya existe una configuración$")
	public void se_despliega_mensaje_de_alerta_indicando_que_ya_existe_una_configuración() throws Throwable {
//	    stockProtegidoPage.msjConfig();
	    BasePage.WaitSleep(2);
	    BasePage.TakeScreenShot();
	}
	
	@Then("^Se presiona botón Cancelar$")
	public void se_presiona_botón_Cancelar() throws Throwable {
	    stockProtegidoPage.btnCancelar();
	    BasePage.WaitSleep(2);
	    BasePage.TakeScreenShot();
	}
	
	@Then("^se despliega mensaje de alerta confirmando la selección$")
	public void se_despliega_mensaje_de_alerta_confirmando_la_selección() throws Throwable {
	    stockProtegidoPage.msjEliminar();
	    BasePage.WaitSleep(2);
	    BasePage.TakeScreenShot();
	}
	
	@Then("^En mensaje emergente se cancela la eliminación$")
	public void en_mensaje_emergente_se_cancela_la_eliminación() throws Throwable {
	    stockProtegidoPage.btnCancelaEliminar();
	    BasePage.WaitSleep(2);
	    BasePage.TakeScreenShot();
	}
	
	@Then("^Presionamos el Botón Cancelar$")
	public void presionamos_el_Botón_Cancelar() throws Throwable {
	    stockProtegidoPage.btnCancelaEditar();
	    BasePage.WaitSleep(2);
	    BasePage.TakeScreenShot();
	}


}
