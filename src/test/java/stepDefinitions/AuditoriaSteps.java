package stepDefinitions;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.awt.AWTException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import org.junit.Assert;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import models.ECargaStockProtegido;
import models.ESku;
import models.sic.EGrillaAuditoria;
import pageObjects.BasePage;
import utils.DriverFactory;

public class AuditoriaSteps extends DriverFactory {

	private EGrillaAuditoria oGrillaAuditoria;
	private List<EGrillaAuditoria> oListaGrillaAuditoria;

	@Given("^tengo una transaccion de un producto con tipo de inventario \"([^\"]*)\"$")
	public void tengo_una_transaccion_de_un_producto_con_tipo_de_inventario(String tipoInventario) throws Throwable {

		String codigoPais = GetSICCodigoPais();
		String sistemaOrigen = "PMM";
		int cantidad = 1;

		String idTrxHeader = EnviarAjusteInventario(sistemaOrigen, codigoPais, cantidad, tipoInventario);
		database.EsperarProcesamientoTransaccion(idTrxHeader);

		oGrillaAuditoria = database.ObtenerTransaccionBy(codigoPais, idTrxHeader, "", "", "", "", "");

		System.out.println("------------DATOS OBTENIDOS--------------");
		System.out.println(oGrillaAuditoria.transaccionId);
		System.out.println(oGrillaAuditoria.idtrxHeader);
		System.out.println(oGrillaAuditoria.codigoBodega);
		System.out.println(oGrillaAuditoria.codigoSistema);
		System.out.println(oGrillaAuditoria.codigoTipoTransaccion);
		System.out.println(oGrillaAuditoria.codigoVenta);
		System.out.println("-----------------------------------------");
	}

	@Given("^tengo una transaccion de un producto con tipo de inventario$")
	public void tengo_una_transaccion_de_un_producto_con_tipo_de_inventario(DataTable dtTipoInventario)
			throws Throwable {

		oListaGrillaAuditoria = new ArrayList<EGrillaAuditoria>();

		List<List<String>> oList = dtTipoInventario.raw();
		int contador = 1;

		String codigoPais = GetSICCodigoPais();
		String sistemaOrigen = "PMM";
		int cantidad = 1;
		List<ESku> oListaProductos = new ArrayList<ESku>();
		oListaProductos = new ArrayList<ESku>();
		oListaProductos = database.ObtenerInformacionProductos(codigoPais, 1, "1");
		oListaProductos.get(0);
		List<ESku> oListaProductosFinal = new ArrayList<ESku>();

		for (List<String> fila : oList) {
			if (contador != 1) {
				String tipoInventario = fila.get(0);
				ESku ProductoFinal = new ESku();

				ProductoFinal.sku = oListaProductos.get(0).sku;
				ProductoFinal.seller = oListaProductos.get(0).seller;
				ProductoFinal.bodega = oListaProductos.get(0).bodega;
				ProductoFinal.tipoInventario = tipoInventario;
				ProductoFinal.stock = oListaProductos.get(0).stock;
				ProductoFinal.iStock = oListaProductos.get(0).iStock;
				ProductoFinal.esImpar = oListaProductos.get(0).esImpar;
				oListaProductosFinal.add(ProductoFinal);
			}
			contador++;
		}
		oListaProductos = oListaProductosFinal;

		String idTrxHeader = EnviarAjusteInventario(sistemaOrigen, codigoPais, oListaProductos);
		database.EsperarProcesamientoTransaccion(idTrxHeader);

		oListaGrillaAuditoria = database.ObtenerTransaccionBy(codigoPais, idTrxHeader);
		oGrillaAuditoria = oListaGrillaAuditoria.get(0);
		System.out.println("------------DATOS OBTENIDOS--------------");
		for (EGrillaAuditoria oGrilla : oListaGrillaAuditoria) {
			System.out.println("transaccionId: " + oGrilla.transaccionId);
			System.out.println("idtrxHeader: " + oGrilla.idtrxHeader);
			System.out.println("tipoInventario: " + oGrilla.tipoInventario);
			System.out.println("codigoBodega: " + oGrilla.codigoBodega);
			System.out.println("codigoSistema: " + oGrilla.codigoSistema);
			System.out.println("codigoTipoTransaccion: " + oGrilla.codigoTipoTransaccion);
			System.out.println("codigoVenta: " + oGrilla.codigoVenta);
			System.out.println("-----------------------------------------");
		}
	}

	public String EnviarAjusteInventario(String sistemaOrigen, String codigoPais, int cantidad, String tipoInventario)
			throws InterruptedException, IOException, SQLException {

		String tipoAjuste = "ADJUST";
		List<ESku> oListaProductos = new ArrayList<ESku>();
		String pattern = "AUT_ADJUST_([0-9])\\w+";
		Pattern r = Pattern.compile(pattern);
		String idTrxHeader = "";

		oListaProductos = new ArrayList<ESku>();
		oListaProductos = database.ObtenerInformacionProductos(codigoPais, cantidad, tipoInventario);
		System.out.println("CANTIDAD DE PRODUCTOS OBTENIDOS:" + oListaProductos.size());
		String jsonString = tramaJson.GenerarTramaJsonAjusteInventario(oListaProductos, sistemaOrigen, codigoPais,
				tipoAjuste, "UserQA", "", "NTD", false, false, false, false, false, false);
		System.out.println(jsonString);

		Matcher m = r.matcher(jsonString);
		if (m.find()) {
			idTrxHeader = m.group(0);
		} else {
			System.out.println("NO MATCH");
		}

		if (sistemaOrigen.equals("PMM"))
			servicio.EnviarRequest(GetAPIWrapperAjuste(), jsonString);
		else
			servicio.EnviarRequest(GetAPIConnectAjuste(), jsonString);

		return idTrxHeader;
	}

	public String EnviarAjusteInventario(String sistemaOrigen, String codigoPais, List<ESku> oListaProductos)
			throws InterruptedException, IOException, SQLException {

		String tipoAjuste = "ADJUST";
		String pattern = "AUT_ADJUST_([0-9])\\w+";
		Pattern r = Pattern.compile(pattern);
		String idTrxHeader = "";

		System.out.println("CANTIDAD DE PRODUCTOS OBTENIDOS:" + oListaProductos.size());
		String jsonString = tramaJson.GenerarTramaJsonAjusteInventario(oListaProductos, sistemaOrigen, codigoPais,
				tipoAjuste, "UserQA", "", "NTD", false, false, false, false, false, false);
		System.out.println(jsonString);

		Matcher m = r.matcher(jsonString);
		if (m.find()) {
			idTrxHeader = m.group(0);
		} else {
			System.out.println("NO MATCH");
		}

		if (sistemaOrigen.equals("PMM"))
			servicio.EnviarRequest(GetAPIWrapperAjuste(), jsonString);
		else
			servicio.EnviarRequest(GetAPIConnectAjuste(), jsonString);

		return idTrxHeader;
	}

	@Given("^tengo una transaccion de un producto con sistema origen \"([^\"]*)\"$")
	public void tengo_una_transaccion_de_un_producto_con_sistema_origen(String sistemaOrigen) throws Throwable {

		String codigoPais = GetSICCodigoPais();
		String tipoInventario = "1";
		int cantidad = 1;

		String idTrxHeader = EnviarAjusteInventario(sistemaOrigen, codigoPais, cantidad, tipoInventario);
		database.EsperarProcesamientoTransaccion(idTrxHeader);
		BasePage.WaitSleep(10);

		oGrillaAuditoria = database.ObtenerTransaccionBy(codigoPais, idTrxHeader, "", "", "", "", "");

		System.out.println("--DATOS OBTENIDOS--");
		System.out.println(oGrillaAuditoria.transaccionId);
		System.out.println(oGrillaAuditoria.idtrxHeader);
		System.out.println(oGrillaAuditoria.codigoBodega);
		System.out.println(oGrillaAuditoria.codigoSistema);
		System.out.println(oGrillaAuditoria.codigoTipoTransaccion);
		System.out.println(oGrillaAuditoria.codigoVenta);
		System.out.println("-------------------------------------");
	}

	@When("^realizo la consulta de auditoria$")
	public void realizo_la_consulta_de_auditoria() throws InterruptedException, AWTException {

		auditoriaPage.QuitarFiltros();
		auditoriaPage.Consultar(oGrillaAuditoria);
	}

	@Then("^se validan en bd los datos mostrados en la pantalla de auditoria$")
	public void se_validan_en_bd_los_datos_mostrados() throws Throwable {

		String mensaje = "No se encontró en la columna [COLUMNA] el valor [VALOR]";
		assertTrue(mensaje.replace("[COLUMNA]", "idtrxHeader").replace("[VALOR]", oGrillaAuditoria.idtrxHeader),
				auditoriaPage.ValidarIDTRX(oGrillaAuditoria.idtrxHeader));
		assertTrue(mensaje.replace("[COLUMNA]", "codigoSistema").replace("[VALOR]", oGrillaAuditoria.codigoSistema),
				auditoriaPage.ValidarSistemaOrigen(oGrillaAuditoria.idtrxHeader, oGrillaAuditoria.codigoSistema));
		assertTrue(mensaje.replace("[COLUMNA]", "codigoBodega").replace("[VALOR]", oGrillaAuditoria.codigoBodega),
				auditoriaPage.ValidarSucursal(oGrillaAuditoria.idtrxHeader, oGrillaAuditoria.codigoBodega));
		assertTrue(mensaje.replace("[COLUMNA]", "codigoVenta").replace("[VALOR]", oGrillaAuditoria.codigoVenta),
				auditoriaPage.ValidarCodigoVenta(oGrillaAuditoria.idtrxHeader, oGrillaAuditoria.codigoVenta));
		assertTrue(
				mensaje.replace("[COLUMNA]", "codigoTipoTransaccion").replace("[VALOR]",
						oGrillaAuditoria.codigoTipoTransaccion),
				auditoriaPage.ValidarTipoTransaccion(oGrillaAuditoria.idtrxHeader,
						oGrillaAuditoria.codigoTipoTransaccion));
		assertTrue(
				mensaje.replace("[COLUMNA]", "documentoTransaccion").replace("[VALOR]",
						oGrillaAuditoria.documentoTransaccion),
				auditoriaPage.ValidarTipoDocumento(oGrillaAuditoria.idtrxHeader,
						oGrillaAuditoria.documentoTransaccion));
		assertTrue(mensaje.replace("[COLUMNA]", "username").replace("[VALOR]", oGrillaAuditoria.username),
				auditoriaPage.ValidarUsuario(oGrillaAuditoria.idtrxHeader, oGrillaAuditoria.username));
		assertTrue(mensaje.replace("[COLUMNA]", "estadoCabecera").replace("[VALOR]", oGrillaAuditoria.estadoCabecera),
				auditoriaPage.ValidarEstadoTransaccion(oGrillaAuditoria.idtrxHeader, oGrillaAuditoria.estadoCabecera));
		assertTrue(mensaje.replace("[COLUMNA]", "estadoDetalle").replace("[VALOR]", oGrillaAuditoria.estadoDetalle),
				auditoriaPage.ValidarEstadoRegistro(oGrillaAuditoria.idtrxHeader, oGrillaAuditoria.estadoDetalle));
		assertTrue(mensaje.replace("[COLUMNA]", "stockAntes").replace("[VALOR]", oGrillaAuditoria.stockAntes),
				auditoriaPage.ValidarStockAntes(oGrillaAuditoria.idtrxHeader, oGrillaAuditoria.stockAntes));

		String operacion = oGrillaAuditoria.operacion;
		int stockAntes = Integer.parseInt(oGrillaAuditoria.stockAntes);
		int stockCambio = Integer.parseInt(oGrillaAuditoria.stockDespues);

		int stockActual = oGrillaAuditoria.estadoDetalle.equals("C")
				? (operacion.equals("A") ? stockAntes + stockCambio : stockAntes - stockCambio)
				: stockAntes;

		assertTrue(mensaje.replace("[COLUMNA]", "stockActual").replace("[VALOR]", String.valueOf(stockActual)),
				auditoriaPage.ValidarStockDespues(oGrillaAuditoria.idtrxHeader, String.valueOf(stockActual)));
		System.out.println("TIPO DE INVENTARIO " + oGrillaAuditoria.tipoInventario);
		assertTrue(mensaje.replace("[COLUMNA]", "tipoInventario").replace("[VALOR]", oGrillaAuditoria.tipoInventario),
				auditoriaPage.ValidarTipoInventario(oGrillaAuditoria.idtrxHeader, oGrillaAuditoria.tipoInventario));
	}

	@Then("^se validan en bd los datos mostrados por tipo de inventario$")
	public void se_validan_en_bd_los_datos_mostrados_por_tipo_de_inventario() throws Throwable {

		for (EGrillaAuditoria oGrillaAuditoria : oListaGrillaAuditoria) {
			String mensaje = "No se encontró en la columna [COLUMNA] el valor [VALOR]";
			assertTrue(mensaje.replace("[COLUMNA]", "idtrxHeader").replace("[VALOR]", oGrillaAuditoria.idtrxHeader),
					auditoriaPage.ValidarIDTRX(oGrillaAuditoria.idtrxHeader));
			assertTrue(mensaje.replace("[COLUMNA]", "codigoSistema").replace("[VALOR]", oGrillaAuditoria.codigoSistema),
					auditoriaPage.ValidarSistemaOrigen(oGrillaAuditoria.idtrxHeader, oGrillaAuditoria.codigoSistema));
			assertTrue(mensaje.replace("[COLUMNA]", "codigoBodega").replace("[VALOR]", oGrillaAuditoria.codigoBodega),
					auditoriaPage.ValidarSucursal(oGrillaAuditoria.idtrxHeader, oGrillaAuditoria.codigoBodega));
			assertTrue(mensaje.replace("[COLUMNA]", "codigoVenta").replace("[VALOR]", oGrillaAuditoria.codigoVenta),
					auditoriaPage.ValidarCodigoVenta(oGrillaAuditoria.idtrxHeader, oGrillaAuditoria.codigoVenta));
			assertTrue(
					mensaje.replace("[COLUMNA]", "codigoTipoTransaccion").replace("[VALOR]",
							oGrillaAuditoria.codigoTipoTransaccion),
					auditoriaPage.ValidarTipoTransaccion(oGrillaAuditoria.idtrxHeader,
							oGrillaAuditoria.codigoTipoTransaccion));
			assertTrue(
					mensaje.replace("[COLUMNA]", "documentoTransaccion").replace("[VALOR]",
							oGrillaAuditoria.documentoTransaccion),
					auditoriaPage.ValidarTipoDocumento(oGrillaAuditoria.idtrxHeader,
							oGrillaAuditoria.documentoTransaccion));
			assertTrue(mensaje.replace("[COLUMNA]", "username").replace("[VALOR]", oGrillaAuditoria.username),
					auditoriaPage.ValidarUsuario(oGrillaAuditoria.idtrxHeader, oGrillaAuditoria.username));
			assertTrue(
					mensaje.replace("[COLUMNA]", "estadoCabecera").replace("[VALOR]", oGrillaAuditoria.estadoCabecera),
					auditoriaPage.ValidarEstadoTransaccion(oGrillaAuditoria.idtrxHeader,
							oGrillaAuditoria.estadoCabecera));
			assertTrue(mensaje.replace("[COLUMNA]", "estadoDetalle").replace("[VALOR]", oGrillaAuditoria.estadoDetalle),
					auditoriaPage.ValidarEstadoRegistro(oGrillaAuditoria.idtrxHeader, oGrillaAuditoria.estadoDetalle));
			assertTrue(mensaje.replace("[COLUMNA]", "stockAntes").replace("[VALOR]", oGrillaAuditoria.stockAntes),
					auditoriaPage.ValidarStockAntes(oGrillaAuditoria.idtrxHeader, oGrillaAuditoria.stockAntes));

			String operacion = oGrillaAuditoria.operacion;
			int stockAntes = Integer.parseInt(oGrillaAuditoria.stockAntes);
			int stockCambio = Integer.parseInt(oGrillaAuditoria.stockDespues);

			int stockActual = oGrillaAuditoria.estadoDetalle.equals("C")
					? (operacion.equals("A") ? stockAntes + stockCambio : stockAntes - stockCambio)
					: stockAntes;

			assertTrue(mensaje.replace("[COLUMNA]", "stockActual").replace("[VALOR]", String.valueOf(stockActual)),
					auditoriaPage.ValidarStockDespues(oGrillaAuditoria.idtrxHeader, String.valueOf(stockActual)));
			System.out.println("TIPO DE INVENTARIO " + oGrillaAuditoria.tipoInventario);
			assertTrue(
					mensaje.replace("[COLUMNA]", "tipoInventario").replace("[VALOR]", oGrillaAuditoria.tipoInventario),
					auditoriaPage.ValidarTipoInventario(oGrillaAuditoria.idtrxHeader, oGrillaAuditoria.tipoInventario));
		}
	}

	@When("^doy clic al boton Exportar$")
	public void doy_click_en_exportar() throws IOException, InterruptedException, AWTException {

		auditoriaPage.ExportarResultado();
	}

	@When("^doy click en limpiar el resultado de auditoria$")
	public void doy_click_en_limpiar_el_resultado_de_auditoria()
			throws IOException, InterruptedException, AWTException {

		auditoriaPage.ClickLimpiar();
	}

	@When("^doy click en consultar auditoria$")
	public void doy_click_en_consultar_auditoria() throws IOException, InterruptedException, AWTException {
		auditoriaPage.ClickConsultar();
	}

	@When("^selecciono rango de fechas mayor a 7 dias$")
	public void selecciono_rango_fechas_mayor_a_x_dias() throws IOException, InterruptedException, AWTException {

	}

	@Then("^validar la descarga del archivo$")
	public void validar_la_descarga_del_archivo() throws IOException, InterruptedException, AWTException {
		String rutaDescargas = GetRutaCarpetaLocalDescargas();
		String fileName = "AuditoriaTransacciones.zip";

		BasePage.WaitSleep(10);
		Assert.assertTrue("No se encontró el archivo", archivo.ValidarDescargaArchivo(rutaDescargas, fileName));
	}

	@Then("^se valida que tenga por defecto el tipo de producto \"([^\"]*)\"$")
	public void se_valida_que_tenga_por_defecto_el_tipo_de_producto(String tipoProducto)
			throws IOException, InterruptedException, AWTException {

		String opcionSeleccionada = auditoriaPage.ObtenerTextoComboTipoProducto();
		System.out.println("OPCION SELECCIONADA " + opcionSeleccionada);
		Assert.assertTrue("No se tiene por defecto la opcion '" + tipoProducto + "'",
				opcionSeleccionada.equals(tipoProducto));
	}

	@Then("^ingreso codigo de venta \"([^\"]*)\"$")
	public void ingreso_codigo_de_venta(String codigoVenta) throws IOException, InterruptedException, AWTException {
		auditoriaPage.IngresarCodigoVenta(codigoVenta);
	}

	@Then("^selecciono rango de fechas mayor a diez dias$")
	public void selecciono_rango_de_fechas_mayor_a_x_dias() throws IOException, InterruptedException, AWTException {

		auditoriaPage.SeleccionarFechaDesde("1");

		auditoriaPage.SeleccionarFechaHasta("11");

	}

	public void realizo_la_consulta() throws Throwable {
		auditoriaPage.QuitarFiltros();
		auditoriaPage.Consultar(oGrillaAuditoria);
	}

	@When("^realizo la consulta de auditoria solo con los filtros obligatorios$")
	public void realizo_la_consulta_solo_con_los_filtros_obligatorios() throws Throwable {

		auditoriaPage.ConsultarPorDefecto(oGrillaAuditoria);
	}

	@When("^realizo la consulta de auditoria sin ingresar codigo de venta$")
	public void realizo_la_consulta_sin_ingresar_codigo_venta() throws Throwable {
		auditoriaPage.ConsultarSinCodigoVenta();
	}

	@When("^realizo la consulta de auditoria sin ingresar fecha desde$")
	public void realizo_la_consulta_sin_ingresar_fecha_desde() throws Throwable {
		auditoriaPage.ConsultarSinFechaDesde(oGrillaAuditoria);
	}

	@When("^realizo la consulta de auditoria sin ingresar fecha hasta$")
	public void realizo_la_consulta_sin_ingresar_fecha_hasta() throws Throwable {
		auditoriaPage.ConsultarSinFechaHasta(oGrillaAuditoria);
	}

	@When("^doy clic al boton Limpiar$")
	public void doy_clic_al_boton_limpiar() throws Throwable {
		auditoriaPage.Limpiar();
	}

	@When("^se limpian todos los filtros utilizados$")
	public void se_limpian_todos_los_filtros_utilizados() throws Throwable {
		assertFalse("La grilla aún tiene resultados", auditoriaPage.ValidarIDTRX(oGrillaAuditoria.idtrxHeader));
	}

	@Then("^muestra mensaje de error \"([^\"]*)\"$")
	public void muestra_mensaje_de_error_x(String mensajeErrorEsperado) throws Throwable {
		assertTrue("No se mostró mensaje de error", auditoriaPage.ValidarMensajeError(mensajeErrorEsperado));
		BasePage.WaitSleep(2);
	}

	@Given("^tengo una transaccion de un producto con estado de transaccion \"([^\"]*)\"$")
	public void tengo_una_transaccion_de_un_producto_con_estado_de_transaccion(String estadoTransaccion)
			throws IOException, InterruptedException, SQLException {
		String codigoPais = GetSICCodigoPais();
		oGrillaAuditoria = database.ObtenerTransaccionBy(codigoPais, "", "", "", estadoTransaccion, "", "");
	}

	@Given("^tengo una transaccion de un producto con estado de registro \"([^\"]*)\"$")
	public void tengo_una_transaccion_de_un_producto_con_estado_de_registro(String estadoRegistro)
			throws IOException, InterruptedException, SQLException {
		String codigoPais = GetSICCodigoPais();
		oGrillaAuditoria = database.ObtenerTransaccionBy(codigoPais, "", "", "", "", estadoRegistro, "");
	}

	@Given("^tengo una transaccion de un producto con tipo de transaccion \"([^\"]*)\"$")
	public void tengo_una_transaccion_de_un_producto_con_tipo_de_transaccion(String tipoTransaccion)
			throws IOException, InterruptedException, SQLException {

		String codigoPais = GetSICCodigoPais();
		oGrillaAuditoria = database.ObtenerTransaccionBy(codigoPais, "", "", "", "", "", tipoTransaccion);
	}

}
