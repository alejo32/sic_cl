package stepDefinitions;

import java.awt.AWTException;
import java.io.IOException;
import org.junit.Assert;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.BasePage;
import utils.DriverFactory;

public class CuadraturaSteps extends DriverFactory {

	@When("^doy clic en el ultimo reporte de cuadratura$")
	public void doy_clic_en_el_ultimo_reporte_de_cuadratura() throws IOException, InterruptedException, AWTException {
		cuadraturaPage.DescargarUltimoReporte();
		BasePage.WaitSleep(2);
	}

	@Then("^se descarga el reporte$")
	public void se_descarga_el_reporte() throws IOException, InterruptedException, AWTException {
		String rutaDescargas = GetRutaCarpetaLocalDescargas();
		BasePage.WaitSleep(2);
		String fileName = cuadraturaPage.ObtenerNombreUltimoReporte();
		System.out.println("Nombre del archivo obtenido: " + fileName);
		BasePage.WaitSleep(20);
		Assert.assertTrue("No se encontró el archivo", archivo.ValidarDescargaArchivo(rutaDescargas, fileName));
	}

	@Then("^valido que se muestren los ultimos registros$")
	public void valido_que_se_muestren_los_x_ultimos_registros() throws InterruptedException, AWTException {
		int numeroPaginas = cuadraturaPage.ObtenerNumeroPaginas();
		System.out.println("NUMERO DE PAGINAS:" + numeroPaginas);
		int contadorReportes = 0;
		for (int i = 1; i <= numeroPaginas; i++) {
			cuadraturaPage.IrPagina(i);
			int numeroReportes = cuadraturaPage.ObtenerNumeroReportesPorPagina();
			System.out.println("# de reportes encontrados en la pagina " + i + " :" + numeroReportes);
			contadorReportes = contadorReportes + numeroReportes;
		}
		Assert.assertTrue("No se muestran los ultimos 30 registros", contadorReportes == 30);
	}

	
}
