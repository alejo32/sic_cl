package stepDefinitions;

import java.io.IOException;
import org.junit.Assert;

import com.aventstack.extentreports.configuration.Config;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.BasePage;
import utils.DriverFactory;

public class ConfiguracionSteps extends DriverFactory {

	@When("^doy click al combo nombre de sistema$")
	public void doy_click_al_combo_nombre_de_sistema() throws IOException, InterruptedException {
		configuracionPage.ClickComboNombreSistema();
		BasePage.WaitSleep(3);
	}

	@When("^doy click en agregar$")
	public void doy_click_en_agregar() throws IOException, InterruptedException {
		configuracionPage.ClickAgregar();
		BasePage.WaitSleep(2);
	}

	@When("^doy click en el combo tipo de transaccion$")
	public void doy_click_al_combo_tipo_de_transaccion() throws IOException, InterruptedException {
		configuracionPage.ClickComboTipoTransaccion();
	}

	@When("^doy click en el combo tipo de inventario$")
	public void doy_click_al_combo_tipo_de_inventario() throws IOException, InterruptedException {
		configuracionPage.ClickComboTipoTransaccion();
	}

	@Then("^se verifica que se muestren todos los sistemas$")
	public void se_valida_que_se_muestren_todos_los_sistemas() throws Exception {
		configuracionPage.selB2B();
		BasePage.WaitSleep(1);
		configuracionPage.selBO();
		BasePage.WaitSleep(1);
		configuracionPage.selBGT();
		BasePage.WaitSleep(1);
		configuracionPage.selMKP();
		BasePage.WaitSleep(1);
		configuracionPage.selMTO();
		BasePage.WaitSleep(1);
		configuracionPage.selPMM();
	}

	@Then("^se verifica que se muestren todos los tipos de transaccion$")
	public void se_verifica_que_se_muestren_todos_los_tipos_de_transaccion() throws Exception {
		configuracionPage.selAdjust();
		BasePage.WaitSleep(1);
		configuracionPage.selSfull();
		BasePage.WaitSleep(1);
		configuracionPage.selSdelta();
		BasePage.WaitSleep(2);
	}

	@Then("^se verifica que se muestren todos los tipos de inventario$")
	public void se_verifica_que_se_muestren_todos_los_tipos_de_inventario() throws Exception {
		configuracionPage.selReservado();
		BasePage.WaitSleep(1);
		configuracionPage.selDisponible();
		BasePage.WaitSleep(1);
	}

	@Given("^el boton consultar esta deshabilitado$")
	public void el_boton_consultar_esta_deshabilitado() throws IOException, InterruptedException {
		Assert.assertTrue("El botón Consultar esta habilitado",
				configuracionPage.ExisteBotonDeshabilitado("Consultar"));
	}
	
	@Given("^el boton consultar esta habilitado$")
	public void el_boton_consultar_esta_habilitado() throws IOException, InterruptedException {
		Assert.assertTrue("El botón Consultar esta habilitado", configuracionPage.ExisteBotonHabilitado("Consultar"));
	}

	@Given("^el boton agregar esta deshabilitado$")
	public void el_boton_agregar_esta_deshabilitado() throws IOException, InterruptedException {
		Assert.assertTrue("El botón Agregar esta habilitado", configuracionPage.ExisteBotonDeshabilitado("Agregar"));
	}

	@Given("^el boton eliminar esta deshabilitado$")
	public void el_boton_eliminar_esta_deshabilitado() throws IOException, InterruptedException {
		Assert.assertTrue("El botón Consultar esta habilitado",
				configuracionPage.ExisteBotonDeshabilitado("Eliminar"));
	}

	@When("^selecciono el sistema \"([^\"]*)\"$")
	public void selecciono_el_sistema(String nombreSistema) throws IOException, InterruptedException {
		configuracionPage.SeleccionarSistema(nombreSistema);
		BasePage.WaitSleep(1);
	}

	@When("^selecciono tipo de transaccion \"([^\"]*)\"$")
	public void selecciono_tipo_de_transaccion(String codigoTipoTransaccion) throws IOException, InterruptedException {
		configuracionPage.SeleccionarTipoTransaccion(codigoTipoTransaccion);
		BasePage.WaitSleep(1);
	}

	@When("^selecciono tipo de inventario (\\d+)$")
	public void selecciono_tipo_de_inventario(String codigoTipoInventario) throws IOException, InterruptedException {
		configuracionPage.SeleccionarTipoInventario(codigoTipoInventario);
	}

	@Then("^se habilita el boton consultar$")
	public void se_habilita_el_boton_consultar() throws IOException, InterruptedException {
		Assert.assertTrue("El botón Consultar esta deshabilitado",
				configuracionPage.ExisteBotonHabilitado("Consultar"));
	}

	@Then("^se habilita el boton agregar$")
	public void se_habilita_el_boton_agregar() throws IOException, InterruptedException {
		Assert.assertTrue("El botón Agregar esta deshabilitado", configuracionPage.ExisteBotonHabilitado("Agregar"));
	}

	@Then("^se habilita el boton elimimar$")
	public void se_habilita_el_boton_elimimar(String nombreSistema) throws IOException, InterruptedException {
		Assert.assertTrue("El botón Consultar esta deshabilitado",
				configuracionPage.ExisteBotonHabilitado("Eliminar"));
	}

	@When("^doy click en consultar$")
	public void doy_click_en_consultar() throws IOException, InterruptedException {
		configuracionPage.ClickConsultar();
		BasePage.WaitSleep(1);
	}

	@When("^doy click en limpiar$")
	public void doy_click_en_limpiar() throws IOException, InterruptedException {
		configuracionPage.ClickLimpiar();
	}

	@Then("^los botones se deshabilitan a excepcion del boton limpiar$")
	public void los_botones_se_deshabilitan_a_excepcion_del_boton_limpiar() throws Exception {
		Assert.assertTrue("El botón Consultar esta habilitado", configuracionPage.ExisteBotonDeshabilitado("Consultar"));
		configuracionPage.btnEliminar();		
		configuracionPage.btnConsultar();
	}
	
	@Then("^se habilita el boton eliminar$")
	public void se_habilita_el_boton_eliminar() throws Exception {
		configuracionPage.btnEliminar();		
	}

	@Then("^se limpia el resultado$")
	public void se_limpia_el_resultado() throws IOException, InterruptedException {
		Assert.assertTrue("No se limpió el resultado", !configuracionPage.ExisteResultadoLimpio());
	}

	@Then("^muestra las configuraciones asociadas al sistema$")
	public void muestra_las_configuraciones_asociadas_al_sistema() throws IOException, InterruptedException {
		configuracionPage.ExisteCargaDeResultados();
	}

	@Then("^muestra mensaje de confirmacion \"([^\"]*)\"$")
	public void muestra_mensaje_de_confirmacion(String mensaje) throws IOException, InterruptedException {
		configuracionPage.ExisteMensajeConfirmacion(mensaje);
		BasePage.WaitSleep(1);
	}

	@Then("^al dar continuar muestra mensaje \"([^\"]*)\"$")
	public void al_dar_continuar_muestra_mensaje(String mensaje) throws IOException, InterruptedException {
		BasePage.WaitSleep(1);
		configuracionPage.ClickContinuar();		
		System.out.println("Mensaje de exito: " + mensaje);
		configuracionPage.VerificarMensajeFinal(mensaje);
	}
	
	@Given("^habilito configuracion para tipo de transaccion \"([^\"]*)\" tipo de inventario \"([^\"]*)\" para el sistema \"([^\"]*)\"$")
	public void habilito_configuracion_para_tipo_de_transaccion_tipo_de_inventario_para_el_sistema(String arg1, String arg2, String arg3) throws Throwable {
	    configuracionPage.selB2BClick();
	    BasePage.WaitSleep(1);
		configuracionPage.selAdjustClick();
	    BasePage.WaitSleep(1);
	    configuracionPage.selDisponibleClick();
	    BasePage.WaitSleep(1);
	}
	
	@Then("^se verifica que exista la columna \"([^\"]*)\"$")
	public void se_verifica_que_exista_la_columna(String arg1) throws Throwable {
	    configuracionPage.gridTransaccion();
	    BasePage.WaitSleep(1);
	}
}
