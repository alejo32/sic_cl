package stepDefinitions;

import java.awt.AWTException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.junit.Assert;

//import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import models.EStock;
import pageObjects.BasePage;
import pageObjects.sic.AjusteStockPage;
import utils.DriverFactory;

public class AjusteStock extends DriverFactory {

	private EStock oProductoStockRipley;
	private List<EStock> oListProductoStockRipley;
	private String tipoActualizacion = "";

	@When("^selecciono una bodega al azar$")
	public void selecciono_una_bodega_al_azar() throws IOException, InterruptedException, SQLException, AWTException {
		menuPage.SeleccionarBodega(database.ObtenerProductoStockRipley("1", "1").bodega);
	}

	@Then("^se muestra mensaje flotante \"([^\"]*)\"$")
	public void se_muestra_mensaje_flotante(String mensaje)
			throws IOException, InterruptedException, SQLException, AWTException {
//		Assert.assertTrue("No se encontró el mensaje flotante",
//				mantenedorIngresarAjusteStockSteps.ExisteMensajeFlotanteError(mensaje));
	}

	@Given("^tengo informacion de un producto con tipo de inventario \"([^\"]*)\"$")
	public void tengo_informacion_de_un_producto_con_tipo_de_inventario(String tipoInventario)
			throws IOException, InterruptedException, SQLException {
		oProductoStockRipley = database.ObtenerProductoStockRipley("1", tipoInventario);
	}

	@Given("^tengo informacion de stock de \"([^\"]*)\" productos de la bodega \"([^\"]*)\"$")
	public void tengo_informacion_de_stock_de_x_productos(String cantidad, String bodega) throws IOException, InterruptedException, SQLException {
		oListProductoStockRipley = database.ObtenerListaProductoStockRipley(cantidad, bodega);
	}

	@Given("^tengo informacion de un producto sin stock$")
	public void tengo_informacion_de_un_producto_sin_stock() throws IOException, InterruptedException, SQLException {
		oProductoStockRipley = database.ObtenerProductoSinStockRipley();
	}

	@Given("^tengo informacion de un producto con stock negativo$")
	public void tengo_informacion_de_un_producto_con_stock_negativo()
			throws IOException, InterruptedException, SQLException {
		oProductoStockRipley = database.ObtenerProductoStockRipleyNegativo("1");
	}

	@Given("^tengo informacion de un producto con stock positivo$")
	public void tengo_informacion_de_un_producto_con_stock_positivo()
			throws IOException, InterruptedException, SQLException {
		oProductoStockRipley = database.ObtenerProductoStockRipleyPositivo("1");
	}

	@Given("^tengo informacion de un producto con stock cero$")
	public void tengo_informacion_de_un_producto_con_stock_cero()
			throws IOException, InterruptedException, SQLException {
		oProductoStockRipley = database.ObtenerProductoStockRipleyCero("1");
	}

	@Given("^doy click a limpiar$")
	public void doy_click_a_limpiar() throws IOException, InterruptedException, SQLException {
		ajusteStockPage.Limpiar();
	}

	@When("^ingreso codigo venta \"([^\"]*)\"$")
	public void ingreso_codigo_de_venta(String codigoVenta) throws AWTException, InterruptedException {
		ajusteStockPage.IngresarCodigoVenta(codigoVenta);
	}

	@Given("^realizo la consulta de stock actual$")
	public void realizo_la_consulta_de_stock_actual() throws InterruptedException, AWTException {

		if (!oProductoStockRipley.bodega.equals(""))
			menuPage.SeleccionarBodega(oProductoStockRipley.bodega);
		ajusteStockPage.ConsultarStock(oProductoStockRipley.codigoVenta, oProductoStockRipley.tipoInventario);
	}
	
	@Given("^realizo la consulta de stock actual SKU fijo$")
	public void realizo_la_consulta_de_stock_actual_SKU_fijo() throws InterruptedException, AWTException {

		if (!oProductoStockRipley.bodega.equals(""))
			menuPage.SeleccionarBodega(oProductoStockRipley.bodega);
		ajusteStockPage.ConsultarStock(/*oProductoStockRipley.codigoVenta*/"2000300111177", oProductoStockRipley.tipoInventario);
	}

	@Given("^se muestra el stock actual$")
	public void se_muestra_el_stock_actual() {

		int stockActual = Integer.parseInt(ajusteStockPage.ObtenerStockActual());

		Assert.assertTrue("Los valores no coinciden",
				ajusteStockPage.ValidarStock(stockActual, oProductoStockRipley.stock));
	}

	@Given("^selecciono tipo de actualizacion \"([^\"]*)\"$")
	public void selecciono_tipo_de_actualizacion(String tipoActualizacion)
			throws IOException, InterruptedException, SQLException, AWTException {
		this.tipoActualizacion = tipoActualizacion;
		switch (tipoActualizacion) {
		case "Incrementar":
			ajusteStockPage.SeleccionarIncrementar();
			break;
		case "Disminuir":
			ajusteStockPage.SeleccionarDisminuir();
			break;
		case "Reemplazar":
			ajusteStockPage.SeleccionarReemplazar();
			break;
		}
	}

	@When("^ingreso el stock a ajustar \"([^\"]*)\"$")
	public void ingreso_el_stock_a_ajustar(String stockAjuste) throws AWTException, InterruptedException {

		ajusteStockPage.IngresarAjuste(stockAjuste);
	}

	@Then("^se actualiza el valor del campo nuevo stock$")
	public void se_actualiza_el_valor_del_campo_nuevo_stock() {

		String operacion = tipoActualizacion;

		int stockActual = oProductoStockRipley.stock;
		int stockAjustar = Integer.parseInt(ajusteStockPage.ObtenerStockAjuste());
		int nuevoStockEsperado = operacion.equals("Incrementar") ? stockActual + stockAjustar
				: (operacion.equals("Disminuir") ? stockActual - stockAjustar : stockAjustar);

		int nuevoStockMostrado = Integer.parseInt(ajusteStockPage.ObtenerNuevoStock());

		Assert.assertTrue("Los valores no coinciden",
				ajusteStockPage.ValidarStock(nuevoStockEsperado, nuevoStockMostrado));
	}

	@When("^selecciono tipo de inventario \"([^\"]*)\"$")
	public void selecciono_tipo_inventario(String tipoInventario) throws InterruptedException, AWTException {
		ajusteStockPage.SeleccionarTipoInventario(tipoInventario);
	}

	@When("^doy click a consultar$")
	public void doy_click_a_consultar() throws InterruptedException, AWTException {
		ajusteStockPage.Consultar();
	}

	@When("^doy clic a actualizar$")
	public void doy_click_a_actualizar() {
		ajusteStockPage.Acualizar();
	}

	@When("^no completo el campo ajuste$")
	public void no_completo_el_campo_ajuste() {
	}

	@When("^completo el campo observacion$")
	public void completo_el_campo_observacion() throws AWTException, InterruptedException {
		ajusteStockPage.IngresarObservacion("QA test automation 01");
	}

	@Then("^se muestra mensaje \"([^\"]*)\"$")
	public void se_muestra_mensaje(String mensaje)
			throws IOException, InterruptedException, SQLException, AWTException {
		Assert.assertTrue("No se encontró el mensaje de error", ajusteStockPage.ExisteMensajeError(mensaje));
	}
	
	@Then("^se muestra mensaje SKU \"([^\"]*)\"$")
	public void se_muestra_mensaje_SKU(String mensaje)
			throws IOException, InterruptedException, SQLException, AWTException {
		/*  */
		BasePage.WaitSleep(3);
		System.out.println("Mensaje Esperado: " + mensaje + "\n");
		String mensajeObtenido = AjusteStockPage.ExisteMensajeError();
		System.out.println("Mensaje Obtenido: " + mensajeObtenido + "\n");
		Assert.assertEquals(mensajeObtenido, mensaje);
	}

	@Then("^el boton actualizar esta deshabilitado$")
	public void el_boton_consultar_esta_deshabilitado() throws IOException, InterruptedException {
		Assert.assertTrue("El botón Actualizar esta habilitado",
				ajusteStockPage.ExisteBotonDeshabilitado("Actualizar"));
	}

	@Then("^se marca en rojo el campo ajuste$")
	public void se_marca_en_rojo_el_campo_ajuste()
			throws IOException, InterruptedException, SQLException, AWTException {
		Assert.assertTrue("El campo ajuste no se marca en rojo", ajusteStockPage.ValidarCampoAjusteError());
	}

	@Then("^se marca en rojo el campo observacion$")
	public void se_marca_en_rojo_el_campo_observacion()
			throws IOException, InterruptedException, SQLException, AWTException {
		Assert.assertTrue("El campo observación no se marca en rojo", ajusteStockPage.ValidarCampoObservacionError());
	}

	@Then("^los campos vuelven a su valor inicial$")
	public void los_campos_vuelven_a_su_valor_inicial()
			throws IOException, InterruptedException, SQLException, AWTException {
		Assert.assertTrue("El campo Cod.Venta no limpió su valor", ajusteStockPage.ObtenerCodigoVenta().equals(""));
		Assert.assertTrue("El campo Ajuste no limpió su valor", ajusteStockPage.ObtenerStockAjuste().equals(""));
		Assert.assertTrue("El campo StockActual no limpió su valor", ajusteStockPage.ObtenerStockActual().equals("0"));
		Assert.assertTrue("El campo NuevoStock no limpió su valor", ajusteStockPage.ObtenerNuevoStock().equals("0"));

		String opcionTipoProductoSeleccionada = ajusteStockPage.ObtenerTextoComboTipoProducto();
		Assert.assertTrue("El combo TipoProducto no tiene por defecto la opcion 'Seleccione'",
				opcionTipoProductoSeleccionada.equals("1-Ripley"));

		String opcionTipoInventarioSeleccionada = ajusteStockPage.ObtenerTextoComboTipoInventario();
		Assert.assertTrue("El combo TipoProducto no tiene por defecto la opcion '1-Ripley'",
				opcionTipoInventarioSeleccionada.equals("Seleccione"));
	}

}
