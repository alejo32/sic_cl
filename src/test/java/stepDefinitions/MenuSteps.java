package stepDefinitions;

import java.awt.AWTException;
import java.io.IOException;
import java.sql.SQLException;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import utils.DriverFactory;

public class MenuSteps extends DriverFactory {

	@Given("^voy a la opcion Consulta de Stock por Jerarquia de Productos$")
	public void voy_a_la_opcion_Consulta_de_Stock_por_Jerarquia_de_Productos()
			throws IOException, InterruptedException {
		menuPage.IrMenu("Información", "Consulta de Stock por Jerarquía de Productos");
	}

	@Given("^voy a la opcion Auditoria de Transacciones de Inventario$")
	public void voy_a_la_opcion_Auditoria_de_Transacciones_de_Inventario() throws IOException, InterruptedException {
		menuPage.IrMenu("Información", "Auditoría de Transacciones de Inventario");
	}

	@Given("^voy a la opcion Configuracion de Movimientos de Inventario$")
	public void voy_a_la_opcion_configuracion_de_movimientos_de_inventario() throws IOException, InterruptedException {
		menuPage.IrMenu("Configuración", "Configuración de movimientos de inventario de sistemas a SIC");
	}

	@Given("^voy a la opcion Mantenedor para Ingresar Ajuste de Inventario$")
	public void voy_a_la_opcion_Mantenedor_para_Ingresar_Ajuste_de_Inventario()
			throws IOException, InterruptedException {
		menuPage.IrMenu("Mantenedor", "Mantenedor para ingresar ajuste de stock");
	}

	@Given("^voy a la opcion Mantenedor para Ingresar Stock de Seguridad$")
	public void voy_a_la_opcion_Mantenedor_para_Ingresar_Stock_de_Seguridad() throws IOException, InterruptedException {
		menuPage.IrMenu("Mantenedor", "Mantenedor para ingresar stock de seguridad");
	}

	@Given("^voy a la opcion Carga Masiva de Ajuste de Stock$")
	public void voy_a_la_opcion_Carga_Masiva_de_Ajuste_de_Stock() throws IOException, InterruptedException {
		menuPage.IrMenu("Cargas Masivas", "Carga Masiva de ajuste de Stock");
	}

	@Given("^voy a la opcion Carga Masiva de Stock Seguridad$")
	public void voy_a_la_opcion_Carga_Masiva_de_Stock_Seguridad() throws IOException, InterruptedException {
		menuPage.IrMenu("Cargas Masivas", "Carga Masiva de Stock Seguridad");
	}

	@Given("^voy a la opcion Mantenedor para ingresar stock de seguridad$")
	public void voy_a_la_opcion_Mantenedor_para_ingresar_stock_de_seguridad() throws IOException, InterruptedException {
		menuPage.IrMenu("Mantenedor", "Mantenedor para ingresar stock de seguridad");
	}

	@Given("^voy a la opcion Reporte de Cuadratura Automatico$")
	public void voy_a_la_opcion_Reporte_de_cuadratura_automatico() throws IOException, InterruptedException {
		menuPage.IrMenu("Información", "Reporte de Cuadratura Automático");
	}

	@When("^selecciono la bodega \"([^\"]*)\"$")
	public void selecciono_la_bodega_x(String bodega)
			throws IOException, InterruptedException, SQLException, AWTException {
		menuPage.SeleccionarBodega(bodega);
	}


}
