package stepDefinitions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import models.*;
import org.junit.Assert;
import pageObjects.BasePage;
import utils.DriverFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;

public class ServicioSteps extends DriverFactory {

	private String jsonString = "";
	private String sfullFileName = "";
	private List<EBodega> oBodegaList;
	private List<File> oArchivosList;
	private List<ESku> oListaProductos;
	private List<ESku> oListaProductosOriginal;
	private String idTrxHeader = "";
	private String sistema = "";
	private String transactionId = "";
	Response response;
	
	
	
	@Given("^quiero realizar un ajuste de inventario via REST para el sistema \"([^\"]*)\"$")
	public void quiero_realizar_un_ajuste_de_inventario_via_REST_para_el_sistema_x(String sistema) throws Throwable {

		this.sistema = sistema;
		SetTipoAjuste("ADJUST");

		EConexionBD oEConexionBD = new EConexionBD();
//		oEConexionBD.sshUser 		= GetSSHUser();
//		oEConexionBD.sshPassword 	= GetSSHPpk();
//		oEConexionBD.sshPort 		= Integer.parseInt(GetSSHPort());
//		oEConexionBD.dbHost 		= GetDBHost();
		oEConexionBD.sshHost 		= GetSSHHost();
		oEConexionBD.dbPort 		= Integer.parseInt(GetDBPort());
		oEConexionBD.dbUser 		= GetDBUser();
		oEConexionBD.dbPassword 	= GetDBPassword();
		database.ConnectarTunelSSH(oEConexionBD);

		database.HabilitarConfiguracionMovimientoInventario(GetSICCodigoPais(), GetTipoAjuste(), sistema, "1");
		database.HabilitarConfiguracionMovimientoInventario(GetSICCodigoPais(), GetTipoAjuste(), sistema, "2");
	}
	
	@Given("^quiero realizar una sincronizacion full via REST para el sistema \"([^\"]*)\"$")
	public void quiero_realizar_una_sincronizacion_full_via_REST_para_el_sistema_x(String sistema) throws Throwable {
		this.sistema = sistema;
		SetTipoAjuste("SFULL");

		EConexionBD oEConexionBD = new EConexionBD();
//		oEConexionBD.sshUser 		= GetSSHUser();
//		oEConexionBD.sshPassword	= GetSSHPpk();
//		oEConexionBD.sshPort 		= Integer.parseInt(GetSSHPort());
//		oEConexionBD.dbHost 		= GetDBHost();
		oEConexionBD.sshHost 		= GetSSHHost();
		oEConexionBD.dbPort 		= Integer.parseInt(GetDBPort());
		oEConexionBD.dbUser 		= GetDBUser();
		oEConexionBD.dbPassword 	= GetDBPassword();
		database.ConnectarTunelSSH(oEConexionBD);

		database.HabilitarConfiguracionMovimientoInventario(GetSICCodigoPais(), GetTipoAjuste(), sistema, "1");
		database.HabilitarConfiguracionMovimientoInventario(GetSICCodigoPais(), GetTipoAjuste(), sistema, "2");
	}

	@Given("^quiero realizar un ajuste delta via REST para el sistema \"([^\"]*)\"$")
	public void quiero_realizar_un_ajuste_delta_via_REST_para_el_sistema_x(String sistema) throws Throwable {
		this.sistema = sistema;
		SetTipoAjuste("SDELTA");

		EConexionBD oEConexionBD = new EConexionBD();
/*		oEConexionBD.sshUser 		= GetSSHUser();
		oEConexionBD.sshPassword	= GetSSHPpk();
		oEConexionBD.sshPort 		= Integer.parseInt(GetSSHPort());
		oEConexionBD.dbHost 		= GetDBHost();*/
		oEConexionBD.sshHost 		= GetSSHHost();
		oEConexionBD.dbPort 		= Integer.parseInt(GetDBPort());
		oEConexionBD.dbUser 		= GetDBUser();
		oEConexionBD.dbPassword 	= GetDBPassword();
		database.ConnectarTunelSSH(oEConexionBD);

		database.HabilitarConfiguracionMovimientoInventario(GetSICCodigoPais(), GetTipoAjuste(), sistema, "1");
		database.HabilitarConfiguracionMovimientoInventario(GetSICCodigoPais(), GetTipoAjuste(), sistema, "2");
	}
	@Given("^quiero realizar un ajuste delta via SQS para el sistema \"([^\"]*)\"$")
	public void quiero_realizar_un_ajuste_delta_via_SQS_para_el_sistema_x(String sistema) throws Throwable {
		this.sistema = sistema;
		SetTipoAjuste("ADJUST");

		EConexionBD oEConexionBD = new EConexionBD();
/*		oEConexionBD.sshUser 		= GetSSHUser();
		oEConexionBD.sshPassword	= GetSSHPpk();
		oEConexionBD.sshPort 		= Integer.parseInt(GetSSHPort());
		oEConexionBD.dbHost 		= GetDBHost();*/
		oEConexionBD.sshHost 		= GetSSHHost();
		oEConexionBD.dbPort 		= Integer.parseInt(GetDBPort());
		oEConexionBD.dbUser 		= GetDBUser();
		oEConexionBD.dbPassword 	= GetDBPassword();
		database.ConnectarTunelSSH(oEConexionBD);

		database.HabilitarConfiguracionMovimientoInventario(GetSICCodigoPais(), GetTipoAjuste(), sistema, "1");
		//database.HabilitarConfiguracionMovimientoInventario(GetSICCodigoPais(), GetTipoAjuste(), sistema, "2");
	}
	//Revisado
	@Then("^se registra en bd la cabecera con estado \"([^\"]*)\" mensaje \"([^\"]*)\"$")
	public void se_registra_en_bd_la_cabecera_con_estado_mensaje(String estado, String mensajeCabecera)
			throws Throwable {
		
		ETransaction oEntidad = database.EsperarProcesamientoTransaccion(idTrxHeader);
		transactionId = oEntidad.transactionId;
		System.out.println("TransactionID: " + oEntidad.transactionId);
		BasePage.WaitSleep(1);
		System.out.println("CABECERA: Estado de la transaccion: " + oEntidad.status);
		BasePage.WaitSleep(1);
		System.out.println("CABECERA: Commentario de la transaccion: " + oEntidad.comments);
		BasePage.WaitSleep(1);
		
		String mensajeError = "Estado esperado incorrecto: Muestra estado " + oEntidad.status + " When debió ser  " + estado;
		Assert.assertTrue(mensajeError, estado.equals(oEntidad.status));

		mensajeError = "Comentario incorrecto";
		Assert.assertTrue(mensajeError, oEntidad.comments.replace("\"", "").trim().contains(mensajeCabecera));
	}
	//Revisado
	@Then("^se registra en bd el detalle con estado \"([^\"]*)\" mensaje \"([^\"]*)\"$")
	public void se_registra_detalle_con_mensaje_de_error(String estadoDetalle, String mensaje) throws Throwable {
		int contador = 1;
		// mensaje=mensaje.replace("\"", "");
		List<ETransactionDetail> oLista = database.ObtenerResultadoDetalleTransaccion(transactionId);

		for (ETransactionDetail oEntidad : oLista) {

			System.out.println("ACTUAL: " + oEntidad.detail.replace("\"", "") + " - ESPERADO: " + mensaje);

			Assert.assertTrue("Estado esperado incorrecto: Muestra estado " + oEntidad.status + " When debió ser  "
					+ estadoDetalle, estadoDetalle.equals(oEntidad.status));
			Assert.assertTrue("Comentario incorrecto", mensaje.equals(oEntidad.detail.replace("\"", "").trim()));
			contador++;
		}
	}

	@Then("^no se registra en bd el detalle$")
	public void no_se_registra_detalle_de_transaccion() throws Throwable {
		List<ETransactionDetail> oLista = database.ObtenerResultadoDetalleTransaccion(transactionId);
		assertThat(oLista == null);

	}
	//Revisado
	@Then("^validar que no se registre en la bd$")
	public void validar_que_no_se_registre_en_la_bd() throws Throwable {
		ETransaction oEntidad = database.EsperarProcesamientoTransaccion(idTrxHeader);

		if (oEntidad == null)
			System.out.println("NULO - OK");
		else
			System.out.println("NO NULO - FAIL");

		assertThat(oEntidad == null).isTrue();

	}
	

	@Then("^se validan los datos del ajuste en bd$")
	public void se_validan_los_datos_del_ajuste_en_bd() throws Throwable {
		BasePage.WaitSleep(30);
		String transactionId	= cargaStockPage.ObtenerTransaccion();

		List<ETransactionDetail> oDetalleList = database.ObtenerResultadoDetalleTransaccion(transactionId);
		for (ETransactionDetail oEntidad : oDetalleList) {
			if (oEntidad.status.equals("C")) {
				EStock oStockProducto = database.ObtenerStockProductoBD(oEntidad.sku, oEntidad.fulfillmentCode,oEntidad.inventoryType);

				if (GetTipoAjuste().equals("ADJUST") || GetTipoAjuste().equals("MDADJUST")) {
					int nuevoStockEsperado = oEntidad.operation.equals("A")
							? Integer.parseInt(oEntidad.prevStock) + Integer.parseInt(oEntidad.stock)
							: Integer.parseInt(oEntidad.prevStock) - Integer.parseInt(oEntidad.stock);

					System.out.println("Stock esperado:" + oStockProducto.stock + " Stock actual:" + nuevoStockEsperado);
//					Assert.assertTrue("ERROR - Stock esperado:" + oStockProducto.stock + " Stock actual:" + nuevoStockEsperado, oStockProducto.stock == nuevoStockEsperado);
				} else {
					int nuevoStockEsperado = Integer.parseInt(oEntidad.stock);
//					Assert.assertTrue("ERROR - Stock esperado:" + oStockProducto.stock + " Stock actual:" + nuevoStockEsperado, oStockProducto.stock == nuevoStockEsperado);
					System.out.println("Stock esperado:" + oStockProducto.stock + " Stock actual:" + nuevoStockEsperado);
				}
			}
		}
	}
	
	@And("^preparo la trama de ajuste con \"([^\"]*)\" productos con tipo de operacion \"([^\"]*)\"$")
	public void preparo_la_trama_con_x_productos(String cantidadSku, String operation) throws Throwable {
		System.out.println("operation: " + operation);
		oListaProductos = database.ObtenerInformacionProductos(GetSICCodigoPais(), Integer.parseInt(cantidadSku));

		System.out.println("TotalProductos: " + oListaProductos.size());
		jsonString = tramaJson.GenerarTramaJsonAjusteInventario(oListaProductos, sistema, GetSICCodigoPais(),
				GetTipoAjuste(), "UserQA", operation, "NTD", false);

		String pattern = "AUT_ADJUST_([0-9])\\w+";
		idTrxHeader = servicio.ExtraerIdTrxHeader(jsonString, pattern);
	}

    @And("^preparo la trama de ajuste con \"([^\"]*)\" productos con operacion \"([^\"]*)\"  y con stock en \"([^\"]*)\"$")
    public void preparo_la_trama_con_x_productos(String cantidadSku, String operation, String Stock_Previo) throws Throwable {
        System.out.println("operation: " + operation);

        if(Stock_Previo == "Cero"){
            oListaProductos = database.ObtenerInformacionProductosConStock0(GetSICCodigoPais(), Integer.parseInt(cantidadSku));

        }else if (Stock_Previo == "Negativo"){

        }else if (Stock_Previo == "Positivo"){

        }

        oListaProductos = database.ObtenerInformacionProductos(GetSICCodigoPais(), Integer.parseInt(cantidadSku));

        System.out.println("TotalProductos: " + oListaProductos.size());
        jsonString = tramaJson.GenerarTramaJsonAjusteInventario(oListaProductos, sistema, GetSICCodigoPais(),
                GetTipoAjuste(), "UserQA", operation, "NTD", false);

        String pattern = "AUT_ADJUST_([0-9])\\w+";
        idTrxHeader = servicio.ExtraerIdTrxHeader(jsonString, pattern);
    }

	@And("^preparo la trama de ajuste con \"([^\"]*)\" productos$")
	public void preparo_la_trama_con_x_productos(String cantidadSku) throws Throwable {

		oListaProductos = database.ObtenerInformacionProductos(GetSICCodigoPais(), Integer.parseInt(cantidadSku));

		System.out.println("TotalProductos: " + oListaProductos.size());
		jsonString = tramaJson.GenerarTramaJsonAjusteInventario(oListaProductos, sistema, GetSICCodigoPais(),
				GetTipoAjuste(), "UserQA", "", "NTD", false);

		String pattern = "AUT_ADJUST_([0-9])\\w+";
		idTrxHeader = servicio.ExtraerIdTrxHeader(jsonString, pattern);
	}

	@And("^preparo la trama de ajuste con \"([^\"]*)\" productos, con stock \"([^\"]*)\" previamente cargado en SIC$")
	public void preparo_la_trama_con_x_productos_con_stock(String cantidadSku) throws Throwable {

		oListaProductos = database.ObtenerInformacionProductos(GetSICCodigoPais(), Integer.parseInt(cantidadSku));

		System.out.println("TotalProductos: " + oListaProductos.size());
		jsonString = tramaJson.GenerarTramaJsonAjusteInventario(oListaProductos, sistema, GetSICCodigoPais(),
				GetTipoAjuste(), "UserQA", "", "NTD", false);

		String pattern = "AUT_ADJUST_([0-9])\\w+";
		idTrxHeader = servicio.ExtraerIdTrxHeader(jsonString, pattern);
	}

	
	@And("^preparo la trama de ajuste con \"([^\"]*)\" productos con error en un registro$")
	public void preparo_la_trama_con_x_productos_con_error_en_un_registro(String cantidadSku) throws Throwable {

		oListaProductos = database.ObtenerInformacionProductos(GetSICCodigoPais(), Integer.parseInt(cantidadSku));

		ESku oEntidad = new ESku();
		oEntidad.sku = "3002121200001";
		oEntidad.seller = "";
		oEntidad.bodega = "";
		oEntidad.tipoInventario = "1";
		oEntidad.stock = "10";
		oEntidad.iStock = 10;
		oEntidad.esImpar = "1";

		oListaProductos.add(oEntidad);

		System.out.println("TotalProductos: " + oListaProductos.size());
		jsonString = tramaJson.GenerarTramaJsonAjusteInventario(oListaProductos, sistema, GetSICCodigoPais(),
				GetTipoAjuste(), "UserQA", "", "NTD", false);

		String pattern = "AUT_ADJUST_([0-9])\\w+";
		idTrxHeader = servicio.ExtraerIdTrxHeader(jsonString, pattern);
	}

	@And("^preparo la trama de ajuste con \"([^\"]*)\" productos con stock igual al actual con ajuste negativo$")
	public void preparo_la_trama_con_x_productos_con_stock_igual_al_actual_c_ajuste_negativo(String cantidadSku)
			throws Throwable {

		oListaProductos = database.ObtenerInformacionProductos(GetSICCodigoPais(), Integer.parseInt(cantidadSku));

		System.out.println("TotalProductos: " + oListaProductos.size());
		jsonString = tramaJson.GenerarTramaJsonAjusteInventario(oListaProductos, sistema, GetSICCodigoPais(),
				GetTipoAjuste(), "UserQA", "S", "NTD", true);

		String pattern = "AUT_ADJUST_([0-9])\\w+";
		idTrxHeader = servicio.ExtraerIdTrxHeader(jsonString, pattern);
	}
	//Revisado
	@And("^preparo la trama de ajuste con \"([^\"]*)\" productos que no existen$")
	public void preparo_la_trama_con_x_productos_que_no_existen(String cantidadSku) throws Throwable {

		oListaProductos = database.ObtenerInformacionProductosNoExiste(GetSICCodigoPais(),
				Integer.parseInt(cantidadSku));

		jsonString = tramaJson.GenerarTramaJsonAjusteInventario(oListaProductos, sistema, GetSICCodigoPais(),
				GetTipoAjuste(), "UserQA", "", "NTD", false);

		String pattern = "AUT_ADJUST_([0-9])\\w+";
		idTrxHeader = servicio.ExtraerIdTrxHeader(jsonString, pattern);
	}

	@And("^preparo la trama de ajuste con \"([^\"]*)\" productos con id detalle duplicado$")
	public void preparo_la_trama_con_x_productos_con_id_detalle_duplicado(String cantidadSku) throws Throwable {

		oListaProductos = database.ObtenerInformacionProductos(GetSICCodigoPais(), Integer.parseInt(cantidadSku));

		jsonString = tramaJson.GenerarTramaJsonAjusteInventarioConIdDetalleDuplicado(oListaProductos, sistema,
				GetSICCodigoPais(), GetTipoAjuste(), "UserQA");

		String pattern = "AUT_ADJUST_([0-9])\\w+";
		idTrxHeader = servicio.ExtraerIdTrxHeader(jsonString, pattern);
	}

	@And("^preparo la trama de ajuste con \"([^\"]*)\" productos con Operacion \"([^\"]*)\"$")
	public void preparo_la_trama_con_x_productos_con_operacion_y(String cantidadSku, String operacion)
			throws Throwable {

		oListaProductos = database.ObtenerInformacionProductos(GetSICCodigoPais(), Integer.parseInt(cantidadSku));

		jsonString = tramaJson.GenerarTramaJsonAjusteInventario(oListaProductos, sistema, GetSICCodigoPais(),
				GetTipoAjuste(), "UserQA", operacion, "NTD", false);

		String pattern = "AUT_ADJUST_([0-9])\\w+";
		idTrxHeader = servicio.ExtraerIdTrxHeader(jsonString, pattern);
	}
	//Revisado
	@Given("^preparo la trama de ajuste con \"([^\"]*)\" productos sin datos en el campo Sistema Origen$")
	public void preparo_la_trama_con_x_productos_sin_datos_en_campo_sistema_origen(String cantidadSku)
			throws Throwable {

		oListaProductos = database.ObtenerInformacionProductos(GetSICCodigoPais(), Integer.parseInt(cantidadSku));

		jsonString = tramaJson.GenerarTramaJsonAjusteInventario(oListaProductos, "", GetSICCodigoPais(),
				GetTipoAjuste(), "UserQA", "", "NTD", false);

		String pattern = "AUT_ADJUST_([0-9])\\w+";
		idTrxHeader = servicio.ExtraerIdTrxHeader(jsonString, pattern);
		BasePage.WaitSleep(15);
	}

	@Given("^preparo la trama de ajuste con \"([^\"]*)\" productos sin datos en el campo Documento de Transaccion$")
	public void preparo_la_trama_con_x_productos_sin_datos_en_campo_documento_transaccion(String cantidadSku)
			throws Throwable {

		oListaProductos = database.ObtenerInformacionProductos(GetSICCodigoPais(), Integer.parseInt(cantidadSku));

		jsonString = tramaJson.GenerarTramaJsonAjusteInventario(oListaProductos, sistema, GetSICCodigoPais(),
				GetTipoAjuste(), "UserQA", "", "", false);

		String pattern = "AUT_ADJUST_([0-9])\\w+";
		idTrxHeader = servicio.ExtraerIdTrxHeader(jsonString, pattern);
	}

	@Given("^preparo la trama de ajuste con \"([^\"]*)\" productos sin datos en el campo IDTRX$")
	public void preparo_la_trama_con_x_productos_sin_datos_en_campo_idtrx(String cantidadSku) throws Throwable {

		oListaProductos = database.ObtenerInformacionProductos(GetSICCodigoPais(), Integer.parseInt(cantidadSku));

		jsonString = tramaJson.GenerarTramaJsonAjusteInventario(oListaProductos, sistema, GetSICCodigoPais(),
				GetTipoAjuste(), "UserQA", "", "NTD", false, false, true, false, false, false);

		String pattern = "AUT_ADJUST_([0-9])\\w+";
		idTrxHeader = servicio.ExtraerIdTrxHeader(jsonString, pattern);
	}

	@Given("^preparo la trama de ajuste con \"([^\"]*)\" productos sin datos en el campo Bodega$")
	public void preparo_la_trama_con_x_productos_sin_datos_en_campo_bodega(String cantidadSku) throws Throwable {

		oListaProductos = database.ObtenerInformacionProductos(GetSICCodigoPais(), Integer.parseInt(cantidadSku));

		jsonString = tramaJson.GenerarTramaJsonAjusteInventario(oListaProductos, sistema, GetSICCodigoPais(),
				GetTipoAjuste(), "UserQA", "", "NTD", false, false, false, true, false, false);

		String pattern = "AUT_ADJUST_([0-9])\\w+";
		idTrxHeader = servicio.ExtraerIdTrxHeader(jsonString, pattern);
	}

	@Given("^preparo la trama de ajuste con \"([^\"]*)\" productos sin datos en el campo Tipo de Inventario$")
	public void preparo_la_trama_con_x_productos_sin_datos_en_campo_tipo_inventario(String cantidadSku)
			throws Throwable {

		oListaProductos = database.ObtenerInformacionProductos(GetSICCodigoPais(), Integer.parseInt(cantidadSku));

		jsonString = tramaJson.GenerarTramaJsonAjusteInventario(oListaProductos, sistema, GetSICCodigoPais(),
				GetTipoAjuste(), "UserQA", "", "NTD", false, false, false, false, true, false);

		String pattern = "AUT_ADJUST_([0-9])\\w+";
		idTrxHeader = servicio.ExtraerIdTrxHeader(jsonString, pattern);
	}

	@Given("^preparo la trama de ajuste con \"([^\"]*)\" productos sin datos en el campo Operacion$")
	public void preparo_la_trama_con_x_productos_sin_datos_en_campo_operacion(String cantidadSku) throws Throwable {

		oListaProductos = database.ObtenerInformacionProductos(GetSICCodigoPais(), Integer.parseInt(cantidadSku));

		jsonString = tramaJson.GenerarTramaJsonAjusteInventario(oListaProductos, sistema, GetSICCodigoPais(),
				GetTipoAjuste(), "UserQA", "", "NTD", false, false, false, false, false, true);

		String pattern = "AUT_ADJUST_([0-9])\\w+";
		idTrxHeader = servicio.ExtraerIdTrxHeader(jsonString, pattern);
	}
	//Revisado
	@Given("^preparo la trama de ajuste con \"([^\"]*)\" productos sin datos en el campo Tipo de Movimiento$")
	public void preparo_la_trama_con_x_productos_sin_datos_en_campo_tipo_de_movimiento(String cantidadSku)
			throws Throwable {

		oListaProductos = database.ObtenerInformacionProductos(GetSICCodigoPais(), Integer.parseInt(cantidadSku));

		jsonString = tramaJson.GenerarTramaJsonAjusteInventario(oListaProductos, sistema, GetSICCodigoPais(), "",
				"UserQA", "", "NTD", false);

		String pattern = "AUT_ADJUST_([0-9])\\w+";
		idTrxHeader = servicio.ExtraerIdTrxHeader(jsonString, pattern);
	}
	//Revisado
	@Given("^preparo la trama de ajuste con \"([^\"]*)\" productos sin datos en el campo Usuario$")
	public void preparo_la_trama_con_x_productos_sin_datos_en_campo_usuario(String cantidadSku) throws Throwable {

		oListaProductos = database.ObtenerInformacionProductos(GetSICCodigoPais(), Integer.parseInt(cantidadSku));

		jsonString = tramaJson.GenerarTramaJsonAjusteInventario(oListaProductos, sistema, GetSICCodigoPais(),
				GetTipoAjuste(), "", "", "NTD", false);

		String pattern = "AUT_ADJUST_([0-9])\\w+";
		idTrxHeader = servicio.ExtraerIdTrxHeader(jsonString, pattern);
	}
	//Revisado
	@And("^preparo la trama de ajuste con \"([^\"]*)\" productos sin stock para la bodega \"([^\"]*)\"$")
	public void preparo_la_trama_con_x_sku_sin_stock(String cantidadSku, String bodega) throws Throwable {

		oListaProductos = database.ObtenerInformacionProductosSinStock(GetSICCodigoPais(),Integer.parseInt(cantidadSku), bodega);

		jsonString = tramaJson.GenerarTramaJsonAjusteInventario(oListaProductos, sistema, GetSICCodigoPais(),
				GetTipoAjuste(), "UserQA", "", "NTD", false);

		String pattern = "AUT_ADJUST_([0-9])\\w+";
		idTrxHeader = servicio.ExtraerIdTrxHeader(jsonString, pattern);
	}
	//Revisado
	@And("^preparo la trama de ajuste con \"([^\"]*)\" productos con stock 0 con ajuste negativo$")
	public void preparo_la_trama_con_x_productos_con_stock_0_con_ajuste_negativo(String cantidadSku) throws Throwable {

		oListaProductos = database.ObtenerInformacionProductosConStock0(GetSICCodigoPais(), Integer.parseInt(cantidadSku));

		jsonString = tramaJson.GenerarTramaJsonAjusteInventarioNegativo(oListaProductos, sistema, GetSICCodigoPais());

		String pattern = "AUT_ADJUST_([0-9])\\w+";
		idTrxHeader = servicio.ExtraerIdTrxHeader(jsonString, pattern);
	}

	@And("^preparo la trama de ajuste con \"([^\"]*)\" productos con ajuste negativo$")
	public void preparo_la_trama_con_x_productos_con_ajuste_negativo(String cantidadSku) throws Throwable {

		oListaProductos = database.ObtenerInformacionProductos(GetSICCodigoPais(), Integer.parseInt(cantidadSku));

		jsonString = tramaJson.GenerarTramaJsonAjusteInventarioNegativo(oListaProductos, sistema, GetSICCodigoPais());

		String pattern = "AUT_ADJUST_([0-9])\\w+";
		idTrxHeader = servicio.ExtraerIdTrxHeader(jsonString, pattern);
	}
	//Revisado
	@When("^envio un request de ajuste al api$")
	public void envio_un_request_de_ajuste_de_inventario_al_api() throws Throwable {

		servicio.EnviarRequest(sistema.contains("PMM") ? GetAPIWrapperAjuste() : GetAPIConnectAjuste(), jsonString);

	}

	/* ++COLA SQS NECESARIA++ */
	@When("^envio un request de ajuste via SQS$")
	public void envio_un_request_de_ajuste_de_inventario_via_SQS() throws Throwable {

		String accountId	= GetAWSAccountId();
		String usuario 		= GetAWSUsuario();
		String password 	= GetAWSPassword();
		String urlSQSPMM	= GetSQSPMM();

		System.out.println("Cargar Página de AWS");
		BasePage.WaitSleep(2);
		awsLoginPage.AbrirAWS();
		System.out.println("Login de Usuario: " + accountId + " Usuario: " + usuario + " password: " + password);
		BasePage.WaitSleep(2);
		awsLoginPage.Login(accountId, usuario, password);
		BasePage.WaitSleep(15);	
		awsLoginPage.IrUrl(urlSQSPMM);
//		awsLoginPage.IrUrl("https://console.aws.amazon.com/sqs/home?region=us-east-1#send-message:selected=https://sqs.us-east-1.amazonaws.com/322525823727/sicm-qa-cl-req-pmm.fifo;noRefresh=true;prefix=sicm-qa-cl-req-pmm.fifo$");
		System.out.println("Ingresar a cola SQS: " + urlSQSPMM);
		BasePage.WaitSleep(10);
		System.out.println("JSON: " + jsonString);
		BasePage.WaitSleep(3);
		awsSQSPage.IngresarDatos(jsonString);
		BasePage.WaitSleep(3);
		System.out.println("Se termina de ingresar datos");
		awsSQSPage.EnviarMensajeSQS();
		BasePage.WaitSleep(3);
//		awsLoginPage.Logout();

	}

	@When("^envio un request de ajuste via SQS a \"([^\"]*)\"$")
	public void envio_un_request_de_ajuste_via_SQS_a(String Sistema) throws Throwable {

		String accountId	= GetAWSAccountId();
		String usuario 		= GetAWSUsuario();
		String password 	= GetAWSPassword();
		String urlSQSPMM	= GetSQSPMM();
		String urlSQSBGT	= GetSQSBGT();
		String urlSQSBO	= GetSQSBO();
		String URLSQS       = "";

		if (sistema.equals("PMM")) {
			URLSQS = urlSQSPMM;
		}else if (sistema.equals("BGT")){
			URLSQS = urlSQSBGT;
		}else if (sistema.equals("BO")){
			URLSQS = urlSQSBO;
		}

		System.out.println("Cdddddargar Página de AWS" + URLSQS);
		BasePage.WaitSleep(2);
		awsLoginPage.AbrirAWS();
		System.out.println("Login de Usuario: " + accountId + " Usuario: " + usuario + " password: " + password);
		BasePage.WaitSleep(2);
		awsLoginPage.Login(accountId, usuario, password);
		BasePage.WaitSleep(3);	//15
		awsLoginPage.IrUrl(URLSQS);
		System.out.println("Ingresar a cola SQS: " + URLSQS);
		BasePage.WaitSleep(3);//10
		System.out.println(jsonString);
		BasePage.WaitSleep(3);
		awsSQSPage.IngresarDatos(jsonString);
		BasePage.WaitSleep(3);
		awsSQSPage.EnviarMensajeSQS();
		BasePage.WaitSleep(3);
//		awsLoginPage.Logout();

	}

	@And("^envio un request de ajuste al API Connect$")
	public void envio_un_request_de_ajuste_de_inventario_al_api_connect() throws Throwable {
		servicio.EnviarRequest(GetAPIConnectAjuste(), jsonString);
	}

	@Given("^tengo informacion de \"([^\"]*)\" productos$")
	public void obtengo_informacion_de_x_productos(String cantidad) throws Throwable {
		List<ESku> oSkuList = new ArrayList<ESku>();
		oSkuList = database.ObtenerCodigoVenta(GetSICCodigoPais(), Integer.parseInt(cantidad));
		System.out.println("Productos obtenidos: " + oSkuList.size());
	}

	@Given("^tengo informacion de \"([^\"]*)\" producto que no existen$")
	public void obtengo_informacion_de_x_productos_que_no_existen(String cantidad) throws Throwable {
		List<ESku> oSkuList = new ArrayList<ESku>();
		Random random = new Random();
		int iCantidad = Integer.parseInt(cantidad);
		for (int i = 0; i < iCantidad; i++) {
			int numeroRandom = random.nextInt(90000 - 10000) + 90000;
			ESku oEntidad = new ESku();
			oEntidad.sku = "33300000" + numeroRandom;
			oSkuList.add(oEntidad);
		}
		System.out.println("Productos obtenidos: " + oSkuList.size());
	}

	@Given("^tengo informacion de \"([^\"]*)\" bodegas$")
	public void obtengo_informacion_de_x_bodegas(String cantidad) throws Throwable {
		oBodegaList = new ArrayList<EBodega>();
		oBodegaList = database.ObtenerBodegas(GetSICCodigoPais(), Integer.parseInt(cantidad));
		System.out.println("Bodegas obtenidas: " + oBodegaList.size());

	}

	@Given("^tengo informacion de \"([^\"]*)\" bodega que no existe$")
	public void obtengo_informacion_de_x_bodega_que_no_existe(String cantidad) throws Throwable {
		oBodegaList = new ArrayList<EBodega>();
		Random random = new Random();
		int iCantidad = Integer.parseInt(cantidad);
		for (int i = 0; i < iCantidad; i++) {
			int numeroRandom = random.nextInt(99 - 11) + 11;
			EBodega oEntidad = new EBodega();
			oEntidad.codigoBodega = "400" + numeroRandom;
			oEntidad.nombre = "Bodega No existe " + "400" + numeroRandom;
			oEntidad.codigoPais = "PE";
			oBodegaList.add(oEntidad);
		}
		System.out.println("Bodegas obtenidas: " + oBodegaList.size());

	}

	@And("^genero un archivo por bodega con extension incorrecta$")
	public void genero_un_archivo_por_bodega_con_extension_incorrecta() throws Throwable {

		String rutaSftpSfullLocal = GetRutaCarpetaLocalSFULL();
		System.out.println("Ruta local destino de achivos: " + rutaSftpSfullLocal);

		oArchivosList = new ArrayList<File>();
		for (EBodega oBodega : oBodegaList) {
			List<ESku> oLista = new ArrayList<ESku>();
			for (ESku oSku : oListaProductos) {
				ESku oEntidad 			= new ESku();
				oEntidad.sku 			= oSku.sku;
				oEntidad.bodega 		= oBodega.codigoBodega;
				oEntidad.seller 		= "Seller";
				oEntidad.tipoInventario	= "1";
				oEntidad.stock 			= "1";
				oEntidad.stockEsDiferente	= "1";
				oLista.add(oEntidad);
			}

			String nombreArchivoCsv = archivo.CrearArchivoSFull(rutaSftpSfullLocal, oLista, ".txt");
			System.out.println("Punto1 - Archivo : " + nombreArchivoCsv);
			oArchivosList.add(new File(rutaSftpSfullLocal + "\\" + nombreArchivoCsv));
		}
	}

	@Given("^comprimo los archivos en formato \"([^\"]*)\"$")
	public void comprimo_los_archivos_en_formato_x(String extension) throws Throwable {

		String rutaSftpSfullLocal = GetRutaCarpetaLocalSFULL();
		sfullFileName = archivo.ComprimirArchivos(rutaSftpSfullLocal, extension, oArchivosList);
	}

	@When("^cargo el archivo sfull en el servidor sftp$")
	public void cargo_el_archivo_en_el_servidor_FTP() throws Throwable {

		String host 		= GetSFTPHost();
		int port 			= Integer.parseInt(GetSFTPPort());
		String user 		= GetSFTPUser();
		String password 	= GetSFTPPassword();
		String source 		= GetRutaCarpetaLocalSFULL();
		String destination	= GetRutaCarpetaRemotaSFULL();

		sftpClient.Connect(host, port, user, password);
		System.out.println("Conectado a SFTP: " + host + ":" + port);
		sftpClient.Upload(host, source + "\\" + sfullFileName, destination);
		sftpClient.Disconnect();
		BasePage.WaitSleep(2);
	}

	@When("^envio un request de sfull al api$")
	public void envio_un_request_de_SFULL_al_api() throws Throwable {

		jsonString = tramaJson.GenerarTramaJsonSincronizacionFull(sfullFileName, sistema, GetSICCodigoPais(), "UserQA");

		String pattern = "AUT_SFULL_([0-9])\\w+";
		idTrxHeader = servicio.ExtraerIdTrxHeader(jsonString, pattern);

		if (sistema.equals("PMM"))
			response = servicio.EnviarRequest(GetAPIWrapperSFULL(), jsonString);
		else
			response = servicio.EnviarRequest(GetAPIConnectSFULL(), jsonString);
	}

	@When("^envio un request de sfull al api sin datos en la columna usuario$")
	public void envio_un_request_de_SFULL_al_API_sin_Datos_en_la_columna_usuario() throws Throwable {

		jsonString = tramaJson.GenerarTramaJsonSincronizacionFull(sfullFileName, sistema, GetSICCodigoPais(), "");

		String pattern = "AUT_SFULL_([0-9])\\w+";
		idTrxHeader = servicio.ExtraerIdTrxHeader(jsonString, pattern);

		if (sistema.equals("PMM"))
			response = servicio.EnviarRequest(GetAPIWrapperSFULL(), jsonString);
		else
			response = servicio.EnviarRequest(GetAPIConnectSFULL(), jsonString);

	}

	@Given("^genero un archivo sfull con extension \"([^\"]*)\"$")
	public void genero_un_archivo_SFull_con_extension(String extension) throws Throwable {

		String rutaSftpSfullLocal = GetRutaCarpetaLocalSFULL();

		sfullFileName = archivo.CrearArchivoSFull(rutaSftpSfullLocal, null, extension);
	}

	@Given("^genero un archivo csv por bodega$")
	public void genero_un_archivo_csv_por_bodega() throws Throwable {

		String rutaSftpSfullLocal	= GetRutaCarpetaLocalSFULL();
		String nombreArchivoCsv 	= "";

		Random random = new Random();

		for (EBodega oBodega : oBodegaList) {
			List<ESku> oLista = new ArrayList<ESku>();
			for (ESku oSku : oListaProductos) {
				ESku oEntidad 			= new ESku();
				oEntidad.sku 			= oSku.sku;
				oEntidad.bodega 		= oBodega.codigoBodega;
				oEntidad.seller 		= "Seller";
				oEntidad.tipoInventario	= "1";
				oEntidad.stock 			= String.valueOf(random.nextInt(399 - 10) + 10);
				oLista.add(oEntidad);
			}

			nombreArchivoCsv = archivo.CrearArchivoSFull(rutaSftpSfullLocal, oLista, ".csv");
			System.out.println("NOMBRE ARCHIVO: " + nombreArchivoCsv);
			System.out.println("DESTINO LOCAL:" + rutaSftpSfullLocal);
			oArchivosList = new ArrayList<File>();
			oArchivosList.add(new File(rutaSftpSfullLocal + "\\" + nombreArchivoCsv));
		}
	}

	@Given("^genero un archivo sfull con la columna stock vacia$")
	public void genero_un_archivo_SFull_con_la_columna_Stock_vacia() throws Throwable {

		String rutaSftpSfullLocal = GetRutaCarpetaLocalSFULL();

		oListaProductos = database.ObtenerCodigoVenta(GetSICCodigoPais(), 1);
		oBodegaList = database.ObtenerBodegas(GetSICCodigoPais(), 1);

		List<ESku> oLista = new ArrayList<ESku>();
		for (EBodega oBodega : oBodegaList) {
			for (ESku oSku : oListaProductos) {
				ESku oEntidad 				= new ESku();
				oEntidad.sku 				= oSku.sku;
				oEntidad.bodega 			= oBodega.codigoBodega;
				oEntidad.seller 			= "Seller";
				oEntidad.tipoInventario		= "1";
				oEntidad.stock 				= "1";
				oEntidad.stockEsDiferente 	= "1";
				oLista.add(oEntidad);
			}
		}
		sfullFileName = archivo.CrearArchivoSFull(rutaSftpSfullLocal, oLista, true, true, true, true, false);

	}

	@Then("^se obtiene un response con mensaje \"([^\"]*)\"$")
	public void se_obtiene_un_response_con_mensaje(String mensaje) throws Throwable {
		Map<String, String> company = response.jsonPath().getMap("extendedError.details[0]");
		String mensajeObtenido = company.get("message");

		System.out.println("Mensaje esperado: " + mensaje);
		System.out.println("Mensaje obtenido: " + mensajeObtenido);
		assertThat(mensajeObtenido.contains(mensaje)).isTrue();
	}

	@Then("^se obtiene un response con mensaje de error \"([^\"]*)\"$")
	public void se_obtiene_un_response_con_mensaje_de_error(String mensaje) throws Throwable {
		String mensajeObtenido = response.jsonPath().get("message");

		System.out.println("Mensaje esperado: " + mensaje);
		System.out.println("Mensaje obtenido: " + mensajeObtenido);
		assertThat(mensajeObtenido.contains(mensaje)).isTrue();
	}

	@Given("^genero un archivo sfull para una bodega que no existe$")
	public void genero_un_archivo_SFull_para_una_bodega_que_no_existe() throws Throwable {

		String rutaSftpSfullLocal = GetRutaCarpetaLocalSFULL();

		oListaProductos = database.ObtenerCodigoVenta(GetSICCodigoPais(), 1);
		oBodegaList = database.ObtenerBodegas(GetSICCodigoPais(), 1);

		List<ESku> oLista = new ArrayList<ESku>();
		for (EBodega oBodega : oBodegaList) {
			for (ESku oSku : oListaProductos) {
				ESku oEntidad 				= new ESku();
				oEntidad.sku 				= oSku.sku;
				oEntidad.bodega 			= "99933";
				oEntidad.seller 			= "Seller";
				oEntidad.tipoInventario 	= "1";
				oEntidad.stock 				= "1";
				oEntidad.stockEsDiferente	= "1";
				oLista.add(oEntidad);
			}
		}
		sfullFileName = archivo.CrearArchivoSFull(rutaSftpSfullLocal, oLista, true, true, true, true, true);
	}

	@Given("^genero un archivo sfull con la columna seller vacia$")
	public void genero_un_archivo_SFull_con_la_columna_SellerId_vacia() throws Throwable {

		String rutaSftpSfullLocal = GetRutaCarpetaLocalSFULL();

		oListaProductos = database.ObtenerCodigoVenta(GetSICCodigoPais(), 1);
		oBodegaList = database.ObtenerBodegas(GetSICCodigoPais(), 1);

		List<ESku> oLista = new ArrayList<ESku>();
		for (EBodega oBodega : oBodegaList) {
			for (ESku oSku : oListaProductos) {
				ESku oEntidad 				= new ESku();
				oEntidad.sku 				= oSku.sku;
				oEntidad.bodega 			= oBodega.codigoBodega;
				oEntidad.seller 			= "Seller";
				oEntidad.tipoInventario 	= "1";
				oEntidad.stock 				= "1";
				oEntidad.stockEsDiferente	= "1";
				oLista.add(oEntidad);
			}
		}
		sfullFileName = archivo.CrearArchivoSFull(rutaSftpSfullLocal, oLista, true, true, false, true, true);
	}

	@Given("^genero un archivo sfull con la columna sku vacia$")
	public void genero_un_archivo_SFull_con_la_columna_Sku_vacia() throws Throwable {

		String rutaSftpSfullLocal = GetRutaCarpetaLocalSFULL();

		List<ESku> oLista = new ArrayList<ESku>();

		ESku oEntidad = new ESku();
		oEntidad.sku 				= "";
		oEntidad.bodega 			= "10012";
		oEntidad.seller 			= "Seller";
		oEntidad.tipoInventario 	= "1";
		oEntidad.stock 				= "1";
		oEntidad.stockEsDiferente	= "1";
		oLista.add(oEntidad);

		sfullFileName = archivo.CrearArchivoSFull(rutaSftpSfullLocal, oLista, false, true, true, true, true);
	}

	@Given("^genero un archivo sfull sin datos$")
	public void genero_un_archivo_SFull_sin_datos() throws Throwable {

		String rutaSftpSfullLocal = GetRutaCarpetaLocalSFULL();

		sfullFileName = archivo.CrearArchivoSFull(rutaSftpSfullLocal, null, false, true, true, true, true);
	}

	@Given("^genero un archivo sfull con los datos obtenidos$")
	public void genero_un_archivo_sfull_con_los_datos_obtenidos() throws Throwable {

		String rutaSftpSfullLocal = GetRutaCarpetaLocalSFULL();
		sfullFileName = archivo.CrearArchivoSFull(rutaSftpSfullLocal, oListaProductos, true, true, true, true, true);
	}

	@Given("^obtengo \"([^\"]*)\" registros con stock igual al actual$")
	public void genero_un_archivo_SFull_con_x_registros_con_stock_igual_al_actual(String dbLimit) throws Throwable {
		oListaProductos = database.ObtenerSkuConStockActual(GetSICCodigoPais(), Integer.parseInt(dbLimit));
	}

	@Given("^obtengo \"([^\"]*)\" registros con stock igual y diferente$")
	public void genero_un_archivo_SFull_con_x_registros_con_stock_igual_y_diferente(String dbLimit) throws Throwable {

		oListaProductos = database.ObtenerSkuConStockActualYDiferente(GetSICCodigoPais(), Integer.parseInt(dbLimit));
	}

	@And("^envio nuevamente el mismo request de ajuste via SQS$")
	public void envio_nuevamente_el_mismo_request_de_ajuste_via_SQS() throws Throwable {
		Thread.sleep(5000);

		String accountId	= GetAWSAccountId();
		String usuario 		= GetAWSUsuario();
		String password		= GetAWSPassword();
		String url 			= GetAWSUrlMensajeSQSAjuste();

		awsLoginPage.AbrirAWS();
		awsLoginPage.Login(accountId, usuario, password);
		awsLoginPage.IrUrl(url);

		System.out.println(jsonString);
		String nuevoIdTrxHeader = idTrxHeader + "_2";
		System.out.println("IDTRX_ANTES: " + idTrxHeader);

		jsonString = jsonString.replace(idTrxHeader, nuevoIdTrxHeader);
		idTrxHeader = nuevoIdTrxHeader;
		System.out.println("IDTRX_DESPUES: " + idTrxHeader);
		System.out.println("NUEVO JSON: " + jsonString);
		awsSQSPage.IngresarDatos(jsonString);
		awsSQSPage.EnviarMensajeSQS();
		awsLoginPage.Logout();
	}

	@And("^borro el valor de el campo producto$")
	public void borro_el_valor_de_el_campo_producto() throws Throwable {
		for (ESku oEntidad : oListaProductos) {
			jsonString = jsonString.replace(oEntidad.sku, "");
		}
		System.out.println("NUEVO JSON: " + jsonString);
	}

	@And("^asigno la bodega \"([^\"]*)\"$")
	public void asigno_la_bodega(String bodega) throws Throwable {
		for (ESku oEntidad : oListaProductos) {
			jsonString = jsonString.replace("\"" + oEntidad.bodega + "\"", "\"" + bodega + "\"");
		}
		System.out.println("NUEVO JSON: " + jsonString);
	}

	@And("^asigno stock igual al actual$")
	public void asigno_stock_igual_al_actual() throws Throwable {

		String pattern = ":([0-9])\\w+,";
		String textoReemplazar = "";
		int contador = 0;
		for (ESku oEntidad : oListaProductos) {
			Pattern r = Pattern.compile(pattern);
			Matcher m = r.matcher(jsonString);
			if (m.find())
				textoReemplazar = m.group(contador);
			else
				throw new Exception("Ocurrió un error al reemplazar el stock: asigno_stock_igual_al_actual()");

			jsonString = jsonString.replace(textoReemplazar, ":" + oEntidad.stock + ",");

			contador++;
		}
		System.out.println("NUEVO JSON: " + jsonString);
	}
	//Revisado
	@And("^se valida en SIC el inventario$")
	public void se_verifica_en_SIC_el_cambio_de_inventario() throws Throwable {

		String usuario	= GetSICUsuarioAdministrador();
		String password = GetSICPasswordAdministrador();

		loginPage.AbrirSIC();
		loginPage.Login(usuario, password);

		menuPage.IrMenu("Información", "Consulta de Stock por Jerarquía de Productos");

		List<ETransactionDetail> oDetalleList = database.ObtenerResultadoDetalleTransaccion(transactionId);
		int stockSIC;

		for (ETransactionDetail oEntidad : oDetalleList) {
			stockSIC = 0;
			menuPage.SeleccionarBodega(oEntidad.fulfillmentCode);

			EStock oStockProductoBD = database.ObtenerStockProductoBD(oEntidad.sku, oEntidad.fulfillmentCode,
					oEntidad.inventoryType);
			jerarquiaPage.ConsultarProducto(oEntidad.sku);
			jerarquiaPage.ConsultarProducto(oEntidad.sku, oStockProductoBD.stock);
			BasePage.WaitSleep(5);

			stockSIC = jerarquiaPage.ObtenerStockSIC();

			System.out.println("Stock esperado:" + oStockProductoBD.stock + " Stock actual:" + stockSIC);
//			Assert.assertTrue("ERROR - Stock esperado:" + oStockProductoBD.stock + " Stock actual:" + stockSIC,
//					oStockProductoBD.stock == stockSIC);
		}
	}

	@Given("^tengo informacion de jerarquia de \"([^\"]*)\" productos$")
	public void tengo_informacion_de_jerarquia_de_productos(String cantidad) throws Throwable {

		EConexionBD oEConexionBD = new EConexionBD();
//		oEConexionBD.sshUser 		= GetSSHUser();
//		oEConexionBD.sshPassword 	= GetSSHPpk();
//		oEConexionBD.sshPort 		= Integer.parseInt(GetSSHPort());
//		oEConexionBD.dbHost 		= GetDBHost();
		oEConexionBD.sshHost 		= GetSSHHost();
		oEConexionBD.dbPort 		= Integer.parseInt(GetDBPort());
		oEConexionBD.dbUser 		= GetDBUser();
		oEConexionBD.dbPassword		= GetDBPassword();
		database.ConnectarTunelSSH(oEConexionBD);

		oListaProductos = new ArrayList<ESku>();
		oListaProductos = database.ObtenerDatosIncrementalProducto(GetSICCodigoPais(), cantidad);
		oListaProductosOriginal=oListaProductos;
	}

	@Given("^preparo la trama de incremental de productos$")
	public void preparo_la_trama_de_incremental_de_productos() throws Throwable {
		System.out.println("TotalProductos: " + oListaProductos.size());
		jsonString = tramaJson.GenerarTramaJsonIncrementalProducto(oListaProductos, "PMM", GetSICCodigoPais());
	}
	
	@Given("^preparo la trama de incremental de productos con el campo source erroneo$")
	public void preparo_la_trama_de_incremental_de_productos_con_el_campo_source_erroneo() throws Throwable {
		System.out.println("TotalProductos: " + oListaProductos.size());
		jsonString = tramaJson.GenerarTramaJsonIncrementalProducto(oListaProductos, "PMMDS", GetSICCodigoPais());
	}
	

	@When("^envio un request de incremental de productos via SQS$")
	public void envio_un_request_de_incremental_de_productos_via_SQS() throws Throwable {

		String accountId	= GetAWSAccountId();
		String usuario 		= GetAWSUsuario();
		String password 	= GetAWSPassword();
		String url 			= GetAWSUrlMensajeSQSIncrementalProductos();

		awsLoginPage.AbrirAWS();
		awsLoginPage.Login(accountId, usuario, password);
		BasePage.WaitSleep(1);
		
		/* Modificación Dani (sicm-qa-req-sku) */
		System.out.println("REDIRECCIONANDO A " + url);
		awsLoginPage.IngresarFiltro("sicm-qa-cl-req-sku");
		BasePage.WaitSleep(3);
		awsLoginPage.SeleccionarFiltro("sicm-qa-cl-req-sku");
		awsLoginPage.EnviarMensaje("sicm-qa-cl-req-sku");
		awsLoginPage.EsperarVentanaMensajeSQS();
		System.out.println(jsonString);
		awsSQSPage.IngresarDatos(jsonString);
		awsSQSPage.EnviarMensajeSQS();
	}

	@Then("^se valida en bd que el incremental de productos no ha realizado cambios$")
	public void se_valida_en_bd_que_el_incremental_de_productos_no_ha_realizado_cambios() throws Throwable {
		BasePage.WaitSleep(60);

		for (ESku oEsperado : oListaProductosOriginal) {
			ESku oActual = database.ObtenerInformacionJerarquiaProducto(oEsperado.codigoVenta, oEsperado.countryCode);

//			SKU,COD_VENTA,STYLE,SUBLINE_CODE,LINE_CODE,DEPTO_CODE, NAME,COUNTRY_CODE
			String mensaje = "Error en [C] - Actual: [A] | Esperado: [E]";

			Assert.assertTrue(mensaje.replace("[C]", oActual.codigoVenta + "-SKU").replace("[A]", oActual.sku)
					.replace("[E]", oEsperado.sku), CompararString(oActual.sku, oEsperado.sku));
			Assert.assertTrue(
					mensaje.replace("[C]", oActual.codigoVenta + "-COD_VENTA").replace("[A]", oActual.codigoVenta)
							.replace("[E]", oEsperado.codigoVenta),
					CompararString(oActual.codigoVenta, oEsperado.codigoVenta));
			Assert.assertTrue(mensaje.replace("[C]", oActual.codigoVenta + "-STYLE").replace("[A]", oActual.style)
					.replace("[E]", oEsperado.style), CompararString(oActual.style, oEsperado.style));
			Assert.assertTrue(
					mensaje.replace("[C]", oActual.codigoVenta + "-SUBLINE_CODE").replace("[A]", oActual.sublineCode)
							.replace("[E]", oEsperado.sublineCode),
					CompararString(oActual.sublineCode, oEsperado.sublineCode));
			Assert.assertTrue(mensaje.replace("[C]", oActual.codigoVenta + "-LINE_CODE")
					.replace("[A]", oActual.lineCode).replace("[E]", oEsperado.lineCode),
					CompararString(oActual.lineCode, oEsperado.lineCode));
			Assert.assertTrue(
					mensaje.replace("[C]", oActual.codigoVenta + "-DEPTO_CODE").replace("[A]", oActual.deptoCode)
							.replace("[E]", oEsperado.deptoCode),
					CompararString(oActual.deptoCode, oEsperado.deptoCode));
			Assert.assertTrue(
					mensaje.replace("[C]", oActual.codigoVenta + "-DESCRIPCION").replace("[A]", oActual.description)
							.replace("[E]", oEsperado.description),
					CompararString(oActual.description, oEsperado.description));
			Assert.assertTrue(
					mensaje.replace("[C]", oActual.codigoVenta + "-COUNTRY_CODE").replace("[A]", oActual.countryCode)
							.replace("[E]", oEsperado.countryCode),
					CompararString(oActual.countryCode, oEsperado.countryCode));

		}
	}

	@Then("^se valida en bd los cambios realizados por el incremental del productos$")
	public void se_valida_en_bd_los_cambios_realizados_por_el_incremental_del_productos() throws Throwable {
		BasePage.WaitSleep(40);

		for (ESku oEsperado : oListaProductos) {
			ESku oActual = database.ObtenerInformacionJerarquiaProducto(oEsperado.codigoVenta, oEsperado.countryCode);

//			SKU,COD_VENTA,STYLE,SUBLINE_CODE,LINE_CODE,DEPTO_CODE, NAME,COUNTRY_CODE
			String mensaje = "Error en [C] - Actual: [A] | Esperado: [E]";

			Assert.assertTrue(mensaje.replace("[C]", oActual.codigoVenta + "-SKU").replace("[A]", oActual.sku)
					.replace("[E]", oEsperado.sku), CompararString(oActual.sku, oEsperado.sku));
			Assert.assertTrue(
					mensaje.replace("[C]", oActual.codigoVenta + "-COD_VENTA").replace("[A]", oActual.codigoVenta)
							.replace("[E]", oEsperado.codigoVenta),
					CompararString(oActual.codigoVenta, oEsperado.codigoVenta));
			Assert.assertTrue(mensaje.replace("[C]", oActual.codigoVenta + "-STYLE").replace("[A]", oActual.style)
					.replace("[E]", oEsperado.style), CompararString(oActual.style, oEsperado.style));
			Assert.assertTrue(
					mensaje.replace("[C]", oActual.codigoVenta + "-SUBLINE_CODE").replace("[A]", oActual.sublineCode)
							.replace("[E]", oEsperado.sublineCode),
					CompararString(oActual.sublineCode, oEsperado.sublineCode));
			Assert.assertTrue(mensaje.replace("[C]", oActual.codigoVenta + "-LINE_CODE")
					.replace("[A]", oActual.lineCode).replace("[E]", oEsperado.lineCode),
					CompararString(oActual.lineCode, oEsperado.lineCode));
			Assert.assertTrue(
					mensaje.replace("[C]", oActual.codigoVenta + "-DEPTO_CODE").replace("[A]", oActual.deptoCode)
							.replace("[E]", oEsperado.deptoCode),
					CompararString(oActual.deptoCode, oEsperado.deptoCode));
			Assert.assertTrue(
					mensaje.replace("[C]", oActual.codigoVenta + "-DESCRIPCION").replace("[A]", oActual.description)
							.replace("[E]", oEsperado.description),
					CompararString(oActual.description, oEsperado.description));
			Assert.assertTrue(
					mensaje.replace("[C]", oActual.codigoVenta + "-COUNTRY_CODE").replace("[A]", oActual.countryCode)
							.replace("[E]", oEsperado.countryCode),
					CompararString(oActual.countryCode, oEsperado.countryCode));

		}
	}

	public boolean CompararString(String valor1, String valor2) {
		String mensaje = "COMPARAR STRING - Actual: [V1] | Esperado: [V2]";
		System.out.println(mensaje.replace("[V1]", valor1).replace("[V2]", valor2));
		return valor1.equals(valor2);
	}

	@Given("^agrego el sufijo \"([^\"]*)\" a la descripcion del producto$")
	public void agrego_el_sufijo_x_a_la_descripcion_del_producto(String sufijo) {
		List<ESku> oListaProductoNew = new ArrayList<ESku>();

		for (ESku oProducto : oListaProductos) {
			ESku oProductoNew 			= new ESku();
			oProductoNew.sku 			= oProducto.sku;
			oProductoNew.codigoVenta 	= oProducto.codigoVenta;
			oProductoNew.style 			= oProducto.style;
			oProductoNew.sublineCode 	= oProducto.sublineCode;
			oProductoNew.lineCode 		= oProducto.lineCode;
			oProductoNew.deptoCode 		= oProducto.deptoCode;
			oProductoNew.description 	= oProducto.description + sufijo;
			oProductoNew.countryCode	= oProducto.countryCode;
			oListaProductoNew.add(oProductoNew);
		}
		oListaProductosOriginal = oListaProductos;
		oListaProductos = oListaProductoNew;
	}
	
	@Given("^modifico el departamento al valor \"([^\"]*)\"$")
	public void modifico_el_departamento_al_valor(String valor) {
		List<ESku> oListaProductoNew = new ArrayList<ESku>();

		for (ESku oProducto : oListaProductos) {
			ESku oProductoNew 			= new ESku();
			oProductoNew.sku 			= oProducto.sku;
			oProductoNew.codigoVenta 	= oProducto.codigoVenta;
			oProductoNew.style 			= oProducto.style;
			oProductoNew.sublineCode 	= oProducto.sublineCode;
			oProductoNew.lineCode 		= oProducto.lineCode;
			oProductoNew.deptoCode 		= valor;
			oProductoNew.description 	= oProducto.description;
			oProductoNew.countryCode	= oProducto.countryCode;
			oListaProductoNew.add(oProductoNew);
		}
		oListaProductosOriginal = oListaProductos;
		oListaProductos = oListaProductoNew;
	}
	


	@Given("^modifico el itemId al valor \"([^\"]*)\"$")
	public void modifico_el_itemid_al_valor(String valor) {
		List<ESku> oListaProductoNew = new ArrayList<ESku>();

		for (ESku oProducto : oListaProductos) {
			ESku oProductoNew = new ESku();
			oProductoNew.sku 			= valor;
			oProductoNew.codigoVenta 	= oProducto.codigoVenta;
			oProductoNew.style 			= oProducto.style;
			oProductoNew.sublineCode 	= oProducto.sublineCode;
			oProductoNew.lineCode 		= oProducto.lineCode;
			oProductoNew.deptoCode 		= oProducto.deptoCode;
			oProductoNew.description 	= oProducto.description;
			oProductoNew.countryCode 	= oProducto.countryCode;
			oListaProductoNew.add(oProductoNew);
		}
		oListaProductosOriginal = oListaProductos;
		oListaProductos = oListaProductoNew;
	}
	
	
	@Given("^modifico el style al valor \"([^\"]*)\"$")
	public void modifico_el_style_al_valor(String valor) {
		List<ESku> oListaProductoNew = new ArrayList<ESku>();

		for (ESku oProducto : oListaProductos) {
			ESku oProductoNew 			= new ESku();
			oProductoNew.sku 			= oProducto.sku;
			oProductoNew.codigoVenta 	= oProducto.codigoVenta;
			oProductoNew.style 			= valor;
			oProductoNew.sublineCode 	= oProducto.sublineCode;
			oProductoNew.lineCode 		= oProducto.lineCode;
			oProductoNew.deptoCode 		= oProducto.deptoCode;
			oProductoNew.description 	= oProducto.description;
			oProductoNew.countryCode 	= oProducto.countryCode;
			oListaProductoNew.add(oProductoNew);
		}
		oListaProductosOriginal = oListaProductos;
		oListaProductos = oListaProductoNew;
	}
	
	@Given("^modifico la linea al valor \"([^\"]*)\"$")
	public void modifico_la_linea_al_valor(String valor) {
		List<ESku> oListaProductoNew = new ArrayList<ESku>();

		for (ESku oProducto : oListaProductos) {
			ESku oProductoNew 			= new ESku();
			oProductoNew.sku 			= oProducto.sku;
			oProductoNew.codigoVenta 	= oProducto.codigoVenta;
			oProductoNew.style 			= oProducto.style;
			oProductoNew.sublineCode 	= oProducto.sublineCode;
			oProductoNew.lineCode 		= valor;
			oProductoNew.deptoCode 		= oProducto.deptoCode;
			oProductoNew.description 	= oProducto.description;
			oProductoNew.countryCode 	= oProducto.countryCode;
			oListaProductoNew.add(oProductoNew);
		}
		oListaProductosOriginal = oListaProductos;
		oListaProductos = oListaProductoNew;
	}
	
	
	@Given("^modifico la sublinea al valor \"([^\"]*)\"$")
	public void modifico_la_sublinea_al_valor(String valor) {
		List<ESku> oListaProductoNew = new ArrayList<ESku>();

		for (ESku oProducto : oListaProductos) {
			ESku oProductoNew 			= new ESku();
			oProductoNew.sku 			= oProducto.sku;
			oProductoNew.codigoVenta 	= oProducto.codigoVenta;
			oProductoNew.style 			= oProducto.style;
			oProductoNew.sublineCode 	= valor;
			oProductoNew.lineCode 		= oProducto.lineCode;
			oProductoNew.deptoCode 		= oProducto.deptoCode;
			oProductoNew.description 	= oProducto.description;
			oProductoNew.countryCode 	= oProducto.countryCode;
			oListaProductoNew.add(oProductoNew);
		}
		oListaProductosOriginal = oListaProductos;
		oListaProductos = oListaProductoNew;
	}
	
	List<String> oListaTramaIncrementalProducto;

	@Given("^tengo \"([^\"]*)\" tramas de \"([^\"]*)\" productos con el sufijo \"([^\"]*)\" a la descripcion del producto$")
	public void quiero_enviar_tramas_de_productos_con_el_sufijo_a_la_descripcion_del_producto(String numeroTramas,
			String productosPorTrama, String sufijo) throws Throwable {

		EConexionBD oEConexionBD	= new EConexionBD();
//		oEConexionBD.sshUser 		= GetSSHUser();
//		oEConexionBD.sshPassword	= GetSSHPpk();
//		oEConexionBD.sshPort 		= Integer.parseInt(GetSSHPort());
//		oEConexionBD.dbHost 		= GetDBHost();
		oEConexionBD.sshHost 		= GetSSHHost();
		oEConexionBD.dbPort 		= Integer.parseInt(GetDBPort());
		oEConexionBD.dbUser 		= GetDBUser();
		oEConexionBD.dbPassword		= GetDBPassword();
		database.ConnectarTunelSSH(oEConexionBD);

		oListaTramaIncrementalProducto = new ArrayList<String>();

		int cantidad = Integer.parseInt(numeroTramas) * Integer.parseInt(productosPorTrama);
		System.out.println("ObtenerDatosIncrementalProducto:" + String.valueOf(cantidad));
		oListaProductos = database.ObtenerDatosIncrementalProducto(GetSICCodigoPais(), String.valueOf(cantidad));
		int contador = 0;
		int multiplo = Integer.parseInt(productosPorTrama);
		List<ESku> oListaProductoNew = new ArrayList<ESku>();

		for (ESku oProducto : oListaProductos) {
			contador++;

			ESku oProductoNew 			= new ESku();
			oProductoNew.sku 			= oProducto.sku;
			oProductoNew.codigoVenta 	= oProducto.codigoVenta;
			oProductoNew.style 			= oProducto.style;
			oProductoNew.sublineCode 	= oProducto.sublineCode;
			oProductoNew.lineCode 		= oProducto.lineCode;
			oProductoNew.deptoCode 		= oProducto.deptoCode;
			oProductoNew.description 	= oProducto.description + sufijo;
			oProductoNew.countryCode	= oProducto.countryCode;
			oListaProductoNew.add(oProductoNew);

			if (contador % multiplo == 0 && contador != 0) {
				System.out.println("TotalProductos: " + oListaProductos.size());
				jsonString = tramaJson.GenerarTramaJsonIncrementalProducto(oListaProductoNew, "PMM", GetSICCodigoPais());
				oListaTramaIncrementalProducto.add(jsonString);
				oListaProductoNew = new ArrayList<ESku>();
			}

			oListaProductos = oListaProductoNew;

		}
	}

	@When("^envio varios request de incremental de productos via SQS$")
	public void envio_varios_request_de_incremental_de_productos_via_SQS() throws Throwable {

		String accountId	= GetAWSAccountId();
		String usuario 		= GetAWSUsuario();
		String password 	= GetAWSPassword();

		awsLoginPage.AbrirAWS();
		awsLoginPage.Login(accountId, usuario, password);
		BasePage.WaitSleep(1);
		
		/* Modificación Dani (sicm-qa-req-sku) */
		awsLoginPage.IngresarFiltro("sicm-qa-cl-req-sku");
		BasePage.WaitSleep(3);
		awsLoginPage.SeleccionarFiltro("sicm-qa-cl-req-sku");
		awsLoginPage.EnviarMensaje("sicm-qa-cl-req-sku");
		awsLoginPage.EsperarVentanaMensajeSQS();

		for (String tramaJson : oListaTramaIncrementalProducto) {
			System.out.println("INGRESANDO JSON:" + tramaJson);
			awsSQSPage.IngresarDatos(tramaJson);
			awsSQSPage.EnviarMensajeSQS();
			awsSQSPage.EnviarOtroMensaje();
		}

	}

}
