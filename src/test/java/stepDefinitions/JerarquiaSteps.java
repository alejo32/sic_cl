package stepDefinitions;

import java.awt.AWTException;
import java.io.IOException;
import java.util.List;

import org.junit.Assert;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import models.EStockJerarquia;
import models.EStock;
import models.ETransactionDetail;
import pageObjects.BasePage;
import utils.PropertyLoader;
import utils.DriverFactory;

public class JerarquiaSteps extends DriverFactory {

	private static EStockJerarquia oProductoStockJerarquia;

	public JerarquiaSteps() {
		oProductoStockJerarquia = new EStockJerarquia();
	}

	@Then("^se valida que tenga por defecto el valor \"([^\"]*)\"$")
	public void se_valida_que_tenga_por_defect_el_valor_x(String valor) throws InterruptedException, AWTException {
		String opcionSeleccionada = jerarquiaPage.ObtenerTextoComboTipoProducto();
		Assert.assertTrue("No se tiene por defecto la opcion '" + valor + "'", opcionSeleccionada.equals(valor));
	}

	@Then("^se valida que el rango de stock tenga por defecto el valor \"([^\"]*)\"$")
	public void se_valida_que_el_rango_de_stock_tenga_por_defecto_el_valor(String valor)
			throws InterruptedException, AWTException {
		String opcionSeleccionada = jerarquiaPage.ObtenerTextoComboRangoStock();
		Assert.assertTrue("No se tiene por defecto la opcion '" + valor + "'", opcionSeleccionada.equals(valor));
	}

	@When("^no selecciono un departamento$")
	public void no_selecciono_un_departamento() {
		// Ninguna acción, no selecciono nada
	}

	@When("^no ingreso un sku$")
	public void no_ingreso_un_sku() {
		// Ninguna acción, no selecciono nada
	}

	@When("^selecciono el departamento \"([^\"]*)\"$")
	public void selecciono_el_departamento(String nombre) throws IOException, InterruptedException, AWTException {
		BasePage.WaitSleep(1);
		jerarquiaPage.SeleccionarDepartamento(nombre);
	}

	@When("^selecciono la linea \"([^\"]*)\"$")
	public void selecciono_la_linea(String nombre) throws IOException, InterruptedException, AWTException {
		BasePage.WaitSleep(1);
		jerarquiaPage.SeleccionarLinea(nombre);
	}

	@Then("^se muestra la linea \"([^\"]*)\"$")
	public void se_muestra_la_linea(String nombre) throws IOException, InterruptedException, AWTException {
		BasePage.WaitSleep(2);
		jerarquiaPage.SeleccionarLinea(nombre);
//		Assert.assertTrue("No se encontró la línea", jerarquiaPage.VerificarLineaSeleccionada(nombre));

	}

	@Then("^se muestra la sublinea \"([^\"]*)\"$")
	public void se_muestra_la_sublinea(String nombre) throws IOException, InterruptedException, AWTException {
		BasePage.WaitSleep(1);
		jerarquiaPage.SeleccionarSubLinea(nombre);
//		Assert.assertTrue("No se encontró la sublínea", jerarquiaPage.VerificarSubLineaSeleccionada(nombre));

	}

	@When("^doy click al combo tipo de inventario$")
	public void doy_click_al_combo_tipo_de_inventario() throws InterruptedException {
		jerarquiaPage.ClickComboTipoInventario();
	}

	@When("^doy click al combo rango de stock$")
	public void doy_click_al_combo_rango_de_stock() throws InterruptedException {
		jerarquiaPage.ClickComboRangoStock();
	}

	@Then("^se verifica se muestren todos los tipos de inventario$")
	public void se_verifica_que_se_muestren_todos_los_tipos_de_inventario() throws Exception {
		jerarquiaPage.selReservado();
		BasePage.WaitSleep(1);
		jerarquiaPage.selDisponible();
		BasePage.WaitSleep(1);
		jerarquiaPage.selReservado();
		BasePage.WaitSleep(1);
//		Assert.assertTrue("No se encontró la opción tipo inventario 1", jerarquiaPage.ExisteTipoInventarioOpcion("1"));
//		Assert.assertTrue("No se encontró la opción tipo inventario 2", jerarquiaPage.ExisteTipoInventarioOpcion("2"));
//		Assert.assertTrue("No se encontró la opción tipo inventario 3", jerarquiaPage.ExisteTipoInventarioOpcion("3"));

	}

	@Then("^se verifica que se muestren las opciones de rango de stock$")
	public void se_verifica_que_se_muestren_las_opciones_de_rango_de_stock(DataTable dtOpciones) throws Exception {
		jerarquiaPage.selMayor();
		jerarquiaPage.selMenor();
		jerarquiaPage.selMayorIgual();
		jerarquiaPage.selMenorIgual();
		jerarquiaPage.selIgual();
		jerarquiaPage.selDistinto();
	}

	@Given("^tengo informacion de stock por jerarquia de un producto con tipo de inventario \"([^\"]*)\"$")
	public void tengo_informacion_de_stock_por_jerarquia_de_un_producto_con_tipo_de_inventario_x(String tipoInventario)
			throws IOException {

		oProductoStockJerarquia = database.ObtenerInformacionStockJerarquiaProducto(GetSICCodigoPais(), "1",
				tipoInventario);

	}

	@Given("^tengo informacion de stock por jerarquia de un producto con stock mayor a cero$")
	public void tengo_informacion_de_stock_por_jerarquia_de_un_producto_con_stock_mayor_a_cero() throws IOException {

		oProductoStockJerarquia = database.ObtenerInformacionStockJerarquiaProductoMayorCero(GetSICCodigoPais());

	}

	@Given("^tengo informacion de stock por jerarquia de un producto con stock menor a cero$")
	public void tengo_informacion_de_stock_por_jerarquia_de_un_producto_con_stock_menor_a_cero() throws IOException {

		oProductoStockJerarquia = database.ObtenerInformacionStockJerarquiaProductoMenorCero(GetSICCodigoPais());

	}

	@Given("^tengo informacion de stock por jerarquia de un producto con stock igual a cero$")
	public void tengo_informacion_de_stock_por_jerarquia_de_un_producto_con_stock_igual_a_cero() throws IOException {

		oProductoStockJerarquia = database.ObtenerInformacionStockJerarquiaProductoIgualCero(GetSICCodigoPais());

	}

	@Given("^tengo informacion de stock por jerarquia de un producto con stock mayor o igual a \"([^\"]*)\"$")
	public void tengo_informacion_de_stock_por_jerarquia_de_un_producto_con_stock_mayor_o_igual_a_cero(String stock)
			throws IOException {

		oProductoStockJerarquia = database.ObtenerInformacionStockJerarquiaProductoMayorIgualA(GetSICCodigoPais(),
				stock);

	}

	@Given("^tengo informacion de stock por jerarquia de un producto con stock menor o igual a \"([^\"]*)\"$")
	public void tengo_informacion_de_stock_por_jerarquia_de_un_producto_con_stock_menor_o_igual_a_cero(String stock)
			throws IOException {

		oProductoStockJerarquia = database.ObtenerInformacionStockJerarquiaProductoMenorIgualA(GetSICCodigoPais(),
				stock);

	}

	@Given("^tengo informacion de stock por jerarquia de un producto con stock diferente a \"([^\"]*)\"$")
	public void tengo_informacion_de_stock_por_jerarquia_de_un_producto_con_stock_diferente_a_cero(String stock)
			throws IOException {
		stockDiferenteA = stock;
		oProductoStockJerarquia = database.ObtenerInformacionStockJerarquiaProductoDiferenteA(GetSICCodigoPais(),
				stock);

	}

	@When("^realizo la consulta$")
	public void realizo_la_consulta() throws IOException, InterruptedException {

		jerarquiaPage.IngresarCodigoVenta(oProductoStockJerarquia.codigoVenta);
		jerarquiaPage.SeleccionarTipoStock(Integer.parseInt(oProductoStockJerarquia.qty));
		jerarquiaPage.ClickConsultar();
		jerarquiaPage.EsperarCarga();

	}

	@When("^realizo la consulta con filtro tipo inventario$")
	public void realizo_la_consulta_con_filtro_tipo_inventario() throws IOException, InterruptedException {

		jerarquiaPage.IngresarCodigoVenta(oProductoStockJerarquia.codigoVenta);
		jerarquiaPage.SeleccionarTipoStock(Integer.parseInt(oProductoStockJerarquia.qty));
		jerarquiaPage.seleccionarTipoInventario(oProductoStockJerarquia.inventoryType);
		jerarquiaPage.ClickConsultar();
		jerarquiaPage.EsperarCarga();

	}

	@When("^realizo la consulta con el filtro mayor igual a$")
	public void realizo_la_consulta_con_el_filtro_mayor_igual_a() throws IOException, InterruptedException {

		jerarquiaPage.IngresarCodigoVenta(oProductoStockJerarquia.codigoVenta);
		jerarquiaPage.SeleccionarTipoStockMayorIgualA();
		jerarquiaPage.IngresarStock(oProductoStockJerarquia.qty);
		jerarquiaPage.ClickConsultar();
		jerarquiaPage.EsperarCarga();

	}

	@When("^realizo la consulta con el filtro menor igual a$")
	public void realizo_la_consulta_con_el_filtro_menor_igual_a() throws IOException, InterruptedException {

		jerarquiaPage.IngresarCodigoVenta(oProductoStockJerarquia.codigoVenta);
		jerarquiaPage.SeleccionarTipoStockMenorIgualA();
		jerarquiaPage.IngresarStock(oProductoStockJerarquia.qty);
		jerarquiaPage.ClickConsultar();
		jerarquiaPage.EsperarCarga();
	}

	String stockDiferenteA = "";

	@When("^realizo la consulta con el filtro distinto a$")
	public void realizo_la_consulta_con_el_filtro_distinto_a() throws IOException, InterruptedException {

		jerarquiaPage.IngresarCodigoVenta(oProductoStockJerarquia.codigoVenta);
		jerarquiaPage.SeleccionarTipoStockDistintoA();
		jerarquiaPage.IngresarStock(stockDiferenteA);
		jerarquiaPage.ClickConsultar();
		jerarquiaPage.EsperarCarga();

	}

	@When("^selecciono la bodega donde el producto tiene stock$")
	public void selecciono_la_bodega_donde_el_producto_tiene_stock() throws IOException, InterruptedException {

		menuPage.SeleccionarBodega(oProductoStockJerarquia.fulfillmentCode);

	}

	@When("^se verifica el resultado mostrado con la base de datos$")
	public void se_verifica_el_resultado_mostrado_con_la_base_de_datos() throws IOException, InterruptedException {

		String bodega	 		= jerarquiaPage.ObtenerCeldaBodega();
		String dpto 			= jerarquiaPage.ObtenerCeldaDepartamento();
		String linea 			= jerarquiaPage.ObtenerCeldaLinea();
		String subLinea 		= jerarquiaPage.ObtenerCeldaSubLinea();
		String estilo 			= jerarquiaPage.ObtenerCeldaEstilo();
		String codVenta 		= jerarquiaPage.ObtenerCeldaCodigoVenta();
		String stock 			= jerarquiaPage.ObtenerCeldaStock();
		String tipoInventario	= jerarquiaPage.ObtenerCeldaTipoInventario();

		System.out.println("Bodega: " + bodega);
		System.out.println("dpto: " + dpto);
		System.out.println("linea: " + linea);
		System.out.println("subLinea: " + subLinea);
		System.out.println("estilo: " + estilo);
		System.out.println("codVenta: " + codVenta);
		System.out.println("stock: " + stock);
		System.out.println("tipoInventario: " + tipoInventario);

		Assert.assertTrue(
				"Bodega: Esperado '" + oProductoStockJerarquia.fulfillmentCode + "' - Actual '" + bodega + "'",
				jerarquiaPage.VerificarBodega(bodega, oProductoStockJerarquia.fulfillmentCode));
		Assert.assertTrue("Departamento: Esperado '" + oProductoStockJerarquia.deptoCode + "' - Actual '" + dpto + "'",
				jerarquiaPage.VerificarDepartamento(dpto, oProductoStockJerarquia.deptoCode));
		Assert.assertTrue("Linea: Esperado '" + oProductoStockJerarquia.lineCode + "' - Actual '" + linea + "'",
				jerarquiaPage.VerificarLinea(linea, oProductoStockJerarquia.lineCode));
		Assert.assertTrue(
				"SubLinea: Esperado '" + oProductoStockJerarquia.subLineCode + "' - Actual '" + subLinea + "'",
				jerarquiaPage.VerificarSubLinea(subLinea, oProductoStockJerarquia.subLineCode));
		Assert.assertTrue("Estilo: Esperado '" + oProductoStockJerarquia.style + "' - Actual '" + estilo + "'",
				jerarquiaPage.VerificarEstilo(estilo, oProductoStockJerarquia.style));
		Assert.assertTrue(
				"CodigoVenta: Esperado '" + oProductoStockJerarquia.codigoVenta + " - " + oProductoStockJerarquia.name
						+ "' - Actual '" + codVenta + "'",
				jerarquiaPage.VerificarCodigoVenta(codVenta,
						(oProductoStockJerarquia.codigoVenta + " - " + oProductoStockJerarquia.name)));
		Assert.assertTrue("Stock: Esperado '" + oProductoStockJerarquia.qty + "' - Actual '" + stock + "'",
				jerarquiaPage.VerificarStock(stock, oProductoStockJerarquia.qty));

		String nombreTipoInventario = oProductoStockJerarquia.inventoryType.equals("1") ? "Stock Disponible"
				: (oProductoStockJerarquia.inventoryType.equals("2") ? "Reservado para Ventas" : "Stock Protegido");

		/**
		 * SE HAN REALIZADO CAMBIOS QUE TODAVIA NO ESTAN TERMINADOS, VALIDAR LO DEMAS
		 * CUANDO SE TERMINE: PENDIENTE
		 **/
//		Assert.assertTrue("TipoInventario: Esperado '" + oProductoStockJerarquia.inventoryType + "' - Actual '"
//				+ tipoInventario + "'", jerarquiaPage.VerificarTipoInventario(tipoInventario, nombreTipoInventario));

	}

	@Then("^validar la descarga del archivo de jerarquia$")
	public void validar_la_descarga_del_archivo_de_jerarquia() throws IOException, InterruptedException, AWTException {

		String rutaDescargas = GetRutaCarpetaLocalDescargas();

		String fileName = "StockJerarquia.zip";
		Thread.sleep(10000);
		Assert.assertTrue("No se encontró el archivo", archivo.ValidarDescargaArchivo(rutaDescargas, fileName));
	}

	@When("^doy click a exportar$")
	public void doy_click_a_exportar() throws Throwable {
		jerarquiaPage.ClickExportar();
	}

}
