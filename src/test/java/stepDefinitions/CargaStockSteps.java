package stepDefinitions;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import models.ECargaStockProtegido;
import models.EStock;
import org.junit.Assert;
import pageObjects.BasePage;
import utils.DriverFactory;

import java.awt.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CargaStockSteps extends DriverFactory {

	private List<EStock> oListaStockProductos;
	private List<ECargaStockProtegido> oListaCargaStockProtegido;
	private String nombreArchivo;

	public CargaStockSteps() {

		oListaStockProductos = new ArrayList<EStock>();
	}

	@Given("^tengo informacion de stock protegido$")
	public void tengo_informacion_de_stock_protegido(DataTable dtStockProtegido) {

		List<List<String>> oList = dtStockProtegido.raw();
		oListaCargaStockProtegido = new ArrayList<ECargaStockProtegido>();
		int contador = 1;
		for (List<String> fila : oList) {
			if (contador != 1) {
				ECargaStockProtegido oEntidad = new ECargaStockProtegido();
				oEntidad.effectiveDate = fila.get(0);
				oEntidad.expiryDate = fila.get(1);
				oEntidad.itemId = fila.get(2);
				oEntidad.locationId = fila.get(3);
				oEntidad.inventoryProtection1 = fila.get(4);
				oEntidad.inventoryProtection2 = fila.get(5);
				oEntidad.inventoryProtection3 = fila.get(6);
				oEntidad.inventoryProtection4 = fila.get(7);
				oEntidad.inventoryProtection5 = fila.get(8);
				oListaCargaStockProtegido.add(oEntidad);
			}
			contador++;
		}

	}

	@When("^genero un archivo de carga masiva de stock protegido$")
	public void genero_un_archivo_de_carga_masiva_de_stock_protegido() throws IOException {

		String cmspPathLocal = GetRutaCarpetaLocalCMSP();
		System.out.println("Ruta local destino de achivo: " + cmspPathLocal);
		System.out.println("getListaProductoStockRipley CANTIDAD DE PRODUCTOS :" + oListaCargaStockProtegido.size());

		nombreArchivo = archivo.CrearArchivoCMSP(cmspPathLocal, ".csv", oListaCargaStockProtegido, false);
		System.out.println("NOMBRE ARCHIVO CMSP:" + nombreArchivo);
	}

	@When("^genero un archivo de carga masiva de stock protegido vacio$")
	public void genero_un_archivo_de_carga_masiva_de_stock_protegido_vacio() throws IOException {

		String cmspPathLocal = GetRutaCarpetaLocalCMSP();
		System.out.println("Ruta local destino de achivo: " + cmspPathLocal);
		oListaCargaStockProtegido = null;
		nombreArchivo = archivo.CrearArchivoCMSPVacio(cmspPathLocal, ".csv", false);
		System.out.println("NOMBRE ARCHIVO CMSP VACIO:" + nombreArchivo);
	}

	@When("^genero un archivo de carga masiva de stock protegido con nombres de cabecera incorrectos$")
	public void genero_un_archivo_de_carga_masiva_de_stock_protegido_con_nombres_de_cabecera_incorrectos()
			throws IOException {
		String cmspPathLocal = GetRutaCarpetaLocalCMSP();
		System.out.println("Ruta local destino de achivo: " + cmspPathLocal);
		System.out.println("getListaProductoStockRipley CANTIDAD DE PRODUCTOS :" + oListaCargaStockProtegido.size());

		nombreArchivo = archivo.CrearArchivoCMSP(cmspPathLocal, ".csv", oListaCargaStockProtegido, true);
		System.out.println("NOMBRE ARCHIVO CMSP:" + nombreArchivo);
	}

	@Given("^tengo informacion de stock de ([^\"]*) productos de la bodega ([^\"]*)$")
	public void tengo_informacion_de_stock_de_x_productos(String cantidad, String bodega)
			throws IOException, InterruptedException, SQLException {
		oListaStockProductos = database.ObtenerListaProductoStockRipley(cantidad, bodega);
	}

	@Given("^tengo informacion de stock de \"([^\"]*)\" productos para la carga masiva de stock$")
	public void tengo_informacion_de_stock_de_x_productos_para_la_carga_masiva_de_stock(String cantidad)
			throws IOException, InterruptedException, SQLException {
		oListaStockProductos = database.ObtenerListaProductoStockRipley(cantidad);
	}
	
	@Given("^genero un archivo de carga masiva de ajuste de stock \"([^\"]*)\"$")
	public void genero_un_archivo_de_carga_masiva_de_ajuste_de_stock(String cantidad) throws Throwable {
		SetTipoAjuste("MDADJUST");

		String mdadjustPathLocal = GetRutaCarpetaLocalMDADJUST();

		System.out.println("Ruta local destino de achivo: " + mdadjustPathLocal);
		System.out.println("getListaProductoStockRipley CANTIDAD DE PRODUCTOS :" + oListaStockProductos.size());
		
		oListaStockProductos = database.ObtenerListaProductoStockRipley(cantidad);
		
		nombreArchivo = archivo.CrearArchivoMDADJUST(mdadjustPathLocal, ".csv", oListaStockProductos, true,true, true, true);
		System.out.println("NOMBRE ARCHIVO:" + nombreArchivo);
		System.out.println("Lista de Productos: " + oListaStockProductos);
	}
	
	/* REVISAR */
	@When("^genero un archivo de carga masiva de ajuste de stock$")
	public void genero_un_archivo_de_carga_masiva_de_ajuste_de_stock() throws IOException {
		
		SetTipoAjuste("MDADJUST");

		String mdadjustPathLocal = GetRutaCarpetaLocalMDADJUST();

		System.out.println("Ruta local destino de achivo: " + mdadjustPathLocal);
		System.out.println("getListaProductoStockRipley CANTIDAD DE PRODUCTOS :" + oListaStockProductos.size());
		
		//oListaStockProductos = database.ObtenerListaProductoStockRipley("10");
		
		nombreArchivo = archivo.CrearArchivoMDADJUST(mdadjustPathLocal, ".csv", oListaStockProductos, true,true, true, true);
		System.out.println("NOMBRE ARCHIVO:" + nombreArchivo);
		System.out.println("Lista de Productos: " + oListaStockProductos);
	}
	
	@Given("^genero un archivo de carga masiva de ajuste de stock por \"([^\"]*)\"$")
	public void genero_un_archivo_de_carga_masiva_de_ajuste_de_stock_por(String cantidad) throws Throwable {
	    
		SetTipoAjuste("MDADJUST");

		String mdadjustPathLocal = GetRutaCarpetaLocalMDADJUST();

		System.out.println("Ruta local destino de achivo: " + mdadjustPathLocal);
		System.out.println("getListaProductoStockRipley CANTIDAD DE PRODUCTOS :" + oListaStockProductos.size());
		
		oListaStockProductos = database.ObtenerListaProductoStockRipley(cantidad);
		
		nombreArchivo = archivo.CrearArchivoMDADJUST(mdadjustPathLocal, ".csv", oListaStockProductos, true, true, true, true);
	}

	@When("^genero un archivo de carga masiva de ajuste de stock con errores en algunos registros$")
	public void genero_un_archivo_de_carga_masiva_de_ajuste_de_stock_con_errores_en_algunos_registros()
			throws IOException {

		SetTipoAjuste("MDADJUST");
		String mdadjustPathLocal = GetRutaCarpetaLocalMDADJUST();
		System.out.println("Ruta local destino de achivo: " + mdadjustPathLocal + "\n");
		System.out.println("getListaProductoStockRipley CANTIDAD DE PRODUCTOS :" + oListaStockProductos.size() + "\n");
		
		oListaStockProductos = database.ObtenerListaProductoStockRipley("5");
		
		nombreArchivo = archivo.CrearArchivoMDADJUSTConErrores(mdadjustPathLocal, ".csv", oListaStockProductos, true,true, true);
		System.out.println("NOMBRE ARCHIVO:" + nombreArchivo);
	}

	@When("^genero un archivo de carga masiva de ajuste de stock con tipo de operacion \"([^\"]*)\"$")
	public void genero_un_archivo_de_carga_masiva_de_ajuste_de_stock_con_tipo_de_operacion_x(String tipoOperacion)
			throws IOException {

		SetTipoAjuste("MDADJUST");
		String mdadjustPathLocal = GetRutaCarpetaLocalMDADJUST();
		System.out.println("Ruta local destino de achivo: " + mdadjustPathLocal);
		System.out.println("getListaProductoStockRipley CANTIDAD DE PRODUCTOS :" + oListaStockProductos.size());

		nombreArchivo = archivo.CrearArchivoMDADJUST(mdadjustPathLocal, ".csv", oListaStockProductos, tipoOperacion,
				"10");

		System.out.println("NOMBRE ARCHIVO:" + nombreArchivo);
	}

	@When("^genero un archivo de carga masiva de ajuste de stock con stock \"([^\"]*)\"$")
	public void genero_un_archivo_de_carga_masiva_de_ajuste_de_stock_con_stock(String stock) throws IOException {

		SetTipoAjuste("MDADJUST");

		String mdadjustPathLocal = GetRutaCarpetaLocalMDADJUST();
		System.out.println("Ruta local destino de achivo: " + mdadjustPathLocal);

		oListaStockProductos = new ArrayList<EStock>();

		System.out.println("getListaProductoStockRipley CANTIDAD DE PRODUCTOS :" + oListaStockProductos.size());

		nombreArchivo = archivo.CrearArchivoMDADJUST(mdadjustPathLocal, ".csv", oListaStockProductos, "A", stock);

		System.out.println("NOMBRE ARCHIVO:" + nombreArchivo);
	}

	@When("^genero un archivo de carga masiva de ajuste de stock con adicion y sustraccion$")
	public void genero_un_archivo_de_carga_masiva_de_ajuste_de_stock_con_adicion_y_sustraccion() throws IOException {

		SetTipoAjuste("MDADJUST");

		String mdadjustPathLocal = GetRutaCarpetaLocalMDADJUST();
		System.out.println("Ruta local destino de achivo: " + mdadjustPathLocal);

		oListaStockProductos = new ArrayList<EStock>();
		oListaStockProductos = database.ObtenerListaProductoStockRipley("10");
		System.out.println("getListaProductoStockRipley CANTIDAD DE PRODUCTOS :" + oListaStockProductos.size());

		nombreArchivo = archivo.CrearArchivoMDADJUST(mdadjustPathLocal, ".csv", oListaStockProductos, true, true, true, true);

		System.out.println("NOMBRE ARCHIVO:" + nombreArchivo);
	}

	@When("^genero un archivo de carga masiva de ajuste de stock sin valor en el campo sku$")
	public void genero_un_archivo_de_carga_masiva_de_ajuste_de_stock_sin_valor_en_el_campo_sku() throws IOException {

		SetTipoAjuste("MDADJUST");

		String mdadjustPathLocal = GetRutaCarpetaLocalMDADJUST();
		System.out.println("Ruta local destino de achivo: " + mdadjustPathLocal);

		oListaStockProductos = new ArrayList<EStock>();

		System.out.println("getListaProductoStockRipley CANTIDAD DE PRODUCTOS :" + oListaStockProductos.size());

		nombreArchivo = archivo.CrearArchivoMDADJUST(mdadjustPathLocal, ".csv", oListaStockProductos, true,false, true,
				true);

		System.out.println("NOMBRE ARCHIVO:" + nombreArchivo);
	}

	@When("^genero un archivo de carga masiva de ajuste de stock sin valor en el campo stock$")
	public void genero_un_archivo_de_carga_masiva_de_ajuste_de_stock_sin_valor_en_el_campo_stock() throws IOException {

		SetTipoAjuste("MDADJUST");

		String mdadjustPathLocal = GetRutaCarpetaLocalMDADJUST();
		System.out.println("Ruta local destino de achivo: " + mdadjustPathLocal);

		oListaStockProductos = new ArrayList<EStock>();

		System.out.println("getListaProductoStockRipley CANTIDAD DE PRODUCTOS :" + oListaStockProductos.size());

		nombreArchivo = archivo.CrearArchivoMDADJUST(mdadjustPathLocal, ".csv", oListaStockProductos, true, true, true,
				false);

		System.out.println("NOMBRE ARCHIVO:" + nombreArchivo);
	}

	@When("^genero un archivo de carga masiva de ajuste de stock sin valor en el campo tipo operacion$")
	public void genero_un_archivo_de_carga_masiva_de_ajuste_de_stock_sin_valor_en_el_campo_tipo_operacion()
			throws IOException {

		SetTipoAjuste("MDADJUST");

		String mdadjustPathLocal = GetRutaCarpetaLocalMDADJUST();
		System.out.println("Ruta local destino de achivo: " + mdadjustPathLocal);

		oListaStockProductos = new ArrayList<EStock>();

		System.out.println("getListaProductoStockRipley CANTIDAD DE PRODUCTOS :" + oListaStockProductos.size());

		nombreArchivo = archivo.CrearArchivoMDADJUST(mdadjustPathLocal, ".csv", oListaStockProductos, true, true, false,
				true);

		System.out.println("NOMBRE ARCHIVO:" + nombreArchivo);
	}

	@When("^genero un archivo de carga masiva de ajuste de stock con extension \"([^\"]*)\"$")
	public void genero_un_archivo_de_carga_masiva_de_ajuste_de_stock_con_extension_x(String extension)
			throws IOException {

		SetTipoAjuste("MDADJUST");

		String mdadjustPathLocal = GetRutaCarpetaLocalMDADJUST();
		System.out.println("Ruta local destino de achivo: " + mdadjustPathLocal);

		oListaStockProductos = new ArrayList<EStock>();

		System.out.println("getListaProductoStockRipley CANTIDAD DE PRODUCTOS :" + oListaStockProductos.size());

		nombreArchivo = archivo.CrearArchivoMDADJUST(mdadjustPathLocal, extension, oListaStockProductos, true, true, true,
				true);

		System.out.println("NOMBRE ARCHIVO:" + nombreArchivo);
	}

	@When("^genero un archivo de carga masiva de stock protegido con extension \"([^\"]*)\"$")
	public void genero_un_archivo_de_carga_masiva_de_stock_protegido_con_extension(String extension)
			throws IOException {

		String cmspPathLocal = GetRutaCarpetaLocalCMSP();
		System.out.println("Ruta local destino de achivo: " + cmspPathLocal);

		oListaStockProductos = new ArrayList<EStock>();

		System.out.println("getListaProductoStockRipley CANTIDAD DE PRODUCTOS :" + oListaStockProductos.size());

		nombreArchivo = archivo.CrearArchivoCMSP(cmspPathLocal, extension, oListaCargaStockProtegido, false);

		System.out.println("NOMBRE ARCHIVO:" + nombreArchivo);
	}

	@When("^cargo el archivo de carga masiva$")
	public void cargo_el_archivo_de_carga_masiva() throws IOException, InterruptedException, AWTException {

		String mdadjustPathLocal = GetRutaCarpetaLocalMDADJUST();

		cargaStockPage.SubirArchivoCMS(mdadjustPathLocal, nombreArchivo);
		BasePage.WaitSleep(1);
	}

	@When("^cargo el archivo de carga masiva de stock protegido$")
	public void cargo_el_archivo_de_carga_masiva_de_stock_protegido()
			throws IOException, InterruptedException, AWTException {

		String cmspPathLocal = GetRutaCarpetaLocalCMSP();

		cargaStockPage.SubirArchivoCMSP(cmspPathLocal, nombreArchivo);
		Thread.sleep(1000);
	}

	@Then("^doy click a realizar carga$")
	public void doy_click_a_realizar_carga() throws InterruptedException, AWTException {
		cargaStockPage.ClickRealizarCarga();
	}

	@Then("^se valida lo mostrado en el resumen de ejecucion$")
	public void se_valida_lo_mostrado_en_el_resumen_de_ejecucion() throws InterruptedException {
		
		String transaccion 			= cargaStockPage.ObtenerTransaccion();
		String estadoCabecera 		= cargaStockPage.ObtenerEstado();
		String totalProcesados 		= cargaStockPage.ObtenerTotalProcesados();
		String duplicados 			= cargaStockPage.ObtenerDuplicados();
		String noEnviables 			= cargaStockPage.ObtenerNoEnviables();
		String sinVariacion 		= cargaStockPage.ObtenerSinVariacion();
		String procesadosConExito 	= cargaStockPage.ObtenerProcesadosConExito();
		String procesadosConError	= cargaStockPage.ObtenerProcesadosConError();
		String detalleCabecera 		= cargaStockPage.ObtenerDetalle();
		Assert.assertTrue("Los datos del resumen de ejecución no coinciden con la BD", cargaStockPage.ValidarDatosResumen(
				transaccion, estadoCabecera, totalProcesados, duplicados,noEnviables, sinVariacion, procesadosConExito, procesadosConError));		
	}

	@Then("^se valida la carga en estado \"([^\"]*)\"$")
	public void se_valida_la_carga_en_estado_x(String estado) throws InterruptedException, IOException {
		cargaStockPage.EsperarCarga();
		String estadoCabecera = cargaStockPage.ObtenerEstado();
		Assert.assertTrue("El estado es " + estadoCabecera + " cuando debería ser " + estado, estadoCabecera.equals(estado));
	}

	@Then("^se muestra el boton examinar$")
	public void se_muestra_el_boton_examinar() throws InterruptedException {
		Assert.assertTrue("No se muestra el botón examinar", cargaStockPage.ExisteBotonExaminar());

	}

	@Then("^se muestra en la cabecera el detalle \"([^\"]*)\"$")
	public void se_muestra_en_la_cabecera_el_detalle(String detalleCabeceraEsperado) throws InterruptedException, IOException {
		cargaStockPage.EsperarCarga();
		String detalleCabeceraAcual = cargaStockPage.ObtenerDetalleProtect();
		Assert.assertTrue("El detale de la cabecera es " + detalleCabeceraAcual + " cuando debería ser "
				+ detalleCabeceraEsperado, detalleCabeceraEsperado.equals(detalleCabeceraAcual));

	}

	@Then("^se muestra el detalle de los errores$")
	public void se_muestra_el_detalle_de_los_errores(DataTable dtErrores) throws InterruptedException, IOException {

		List<List<String>> oList = dtErrores.raw();
		oListaCargaStockProtegido = new ArrayList<ECargaStockProtegido>();
		int contador = 1;
		for (List<String> fila : oList) {
			if (contador != 1) {
				String codVentaEsperado = fila.get(0);
				String estadoEsperado = fila.get(1);
				String detalleEsperado = fila.get(2);
				
				BasePage.WaitSleep(2);
				
				String codVentaActual = cargaStockPage.ObtenerDetalleCodigoVenta(codVentaEsperado);
				String estadoActual = cargaStockPage.ObtenerDetalleEstado(codVentaEsperado);
				String detalleActual = cargaStockPage.ObtenerDetalleDetalle(codVentaEsperado);

				Assert.assertTrue("DETALLE - Estado incorrecto.", estadoEsperado.equals(estadoActual));
				Assert.assertTrue("DETALLE - Detalle de error incorrecto.", detalleEsperado.equals(detalleActual));

			}
			contador++;
		}
	}

	@Then("^se valida que por defecto tenga el valor \"([^\"]*)\"$")
	public void se_valida_que_tenga_por_defect_el_valor_x(String valor) throws InterruptedException, AWTException {
		String opcionSeleccionada = cargaStockPage.ObtenerTextoComboTipoProducto();
		Assert.assertTrue("No se tiene por defecto la opcion '" + valor + "'", opcionSeleccionada.equals(valor));
	}

	@Then("^se valida mensaje de error \"([^\"]*)\"$")
	public void se_valida_mensaje_de_error_x(String mensajeEsperado) throws InterruptedException, AWTException {
		String mensajeErrorActual = cargaStockPage.ObtenerMensajeError();
		Assert.assertTrue("El mensaje mostrado es incorrecto", mensajeErrorActual.equals(mensajeEsperado));
	}

	@Then("^se valida que se visualice el nombre del archivo seleccionado$")
	public void se_valida_que_se_visualice_el_nombre_del_archivo_selecionado()
			throws InterruptedException, AWTException {
		String archivoCargadoAcual = cargaStockPage.ObtenerTextoArchivoSeleccionado();
		String archivoEsperado = nombreArchivo;
		Assert.assertTrue("El mensaje mostrado es incorrecto", archivoCargadoAcual.equals(archivoEsperado));
	}

}
