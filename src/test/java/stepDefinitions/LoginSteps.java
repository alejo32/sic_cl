package stepDefinitions;

import java.io.IOException;
import org.junit.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import models.EConexionBD;
import pageObjects.BasePage;
import pageObjects.aws.LoginPage;
import utils.DriverFactory;

public class LoginSteps extends DriverFactory {

	@Given("^ingreso al sistema sic$")
	public void ingreso_al_sistema_sic() throws Exception {
		String usuario = GetSICUsuarioAdministrador();
		String password = GetSICPasswordAdministrador();

		loginPage.AbrirSIC();
		loginPage.Login(usuario, password);

		EConexionBD oEConexionBD	= new EConexionBD();
//		oEConexionBD.sshUser		= GetSSHUser();
//		oEConexionBD.sshPassword	= GetSSHPpk();
		oEConexionBD.sshHost		= GetSSHHost();
//		oEConexionBD.sshPort		= Integer.parseInt(GetSSHPort());
//		oEConexionBD.dbHost			= GetDBHost();
		oEConexionBD.dbPort			= Integer.parseInt(GetDBPort());
		oEConexionBD.dbUser 		= GetDBUser();
		oEConexionBD.dbPassword 	= GetDBPassword();

		database.ConnectarTunelSSH(oEConexionBD);
	}

	@Given("^ingreso al sistema sic con perfil administrador$")
	public void ingreso_al_sistema_sic_con_perfil_administrador() throws Exception {
		String usuario = GetSICUsuarioAdministrador();
		String password = GetSICPasswordAdministrador();

		loginPage.AbrirSIC();
		loginPage.Login(usuario, password);
	}

	@Given("^ingreso al sistema sic con perfil estandar$")
	public void ingreso_al_sistema_sic_con_perfil_estandar() throws Exception {
		String usuario = GetSICUsuarioEstandar();
		String password = GetSICPasswordEstandar();

		loginPage.AbrirSIC();
		loginPage.Login(usuario, password);
	}

	@Then("^se muestra la pantalla dashboard$")
	public void se_muestra_la_pantalla_dashboard() throws InterruptedException {
		Assert.assertTrue("No se muestra la pantalla Dashboard", loginPage.EsperarPantallaDashboard());
	}

	@Then("^se muestra la pantalla login$")
	public void se_muestra_la_pantalla_login() throws InterruptedException {
		Assert.assertTrue("No se muestra la pantalla Login", loginPage.EsperarPantallaLogin());
	}

	@When("^cierro sesion$")
	public void cierro_sesion() throws InterruptedException, IOException {
		menuPage.ClickOpcionesUsuario();
		menuPage.ClickCerrarSesion();
	}

	@Given("^ingreso al login$")
	public void ingreso_al_login() throws Exception {

		loginPage.AbrirSIC();
	}

	@When("^ingreso usuario \"([^\"]*)\"$")
	public void ingreso_usuario(String usuario) throws InterruptedException, IOException {
		loginPage.IngresarUsuario(usuario);
	}

	@When("^ingreso password \"([^\"]*)\"$")
	public void ingreso_password(String password) throws InterruptedException, IOException {
		loginPage.IngresarPassword(password);
	}

	@When("^doy click a iniciar sesion$")
	public void doy_click_a_iniciar_sesion() throws InterruptedException, IOException {
		loginPage.ClickIniciarSesion();
	}

	@When("^muestra mensaje de clave \"([^\"]*)\"$")
	public void muestra_mensaje_de_clave(String mensajeEsperado) throws InterruptedException, IOException {
		String mensajeObtenido = loginPage.ObtenerMensajeErrorPassword();
		String mensajeError = "El mensaje mostrado es incorrecto";
		Assert.assertTrue(mensajeError, mensajeEsperado.equals(mensajeObtenido));
	}

	@Then("^muestra mensaje de usuario \"([^\"]*)\"$")
	public void muestra_mensaje_de_usuario(String mensajeEsperado) throws InterruptedException, IOException {
		String mensajeObtenido = loginPage.ObtenerMensajeErrorUsuario();
		String mensajeError = "El mensaje mostrado es incorrecto";
		Assert.assertTrue(mensajeError, mensajeEsperado.equals(mensajeObtenido));
	}

	@Then("^muestra mensaje de validacion \"([^\"]*)\"$")
	public void muestra_mensaje_de_validacion(String mensajeEsperado) throws InterruptedException, IOException {
		BasePage.WaitSleep(3);
		String mensajeObtenido = loginPage.ObtenerMensajeValidacion();
		Assert.assertEquals(mensajeObtenido, mensajeEsperado);
//		Assert.assertTrue(mensajeError, mensajeObtenido.contains(mensajeError));
	}

	@Then("^verifico que existe el campo usuario$")
	public void verifico_que_existe_el_campo_usuario() throws InterruptedException, IOException {
		loginPage.EsperarPantallaLogin();
		Assert.assertTrue("No se encontró el campo Usuario", loginPage.ExisteCampoUsuario());
	}

	@Then("^verifico que existe el campo password$")
	public void verifico_que_existe_el_campo_password() throws InterruptedException, IOException {
		Assert.assertTrue("No se encontró el campo Password", loginPage.ExisteCampoPassword());
	}

	@Then("^verifico que existe el boton iniciar sesion$")
	public void verifico_que_existe_el_boton_iniciar_sesion() throws InterruptedException, IOException {
		Assert.assertTrue("No se encontró el boton Iniciar Sesion", loginPage.ExisteBotonIniciarSesion());
	}

	@When("^espero (\\d+) minutos$")
	public void espero_minutos(int arg1) throws Throwable {
	    
	}

	@Then("^se muestra mensaje de expiro se sesion$")
	public void se_muestra_mensaje_de_expiro_se_sesion() throws Throwable {
	    Assert.assertTrue("No se muestra mensaje de expiro de sesion", loginPage.EsperarMensajeSesionExpirada());
	}
}
