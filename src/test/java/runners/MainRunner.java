package runners;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = { "src/test/resources/features/" },
		glue = {"stepDefinitions" }, 
		monochrome = true,
		tags = {"@ADJUST_SQS_01_todo"},
		plugin = { "pretty", "html:target/cucumber", "json:target/cucumber.json", "com.cucumber.listener.ExtentCucumberFormatter:output/report_@SDELTA_REST.html" }
	)

public class MainRunner extends AbstractTestNGCucumberTests {
	
} 