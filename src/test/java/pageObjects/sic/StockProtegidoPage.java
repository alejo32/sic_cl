package pageObjects.sic;

import static org.testng.Assert.assertTrue;
import java.io.IOException;
import java.util.Calendar;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import pageObjects.BasePage;

public class StockProtegidoPage extends BasePage{
	
	public @FindBy(xpath = "//*[@id=\'system-mss\']/option[1]") WebElement sel_Producto;
	public @FindBy(xpath = "//*[@id=\'dpto-dropdown-js\']/div/div/span[2]/input[1]") WebElement sel_Depto;
	public @FindBy(id = "incrementar") WebElement rdb_Editar;
	public @FindBy(id = "search-protec-setting") WebElement btn_Consultar;
	public @FindBy(xpath = "//*[@id=\'app\']/section/div[3]/div[2]/div[2]/form/div[2]/div[3]/a[1]") WebElement btn_Evento;
	public @FindBy(xpath = "//*[@id=\'app\']/section/div[3]/div[2]/div[2]/form/div[2]/div[3]/a[2]") WebElement btn_Editar;
	public @FindBy(xpath = "//*[@id=\'app\']/section/div[3]/div[2]/div[2]/form/div[2]/div[3]/a[3]") WebElement btn_Eliminar;
	public @FindBy(xpath  = "/html/body/div[2]/div/div[3]/button[2]") WebElement btn_Confirmar;
	public @FindBy(xpath = "//*[@id=\'form-foo-sic-protected\']/div[4]/a[1]") WebElement btn_Guardar;
	public @FindBy(className = "sic-scroll-table") WebElement scroll_Config;
	public @FindBy(id = "form-foo-sic-protected") WebElement scroll_Stock;
	public @FindBy(xpath = "/html/body/div[2]/div") WebElement pop_up;
	public @FindBy(id = "date-date-sic") WebElement inp_FecInicio;
	public @FindBy(id = "date-end-sic") WebElement inp_FecFin;
	public @FindBy(id = "stock-sic-level1") WebElement inp_Level1;
	public @FindBy(id = "stock-sic-level2") WebElement inp_Level2;
	public @FindBy(id = "stock-sic-level3") WebElement inp_Level3;
	public @FindBy(id = "stock-sic-level4") WebElement inp_Level4;
	public @FindBy(id = "stock-sic-level5") WebElement inp_Level5;
	public @FindBy(id = "Permanente") WebElement chk_Permanente;
	//
	public @FindBy(xpath = "/html/body/div/section/div[3]/div[2]/div[2]/form/div[3]/div[4]/a[2]") WebElement btn_Cancelar;
	public @FindBy(xpath = "/html/body/div[2]/div/div[3]/button[1]") WebElement btn_CancelaEliminar;
	public @FindBy(xpath = "//*[@id=\'form-foo-sic-protected\']/div[4]/a[2]") WebElement btn_CancelarEditar;
	public @FindBy(id = "system-mss") WebElement sel_TipoProducto;
	public @FindBy(xpath = "//*[@id=\'line-prot-sic\']") WebElement txt_Linea;
	public @FindBy(xpath = "//*[@id=\'line-dropdown-js\']/div/div/span[2]/input[1]") WebElement txt_LineaProd;
	public @FindBy(xpath = "//*[@id=\'sub-line-dropdown-js\']") WebElement txt_SubLinea;
	public @FindBy(xpath = "//*[@id=\'sub-line-dropdown-js\']/div/div/span[2]/input[1]") WebElement txt_SubLineaProd;
	public @FindBy(xpath = "//*[@id=\'sic-filter-container-proct\']/div[3]/p") WebElement click_Out;
	public @FindBy(xpath = "//*[@id=\'dpto-dropdown-js\']/div/div/span[1]/span/span[2]") WebElement del_Depto;
	public @FindBy(xpath = "//*[@id=\'line-dropdown-js\']/div/div/span[1]/span/span[2]") WebElement del_Linea;
	public @FindBy(xpath = "//*[@id=\'sub-sku-dropdown-js\']/div/div/span[2]") WebElement txt_SKU;
	public @FindBy(xpath = "//*[@id=\'sub-sku-dropdown-js\']/div/div/span[2]/input[1]") WebElement txt_SkuInp;
	public @FindBy(xpath = "/html/body/div[2]") WebElement div_Mensaje;
	public @FindBy(xpath = "/html/body/div[2]/div/div[2]/div[1]/p[1]") WebElement msj_Alerta; 
	public @FindBy(xpath = "/html/body/div/section/div[3]/div[2]/div[2]/form/div[2]/div[2]/div/div[2]/p") WebElement msj_Grilla;
	public @FindBy(xpath = "/html/body/div/section/div[3]/div[2]/div[2]/form/div[2]/div[2]/div/div[2]") WebElement grilla_Cont;
	public @FindBy(xpath = "/html/body/div/section/div[3]/div[2]/div[2]/form/div[3]/div[3]/p") WebElement msj_Obligatorio;
	public @FindBy(xpath = "/html/body/div[2]/div/div[2]/div[1]/p[1]") WebElement msj_Niveles;
	public @FindBy(xpath = "/html/body/div[2]/div/div[2]/div[1]/p[1]") WebElement msj_COnfig;
	public @FindBy(xpath = "/html/body/div[2]/div/div[2]/div[1]/p[1]") WebElement msj_Eliminar;
	
	public @FindBy(id = "sub-line-prot-sic")	WebElement txt_Sublinea;
	public @FindBy(id = "sku-prot-sic")			WebElement txt_CodVenta;
	public @FindBy(id = "search-protec-setting")	WebElement btn_Consulta; 
	public @FindBy(xpath = "//*[@id=\'app\']/section/div[3]/div[2]/div[2]/form/div[2]/div[2]/div") WebElement grilla_Config;
	
 
	public StockProtegidoPage() throws IOException {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public StockProtegidoPage selTipoProd() throws Exception {
		sel_TipoProducto.isSelected();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage selTipoProducto() throws Exception {
		assertTrue(isElementClickable(sel_TipoProducto));
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage selDepartamento() throws Exception {
		sel_Depto.isEnabled();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage txtLinea() throws Exception {
		txt_Linea.isEnabled();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage txtLineaClick() throws Exception {
		txt_Linea.click();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage txtLineaSel() throws Exception {
		txt_Linea.sendKeys(Keys.ENTER);
		BasePage.WaitSleep(2);
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage txtLineaProd() throws Exception {
		txt_LineaProd.sendKeys("000162");
		BasePage.WaitSleep(1);
		txt_LineaProd.sendKeys(Keys.ENTER);
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage txtLineaActiva() throws Exception {
		txt_LineaProd.sendKeys("500536");
		BasePage.WaitSleep(1);
		txt_LineaProd.sendKeys(Keys.ENTER);
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage txtSubLineaSel() throws Exception {
		click_Out.click();
		BasePage.WaitSleep(1);
		txt_SubLineaProd.click();
		BasePage.WaitSleep(2);
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage txtSubLineaProd() throws Exception {
		txt_SubLineaProd.sendKeys("S001121");
		BasePage.WaitSleep(1);
		txt_SubLineaProd.sendKeys(Keys.ENTER);
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage txtSubLineaActiva() throws Exception {
		txt_SubLineaProd.sendKeys("S502038");
		BasePage.WaitSleep(1);
		txt_SubLineaProd.sendKeys(Keys.ENTER);
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage txtSubLinea() throws Exception {
		txt_Sublinea.isEnabled();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage txtCodVenta() throws Exception {
		txt_CodVenta.isEnabled();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage txtCodVentaSel() throws Exception {
		txt_CodVenta.click();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage btnCOnsulta() throws Exception {
		btn_Consulta.isEnabled();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage btnCOnsultaDis() throws Exception {
		btn_Consulta.click();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage grillConfig() throws Exception {
		grilla_Config.isEnabled();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage btnEventoDis() throws Exception {
		btn_Evento.isEnabled();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage btnEditarDis() throws Exception {
		btn_Editar.isEnabled();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage btnEliminarDis() throws Exception {
		btn_Eliminar.isEnabled();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage selProducto() throws IOException {
		sel_Producto.click();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage btnConsultarEnable() throws IOException {
		btn_Consulta.isEnabled();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage delDepto() throws IOException {
		del_Depto.click();
		BasePage.WaitSleep(1);
		click_Out.click();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage delLinea() throws IOException {
		del_Linea.click();
		BasePage.WaitSleep(1);
		click_Out.click();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage clickOut() throws IOException {
		click_Out.click();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage txtSKU() throws IOException {
		WaitUntilWebElementIsVisible(txt_SKU);
		txt_SKU.isEnabled();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage txtSKUClick() throws IOException {
		txt_SKU.click();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage txtSKUSel() throws IOException {
		txt_SkuInp.sendKeys("2026050220005");
		txt_SkuInp.sendKeys(Keys.ENTER);
		BasePage.WaitSleep(1);
		txt_SkuInp.sendKeys("2026050240003");
		txt_SkuInp.sendKeys(Keys.ENTER);
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage divMensaje() throws IOException {
		div_Mensaje.click();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage msjAlerta() throws IOException {
		Assert.assertEquals("Debe seleccionar una bodega para continuar.", msj_Alerta.getText());
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage txtSKUUnit() throws IOException {
		txt_SkuInp.sendKeys("2026050220005");
		txt_SkuInp.sendKeys(Keys.ENTER);
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage txtSKUActivo() throws IOException {
		txt_SkuInp.sendKeys("2012352050006");
		txt_SkuInp.sendKeys(Keys.ENTER);
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage msjGrilla() throws IOException {
		grilla_Cont.click();
		BasePage.WaitSleep(1);
		Assert.assertEquals("No se encontraron resultados para los criterios de búsqueda ingresados", msj_Grilla.getText());
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage chkPermanente() throws IOException {
		chk_Permanente.click();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage msjObligatorio() throws IOException {
		Assert.assertEquals("Debe completar los campos obligatorios", msj_Obligatorio.getText());
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage msjNiveles() throws IOException {
		Assert.assertEquals("Hay niveles de protección sin completar, éstos quedarán sin valores en dichos niveles.", msj_Niveles.getText());
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage msjConfig() throws IOException {
		System.out.println("Mensaje: "+msj_COnfig.getText());
		BasePage.WaitSleep(10);
		Assert.assertEquals("Ya existe una configuración para la jerarquía \n\n seleccionada en esas fechas", msj_COnfig.getText());
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage btnCancelar() throws IOException {
		btn_Cancelar.click();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage msjEliminar() throws IOException {
		Assert.assertEquals("¿Está seguro que desea eliminar la configuración seleccionada?", msj_Eliminar.getText());
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage btnCancelaEliminar() throws IOException {
		btn_CancelaEliminar.click();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage btnCancelaEditar() throws IOException {
		btn_CancelarEditar.click();
	return new StockProtegidoPage();
	}
	
	
	//
	public StockProtegidoPage selDepto() throws Exception {
		sel_Depto.click();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage selDeptoEsc() throws Exception {
		sel_Depto.sendKeys("D106");
		sel_Depto.sendKeys(Keys.ENTER);
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage selDeptoActiva() throws Exception {
		sel_Depto.sendKeys("D101");
		sel_Depto.sendKeys(Keys.ENTER);
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage btnConsultar() throws Exception {
		btn_Consultar.click();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage btnEvento() throws Exception {
		btn_Evento.click();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage scrollConfig() throws Exception {
		scrollToElementByWebElementLocator(scroll_Config);
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage scrollStock() throws Exception {
		scrollToElementByWebElementLocator(scroll_Stock);
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage inpFecIni() throws Exception {
		inp_FecInicio.click();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage fechaInicio() throws IOException {
		Calendar fecha = Calendar.getInstance();
    	int dia = fecha.get(Calendar.DAY_OF_MONTH);	
    	WebElement fechaIniOK = null;
    	
    	for(int i=1; i<=41; i++) {
    			WebElement fecInicio = driver.findElement(By.xpath("//*[@id='form-protected-stock-js']/div[1]/div[1]/div[1]/div/div[2]/div/span["+i+"]"));
    			String fechaIni = fecInicio.getText();
    			try {
    				if(dia == Integer.parseInt(fechaIni)) {
	    				fechaIniOK = driver.findElement(By.xpath("//*[@id='form-protected-stock-js']/div[1]/div[1]/div[1]/div/div[2]/div/span["+i+"]"));	    				
	    			} else {
	    				System.out.println("");
	    			}
    			} catch(Exception e) {
    				e.getMessage();
    			}
    	}
    	fechaIniOK.click();    	
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage inpFecFin() throws Exception {
		inp_FecFin.click();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage fechaFin() throws IOException {
		Calendar fecha = Calendar.getInstance();
    	int dia = fecha.get(Calendar.DAY_OF_MONTH);
    	int diaMas = dia + 1;
    	WebElement fechaIniOK = null;
    	
    	for(int i=1; i<=41; i++) {
    			WebElement fecInicio = driver.findElement(By.xpath("//*[@id=\'form-protected-stock-js\']/div[1]/div[1]/div[2]/div/div[2]/div/span["+i+"]"));
    			String fechaIni = fecInicio.getText();
    			try {
    					if(diaMas == Integer.parseInt(fechaIni)) {
    	    				fechaIniOK = driver.findElement(By.xpath("//*[@id=\'form-protected-stock-js\']/div[1]/div[1]/div[2]/div/div[2]/div/span["+i+"]"));
    	    			} else {
    	    				System.out.println("");
    	    			}
    			} catch(Exception e) {
    				e.getMessage();
    			}
    	}
    	fechaIniOK.click();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage fechaIniMayor() throws IOException {
		Calendar fecha = Calendar.getInstance();
    	int dia = fecha.get(Calendar.DAY_OF_MONTH);
    	int diaMas = dia + 1;
    	WebElement fechaIniOK = null;
    	
    	for(int i=1; i<=41; i++) {
    			WebElement fecInicio = driver.findElement(By.xpath("//*[@id='form-protected-stock-js']/div[1]/div[1]/div[1]/div/div[2]/div/span["+i+"]"));
    			String fechaIni = fecInicio.getText();
    			try {
    					if(diaMas == Integer.parseInt(fechaIni)) {
    	    				fechaIniOK = driver.findElement(By.xpath("//*[@id='form-protected-stock-js']/div[1]/div[1]/div[1]/div/div[2]/div/span["+i+"]"));
    	    			} else {
    	    				System.out.println("");
    	    			}
    			} catch(Exception e) {
    				e.getMessage();
    			}
    	}
    	fechaIniOK.click();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage fechaFinMenor() throws IOException {
		Calendar fecha = Calendar.getInstance();
    	int dia = fecha.get(Calendar.DAY_OF_MONTH);	
    	WebElement fechaIniOK = null;
    	
    	for(int i=1; i<=41; i++) {
    			WebElement fecInicio = driver.findElement(By.xpath("//*[@id=\'form-protected-stock-js\']/div[1]/div[1]/div[2]/div/div[2]/div/span["+i+"]"));
    			String fechaIni = fecInicio.getText();
    			try {
    				if(dia == Integer.parseInt(fechaIni)) {
	    				fechaIniOK = driver.findElement(By.xpath("//*[@id=\'form-protected-stock-js\']/div[1]/div[1]/div[2]/div/div[2]/div/span["+i+"]"));
	    			} else {
	    				System.out.println("");
	    			}	    		    	
    			} catch(Exception e) {
    				e.getMessage();
    			}
    	}
    	fechaIniOK.click();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage popUp() throws Exception {
		pop_up.click();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage btnGuardar() throws Exception {
		btn_Guardar.click();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage inpLevel1() throws Exception {
		inp_Level1.sendKeys("2");
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage inpLevel2() throws Exception {
		inp_Level2.sendKeys("2");
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage inpLevel3() throws Exception {
		inp_Level3.sendKeys("2");
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage inpLevel4() throws Exception {
		inp_Level4.sendKeys("2");
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage inpLevel5() throws Exception {
		inp_Level5.sendKeys("2");
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage btnConfirmar() throws Exception {
		btn_Confirmar.click();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage rdbEditar() throws Exception {
		rdb_Editar.click();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage btnEditar() throws Exception {
		btn_Editar.click();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage btnEliminar() throws Exception {
		btn_Eliminar.click();
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage editLevel1() throws Exception {
		inp_Level1.clear();
		inp_Level1.sendKeys("3");
	return new StockProtegidoPage();
	}
	
	public StockProtegidoPage editLevel2() throws Exception {
		inp_Level2.clear();
		inp_Level2.sendKeys("3");
	return new StockProtegidoPage();
	}

}
