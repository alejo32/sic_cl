package pageObjects.sic;

import java.awt.AWTException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import models.sic.EGrillaAuditoria;
import pageObjects.BasePage;

public class AuditoriaPage extends BasePage {

	public AuditoriaPage() throws IOException {
		super();
		WaitSleep(5);
	}

	@FindBy(xpath = "//input[@type='text' and @placeholder='Ingrese Cod. de venta']")
	private WebElement txtSku;

	@FindBy(xpath = "//p[contains(text(),'Desde')]/../div/div/input")
	private WebElement txtDesde;

	@FindBy(xpath = "//p[contains(text(),'Hasta')]/../div/div/input")
	private WebElement txtHasta;

	@FindBy(xpath = "//p[contains(text(),'Desde')]/../div/div/div//span[@class='cell day today']")
	private WebElement lblTodayDesde;

	@FindBy(xpath = "//p[contains(text(),'Hasta')]/../div/div/div//span[@class='cell day today']")
	private WebElement lblTodayHasta;

	@FindBy(xpath = "//a[text()='Consultar']")
	private WebElement btnConsultar;

	@FindBy(xpath = "//a[text()='Exportar']")
	private WebElement btnExportar;
	
	public @FindBy(xpath = "/html/body/div/section/div[3]/div[2]/div[3]/div[2]/a") WebElement btn_Exportar;

	@FindBy(xpath = "//a[text()='Limpiar']")
	private WebElement btnLimpiar;

	@FindBy(id = "ddInventoryType")
	private WebElement cbTipoInventario;
	@FindBy(xpath = "//input[@aria-owns='ddTrxStatus_options']")
	private WebElement txtEstadoTransaccion;

	@FindBy(xpath = "//input[@aria-owns='ddTrxdStatus_options']")
	private WebElement txtEstadoRegistro;

	@FindBy(xpath = "//input[@aria-owns='ddEntryType_options']")
	private WebElement txtTipoTransaccion;

	@FindBy(xpath = "//*[contains(text(),'Tipo de Producto')]/../div/select")
	private WebElement cbTipoProducto;

	@FindBy(xpath = "//div[@class='error error-box inventory-error show']")
	private WebElement lblMensajeError;

	String xpathTipoInventarioOpcion = "//select[@id='ddInventoryType']/option[@value=[TIPO_INVENTARIO]]";
	String xpathDesdeNumeroDia = "//p[text()='Desde ']/../div/div[2]/div/span[text()='[NUMERO_DIA]']";
	String xpathHastaNumeroDia = "//p[text()='Hasta ']/../div/div[2]/div/span[text()='[NUMERO_DIA]']";
	String xpathGrillaIdtrxHeader = "//table/tr/td[3][text()='[ID_TRX_HEADER]']";
	String xpathgrillaSistema = "//table/tr/td[3][text()='[ID_TRX_HEADER]']/../td[4][text()='[SISTEMA]']";
	String xpathGrillaSucursal = "//table/tr/td[3][text()='[ID_TRX_HEADER]']/../td[5][text()='[SUCURSAL]']";
	String xpathGrillaSku = "//table/tr/td[3][text()='[ID_TRX_HEADER]']/../td[6][text()='[CODIGO_VENTA]']";
	String xpathGrillaTipoTransaccion = "//table/tr/td[3][text()='[ID_TRX_HEADER]']/../td[8][text()='[TIPO_TRANSACCION]']";
	String xpathGrillaEstadoCabecera = "//table/tr/td[3][text()='[ID_TRX_HEADER]']/../td[15][text()='[ESTADO_CABECERA]']";
	String xpathGrillaEstadoDetalle = "//table/tr/td[3][text()='[ID_TRX_HEADER]']/../td[16][text()='[ESTADO_DETALLE]']";
	String xpathGrillaUsuario = "//table/tr/td[3][text()='[ID_TRX_HEADER]']/../td[14][text()='[USERNAME]']";
	String xpathGrillaTipoDocumento = "//table/tr/td[3][text()='[ID_TRX_HEADER]']/../td[12][text()='[DOCUMENT_TYPE]']";
	String xpathGrillaTipoInventario = "//table/tr/td[3][text()='[ID_TRX_HEADER]']/../td[23][text()='[INVENTORY_TYPE]']";
	String xpathGrillaStockAntes = "//table/tr/td[3][text()='[ID_TRX_HEADER]']/../td[19][text()='[STOCK_ANTES]']";
	String xpathGrillaStockDespues = "//table/tr/td[3][text()='[ID_TRX_HEADER]']/../td[20][text()='[STOCK_DESPUES]']";

	public void IngresarEstadoTransaccion(String codigoEstado) {

		SendKeysToWebElement(txtEstadoTransaccion, ObtenerNombreEstadoTransaccion(codigoEstado));
		SendEnterKey();
	}

	public void IngresarEstadoRegistro(String codigoEstado) {

		SendKeysToWebElement(txtEstadoRegistro, ObtenerNombreEstadoRegistro(codigoEstado));
		SendEnterKey();
	}

	public void IngresarTipoTransaccion(String codigoTipoTransaccion) throws AWTException, InterruptedException {

		WaitAndClickElement(txtEstadoRegistro);
		SendKeysToWebElement(txtTipoTransaccion, ObtenerNombreTipoTransaccion(codigoTipoTransaccion));
		SendEnterKey();
	}

	public void SeleccionarTipoInventario(String tipoInventario) throws InterruptedException, AWTException {
		if (!tipoInventario.equals("")) {
			WaitAndClickElement(cbTipoInventario);
			xpathTipoInventarioOpcion = xpathTipoInventarioOpcion.replace("[TIPO_INVENTARIO]", tipoInventario);
			WaitAndClickXpath(xpathTipoInventarioOpcion);
		}
	}

	public void Limpiar() {
		WaitAndClickElement(btnLimpiar);
	}

	public void IngresarCodigoVenta(String codigoVenta) {
		SendKeysToWebElement(txtSku, codigoVenta);
	}

	public void SeleccionarFechaDesde(String numeroDia) {
		WaitAndClickElement(txtDesde);
		xpathDesdeNumeroDia = xpathDesdeNumeroDia.replace("[NUMERO_DIA]", numeroDia);
		WaitAndClickXpath(xpathDesdeNumeroDia);
	}

	public void SeleccionarFechaHasta(String numeroDia) {
		WaitAndClickElement(txtHasta);
		xpathHastaNumeroDia = xpathHastaNumeroDia.replace("[NUMERO_DIA]", numeroDia);
		WaitAndClickXpath(xpathHastaNumeroDia);
	}

	public void IngresarFechaDesde(String fecha) {
		WaitAndClickElement(txtDesde);
		WaitAndClickElement(lblTodayDesde);

	}

	public void IngresarFechaHasta(String fecha) {
		WaitAndClickElement(txtHasta);
		WaitAndClickElement(lblTodayHasta);
	}

	public void ClickConsultar(String idTrxHeaderExpected) {
		WaitAndClickElement(btnConsultar);
		xpathGrillaIdtrxHeader = xpathGrillaIdtrxHeader.replace("[ID_TRX_HEADER]", idTrxHeaderExpected);
		WaitUntilXpathIsVisible(xpathGrillaIdtrxHeader);
	}

	public void ClickConsultar() {
		WaitAndClickElement(btnConsultar);
	}

	public void ClickExportar() {
		WaitAndClickElement(btnExportar);
	}

	public void ClickLimpiar() {
		WaitAndClickElement(btnLimpiar);
	}

	public String ObtenerMensajeError() {
		return GetElementText(lblMensajeError);
	}

	public String ObtenerTextoComboTipoProducto() {
		return GetComboboxText(cbTipoProducto);
	}

	public boolean ExisteIDTRX(String valorEsperado) {
		xpathGrillaIdtrxHeader = xpathGrillaIdtrxHeader.replace("[ID_TRX_HEADER]", valorEsperado);
		
		System.out.println("xpath: " + xpathGrillaIdtrxHeader);
		return ElementExist(xpathGrillaIdtrxHeader);
	}

	public boolean ExisteSistemaOrigen(String idTrxHeader, String valorEsperado) {
		xpathgrillaSistema = xpathgrillaSistema.replace("[ID_TRX_HEADER]", idTrxHeader).replace("[SISTEMA]",
				valorEsperado);
		System.out.println("xpath: " + xpathgrillaSistema);
		return ElementExist(xpathgrillaSistema);
	}

	public boolean ExisteSucursal(String idTrxHeader, String valorEsperado) {
		xpathGrillaSucursal = xpathGrillaSucursal.replace("[ID_TRX_HEADER]", idTrxHeader).replace("[SUCURSAL]",
				valorEsperado);
		System.out.println("xpath: " + xpathGrillaSucursal);
		return ElementExist(xpathGrillaSucursal);
	}

	public boolean ExisteCodigoVenta(String idTrxHeader, String valorEsperado) {
		xpathGrillaSku = xpathGrillaSku.replace("[ID_TRX_HEADER]", idTrxHeader).replace("[CODIGO_VENTA]",
				valorEsperado);
		System.out.println("xpath: " + xpathGrillaSku);
		return ElementExist(xpathGrillaSku);
	}

	public boolean ExisteTipoTransaccion(String idTrxHeader, String valorEsperado) {

		String nombreTipoTransaccion = ObtenerNombreTipoTransaccion(valorEsperado);

		xpathGrillaTipoTransaccion = xpathGrillaTipoTransaccion.replace("[ID_TRX_HEADER]", idTrxHeader)
				.replace("[TIPO_TRANSACCION]", nombreTipoTransaccion);
		System.out.println("xpath: " + xpathGrillaTipoTransaccion);
		return ElementExist(xpathGrillaTipoTransaccion);
	}

	public boolean ExisteTipoDocumento(String idTrxHeader, String valorEsperado) {
		xpathGrillaTipoDocumento = xpathGrillaTipoDocumento.replace("[ID_TRX_HEADER]", idTrxHeader)
				.replace("[DOCUMENT_TYPE]", valorEsperado);
		System.out.println("xpath: " + xpathGrillaTipoDocumento);
		return ElementExist(xpathGrillaTipoDocumento);
	}

	public boolean ExisteUsuario(String idTrxHeader, String valorEsperado) {
		xpathGrillaUsuario = xpathGrillaUsuario.replace("[ID_TRX_HEADER]", idTrxHeader).replace("[USERNAME]",
				valorEsperado);
		System.out.println("xpath: " + xpathGrillaUsuario);
		return ElementExist(xpathGrillaUsuario);
	}

	public boolean ExisteEstadoTransaccion(String idTrxHeader, String valorEsperado) {

		String nombreEstado = ObtenerNombreEstadoTransaccionGrilla(valorEsperado);

		xpathGrillaEstadoCabecera = xpathGrillaEstadoCabecera.replace("[ID_TRX_HEADER]", idTrxHeader)
				.replace("[ESTADO_CABECERA]", nombreEstado);
		System.out.println("xpath: " + xpathGrillaEstadoCabecera);
		return ElementExist(xpathGrillaEstadoCabecera);
	}

	public boolean ExisteEstadoRegistro(String idTrxHeader, String valorEsperado) {

		String nombreEstado = ObtenerNombreEstadoRegistroGrilla(valorEsperado);

		xpathGrillaEstadoDetalle = xpathGrillaEstadoDetalle.replace("[ID_TRX_HEADER]", idTrxHeader)
				.replace("[ESTADO_DETALLE]", nombreEstado);
		System.out.println("xpath: " + xpathGrillaEstadoDetalle);
		return ElementExist(xpathGrillaEstadoDetalle);
	}

	public boolean ExisteStockAntes(String idTrxHeader, String valorEsperado) {
		xpathGrillaStockAntes = xpathGrillaStockAntes.replace("[ID_TRX_HEADER]", idTrxHeader).replace("[STOCK_ANTES]",
				valorEsperado);
		System.out.println("xpath: " + xpathGrillaStockAntes);
		return ElementExist(xpathGrillaStockAntes);
	}

	public boolean ExisteStockDespues(String idTrxHeader, String valorEsperado) {
		xpathGrillaStockDespues = xpathGrillaStockDespues.replace("[ID_TRX_HEADER]", idTrxHeader)
				.replace("[STOCK_DESPUES]", valorEsperado);
		System.out.println("xpath: " + xpathGrillaStockDespues);
		return ElementExist(xpathGrillaStockDespues);
	}

	public boolean ExisteTipoInventario(String idTrxHeader, String valorEsperado) {
		
		String nombreTipoInventario = ObtenerNombreTipoInventario(valorEsperado);

		xpathGrillaTipoInventario = xpathGrillaTipoInventario.replace("[ID_TRX_HEADER]", idTrxHeader)
				.replace("[INVENTORY_TYPE]", nombreTipoInventario);
		System.out.println("xpath: " + xpathGrillaTipoInventario);
		return ElementExist(xpathGrillaTipoInventario);
	}

	private String ObtenerNombreTipoTransaccion(String codigoTipoTransaccion) {
		String nombreTipoTransaccion = "";
		switch (codigoTipoTransaccion) {
		case "ADJUST":
			nombreTipoTransaccion = "Ajuste de Inventario";
			break;
		case "MDADJUST":
			nombreTipoTransaccion = "Masivo de Ajustes";
			break;

		case "SFULL":
			nombreTipoTransaccion = "Sincronización Full";
			break;

		case "SDELTA":
			nombreTipoTransaccion = "Sincronización Parcial";
			break;
		}

		return nombreTipoTransaccion;

	}

	private String ObtenerNombreEstadoTransaccion(String codigoEstado) {

		String nombreEstado = "";
		switch (codigoEstado) {
		case "C":
			nombreEstado = "Completado";
			break;
		case "CF":
			nombreEstado = "Completado con errores";
			break;

		case "F":
			nombreEstado = "Con errores";
			break;

		case "R":
			nombreEstado = "En Proceso";
			break;

		case "P":
			nombreEstado = "Pendiente";
			break;
		}
		return nombreEstado;
	}

	private String ObtenerNombreEstadoTransaccionGrilla(String codigoEstado) {

		String nombreEstado = "";
		switch (codigoEstado) {
		case "C":
			nombreEstado = "Completado";
			break;
		case "CF":
			nombreEstado = "Completado con errores";
			break;

		case "F":
			nombreEstado = "Fallido";
			break;

		case "R":
			nombreEstado = "En Proceso";
			break;

		case "P":
			nombreEstado = "Pendiente";
			break;
		}
		return nombreEstado;
	}

	public String ObtenerNombreEstadoRegistro(String codigoEstado) {
		String nombreEstado = "";
		switch (codigoEstado) {
		case "C":
			nombreEstado = "Completado";
			break;
		case "F":
			nombreEstado = "Con error";
			break;

		case "D":
			nombreEstado = "Duplicados";
			break;
		}
		return nombreEstado;
	}

	public String ObtenerNombreEstadoRegistroGrilla(String codigoEstado) {
		String nombreEstado = "";
		switch (codigoEstado) {
		case "C":
			nombreEstado = "Completado";
			break;
		case "F":
			nombreEstado = "Fallido";
			break;

		case "D":
			nombreEstado = "Duplicados";
			break;
		}
		return nombreEstado;
	}

	public String ObtenerNombreTipoInventario(String codigoTipoInventario) {
		String nombreTipoInventario = "";
		switch (codigoTipoInventario) {
		case "1":
			nombreTipoInventario = "Stock Disponible";
			break;
		case "2":
			nombreTipoInventario = "Reservado para Ventas";
			break;

		case "3":
			nombreTipoInventario = "Stock Protegido";
			break;
		}
		return nombreTipoInventario;
	}

	public void QuitarFiltros() throws InterruptedException {
		WaitSleep(2);

		List<WebElement> filtros = new ArrayList<WebElement>();
		filtros = getDriver().findElements(By.xpath("//span[@class='e-chipcontent']/../span[@class='e-chips-close']"));

		for (WebElement filtro : filtros) {
			filtro.click();
			WaitSleep(1);
		}

	}

	
	
	
	/* TAKS */

	public void Consultar(EGrillaAuditoria oEntidad) throws InterruptedException, AWTException {

		SeleccionarTipoInventario(oEntidad.tipoInventario);
		IngresarEstadoTransaccion(oEntidad.estadoCabecera);
		IngresarEstadoRegistro(oEntidad.estadoDetalle);
		IngresarTipoTransaccion(oEntidad.codigoTipoTransaccion);
		IngresarCodigoVenta(oEntidad.codigoVenta);
		IngresarFechaDesde(oEntidad.processPrev);
		IngresarFechaHasta(oEntidad.processNext);

		ClickConsultar(oEntidad.idtrxHeader);
	}

	public void ConsultarPorDefecto(EGrillaAuditoria oEntidad) throws InterruptedException, AWTException {

		IngresarCodigoVenta(oEntidad.codigoVenta);
		IngresarFechaDesde(oEntidad.processPrev);
		IngresarFechaHasta(oEntidad.processNext);

		ClickConsultar(oEntidad.idtrxHeader);
	}

	public void ConsultarSinCodigoVenta() {
		IngresarFechaDesde("");
		IngresarFechaHasta("");
		ClickConsultar();
	}

	public void ExportarResultado() {
		btn_Exportar.click();
	}

	public void ConsultarSinFechaDesde(EGrillaAuditoria oEntidad) {
		IngresarFechaHasta("");
		IngresarCodigoVenta(oEntidad.codigoVenta);
		ClickConsultar();
	}

	public void ConsultarSinFechaHasta(EGrillaAuditoria oEntidad) {
		IngresarFechaDesde("");
		IngresarCodigoVenta(oEntidad.codigoVenta);
		ClickConsultar();
	}

	public boolean ValidarMensajeError(String mensajeErrorEsperado) {
		String mensajeError = ObtenerMensajeError();
		return mensajeError.equals(mensajeErrorEsperado);

	}

	public boolean ValidarIDTRX(String valorEsperado) {
		return ExisteIDTRX(valorEsperado);
	}

	public boolean ValidarSistemaOrigen(String idTrxHeader, String valorEsperado) {
		return ExisteSistemaOrigen(idTrxHeader, valorEsperado);
	}

	public boolean ValidarSucursal(String idTrxHeader, String valorEsperado) {
		return ExisteSucursal(idTrxHeader, valorEsperado);
	}

	public boolean ValidarCodigoVenta(String idTrxHeader, String valorEsperado) {
		return ExisteCodigoVenta(idTrxHeader, valorEsperado);
	}

	public boolean ValidarTipoTransaccion(String idTrxHeader, String valorEsperado) {
		return ExisteTipoTransaccion(idTrxHeader, valorEsperado);
	}

	public boolean ValidarTipoDocumento(String idTrxHeader, String valorEsperado) {
		return ExisteTipoDocumento(idTrxHeader, valorEsperado);
	}

	public boolean ValidarUsuario(String idTrxHeader, String valorEsperado) {
		return ExisteUsuario(idTrxHeader, valorEsperado);
	}

	public boolean ValidarEstadoTransaccion(String idTrxHeader, String valorEsperado) {
		return ExisteEstadoTransaccion(idTrxHeader, valorEsperado);
	}

	public boolean ValidarEstadoRegistro(String idTrxHeader, String valorEsperado) {
		return ExisteEstadoRegistro(idTrxHeader, valorEsperado);
	}

	public boolean ValidarStockAntes(String idTrxHeader, String valorEsperado) {
		return ExisteStockAntes(idTrxHeader, valorEsperado);
	}

	public boolean ValidarStockDespues(String idTrxHeader, String valorEsperado) {
		return ExisteStockDespues(idTrxHeader, valorEsperado);
	}

	public boolean ValidarTipoInventario(String idTrxHeader, String valorEsperado) {
		return ExisteTipoInventario(idTrxHeader, valorEsperado);
	}

}