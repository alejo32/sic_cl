package pageObjects.sic;

import java.io.IOException;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import pageObjects.BasePage;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ConfiguracionPage extends BasePage {

	public ConfiguracionPage() throws IOException {
		super();
		// TODO Auto-generated constructor stub
	}

	@FindBy(xpath = "//a[contains(@class,'main_button') and text()='Consultar']")
	private WebElement btnConsultar;

	@FindBy(xpath = "//a[contains(@class,'main_button') and text()='Limpiar']")
	private WebElement btnLimpiar;

	@FindBy(xpath = "//a[contains(@class,'main_button') and text()='Eliminar']")
	private WebElement btnEliminar;

	@FindBy(xpath = "//a[contains(@class,'main_button') and text()='Agregar']")
	private WebElement btnAgregar;

	@FindBy(xpath = "//p[text()='Nombre de Sistema ']/../div/select")
	private WebElement cbNombreSistema;

	@FindBy(xpath = "//p[text()='Tipo de Transacción ']/../div/select")
	private WebElement cbTipoTransaccion;

	@FindBy(xpath = "//p[text()='Tipo de Inventario ']/../div/select")
	private WebElement cbTipoInventario;

	@FindBy(xpath = "//button[text()='Continuar']")
	private WebElement btnContinuar;

	@FindBy(xpath = "//input[@type='checkbox']")
	private WebElement chkResultado;
	
	public @FindBy(xpath = "//*[@id=\'movements-conf-form-control\']/form/div[1]/div/select/option[2]") WebElement sel_B2B;
	public @FindBy(xpath = "//*[@id=\'movements-conf-form-control\']/form/div[1]/div/select/option[3]") WebElement sel_BO;
	public @FindBy(xpath = "//*[@id=\'movements-conf-form-control\']/form/div[1]/div/select/option[4]") WebElement sel_BGT;
	public @FindBy(xpath = "//*[@id=\'movements-conf-form-control\']/form/div[1]/div/select/option[5]") WebElement sel_MKP;
	public @FindBy(xpath = "//*[@id=\'movements-conf-form-control\']/form/div[1]/div/select/option[6]") WebElement sel_MTO;
	public @FindBy(xpath = "//*[@id=\'movements-conf-form-control\']/form/div[1]/div/select/option[7]") WebElement sel_PMM;
	
	public @FindBy(xpath = "//*[@id=\'movements-conf-footer-form-control\']/form/div[1]/div/select/option[2]") WebElement sel_Adjust;
	public @FindBy(xpath = "//*[@id=\'movements-conf-footer-form-control\']/form/div[1]/div/select/option[3]") WebElement sel_Sfull;
	public @FindBy(xpath = "//*[@id=\'movements-conf-footer-form-control\']/form/div[1]/div/select/option[4]") WebElement sel_Sdelta;
	
	public @FindBy(xpath = "//*[@id=\'movements-conf-form-control\']/div/a[1]") WebElement btn_Consultar;
	public @FindBy(xpath = "//*[@id=\'movements-conf-form-control\']/div/a[2]") WebElement btn_Eliminar;
	
	public @FindBy(xpath = "//*[@id=\'movements-conf-footer-form-control\']/form/div[2]/div/select/option[2]") WebElement sel_Reservado;
	public @FindBy(xpath = "//*[@id=\'movements-conf-footer-form-control\']/form/div[2]/div/select/option[3]") WebElement sel_Disponible;
	
	public @FindBy(xpath = "//*[@id=\'app\']/section/div[3]/div[2]/div[2]/div[2]/div[1]/table/tr/th[1]") WebElement grid_Transaccion;
	public @FindBy(xpath = "//*[@id=\'app\']/section/div[3]/div[2]/div[2]/div[2]/div[1]/table/tr/th[2]") WebElement grid_Inventario;

	String xpathCheckBoxResultado 		= "//input[@type='checkbox']";
	String xpathSistemaOpcion 			= "//p[text()='Nombre de Sistema ']/../div/select/option[@value='[CODIGO_SISTEMA]']";
	String xpathBtnDeshabilitado 		= "//a[@class='main_button disabled' and text()='[NOMBRE_BOTON]']";
	String xpathBtnHabilitado 			= "//a[@class='main_button' and text()='Consultar']";
	String xpathBtnEliminarHabilitado	= "//a[@class='main_button' and text()='[NOMBRE_BOTON]']";
	String xpathTipoTransaccionOpcion	= "//p[text()='Tipo de Transacción ']/../div/select/option[@value='[CODIGO_TRANSACCION]']";
	String xpathTipoInventarioOpcion 	= "//p[text()='Tipo de Inventario ']/../div/select/option[@value='[CODIGO_INVENTARIO]']";	
	String xpathColumnaGrilla 			= "//th[text()='[NOMBRE_COLUMNA]']";
	String xpathMensajeConfirmacion 	= "//p[text()='Ya existe una configuración ingresada, ¿desea actualizarla?']";
	String xpathMensajeFinal 			= "//p[text()='Operación realizada con éxito']";

	public void ClickConsultar() throws InterruptedException {
		WaitSleep(1);
		WaitAndClickElement(btnConsultar);
	}

	public void EsperarConsulta() {
		WaitUntilWebElementIsVisible(chkResultado);
	}

	public void ClickLimpiar() throws InterruptedException {
		WaitAndClickElement(btnLimpiar);
	}

	public void ClickEliminar() throws InterruptedException {
		WaitAndClickElement(btnEliminar);
	}

	public void ClickAgregar() throws InterruptedException {
		WaitAndClickElement(btnAgregar);
	}

	public void ClickContinuar() throws InterruptedException {
		WaitAndClickElement(btnContinuar);
	}

	public void ClickComboNombreSistema() throws InterruptedException {
		WaitAndClickElement(cbNombreSistema);
	}

	public void ClickComboTipoTransaccion() throws InterruptedException {
		WaitAndClickElement(cbTipoTransaccion);
	}

	public void ClickComboTipoInventario() throws InterruptedException {
		WaitAndClickElement(cbTipoInventario);
	}
	
	/*		***********		*/
	public ConfiguracionPage selB2B() throws Exception {
		String selB2BText = sel_B2B.getText();
		Assert.assertEquals(selB2BText, "B2B");
	return new ConfiguracionPage();
	}
	
	public ConfiguracionPage selBO() throws Exception {
		String selBOText = sel_BO.getText();
		Assert.assertEquals(selBOText, "Back Office");
	return new ConfiguracionPage();
	}
	
	public ConfiguracionPage selBGT() throws Exception {
		String selBGTText = sel_BGT.getText();
		Assert.assertEquals(selBGTText, "Big Ticket");
	return new ConfiguracionPage();
	}
	
	public ConfiguracionPage selMKP() throws Exception {
		String selMKPText = sel_MKP.getText();
		Assert.assertEquals(selMKPText, "MarketPlace");
	return new ConfiguracionPage();
	}
	
	public ConfiguracionPage selMTO() throws Exception {
		String selMTOText = sel_MTO.getText();
		Assert.assertEquals(selMTOText, "MTO");
	return new ConfiguracionPage();
	}
	
	public ConfiguracionPage selPMM() throws Exception {
		String selPMMText = sel_PMM.getText();
		Assert.assertEquals(selPMMText, "PMM");
	return new ConfiguracionPage();
	}
	
	public ConfiguracionPage btnConsultar() throws Exception {	
		btn_Consultar.isDisplayed();		
	return new ConfiguracionPage();
	}
	
	public ConfiguracionPage btnEliminar() throws Exception {	
		btn_Eliminar.isEnabled();
	return new ConfiguracionPage();
	}
	
	public ConfiguracionPage selAdjust() throws Exception {
		String selAdjustText = sel_Adjust.getText();
		Assert.assertEquals(selAdjustText, "Ajuste de Inventario");
	return new ConfiguracionPage();
	}
	
	public ConfiguracionPage selSfull() throws Exception {
		String selSfullText = sel_Sfull.getText();
		Assert.assertEquals(selSfullText, "Sincronización Full");
	return new ConfiguracionPage();
	}
	
	public ConfiguracionPage selSdelta() throws Exception {
		String selSdeltaText = sel_Sdelta.getText();
		Assert.assertEquals(selSdeltaText, "Sincronización Parcial");
	return new ConfiguracionPage();
	}
	
	public ConfiguracionPage selReservado() throws Exception {
		String selReservadoText = sel_Reservado.getText();
		Assert.assertEquals(selReservadoText, "Reservado para Ventas");
	return new ConfiguracionPage();
	}
	
	public ConfiguracionPage selDisponible() throws Exception {
		String selDisponibleText = sel_Disponible.getText();
		Assert.assertEquals(selDisponibleText, "Stock Disponible");
	return new ConfiguracionPage();
	}
	
	public ConfiguracionPage selB2BClick() throws Exception {
		sel_B2B.click();
	return new ConfiguracionPage();
	}
	
	public ConfiguracionPage selAdjustClick() throws Exception {
		sel_Adjust.click();
	return new ConfiguracionPage();
	}
	
	public ConfiguracionPage selDisponibleClick() throws Exception {
		sel_Disponible.click();
	return new ConfiguracionPage();
	}
	
	public ConfiguracionPage gridTransaccion() throws Exception {
		Assert.assertEquals("Tipo de Transacción", grid_Transaccion.getText());
		Assert.assertEquals("Tipo de Inventario", grid_Inventario.getText());
	return new ConfiguracionPage();
	}
	/*		***********		*/

	public boolean ExisteSistemaOpcion(String codigoSistema) {
		xpathSistemaOpcion = xpathSistemaOpcion.replace("[CODIGO_SISTEMA]", codigoSistema);
		System.out.println("Nombre de SISTEMA: " + "[CODIGO_SISTEMA]" + " \n");
		BasePage.WaitSleep(2);
		return ElementExist(xpathSistemaOpcion);

	}

	public boolean ExisteTipoTransaccion(String codigoTipoTransaccion) {
		xpathTipoTransaccionOpcion = xpathTipoTransaccionOpcion.replace("[CODIGO_TRANSACCION]", codigoTipoTransaccion);
		return ElementExist(xpathTipoTransaccionOpcion);
	}

	public boolean ExisteTipoInventario(String codigoTipoInventario) {
		xpathTipoInventarioOpcion = xpathTipoInventarioOpcion.replace("[CODIGO_INVENTARIO]", codigoTipoInventario);
		return ElementExist(xpathTipoInventarioOpcion);
	}

	public boolean ExisteColumnaGrilla(String nombreColumnaGrilla) {
		xpathColumnaGrilla = xpathColumnaGrilla.replace("[NOMBRE_COLUMNA]", nombreColumnaGrilla);
		return ElementExist(xpathColumnaGrilla);
	}

	public boolean ExisteBotonDeshabilitado(String nombreBoton) {		
		xpathBtnDeshabilitado = xpathBtnDeshabilitado.replace("[NOMBRE_BOTON]", nombreBoton);
		return ElementExist(xpathBtnDeshabilitado);
	}

	public boolean ExisteBotonHabilitado(String nombreBoton) {
		xpathBtnHabilitado = xpathBtnHabilitado.replace("[NOMBRE_BOTON]", nombreBoton);
		return ElementExist(xpathBtnHabilitado);
	}

	public boolean ExisteBotonEliminarHabilitado(String nombreBoton) {
		xpathBtnEliminarHabilitado = xpathBtnEliminarHabilitado.replace("[NOMBRE_BOTON]", nombreBoton);
		return ElementExist(xpathBtnEliminarHabilitado);
	}
	
	public boolean ExisteMensajeConfirmacion(String mensaje) {
		return ElementExist(xpathMensajeConfirmacion);
	}

	public boolean VerificarMensajeFinal(String mensaje) throws InterruptedException {
		xpathMensajeFinal = xpathMensajeFinal.replace("[MENSAJE]", mensaje);
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathMensajeFinal)));
		return true;
	}

	public boolean ExisteResultadoLimpio() {
		return ElementExist(xpathCheckBoxResultado);
	}

	public boolean ExisteCargaDeResultados() {
		return ElementExist(xpathCheckBoxResultado);
	}

	public void SeleccionarSistema(String nombreSistema) throws InterruptedException {
		WaitAndClickElement(cbNombreSistema);
		WaitSleep(1);
		xpathSistemaOpcion = xpathSistemaOpcion.replace("[CODIGO_SISTEMA]", nombreSistema);
		WaitAndClickXpath(xpathSistemaOpcion);
		WaitSleep(1);
	}

	public void SeleccionarTipoTransaccion(String codigoTipoTransaccion) throws InterruptedException {
		WaitAndClickElement(cbTipoTransaccion);
		WaitSleep(1);
		xpathTipoTransaccionOpcion = xpathTipoTransaccionOpcion.replace("[CODIGO_TRANSACCION]", codigoTipoTransaccion);
		WaitAndClickXpath(xpathTipoTransaccionOpcion);
		WaitSleep(1);
	}

	public void SeleccionarTipoInventario(String codigoTipoInventario) throws InterruptedException {
		WaitAndClickElement(cbTipoInventario);
		WaitSleep(1);
		xpathTipoInventarioOpcion = xpathTipoInventarioOpcion.replace("[CODIGO_INVENTARIO]", codigoTipoInventario);
		WaitAndClickXpath(xpathTipoInventarioOpcion);
		WaitSleep(1);
	}

}
