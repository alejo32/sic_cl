package pageObjects.sic;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import pageObjects.BasePage;

public class LoginPage extends BasePage {

	public LoginPage() throws IOException {
		super();
		// TODO Auto-generated constructor stub
	}

	@FindBy(id = "username")
	private WebElement txtUsername;
	@FindBy(id = "password")
	private WebElement txtPassword;
	@FindBy(id = "btn-login-sic")
	private WebElement btnIniciarSesion;

	@FindBy(id = "user-options")
	private WebElement imgUser;

	@FindBy(xpath = "//div[@class='error error-box-login open']")
	private WebElement lblMensajeUsuario;

	@FindBy(xpath = "//div[@class='error error-box-login open']")
	private WebElement lblMensajePassword;

	@FindBy(xpath = "//div[@class='error login-error']")
	private WebElement lblMensajeValidacion;
	
	public @FindBy (xpath = "/html/body/div/section/div[2]/section/div/div[1]/div/div/form/div[6]") WebElement txtMensajeValidacion;

	private String xpathCampoUsuario = "//input[@id='username']";
	private String xpathCampoPassword = "//input[@id='password']";
	private String xpathBotonIniciarSesion = "//a[@class='main_button']";
	private String xpathPantallaDashboard = "//h1[text()='Dashboard']";
	private String xpathLoading = "//div[@id='prelod-login']/img";
	private String xpathSesionExpirada = "//p[text()='La sesión ha expirado.']";

	public void CerrarSIC() {
		try {
			getDriver().close();
		} catch (Exception ex) {

		}
	}

	public void AbrirSIC() throws Exception {
		String url = GetSICUrl();
		loadUrl(url);
	}

	public void IngresarUsuario(String usuario) throws IOException, InterruptedException {
		SendKeysToWebElement(txtUsername, usuario);
	}

	public void IngresarPassword(String password) throws IOException, InterruptedException {
		SendKeysToWebElement(txtPassword, password);
	}

	public void ClickIniciarSesion() throws InterruptedException {
		WaitAndClickElement(btnIniciarSesion);
	}

	public String ObtenerMensajeErrorUsuario() {
		return GetElementText(lblMensajeUsuario);
	}

	public String ObtenerMensajeErrorPassword() {
		return GetElementText(lblMensajePassword);
	}

	public String ObtenerMensajeValidacion() {
		 String msjError = txtMensajeValidacion.getText();
		 return msjError;
//		return GetElementText(lblMensajeValidacion);
	}

	public boolean EsperarPantallaDashboard() throws InterruptedException {
		WaitUntilXpathIsVisible(xpathPantallaDashboard);
		return true;
	}

	public boolean EsperarPantallaLogin() {
		WaitUntilXpathIsVisible(xpathCampoUsuario);
		return true;
	}

	public boolean EsperarMensajeSesionExpirada() {
		SimpleDateFormat DateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();		
		System.out.println("EsperarMensajeSesionExpirada() - INICIO - "+DateFormat.format(date));
		WaitUntilXpathIsVisible(xpathSesionExpirada, 2400);
		date = new Date();
		System.out.println("EsperarMensajeSesionExpirada() - FIN - "+DateFormat.format(date));

		return true;
	}

	public void Loading() {
		WaitUntilWebElementDissapears(xpathLoading);
	}

	public void Login(String usuario, String password) throws InterruptedException, IOException {
		loginPage.IngresarUsuario(usuario);
		loginPage.IngresarPassword(password);
		loginPage.ClickIniciarSesion();
		loginPage.EsperarPantallaDashboard();
	}

	public boolean ExisteCampoUsuario() {
		return ElementExist(xpathCampoUsuario);
	}

	public boolean ExisteCampoPassword() {
		return ElementExist(xpathCampoPassword);
	}

	public boolean ExisteBotonIniciarSesion() {
		return ElementExist(xpathBotonIniciarSesion);
	}

}