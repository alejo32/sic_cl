package pageObjects.sic;

import java.awt.AWTException;
import java.io.IOException;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageObjects.BasePage;

public class JerarquiaPage extends BasePage {

	public JerarquiaPage() throws IOException {
		super();
		// TODO Auto-generated constructor stub
	}

	@FindBy(xpath = "//input[@type='text' and @placeholder='SKU o Cod Venta']")
	private WebElement txtCodigoVenta;

	@FindBy(xpath = "//input[@type='number']")
	private WebElement txtStock;

	@FindBy(xpath = "//a[@class='main_button' and text()='Limpiar']")
	private WebElement btnLimpiar;

	@FindBy(xpath = "//a[@class='main_button' and text()='Consultar']")
	private WebElement btnConsultar;

	@FindBy(xpath = "//a[@class='main_button' and text()='Exportar']")
	private WebElement btnExportar;

	@FindBy(xpath = "//p[text()='Tipo de Producto ']/../div/select")
	private WebElement cbTipoProducto;

	@FindBy(xpath = "//div[@id='quantityType-hirch']/div/select")
	private WebElement cbRangoStock;

	@FindBy(xpath = "//input[@id='ddDepartment']/../input[1]")
	private WebElement txtDepartamento;

	@FindBy(xpath = "//input[@id='ddLine']/../input[1]")
	private WebElement txtLinea;

	@FindBy(xpath = "//input[@id='ddSubline']/../input[1]")
	private WebElement txtSubLinea;

	@FindBy(xpath = "//p[text()='Tipo de Inventario:']/../div/select")
	private WebElement cbTipoInventario;

	@FindBy(xpath = "//table/tr/td[1]")
	private WebElement tdBodega;

	@FindBy(xpath = "//table/tr/td[2]")
	private WebElement tdDepartamento;

	@FindBy(xpath = "//table/tr/td[3]")
	private WebElement tdLinea;

	@FindBy(xpath = "//table/tr/td[4]")
	private WebElement tdSublinea;

	@FindBy(xpath = "//table/tr/td[5]")
	private WebElement tdEstilo;

	@FindBy(xpath = "//table/tr/td[6]")
	private WebElement tdCodigoVenta;

	@FindBy(xpath = "//table/tr/td[7]")
	private WebElement tdStock;

	@FindBy(xpath = "//table/tr/td[8]")
	private WebElement tdtipoInventario;
	
	public @FindBy(xpath = "//*[@id=\'hierarchy-form-control\']/form/div[7]/div/select/option[1]") WebElement sel_Reservado;
	public @FindBy(xpath = "//*[@id=\'hierarchy-form-control\']/form/div[7]/div/select/option[2]") WebElement sel_Disponible;
	public @FindBy(xpath = "//*[@id=\'hierarchy-form-control\']/form/div[7]/div/select/option[3]") WebElement sel_Protegido;
	
	public @FindBy(xpath = "//*[@id=\'quantityType-hirch\']/div/select/option[1]") WebElement sel_Mayor;
	public @FindBy(xpath = "//*[@id=\'quantityType-hirch\']/div/select/option[2]") WebElement sel_Menor;
	public @FindBy(xpath = "//*[@id=\'quantityType-hirch\']/div/select/option[3]") WebElement sel_MayorIgual;
	public @FindBy(xpath = "//*[@id=\'quantityType-hirch\']/div/select/option[4]") WebElement sel_MenorIgual;
	public @FindBy(xpath = "//*[@id=\'quantityType-hirch\']/div/select/option[5]") WebElement sel_Igual;
	public @FindBy(xpath = "//*[@id=\'quantityType-hirch\']/div/select/option[6]") WebElement sel_Distinto;
		
	

	String xpathStock = "//option[text()='Stock mayor a:']/..";
	String xpathStockOpcion = "//option[text()='Stock mayor a:']/../option[text()='[TIPO_STOCK]']";
	String xpathStockOpciones = "//option[text()='Stock mayor a:']/../option";
	String xpathBtnDeshabilitado = "//a[@class='main_button disabled' and text()='[NOMBRE_BOTON]']";
	String xpathBtnHabilitado = "//a[@class='main_button' and text()='[NOMBRE_BOTON]']";
	String xpathLineaSeleccionada = "//span[text()='[NOMBRE_LINEA]']";
	String xpathSubLineaSeleccionada = "//span[text()='[NOMBRE_SUBLINEA]']";
	String xpathTipoInventario = "//p[text()='Tipo de Inventario:']/../div/select";
	String xpathTipoInventarioOpcion = "//p[text()='Tipo de Inventario:']/../div/select/option[@value=[CODIGO]]";
	String xpathLimiteStockOpcion = "//div[@id='quantityType-hirch']/div/select/option[text()='[NOMBRE]']";
	String xpathImgLoading = "//div[@class='prelod-adjus-stok']/img";

	
	/*	************************	*/
	public JerarquiaPage selReservado() throws Exception {
		String selReservadoTxt = sel_Reservado.getText();
		Assert.assertEquals(selReservadoTxt, "Reservado para Ventas");
	return new JerarquiaPage();
	}
	
	public JerarquiaPage selDisponible() throws Exception {
		String selDisponibleTxt = sel_Disponible.getText();
		Assert.assertEquals(selDisponibleTxt, "Stock Disponible");
	return new JerarquiaPage();
	}
	
	public JerarquiaPage selProtegido() throws Exception {
		String selProtegidoTxt = sel_Protegido.getText();
		Assert.assertEquals(selProtegidoTxt, "Stock Protegido");
	return new JerarquiaPage();
	}
	
	public JerarquiaPage selMayor() throws Exception {
		Assert.assertEquals("Stock mayor a:",sel_Mayor.getText());
	return new JerarquiaPage();
	}
	
	public JerarquiaPage selMenor() throws Exception {
		Assert.assertEquals("Stock menor a:",sel_Menor.getText());
	return new JerarquiaPage();
	}

	public JerarquiaPage selMayorIgual() throws Exception {
		Assert.assertEquals("Stock mayor o igual a:",sel_MayorIgual.getText());
	return new JerarquiaPage();
	}
	
	public JerarquiaPage selMenorIgual() throws Exception {
		Assert.assertEquals("Stock menor o igual a:",sel_MenorIgual.getText());
	return new JerarquiaPage();
	}
	
	public JerarquiaPage selIgual() throws Exception {
		Assert.assertEquals("Stock igual a:",sel_Igual.getText());
	return new JerarquiaPage();
	}
	
	public JerarquiaPage selDistinto() throws Exception {
		Assert.assertEquals("Stock distinto a:",sel_Distinto.getText());
	return new JerarquiaPage();
	}
	
	/*	************************	*/
	
	public String ObtenerCeldaBodega() {
		return GetElementText(tdBodega);
	}

	public String ObtenerCeldaDepartamento() {
		return GetElementText(tdDepartamento);
	}

	public String ObtenerCeldaLinea() {
		return GetElementText(tdLinea);
	}

	public String ObtenerCeldaSubLinea() {
		return GetElementText(tdSublinea);
	}

	public String ObtenerCeldaEstilo() {
		return GetElementText(tdEstilo);
	}

	public String ObtenerCeldaCodigoVenta() {
		return GetElementText(tdCodigoVenta);
	}

	public String ObtenerCeldaStock() {
		return GetElementText(tdStock);
	}

	public String ObtenerCeldaTipoInventario() {
		return GetElementText(tdtipoInventario);
	}

	public void IngresarCodigoVenta(String codigoVenta) throws InterruptedException {
		SendKeysToWebElement(txtCodigoVenta, codigoVenta);
	}

	public void IngresarStock(String stock) throws InterruptedException {
		SendKeysToWebElement(txtStock, stock);
	}

	public void SeleccionarTipoStock(int stockEnBD) throws InterruptedException {

		if (stockEnBD == 0) {
			WaitAndClickXpath(xpathStock);
			xpathStockOpcion = xpathStockOpcion.replace("[TIPO_STOCK]", "Stock igual a:");
			WaitAndClickXpath(xpathStockOpcion);
		}
		if (stockEnBD < 0) {
			WaitAndClickXpath(xpathStock);
			xpathStockOpcion = xpathStockOpcion.replace("[TIPO_STOCK]", "Stock menor a:");
			WaitAndClickXpath(xpathStockOpcion);
		}

		if (stockEnBD > 0) {
			WaitAndClickXpath(xpathStock);
			xpathStockOpcion = xpathStockOpcion.replace("[TIPO_STOCK]", "Stock mayor a:");
			WaitAndClickXpath(xpathStockOpcion);
		}
	}

	public void SeleccionarTipoStockMayorIgualA() throws InterruptedException {
		WaitAndClickXpath(xpathStock);
		xpathStockOpcion = xpathStockOpcion.replace("[TIPO_STOCK]", "Stock mayor o igual a:");
		WaitAndClickXpath(xpathStockOpcion);
	}

	public void seleccionarTipoInventario(String tipoInventario) throws InterruptedException {
		WaitAndClickXpath(xpathTipoInventario);
		xpathTipoInventarioOpcion = xpathTipoInventarioOpcion.replace("[CODIGO]", tipoInventario);
		WaitAndClickXpath(xpathTipoInventarioOpcion);
	}
	
	
	public void SeleccionarTipoStockMenorIgualA() throws InterruptedException {
		WaitAndClickXpath(xpathStock);
		xpathStockOpcion = xpathStockOpcion.replace("[TIPO_STOCK]", "Stock menor o igual a:");
		WaitAndClickXpath(xpathStockOpcion);
	}

	public void SeleccionarTipoStockDistintoA() throws InterruptedException {
		WaitAndClickXpath(xpathStock);
		xpathStockOpcion = xpathStockOpcion.replace("[TIPO_STOCK]", "Stock distinto a:");
		WaitAndClickXpath(xpathStockOpcion);
	}


	public void ClickConsultar() throws InterruptedException {
		WaitAndClickElement(btnConsultar);
		
	}

	public void ClickComboRangoStock() throws InterruptedException {
		WaitAndClickElement(cbRangoStock);
	}

	public void ClickComboTipoInventario() throws InterruptedException {
		WaitAndClickElement(cbTipoInventario);
	}

	public void ClickLimpiar() throws InterruptedException {
		WaitAndClickElement(btnLimpiar);
	}

	public void ClickExportar() throws InterruptedException {
		WaitAndClickElement(btnExportar);
	}

	public int ObtenerStockSIC() throws InterruptedException {
		return Integer.parseInt(GetElementText(tdStock));
	}

	public String ObtenerTextoComboTipoProducto() {
		return GetComboboxText(cbTipoProducto);
	}

	public String ObtenerTextoComboRangoStock() {
		return GetComboboxText(cbRangoStock);
	}

	public boolean ExisteBotonDeshabilitado(String nombreBoton) {
		xpathBtnDeshabilitado = xpathBtnDeshabilitado.replace("[NOMBRE_BOTON]", nombreBoton);
		return ElementExist(xpathBtnDeshabilitado);
	}

	public boolean ExisteLineaSeleccionada(String nombre) {
		xpathLineaSeleccionada = xpathLineaSeleccionada.replace("[NOMBRE_LINEA]", nombre);
		return ElementExist(xpathLineaSeleccionada);
	}

	public boolean ExisteSubLineaSeleccionada(String nombre) {
		xpathSubLineaSeleccionada = xpathSubLineaSeleccionada.replace("[NOMBRE_SUBLINEA]", nombre);
		return ElementExist(xpathSubLineaSeleccionada);
	}

	public boolean ExisteBotonHabilitado(String nombreBoton) {
		xpathBtnHabilitado = xpathBtnHabilitado.replace("[NOMBRE_BOTON]", nombreBoton);
		return ElementExist(xpathBtnHabilitado);
	}

	public boolean ExisteTipoInventarioOpcion(String codigo) {
		xpathTipoInventarioOpcion = xpathTipoInventarioOpcion.replace("[CODIGO]", codigo);
		return ElementExist(xpathTipoInventarioOpcion);
	}

	public boolean ExisteRangoStockOpcion(String nombre) {
		xpathLimiteStockOpcion = xpathLimiteStockOpcion.replace("[NOMBRE]", nombre);
		return ElementExist(xpathLimiteStockOpcion);
	}
	
	
	public int ObtenerCantidadOpcionesRangoStock() {
		return GetElementsSize(xpathStockOpciones);
	}

	public void SeleccionarDepartamento(String nombre) throws InterruptedException, AWTException {
		SendKeysToWebElement(txtDepartamento, nombre);
		SendDownKey();
		SendEnterKey();
	}

	public void ClickTxtLinea() {
		String xpathLinea = "//input[@id='ddLine']/../input[1][@aria-disabled='false']";
		WaitAndClickXpath(xpathLinea);
	}

	public void ClickTxtSubLinea() {
		String xpathLinea = "//input[@id='ddSubline']/../input[1][@aria-disabled='false']";
		WaitAndClickXpath(xpathLinea);
	}

	public void IngresarLinea(String nombre) {
		SendKeysToWebElement(txtLinea, nombre);
	}

	public void IngresarSubLinea(String nombre) {
		SendKeysToWebElement(txtSubLinea, nombre);
	}

	public void SeleccionarLinea(String nombre) throws AWTException, InterruptedException {
		SendKeysToWebElement(txtLinea, nombre);
		SendDownKey();
		SendEnterKey();
	}

	public void SeleccionarSubLinea(String nombre) throws AWTException, InterruptedException {
		SendKeysToWebElement(txtSubLinea, nombre);
		SendDownKey();
		SendEnterKey();
	}

	public boolean VerificarTipoInventario(String actual, String esperado) {
		return actual.equals(esperado);
	}

	public boolean VerificarSubLineaSeleccionada(String nombre) {
		return ExisteSubLineaSeleccionada(nombre);
	}

	public void ConsultarProducto(String codigoVenta) throws InterruptedException {
		IngresarCodigoVenta(codigoVenta);
		ClickConsultar();
	}

	public void ConsultarProducto(String codigoVenta, int stockEnBD) throws InterruptedException {
		IngresarCodigoVenta(codigoVenta);
		SeleccionarTipoStock(stockEnBD);
		ClickConsultar();
	}

	public boolean VerificarLineaSeleccionada(String nombre) {
		return ExisteLineaSeleccionada(nombre);
	}

	public boolean VerificarBodega(String actual, String esperado) {
		return actual.equals(esperado);
	}

	public boolean VerificarDepartamento(String actual, String esperado) {
		return actual.contains(esperado);
	}

	public boolean VerificarLinea(String actual, String esperado) {
		return actual.contains(esperado);
	}

	public boolean VerificarSubLinea(String actual, String esperado) {
		return actual.contains(esperado);
	}

	public boolean VerificarEstilo(String actual, String esperado) {
		return actual.equals(esperado);
	}

	public boolean VerificarCodigoVenta(String actual, String esperado) {
		System.out.println(actual);
		System.out.println(esperado);
		return actual.equals(esperado);
	}

	public boolean VerificarStock(String actual, String esperado) {
		return actual.equals(esperado);
	}

	public boolean ValidarStockActual(int stockActual, int stockEsperado) throws Exception {

		return stockActual != stockEsperado;
	}

	public boolean ValidarNuevoStock(int stockActual, int stockEsperado) throws Exception {
		
		return stockActual != stockEsperado;
	}
	
	public void EsperarCarga() throws InterruptedException {
		WaitSleep(1);
		WaitUntilWebElementDissapears(xpathImgLoading);
		WaitSleep(1);
	}
}