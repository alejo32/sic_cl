package pageObjects.sic;

import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import models.ETransaction;
import pageObjects.BasePage;

public class CargaStockPage extends BasePage {

	public CargaStockPage() throws IOException {
		super();
		// TODO Auto-generated constructor stub
	}

	@FindBy(xpath = "//a[@class='main_button']")
	private WebElement btnRealizarCarga;
	
	private String  xpathBtnExaminar="//label[@class='main_button']/span";

	@FindBy(xpath = "//div[contains(@class,'row error error-box')]")
	private WebElement lblMensajeError;

	@FindBy(id = "system-mss")
	private WebElement cbTipoProducto;

	@FindBy(id = "massive-file-ip")
	private WebElement file;

	@FindBy(id = "massive-file-pro-ip")
	private WebElement fileCMSP;

	@FindBy(id = "input-sic-file-name")
	private WebElement txtArchivo;

	@FindBy(xpath = "//div[@class='table-container show']/div/p[contains(text(),'Transacci')]/../p[2]")
//	@FindBy(xpath = "//*[@id=\'result-load-massive\']/div[2]/div[2]/p[2]")
	private WebElement lblTransaccion;

	@FindBy(xpath = "//div[@class='table-container show']/div/p[contains(text(),'Estado:')]/../p[2]")
	private WebElement lblEstado;

	@FindBy(xpath = "//div[@class='table-container show']/div/p[contains(text(),'Total procesados:')]/../p[2]")
	private WebElement lblTotalProcesados;

	@FindBy(xpath = "//div[@class='table-container show']/div/p[contains(text(),'Duplicados:')]/../p[2]")
	private WebElement lblDuplicados;

	@FindBy(xpath = "//div[@class='table-container show']/div/p[contains(text(),'No enviables:')]/../p[2]")
	private WebElement lblNoEnviables;

	@FindBy(xpath = "//div[@class='table-container show']/div/p[contains(text(),'Sin variación:')]/../p[2]")
	private WebElement lblSinVariacion;

	@FindBy(xpath = "//div[@class='table-container show']/div/p[contains(text(),'Procesados con error:')]/../p[2]")
	private WebElement lblProcesadosConError;

	@FindBy(xpath = "//div[@class='table-container show']/div/p[contains(text(),'Procesados con éxito:')]/../p[2]")
	private WebElement lblProcesadosConExito;

	@FindBy(xpath = "//*[@id=\'result-load-massive\']/div[2]/div[11]/p[2]")
	private WebElement lblDetalle;
	
	@FindBy(xpath = "//*[@id=\'result-load-proct-massive\']/div[2]/div[8]/p[2]")
	private WebElement lblDetalleProtect;

	private String xpathImgLoading = "//div[@class='prelod-adjus-stok']/img";

	private String xpathResumenDetalleCodVenta = "//div[@id='skusError']/ul/li[1]/p[contains(text(),'[COD_VENTA]')]";
	private String xpathResumenDetalleEstado = "//div[@id='skusError']/ul/li[1]/p[contains(text(),'[COD_VENTA]')]/../../li[2]/p";
	private String xpathResumenDetalleDetalle = "//div[@id='skusError']/ul/li[1]/p[contains(text(),'[COD_VENTA]')]/../../li[3]/p";

	public String ObtenerDetalleCodigoVenta(String codVenta) {
		WebElement elemento = driver
				.findElement(By.xpath(xpathResumenDetalleCodVenta.replace("[COD_VENTA]", codVenta)));
		return GetElementText(elemento);
	}

	public String ObtenerDetalleEstado(String codVenta) {
		WebElement elemento = driver.findElement(By.xpath(xpathResumenDetalleEstado.replace("[COD_VENTA]", codVenta)));
		return GetElementText(elemento);
	}

	public String ObtenerDetalleDetalle(String codVenta) {
		WebElement elemento = driver.findElement(By.xpath(xpathResumenDetalleDetalle.replace("[COD_VENTA]", codVenta)));
		return GetElementText(elemento);
	}

	public String ObtenerTransaccion() {
		return GetElementText(lblTransaccion);
	}

	public String ObtenerEstado() throws InterruptedException {
		EsperarCarga();
		return GetElementText(lblEstado);
	}

	public String ObtenerTotalProcesados() {
		return GetElementText(lblTotalProcesados);
	}

	public String ObtenerDuplicados() {
		return GetElementText(lblDuplicados);
	}

	public String ObtenerNoEnviables() {
		return GetElementText(lblNoEnviables);
	}

	public String ObtenerSinVariacion() {
		return GetElementText(lblSinVariacion);
	}

	public String ObtenerProcesadosConError() {
		return GetElementText(lblProcesadosConError);
	}

	public String ObtenerProcesadosConExito() {
		return GetElementText(lblProcesadosConExito);
	}

	public String ObtenerDetalle() {
		return GetElementText(lblDetalle);		
	}
	
	public String ObtenerDetalleProtect() {
		return GetElementText(lblDetalleProtect);		
	}
	
	public boolean ExisteBotonExaminar() {
		
		return driver.findElements(By.xpath(xpathBtnExaminar)).size()>0;
	}
	public void ClickRealizarCarga() throws InterruptedException {
		btnRealizarCarga.click();
	}

	public void CargarArchivoStock(String rutaCompleta) throws InterruptedException {

		WebElement targetDiv = getDriver().findElement(By.id("massive-file-ip"));
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		js.executeScript("arguments[0].classList.remove('closed');", targetDiv);
		WaitSleep(1);
		file.sendKeys(rutaCompleta);

		js.executeScript("arguments[0].classList.add('closed');", targetDiv);
	}

	public void CargarArchivoStockProtegido(String rutaCompleta) throws InterruptedException {
		WebElement targetDiv = getDriver().findElement(By.id("massive-file-pro-ip"));
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		js.executeScript("arguments[0].classList.remove('closed');", targetDiv);
		WaitSleep(1);
		fileCMSP.sendKeys(rutaCompleta);

		js.executeScript("arguments[0].classList.add('closed');", targetDiv);
	}

	public String ObtenerTextoComboTipoProducto() {
		return GetComboboxText(cbTipoProducto);
	}

	public String ObtenerMensajeError() {
		return GetElementText(lblMensajeError);
	}

	public String ObtenerTextoArchivoSeleccionado() {
		return GetElementText(txtArchivo);
	}

	public void EsperarCarga() throws InterruptedException {
		WaitSleep(1);
		WaitUntilWebElementDissapears(xpathImgLoading);
		WaitSleep(3);
		WaitUntilWebElementIsVisible(lblTransaccion);
	}

	public void SubirArchivoCMS(String ruta, String archivo) throws InterruptedException, AWTException {
		CargarArchivoStock(ruta + "\\" + archivo);
	}

	public void SubirArchivoCMSP(String ruta, String archivo) throws InterruptedException, AWTException {
		CargarArchivoStockProtegido(ruta + "\\" + archivo);
	}

	public boolean ValidarStock(int stockActual, int stockEsperado) {
		System.out.println("Stock actual: " + stockActual + " - Stock esperado: " + stockEsperado);
		return stockActual == stockEsperado;
	}

	public boolean ValidarDatosResumen(String transaccionId, String estadoCabecera, String totalProcesados,
			String duplicados, String noEnviables, String sinVariacion, String procesadosConExito,
			String procesadosConError) {

		String codigoEstado = "";
		switch (estadoCabecera) {
		case "Completado":
			codigoEstado = "C";
			break;
		case "Fallido":
			codigoEstado = "F";
			break;
		case "Completado con Errores":
			codigoEstado = "CF";
			break;
		}

		ETransaction oTransaccion = database.ObtenerResultadoTransaccionByTransactionId(transaccionId);
		boolean resultado = true;
		if (!transaccionId.equals(oTransaccion.transactionId))
			resultado = false;
		if (!codigoEstado.equals(oTransaccion.status))
			resultado = false;
		if (!totalProcesados.equals(oTransaccion.totalItems))
			resultado = false;
		if (!duplicados.equals(oTransaccion.duplicatedItems))
			resultado = false;
		if (!noEnviables.equals(oTransaccion.unsentItems))
			resultado = false;
		if (!sinVariacion.equals(oTransaccion.unchangedItems))
			resultado = false;
		if (!procesadosConExito.equals(oTransaccion.successItems))
			resultado = false;
//		if (!detalleCabecera.equals(oTransaccion.comments))
//			resultado = false;

		return resultado;
	}

}