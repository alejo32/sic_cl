package pageObjects.sic;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.FindBy;
import pageObjects.BasePage;

public class MenuPage extends BasePage {

	public MenuPage() throws IOException {
		super();
		// TODO Auto-generated constructor stub
	}

	@FindBy(xpath = "//a[text()='Cerrar Sesión']")
	private WebElement lnkCerrarSesion;

	@FindBy(id = "user-options")
	private WebElement lnkOpcionesUsuario;

	@FindBy(id = "sic-locations-list")
	private WebElement cbBodega;

	String xpathMenu = "//nav[@id='main-menu']/ul/li/a/span[text()='[OPCION]']/..";
	String xpathSubMenu = "//nav[@id='main-menu']/ul/li/a/span/../../ul/li/a/span[text()='[OPCION]']/..";
	String xpathBodega = "//select[@id='sic-locations-list']";
	String xpathBodegaOpcion = "//select[@id='sic-locations-list']/option[@value='[CODIGO_BODEGA]']";
	String xpathPantallaMantenedorIngresarStockSeguridad = "//h1[text()='Mantenedor para ingresar stock de seguridad']";
	String xpathOpcionMantenedorIngresarStockSeguridad = "//span[text()='Mantenedor para ingresar stock de seguridad']";

	public void IrMenu(String menu, String subMenu) throws IOException, InterruptedException {

		xpathMenu = xpathMenu.replace("[OPCION]", menu);
		System.out.println("ClickOption: " + xpathMenu);
		WebDriverWait wait = new WebDriverWait(getDriver(), 60);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpathMenu))).click();

		if (!subMenu.equals("")) {
			WaitSleep(2);
			xpathSubMenu = xpathSubMenu.replace("[OPCION]", subMenu);
			System.out.println("ClickOption: " + xpathSubMenu);
			wait = new WebDriverWait(getDriver(), 60);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpathSubMenu))).click();
		}
	}

	public void ClickOpcionesUsuario() throws InterruptedException {

		WaitAndClickElement(lnkOpcionesUsuario);
	}

	public void ClickCerrarSesion() {
		WaitAndClickElement(lnkCerrarSesion);
	}

	public void ClickComboBodega() throws InterruptedException {
		WaitAndClickXpath(xpathBodega);
	}

	public void ClickComboBodegaOpcion(String codigoBodega) throws InterruptedException {
		String xpathBodegaOpcionMod = xpathBodegaOpcion.replace("[CODIGO_BODEGA]", codigoBodega);
		System.out.println("RUTA XPATH:" + xpathBodegaOpcionMod);
		WaitAndClickXpath(xpathBodegaOpcionMod);
	}

	public String ObtenerTextoComboBodegas() {
		return GetComboboxText(cbBodega);
	}

	public boolean ExistePantallaMantenedorIngresarStockSeguridad() throws InterruptedException {
		WaitSleep(1);
		return ElementExist(xpathPantallaMantenedorIngresarStockSeguridad);
	}

	public void ClickMenu(String menu) throws InterruptedException {
		xpathMenu = xpathMenu.replace("[OPCION]", menu);
		System.out.println("ClickOption: " + xpathMenu);
		WaitAndClickXpath(xpathMenu);
	}

	public boolean ExisteOpcionMantenedorIngresarStockSeguridad() throws InterruptedException {
		WaitSleep(2);
		return ElementExist(xpathOpcionMantenedorIngresarStockSeguridad);

	}

	public void SeleccionarBodega(String codigoBodega) throws InterruptedException {
		menuPage.ClickComboBodega();
		System.out.println("CODIGO BODEGA:" + codigoBodega);
		BasePage.WaitSleep(1);
		menuPage.ClickComboBodegaOpcion(codigoBodega);
		BasePage.WaitSleep(1);
		menuPage.ClickComboBodega();
	}

}
