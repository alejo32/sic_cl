package pageObjects.aws;

import java.io.IOException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageObjects.BasePage;
import utils.BaseUtil;

public class SQSPage extends BasePage {

	BaseUtil util;
	public SQSPage() throws IOException {
		super();

		util = new BaseUtil();
	}


	@FindBy(xpath = "//textarea[@id='awsui-textarea-0']")
	private WebElement taCuerpoMensaje;
	
	/*@FindBy(xpath = "//div[text()='ID de grupo de mensajes']/../../td/input")
	private WebElement txtIdGrupoMensaje;*/

	@FindBy(xpath = "//input[@placeholder='Introduzca el ID del grupo de mensajes']")
	private WebElement txtIdGrupoMensaje;

	@FindBy(xpath = "//input[@placeholder='Enter message deduplication id']")
	private WebElement txtIdDuplicacionMensaje;

	/*@FindBy(xpath = "//button[text()='Enviar mensaje']")
	private WebElement btnEnviarMensaje;*/
	
	@FindBy(xpath = "//awsui-button[@id='send-message-form-action']//button[@class='awsui-button awsui-button-variant-primary awsui-hover-child-icons']")
	private WebElement btnEnviarMensaje;

	@FindBy(xpath = "//button[text()='Enviar otro mensaje']")
	private WebElement btnEnviarOtroMensaje;

	public void IngresarDatos(String jsonString) throws IOException, InterruptedException {
System.out.println("Ingresar datos: -" +jsonString);
		SendKeysToWebElement(taCuerpoMensaje, jsonString);
		//SendKeysToWebElement(txtIdGrupoMensaje, String.valueOf(util.GenerarRandom(500, 1)));
		//SendKeysToWebElement(txtIdDuplicacionMensaje, String.valueOf(util.GenerarRandom(500, 1)));

	}

	public void EnviarMensajeSQS() {
		WaitAndClickElement(btnEnviarMensaje);
//		WaitUntilWebElementIsVisible(btnEnviarOtroMensaje);
	}

	

	public void EnviarOtroMensaje() {
		WaitAndClickElement(btnEnviarOtroMensaje);
		WaitUntilWebElementIsVisible(taCuerpoMensaje);
	}
	

	
}